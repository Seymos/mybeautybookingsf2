<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\RegisterBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;
use Symfony\Component\HttpFoundation\RedirectResponse;

use FAPROD\UserBundle\Form\ProfessionnelType;
use FAPROD\UserBundle\Form\ClienteType;
use FAPROD\UserBundle\Form\CheveuxType;
use FAPROD\UserBundle\Form\PeauType;
use FAPROD\UserBundle\Form\PreferencesType;
use FAPROD\UserBundle\Entity\User;
use FAPROD\OrderBundle\Entity\Order;
use FAPROD\OrderBundle\Entity\Orderitem;

use MangoPay\MangoPayApi;

use FAPROD\CoreBundle\Lib\MyConst;

class RegisterController extends Controller
{
	
	public function preExecute(Request $request)
    {        
        if ($request->query->get('parrain')){
        	$this->container->get('session')->set('parrain', $request->query->get('parrain'));
        }
    }
	
	/**
     * @Route("/autocomplete", name="register_autocomplete")
     * @Template("RegisterBundle:Register:autocomplete.html.twig")
     */
	public function autocompleteAction(Request $request){
		$repository_ville = $this->getDoctrine()->getManager()->getRepository('UserBundle:Ville');
    	$queryBuilder = $repository_ville->createQueryBuilder('v');
		
		$mot = strtolower($request->request->get('queryString'));

	    $mot = $this->container->get('FAPROD.MyTools')->netoyage($mot);
	    $mots = explode(" ",$mot);
	    $nombre_mots=count($mots);
	    $z=1;
		
		if (count($nombre_mots) > 0){
			$queryBuilder->where('v.title LIKE :mot_0 or v.postal_code LIKE :mot_0');
			$queryBuilder->setParameter('mot_0', "%".$mots[0]."%");
			
			while($z<$nombre_mots)
	    	{
	    		$queryBuilder->orWhere('v.title LIKE :mot_'.$z.' or v.postal_code LIKE :mot_'.$z);
	    		$queryBuilder->setParameter('mot_'.$z, "%".$mots[$z]."%");
	    		$z++;
			}
		}
		
    	$villes = $queryBuilder->orderBy('v.title', 'ASC')->getQuery()->setMaxResults(30)->getResult();
    	
		return array(
					"villes" => $villes,
				);
	}
	
	/**
     * @Route("/", name="register_index")
     * @Template("RegisterBundle:Register:index.html.twig")
	 */
	public function indexAction(Request $request)
	{
		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			$user = $this->get('security.context')->getToken()->getUser();
			
			$member = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneBy(array(
		      	"email"    => $user->getEmail(),
		      	"password" => $user->getPassword(),
		      	"validate" => 1,
				));
		
			if ($member){
				
				$em = $this->getDoctrine()->getManager();
				$member->setDateLog(new \DateTime('now'));
				$em->flush();
				
		        $token = new UsernamePasswordToken($member, $member->getPassword(), 'member_area', array('ROLE_USER'));
				$this->get("security.context")->setToken($token);
				return $this->redirect($this->generateUrl('home_index'));
			}
	    }
		
		return $this->redirect($this->generateUrl('register_cliente'));
	}
	
	
	/**
     * @Route("/cliente", name="register_cliente")
     * @Template("RegisterBundle:Register:cliente.html.twig")
	 */
	public function clienteAction(Request $request)
	{
	    
	    $user = new User();
	    $user->setValidate(1);
		$user->setType(1);
	    $form = $this->createForm(new ClienteType(), $user);
	    
	    $form->add('validate', 'hidden')
	          ->add('cgu', 'checkbox', array('required' => true))
	          ->add('password', 'repeated', array('type' => 'password', 'invalid_message' => 'Les mots de passe ne sont pas identiques', 'required' => true));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($user);
	        $em->flush();
			
			$mailBody = $this->renderView('RegisterBundle:Mail:cliente.html.twig', array('user' => $user));
	          $message = \Swift_Message::newInstance()
		        ->setSubject($this->get('translator')->trans('Validation de votre inscription'))
		        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		        ->setTo($user->getEmail())
		        ->setBody($mailBody, 'text/html');
		      //echo $mailBody;exit;
		    $this->get('mailer')->send($message);
			  
			$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
	        $this->get("security.context")->setToken($token);
	        
	        $response = new RedirectResponse($this->generateUrl('register_cheveux'));
			
			$response->headers->clearCookie('REMEMBERME');
	        $event = new InteractiveLoginEvent($request, $token);
        	$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
			
			
			// CREATION COMPTE MAGO PAY
			require_once __DIR__ . '/../../BookingBundle/mangopay/MangoPay/Autoloader.php';
			$mangoPayApi = new MangoPayApi();
			$conf = $this->get('FAPROD.MyConst')->getMangoConf();
			$mangoPayApi->Config->ClientId = $conf[0];
			$mangoPayApi->Config->ClientPassword = $conf[1];
			$mangoPayApi->Config->TemporaryFolder = $conf[2];
			$mangoPayApi->Config->BaseUrl = $conf[3];
		
			$user_mango = new \MangoPay\UserNatural();
			$user_mango->PersonType = "NATURAL";
			$user_mango->FirstName = $user->getLastName();
			$user_mango->LastName = $user->getFirstName();
			$user_mango->Birthday = strtotime("1979/09/24");//$this->getUser()->getBirthdate()->getTimestamp();
			$user_mango->Nationality = "FR";//$this->getUser()->getCountry();
			$user_mango->CountryOfResidence = "FR";//$this->getUser()->getCountry();
			$user_mango->Email = $user->getEmail();
			$user_mango->Tag = 'CLIENTE n°'.$user->getId();
			
			$result = $mangoPayApi->Users->Create($user_mango);
			
			if ($result) {
				
				$mangoId = $result->Id;
				
				$user->setMangoId($mangoId);
				$em->flush();
				
				$Wallet = new \MangoPay\Wallet();
				$Wallet->Owners = array($mangoId);
				$Wallet->Description = $user->getLastName().' '.$user->getFirstName();
				$Wallet->Currency = "EUR";
				$Wallet->Tag = 'CLIENTE n°'.$user->getId();
				$result = $mangoPayApi->Wallets->Create($Wallet);
				
			}
        	
        	return $response;
			
		}
	    
		return array(
				'form'        => $form->createView(),
				);
	}
	
	/**
     * @Route("/cliente/cheveux", name="register_cheveux")
	 * @Template("RegisterBundle:Register:cheveux.html.twig")
	 */
    public function cheveuxAction(Request $request)
    {
		$user = $this->getUser();
		$form = $this->createForm(new CheveuxType(), $user);
		
		$form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();

		   $em->flush();
	
	       //$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Enregistrement effectué'));
		   
		   return $this->redirect($this->generateUrl('register_peau'));
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$cheveux = $repository_category->find(165);
	
	    return array(
	       'form'        => $form->createView(),
	       'cheveux'     => $cheveux,
	    );
		   
	}
	
	/**
     * @Route("/cliente/peau", name="register_peau")
	 * @Template("RegisterBundle:Register:peau.html.twig")
	 */
    public function peauAction(Request $request)
    {
		$user = $this->getUser();
		$form = $this->createForm(new PeauType(), $user);
		
		$form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();

		   $em->flush();
	
	       //$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Enregistrement effectué'));
		   
		   return $this->redirect($this->generateUrl('register_preferences'));
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$peau = $repository_category->find(199);
	
	    return array(
	       'form'        => $form->createView(),
	       'peau'        => $peau,
	    );
		   
	}
	
	/**
     * @Route("/cliente/preferences", name="register_preferences")
	 * @Template("RegisterBundle:Register:preferences.html.twig")
	 */
    public function preferencesAction(Request $request)
    {
		$user = $this->getUser();
		$form = $this->createForm(new PreferencesType(), $user);
		
		$form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();

		   $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Enregistrement effectué'));
		   
		   return $this->redirect($this->generateUrl('user_show'));
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$preferences = $repository_category->find(216);
		
		$lisse = $repository_category->find(168);
		$boucle = $repository_category->find(169);
		$crepu = $repository_category->find(170);
		$frise = $repository_category->find(171);
		
		$is_crepu = $user->getCheveux()->contains($crepu) ? true : false;
		$is_lisse = $user->getCheveux()->contains($lisse) ? true : false;
		$is_boucle = ($user->getCheveux()->contains($boucle) or $user->getCheveux()->contains($frise)) ? true : false;

	    return array(
	       'form'            => $form->createView(),
	       'preferences'     => $preferences,
		   'is_crepu'        => $is_crepu,
		   'is_lisse'        => $is_lisse,
		   'is_boucle'       => $is_boucle,
	    );
		   
	}
	
	/**
     * @Route("/professionnel", name="register_professionnel")
     * @Template("RegisterBundle:Register:professionnel.html.twig")
	 */
	public function professionnelAction(Request $request)
	{
	    
	    $user = new User();
	    $user->setValidate(1);
		$user->setType(2);
	    $statut = MyConst::getStatutFiscal();
		$metier = MyConst::getMetier();
		$form = $this->createForm(new ProfessionnelType(), $user, array(
																	'statut'  => $statut,
																	'metiers' => $metier,
																	));
	    
	    $form->add('validate', 'hidden')
	          ->add('cgu', 'checkbox', array('required' => true))
	          ->add('password', 'repeated', array('type' => 'password', 'invalid_message' => 'Les mots de passe ne sont pas identiques', 'required' => true))
			  ;
		$form->handleRequest($request);
	    $erreur = '';
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($user);
			
	        $em->flush();
			  
			//$mailBody = $this->renderView('RegisterBundle:Mail:welcome.html.twig', array('user' => $user));
	        //$message = \Swift_Message::newInstance()
		    //    ->setSubject($this->get('translator')->trans('Validation de votre inscription'))
		    //    ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		    //    ->setTo($user->getEmail())
		    //    ->setBody($mailBody, 'text/html');
		    //echo $mailBody;exit;
		    //$this->get('mailer')->send($message);
			  
			$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
	        $this->get("security.context")->setToken($token);
	        
	        $response = new RedirectResponse($this->generateUrl('user_index'));
	    	
	        $response->headers->clearCookie('REMEMBERME');
	        $event = new InteractiveLoginEvent($request, $token);
        	$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
			
			
			// CREATION COMPTE MAGO PAY
			require_once __DIR__ . '/../../BookingBundle/mangopay/MangoPay/Autoloader.php';
			$mangoPayApi = new MangoPayApi();
			$conf = $this->get('FAPROD.MyConst')->getMangoConf();
			$mangoPayApi->Config->ClientId = $conf[0];
			$mangoPayApi->Config->ClientPassword = $conf[1];
			$mangoPayApi->Config->TemporaryFolder = $conf[2];
			$mangoPayApi->Config->BaseUrl = $conf[3];
		
			$user_mango = new \MangoPay\UserNatural();
			$user_mango->PersonType = "NATURAL";
			$user_mango->FirstName = $user->getLastName();
			$user_mango->LastName = $user->getFirstName();
			$user_mango->Birthday = strtotime("1979/09/24");//$this->getUser()->getBirthdate()->getTimestamp();
			$user_mango->Nationality = "FR";//$this->getUser()->getCountry();
			$user_mango->CountryOfResidence = "FR";//$this->getUser()->getCountry();
			$user_mango->Email = $user->getEmail();
			$user_mango->Tag = 'PRO n°'.$user->getId();
			
			$result = $mangoPayApi->Users->Create($user_mango);
			
			if ($result) {
				
				$mangoId = $result->Id;
				
				$user->setMangoId($mangoId);
				$em->flush();
				
				$Wallet = new \MangoPay\Wallet();
				$Wallet->Owners = array($mangoId);
				$Wallet->Description = $user->getLastName().' '.$user->getFirstName();
				$Wallet->Currency = "EUR";
				$Wallet->Tag = 'PRO n°'.$user->getId();
				$result = $mangoPayApi->Wallets->Create($Wallet);
				
			}
        	
        	return $response;
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$queryBuilder = $repository_category->createQueryBuilder('c');
		$queryBuilder->where('c.level = 0');
    	$categorys = $repository_category->getCategorys(null, null, 'no_order', 1, $queryBuilder);
		
		return array(
				'form'        => $form->createView(),
				'categorys'   => $categorys,
				);
	}
	
	/**
     * @Route("/endregister", name="register_endregister")
     * @Template("RegisterBundle:Register:endregister.html.twig")
	 */
	public function endregisterAction(Request $request)
	{

	}
	
	/**
     * @Route("/connexion", name="register_connexion")
     * @Template("RegisterBundle:Register:connexion.html.twig")
	 */
	public function connexionAction(Request $request)
	{

	}
	
	
	/**
     * @Route("/validate", name="register_validate")
     * @Template("RegisterBundle:Register:validate.html.twig")
	 */
	public function validateAction(Request $request)
	{
		if ($request->query->get('e')){
			$user = $this->getDoctrine()
		        ->getManager()
		        ->getRepository('UserBundle:User')
		        ->findOneBy(
				    array(
				    	'email' => $request->query->get('e'),
				    	'id' => $request->query->get('id'),
				    	));

			if ($user){
				
				$em = $this->getDoctrine()->getManager();
				$user->setValidate(1);
				$em->flush();
				
				$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
		        $this->get("security.context")->setToken($token);
		        
		        $response = new RedirectResponse($this->generateUrl('register_validate'));
		        $response->headers->clearCookie('REMEMBERME');
		        $event = new InteractiveLoginEvent($request, $token);
	        	$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
	        	
	        	return $response;
			}
		}
		    
		return array();
	}
	
	/**
     * @Route("/loginorder/{p1}/{p2}/{p3}/{p4}", name="register_login_order")
	 */
    public function loginorderAction($p1, $p2, $p3, $p4, Request $request)
	{
		$id = base64_decode($p1);
    	$nickname = base64_decode($p2);
    	$password = base64_decode($p3);

	    $user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneBy(array(
		      	"id"       => $id,
		      	"email"    => $nickname,
		      	"password" => $password,
				));

		if ($user){
			
			$em = $this->getDoctrine()->getManager();
			$user->setDateLog(new \DateTime('now'));
			$em->flush();
			
			$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
	        $this->get("security.context")->setToken($token);
	        
	        $response = new RedirectResponse($this->generateUrl('order_show', array('order_id' => $p4)));
	        
	        $response->headers->clearCookie('REMEMBERME');
	        
	        $event = new InteractiveLoginEvent($request, $token);
        	$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        	
        	return $response;
		}
		
		return $this->redirect($this->generateUrl('home_index'));
	}
	
	/**
     * @Route("/logout", name="register_logout")
	 */
	public function logoutAction(Request $request)
	{
		$this->get('security.context')->setToken(null);
		//$this->get('request')->getSession()->invalidate();
		$response = new RedirectResponse($this->generateUrl('home_index'));
		$response->headers->clearCookie('REMEMBERME');
		
		return $response;
	}
	
	/**
     * @Route("/login", name="register_login")
     * @Template("RegisterBundle:Register:index.html.twig")
	 */
    public function loginAction(Request $request)
	{
	    $user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneBy(array(
		      	"email"    => $request->get('email'),
		      	"password" => md5($request->get('password')),
		      	"validate" => 1,
				));
		
		if ($user){
			
			$em = $this->getDoctrine()->getManager();
			$user->setDateLog(new \DateTime('now'));
			$em->flush();
			
			$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
	        $this->get("security.context")->setToken($token);
	        
	        $response = new RedirectResponse($this->generateUrl('home_index'));
	        
	        if ($request->get('_remember_me') == 'on'){

				// write cookie for persistent session storing
				$providerKey = 'main'; // defined in security.yml
				$securityKey = $this->container->getParameter('secret');// defined in security.yml
				
				$rememberMeService = new TokenBasedRememberMeServices(array($this->getDoctrine()->getManager()->getRepository('UserBundle:User')), $securityKey, $providerKey, array(
				                'path' => '/',
				                'name' => 'REMEMBERME',
				                'domain' => null,
				                'secure' => false,
				                'httponly' => true,
				                'lifetime' => 2592000, // 30 days
				                'always_remember_me' => true,
				                'remember_me_parameter' => '_remember_me')
				            );
				
				$rememberMeService->loginSuccess($request, $response, $token);
			} else {
				$response->headers->clearCookie('REMEMBERME');
			}
	        
	        $event = new InteractiveLoginEvent($request, $token);
        	$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        	
        	return $response;
		} else {
			$this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Authentification invalide'));
		}
		
		return $this->redirect($this->generateUrl('register_index'));
	}
	
	/**
     * @Route("/loginmessage/{p1}/{p2}/{p3}", name="register_login_message")
	 */
    public function loginmessageAction($p1, $p2, $p3, Request $request)
	{
		$id = base64_decode($p1);
    	$nickname = base64_decode($p2);
    	$password = base64_decode($p3);

	    $user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneBy(array(
		      	"id"       => $id,
		      	"email"    => $nickname,
		      	"password" => $password,
				));

		if ($user){
			
			$em = $this->getDoctrine()->getManager();
			$user->setDateLog(new \DateTime('now'));
			$em->flush();
			
			$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
	        $this->get("security.context")->setToken($token);
	        
	        $response = new RedirectResponse($this->generateUrl('message_index'));
	        
	        $response->headers->clearCookie('REMEMBERME');
	        
	        $event = new InteractiveLoginEvent($request, $token);
        	$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        	
        	return $response;
		}
		
		return $this->redirect($this->generateUrl('home_index'));
	}
	
	/**
     * @Route("/autologin", name="register_autologin")
	 */
    public function autologinAction(Request $request)
	{
		$id = base64_decode($request->query->get('p1'));
    	$nickname = base64_decode($request->query->get('p2'));
    	$password = base64_decode($request->query->get('p3'));
    
	    $user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneBy(array(
		      	"id"       => $id,
		      	"email"    => $nickname,
		      	"password" => $password,
				));

		if ($user){
			$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
	        $this->get("security.context")->setToken($token);
	        
	        $response = new RedirectResponse($this->generateUrl('user_index'));
	        
	        $response->headers->clearCookie('REMEMBERME');
	        
	        $event = new InteractiveLoginEvent($request, $token);
        	$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        	
        	return $response;
		} else {
			$this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Authentification invalide'));
		}
		
		return $this->redirect($this->generateUrl('home_index'));
	}
	
	/**
     * @Route("/password", name="register_password")
     * @Template("RegisterBundle:Register:password.html.twig")
	 */
    public function passwordAction(Request $request)
	{
		return array();
	}
	
	/**
     * @Route("/sendpassword", name="register_sendpassword")
     * @Template("RegisterBundle:Register:sendpassword.html.twig")
	 */
    public function sendpasswordAction(Request $request)
	{
		$user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneByEmail($request->request->get('email'));
		
		if ($user){
			$mailBody = $this->renderView('RegisterBundle:Mail:password.html.twig', array('user' => $user));
	        $message = \Swift_Message::newInstance()
		        ->setSubject($this->get('translator')->trans('Votre mot de passe'))
		        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		        ->setTo($user->getEmail())
		        ->setBody($mailBody, 'text/html');
		    //echo $mailBody;exit;
		    $this->get('mailer')->send($message);
		    $result = 1;
		} else {
			$result = 0;
		}
		
		return array(
					"result" => $result,
				);
	}
	
	/**
     * @Route("/endpassword", name="register_endpassword")
     * @Template("RegisterBundle:Register:endpassword.html.twig")
	 */
    public function endpasswordAction(Request $request)
	{
		$result = 0;
		
		if ($request->getMethod() == 'POST')
        {
        	$email = $request->request->get('email');
			$password = $request->request->get('oldpassword');
			
			$user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
			      ->findOneBy(array(
			      				"email"     => $email,
			      				"password"  => $password,
			      			  ));
			
			if ($user) {
	            $user->setPassword($request->request->get('password'));
	            $em = $this->getDoctrine()->getManager();
		        $em->flush();
	            $result = 1;
	        }
    	}
		
		return array(
					"result" => $result,
				);
	}
	
	/**
     * @Route("/changepassword", name="register_changepassword")
     * @Template("RegisterBundle:Register:changepassword.html.twig")
	 */
    public function changepasswordAction(Request $request)
	{
		$email = $request->query->get('p1');
		$password = $request->query->get('p2');
		
		$user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneBy(array(
		      				"email"     => $email,
		      				"password"  => $password,
		      			  ));
		      			  
		$result = $user ? 1 : 0;
		
		return array(
			"result"    => $result,
			"email"     => $email,
			"password"  => $password,
			);
	}
}
