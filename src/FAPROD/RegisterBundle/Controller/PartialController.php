<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\RegisterBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;
use Symfony\Component\HttpFoundation\RedirectResponse;

use FAPROD\RegisterBundle\Lib\Facebook;

class PartialController extends Controller
{	

	public function facebookAction(Request $request)
	{    
		$facebook = new Facebook(array(
		  'appId'  => $this->get('FAPROD.MyConst')->getFacebookApi(),
		  'secret' => $this->get('FAPROD.MyConst')->getFacebookSecret(),
		  'cookie' => true
		));
		
		try {
				
			$user_facebook = $facebook->getUser();
			
			if (!$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')){
				if ($user_facebook) {
				  
				    $user_profile = $facebook->api('/me');
					
				    if (isset($user_profile['email'])){
				    	$user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
							      ->findOneBy(array(
							      	"email"    => $user_profile['email'],
							      	"validate" => 1,
									));
				
					    if ($user)
						{
					    	if (!$user->getFacebookId()){
					    		$user->setFacebookId($user_facebook);
	
					    		$em = $this->getDoctrine()->getManager();
					    		$em->flush();
					    	}
					        $token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
		        			$this->get("security.context")->setToken($token);
		        
		        			//$response = new RedirectResponse($this->generateUrl('user_index'));
		        			//return $response;
				        } else {
				        	//sfContext::getInstance()->getController()->redirect('connexion/loginfacebook');
				        }
				    }
				  
				}
			}
		
		} catch (Exception $e) {
		    $user_facebook = null;
		}
		
	  	return $this->render('RegisterBundle:Partial:facebook_js.html.twig');
	}
}
