<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\RegisterBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;
use Symfony\Component\HttpFoundation\RedirectResponse;

use FAPROD\UserBundle\Form\UserType;
use FAPROD\UserBundle\Entity\User;

class FacebookController extends Controller
{
	
	/**
     * @Route("/loginfacebook", name="register_loginfacebook")
	 */
	public function loginfacebookAction(Request $request){
   	
	   	$code = isset($_REQUEST["code"]) ? $_REQUEST["code"] : '';
		$my_url = $this->generateUrl("register_loginfacebook", array(), true);
		
	    if(empty($code)) {
		    $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
		    $dialog_url = "https://www.facebook.com/dialog/oauth?client_id=" 
		       . $this->get('FAPROD.MyConst')->getFacebookApi() . "&redirect_uri=" . urlencode($my_url) . "&state="
		       . $_SESSION['state'] . "&scope=".$this->get('FAPROD.MyConst')->getFacebookScope();
	
		    return $this->redirect($dialog_url);
	    }
		
		try {			
				if($_SESSION['state'] && ($_SESSION['state'] === $_REQUEST['state'])) {
				    $token_url = "https://graph.facebook.com/oauth/access_token?"
				      . "client_id=" . $this->get('FAPROD.MyConst')->getFacebookApi() . "&redirect_uri=" . urlencode($my_url)
				      . "&client_secret=" . $this->get('FAPROD.MyConst')->getFacebookSecret() . "&code=" . $code;
				
				    $response = file_get_contents($token_url);
				    $params = null;
				    parse_str($response, $params);
				
				    $_SESSION['access_token'] = $params['access_token'];
				
				    $graph_url = "https://graph.facebook.com/me?fields=name,email&access_token=".$params['access_token'];
				
				    $user_facebook = json_decode(file_get_contents($graph_url));
			    }
			
		} catch (Exception $e) {
			//echo ' ** *** '.$user_facebook->id.' *** '.$e;exit;
		}
		
		if (!$user_facebook->email){
			return $this->redirect($this->generateUrl('register_index'));
		}
		
		$user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneBy(array(
		      	"email"    => $user_facebook->email,
				));
	    
	    if ($user){
	    	
	    	$em = $this->getDoctrine()->getManager();
			$user->setDateLog(new \DateTime('now'));
			$em->flush();
			
	    	$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
			$this->get("security.context")->setToken($token);

			$response = new RedirectResponse($this->generateUrl('home_index'));
			return $response;
        
	    } else {
			return $this->redirect($this->generateUrl("register_facebook"));
	    }	    
	    
    }
    
    /**
     * @Route("/facebook", name="register_facebook")
	 */
    public function facebookAction(Request $request){
		
		//$link = file_get_contents('https://graph.facebook.com');
		//var_dump($link);
		//exit;
   	
	   	$code = isset($_REQUEST["code"]) ? $_REQUEST["code"] : '';
		$my_url = $this->generateUrl("register_facebook", array(), true);
		
	    if(empty($code)) {
			
		    $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
		    $dialog_url = "https://www.facebook.com/dialog/oauth?client_id=" 
		       . $this->get('FAPROD.MyConst')->getFacebookApi() . "&redirect_uri=" . urlencode($my_url) . "&state="
		       . $_SESSION['state'] . "&scope=".$this->get('FAPROD.MyConst')->getFacebookScope();
			//echo $dialog_url;exit;
		    return $this->redirect($dialog_url);
	    }
		
		try{		
			if($_SESSION['state'] && ($_SESSION['state'] === $_REQUEST['state'])) {
				
			    $token_url = "https://graph.facebook.com/oauth/access_token?"
			       . "client_id=" . $this->get('FAPROD.MyConst')->getFacebookApi() . "&redirect_uri=" . urlencode($my_url)
			       . "&client_secret=" . $this->get('FAPROD.MyConst')->getFacebookSecret() . "&code=" . $code;
				//echo $token_url;exit;
				//$token_url = "http://www.faprod.com";
			    //$response = file_get_contents(str_replace(' ','',$token_url));
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $token_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
				if(substr($url,0,8)=='https://'){
					// The following ensures SSL always works. A little detail:
					// SSL does two things at once:
					//  1. it encrypts communication
					//  2. it ensures the target party is who it claims to be.
					// In short, if the following code is allowed, CURL won't check if the 
					// certificate is known and valid, however, it still encrypts communication.
					curl_setopt($ch,CURLOPT_HTTPAUTH,CURLAUTH_ANY);
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
				}
				$response = json_decode(curl_exec($ch));
				curl_close($ch);
	
				
	
				//var_dump($response);exit;
			    //$params = null;
			    //parse_str($response, $params);
				//echo $response->access_token;exit;
			    $_SESSION['access_token'] = $response->access_token;
			
			    $graph_url = "https://graph.facebook.com/me?fields=name,email&access_token=".$response->access_token;
				//echo $graph_url;exit;
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $graph_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
				if(substr($url,0,8)=='https://'){
					// The following ensures SSL always works. A little detail:
					// SSL does two things at once:
					//  1. it encrypts communication
					//  2. it ensures the target party is who it claims to be.
					// In short, if the following code is allowed, CURL won't check if the 
					// certificate is known and valid, however, it still encrypts communication.
					curl_setopt($ch,CURLOPT_HTTPAUTH,CURLAUTH_ANY);
					curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
				}
				$response = curl_exec($ch);
				curl_close($ch);
				//var_dump($response);exit;
				
			    $user_facebook = json_decode($response);
		    }
			
		} catch (Exception $e) {
			//echo ' ** *** '.$user_facebook->id.' *** '.$e;exit;
		}
		
		//echo $user_facebook->email;exit;
		
		if (!$user_facebook->email){
			return $this->redirect($this->generateUrl('register_index'));
		}
		
		$user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
		      ->findOneBy(array(
		      	"email"    => $user_facebook->email,
				));
	
	    $graph_url = "https://graph.facebook.com/me/friends?fields=name,email&fields=name,email&access_token=".$_SESSION['access_token'];

		$em = $this->getDoctrine()->getManager();
		
	    // nickname exists?
	    if (!$user)
	    {	
	    	//var_dump($user_facebook);exit;
	    	//$user = new User();
	    	//$user->setFacebookId($user_facebook->id);
	    	//$user->setFirstName(explode(' ',$user_facebook->name)[1]);
	    	//$user->setLastName(explode(' ',$user_facebook->name)[0]);
	    	//$user->setPassword($user_facebook->email);
	    	//$user->setEmail($user_facebook->email);
	    	//$user->setCgu(1);
	    	//$user->setValidate(1);
			//$user->setDateLog(new \DateTime('now'));
	    	//$em->persist($user);
			//$em->flush();
			return $this->redirect($this->generateUrl('register_index'));
			
    	} else {
    		
    		if (!$user->getFacebookId()){
	    		$user->setFacebookId($user_facebook->id);
	    		$em->flush();
	    	}
	        //if ($user->getNewsletter()){
	        	$token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
    			$this->get("security.context")->setToken($token);
    
    			$response = new RedirectResponse($this->generateUrl('home_index'));
    			return $response;
	        //}
    	}    
	  
	}
	
	/**
     * @Route("/facebook2", name="register_facebook2")
     * @Template("RegisterBundle:Facebook:edit.html.twig")
	 */
	public function facebook2Action(Request $request){
  	    $user = $this->getUser();
  	 
  	    if ($user){
  	    	$user->setNewsletter(true);
  	    	$user->initPassword('');
        	$type = $this->container->get('FAPROD.MyConst')->getTypeUser();
    	
		    $form = $this->createForm(new UserType(), $user, array('type' => $type));
		    $form->remove('cgu');
		    $form->remove('credits');
		    $form->remove('validate');
		    $form->remove('email');
		    $form->remove('first_name');
		    $form->remove('last_name');
		    $form->add('newsletter', 'hidden');
		    $form->add('password', 'password', array('required' => true));
		    
		    $form->handleRequest($request);
	    
		    if ($form->isSubmitted() and $form->isValid()) {
		       $em = $this->getDoctrine()->getManager();
		       $em->flush();
		
		       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Inscription terminée'));
		       
		       $token = new UsernamePasswordToken($user, $user->getPassword(), 'member_area', array('ROLE_USER'));
			   $this->get("security.context")->setToken($token);
		
			   $response = new RedirectResponse($this->generateUrl('home_index'));
		
		       return $response;
		    }
		    
		    return array(
	       			'form'   => $form->createView(),
	    			);
		    
  	    } else {
  	    	return $this->redirect($this->generateUrl("register_facebook"));
  	    }
  	 
   }
}
