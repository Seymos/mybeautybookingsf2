<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="FAPROD\AdminBundle\Entity\AdminRepository")
 * @ORM\Table(name="admin")
 */
class Admin implements UserInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    private $salt;

    private $roles = array();
    
    /**
     * @ORM\Column(name="validate", type="integer", nullable=true)
     */
    private $validate;

    // Les getters et setters

    public function eraseCredentials()
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {
		if ($password != '')
		{
	       $this->password = md5($password);
    	}
    	
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }
    
     public function setValidate($validate)
    {
        $this->validate = $validate;

        return $this;
    }

    public function getValidate()
    {
        return $this->validate;
    }
    
    public function getValidateLabel()
    {
        switch ($this->getValidate()) {
            case 1:
                return 'Oui';
            break;
            case 0:
                return 'Non';
            break;
            case 2:
                return 'Invalidé';
            break;
            default:
                return 'Non';
        }
    }
}
