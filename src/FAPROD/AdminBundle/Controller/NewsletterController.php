<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\ContentBundle\Form\NewsletterType;
use FAPROD\ContentBundle\Entity\Newsletter;

class NewsletterController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/newsletter/index/{page}/{sort}/{sens}", name="admin_newsletter_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Newsletter:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
	    return $this->initPagination($page, $sort, $sens, '', 'admin_newsletter_index');
    }
    
    /**
     * @Route("/newsletter/show/{newsletter_id}", name="admin_newsletter_show", requirements={"newsletter_id" = "\d+"})
	 * @ParamConverter("newsletter", options={"mapping": {"newsletter_id": "id"}})
	 * @Template("AdminBundle:Newsletter:show.html.twig")
	 */
	public function showAction(Newsletter $newsletter)
	{	
	  	return array(
	    	'newsletter' => $newsletter,
	  	);
	}
	
	/**
     * @Route("/newsletter/add", name="admin_newsletter_add")
     * @Template("AdminBundle:Newsletter:add.html.twig")
	 */
    public function addAction(Request $request)
    {
	    $newsletter = new Newsletter();
	    $form = $this->createForm(new NewsletterType(), $newsletter);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($newsletter);
	        $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        return $this->redirect($this->generateUrl('admin_newsletter_show', array('newsletter_id' => $newsletter->getId())));
	    }
	    
	    return array(
	        'form'   => $form->createView(),
	    );
    }

    /**
     * @Route("/newsletter/edit/{newsletter_id}", name="admin_newsletter_edit", requirements={"newsletter_id" = "\d+"})
	 * @ParamConverter("newsletter", options={"mapping": {"newsletter_id": "id"}})
	 * @Template("AdminBundle:Newsletter:edit.html.twig")
	 */
    public function editAction(Newsletter $newsletter, Request $request)
    {
	    $form = $this->createForm(new NewsletterType(), $newsletter);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
	       $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_newsletter_show', array('newsletter_id' => $newsletter->getId())));
	    }
	
	    return array(
	       'form'   => $form->createView(),
	    );
    }
    
    /**
     * @Route("/newsletter/envoyer/{newsletter_id}", name="admin_newsletter_envoyer", requirements={"newsletter_id" = "\d+"})
	 * @ParamConverter("newsletter", options={"mapping": {"newsletter_id": "id"}})
	 */
	public function envoyerAction(Newsletter $newsletter, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $newsletter->setEnvoyeDate(new \DateTime('now'));
	    $em->flush();
	    
	    $repository_user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User');
    	$queryBuilder = $repository_user->createQueryBuilder('u'); 				  
    	if ($newsletter->getTous())$queryBuilder->where('u.newsletter = 1');
		if ($newsletter->getCliente() and !$newsletter->getPro())$queryBuilder->andWhere('u.type = 1');
		if (!$newsletter->getCliente() and $newsletter->getPro())$queryBuilder->andWhere('u.type = 2');
    	$users = $repository_user->getUsers(1, 10000, 'id', 0, $queryBuilder);
    	
    	foreach ($users as $user){
    		$mailBody = $this->renderView('ContentBundle:Mail:newsletter.html.twig', array('newsletter' => $newsletter));
	        $message = \Swift_Message::newInstance()
		        ->setSubject($newsletter->getTitle())
		        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		        ->setTo($user->getEmail())
		        ->setBody($mailBody, 'text/html');
		    //echo $mailBody;exit;
		    $this->get('mailer')->send($message);
    	}

	    $request->getSession()->getFlashBag()->add('message', 'Newsletter envoyée.');

        return $this->redirect($this->generateUrl('admin_newsletter_show', array('newsletter_id' => $newsletter->getId())));
	}
	
	/**
     * @Route("/newsletter/test/{newsletter_id}", name="admin_newsletter_test", requirements={"newsletter_id" = "\d+"})
	 * @ParamConverter("newsletter", options={"mapping": {"newsletter_id": "id"}})
	 */
	public function testAction(Newsletter $newsletter, Request $request)
	{	
		$mailBody = $this->renderView('ContentBundle:Mail:newsletter.html.twig', array('newsletter' => $newsletter));
        $message = \Swift_Message::newInstance()
	        ->setSubject($newsletter->getTitle())
	        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
	        ->setTo($this->get('FAPROD.MyConst')->getSiteEmail())
	        ->setBody($mailBody, 'text/html');
	    //echo $mailBody;exit;
	    $this->get('mailer')->send($message);

	    $request->getSession()->getFlashBag()->add('message', 'Newsletter envoyée.');

        return $this->redirect($this->generateUrl('admin_newsletter_show', array('newsletter_id' => $newsletter->getId())));
	}
     
  
    /**
     * @Route("/newsletter/delete/{newsletter_id}", name="admin_newsletter_delete", requirements={"newsletter_id" = "\d+"})
	 * @ParamConverter("newsletter", options={"mapping": {"newsletter_id": "id"}})
	 */
	public function deleteAction(Newsletter $newsletter, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($newsletter);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_newsletter_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = NewsletterController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('ContentBundle:Newsletter')
	      ->getNewsletters($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
	    );
    }
}
