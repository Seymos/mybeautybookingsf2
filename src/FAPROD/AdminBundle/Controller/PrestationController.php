<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @prestation   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use FAPROD\UserBundle\Form\PrestationType;
use FAPROD\UserBundle\Entity\Prestation;

use FAPROD\CoreBundle\Lib\MyConst;

class PrestationController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/prestation/index/{page}/{sort}/{sens}", name="admin_prestation_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "no_order", "sens" = "1"})
     * @Template("AdminBundle:Prestation:index.html.twig")
     */
    public function indexAction($page, $sort, $sens, Request $request)
    {
    	$QueryBuilder = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation')->createQueryBuilder('p');
    	$QueryBuilder->where('p.level = :level')
			  		  ->setParameter('level', 0);
    	
	    return $this->initPagination($page, $sort, $sens, $QueryBuilder, 'admin_prestation_index');
    }
    
    /**
     * @Route("/prestation/show/{prestation_id}", name="admin_prestation_show", requirements={"prestation_id" = "\d+"})
	 * @ParamConverter("prestation", options={"mapping": {"prestation_id": "id"}})
	 * @Template("AdminBundle:Prestation:show.html.twig")
	 */
	public function showAction(Prestation $prestation, Request $request)
	{	
	  	return array(
	    	'prestation' => $prestation,
	  	);
	}

	/**
     * @Route("/prestation/add", name="admin_prestation_add")
     * @Template("AdminBundle:Prestation:add.html.twig")
     * @I18nDoctrine
	 */
    public function addAction(Request $request)
    {
    	$langues = $this->container->get('FAPROD.Configs')->getBoLangues();
		
    	$prestation = new Prestation();
    	
		if ($request->query->get('id_ref')){
			$parent = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation')->find($request->query->get('id_ref'));
			$prestation->setParent($parent);
			$prestation->setLevel($parent->getLevel() + 1);
		}
		
		$repository_prestation = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation');
    	$queryBuilder = $repository_prestation->createQueryBuilder('p');
    	$queryBuilder->select('MAX(p.no_order)');
		$queryBuilder->where('p.level = :level')->setParameter('level', $prestation->getLevel());
		if ($prestation->getParent())$queryBuilder->andWhere('p.parent = :parent')->setParameter('parent', $prestation->getParent());
    	$prestation->setNoOrder($queryBuilder->getQuery()->getSingleScalarResult()+1);
	    
		$metier = MyConst::getMetier();
		
	    $form = $this->createForm(new PrestationType, $prestation, array(
																	'langues'   => $langues,
																	'metiers'   => $metier,
																	));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($prestation);
	        $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        if ($prestation->getParent()){
	       		return $this->redirect($this->generateUrl('admin_prestation_show', array('prestation_id' => $prestation->getParent()->getId())));
	        } else {
	       		return $this->redirect($this->generateUrl('admin_prestation_show', array('prestation_id' => $prestation->getId())));
	        }
	    }
	    
	    return array(
	        'form'       => $form->createView(),
	        'langues'    => $langues,
	    );
    }
    
    /**
     * @Route("/prestation/edit/{prestation_id}", name="admin_prestation_edit", requirements={"prestation_id" = "\d+"})
	 * @ParamConverter("prestation", options={"mapping": {"prestation_id": "id"}})
	 * @Template("AdminBundle:Prestation:edit.html.twig")
	 * @I18nDoctrine
	 */
    public function editAction(Prestation $prestation, Request $request)
    {
    	$langues = $this->container->get('FAPROD.Configs')->getBoLangues();
    	$metier = MyConst::getMetier();
		
	    $form = $this->createForm(new PrestationType(), $prestation, array(
																			'langues'   => $langues,
																			'metiers'   => $metier,
																			));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
	       $em->flush();
		   
		   foreach ($prestation->getChildren() as $sub_presta) {
			   if (!count($sub_presta->getMetiers())) {
				   $sub_presta->setMetiers($prestation->getMetiers());
			   }
				   
			   foreach ($sub_presta->getChildren() as $sub_sub_presta) {
				   if (!count($sub_sub_presta->getMetiers())) {
					   $sub_sub_presta->setMetiers($prestation->getMetiers());
				   }
			   }		   
		   }
		   
		   $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       if ($prestation->getParent()){
	       		return $this->redirect($this->generateUrl('admin_prestation_show', array('prestation_id' => $prestation->getParent()->getId())));
	       } else {
	       		return $this->redirect($this->generateUrl('admin_prestation_show', array('prestation_id' => $prestation->getId())));
	       }
	    }
	
	    return array(
	       'form'       => $form->createView(),
	       'langues'    => $langues,
	    );
    }
     
  
    /**
     * @Route("/prestation/delete/{prestation_id}", name="admin_prestation_delete", requirements={"prestation_id" = "\d+"})
	 * @ParamConverter("prestation", options={"mapping": {"prestation_id": "id"}})
	 */
	public function deleteAction(Prestation $prestation, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($prestation);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_prestation_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = PrestationController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('UserBundle:Prestation')
	      ->getPrestations($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }

	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
	    );
    }
}
