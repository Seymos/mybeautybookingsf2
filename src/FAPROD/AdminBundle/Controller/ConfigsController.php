<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\AdminBundle\Form\ConfigsType;
use FAPROD\AdminBundle\Entity\Configs;

class ConfigsController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/configs/index/{page}/{sort}/{sens}", name="admin_configs_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "label", "sens" = "1"})
     * @Template("AdminBundle:Configs:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
    	$repository_configs = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Configs');
    	$queryBuilder = $repository_configs->createQueryBuilder('c');
    	
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_configs_index');
    }
    
    /**
     * @Route("/configs/show/{configs_id}", name="admin_configs_show", requirements={"configs_id" = "\d+"})
	 * @ParamConverter("configs", options={"mapping": {"configs_id": "id"}})
	 * @Template("AdminBundle:Configs:show.html.twig")
	 */
	public function showAction(Configs $configs)
	{	
	  	return array(
	    	'config' => $configs,
	  	);
	}

    /**
     * @Route("/configs/edit/{configs_id}", name="admin_configs_edit", requirements={"configs_id" = "\d+"})
	 * @ParamConverter("configs", options={"mapping": {"configs_id": "id"}})
	 * @Template("AdminBundle:Configs:edit.html.twig")
	 */
    public function editAction(Configs $configs, Request $request)
    {
	    $form = $this->createForm(new ConfigsType(), $configs);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
	       $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_configs_show', array('configs_id' => $configs->getId())));
	    }
	
	    return array(
	       'form'   => $form->createView(),
	    );
    }
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = ConfigsController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('AdminBundle:Configs')
	      ->getConfigss($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
		  'action'      => $action,
	    );
    }
}
