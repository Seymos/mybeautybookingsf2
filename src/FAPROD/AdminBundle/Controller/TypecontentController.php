<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\ContentBundle\Form\TypeContentType;
use FAPROD\ContentBundle\Entity\TypeContent;
use FAPROD\CoreBundle\Lib\Tools\FichierExcel;

class TypecontentController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/typecontent/index/{page}/{sort}/{sens}", name="admin_typecontent_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:TypeContent:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
	    return $this->initPagination($page, $sort, $sens, '', 'admin_typecontent_index');
    }
    
    /**
     * @Route("/typecontent/show/{typecontent_id}", name="admin_typecontent_show", requirements={"typecontent_id" = "\d+"})
	 * @ParamConverter("typecontent", options={"mapping": {"typecontent_id": "id"}})
	 * @Template("AdminBundle:TypeContent:show.html.twig")
	 */
	public function showAction(TypeContent $typecontent)
	{	
	  	return array(
	    	'typecontent' => $typecontent,
	  	);
	}
	
	/**
     * @Route("/typecontent/add", name="admin_typecontent_add")
     * @Template("AdminBundle:TypeContent:add.html.twig")
	 */
    public function addAction(Request $request)
    {
	    $typecontent = new TypeContent();
	    $form = $this->createForm(new TypeContentType(), $typecontent);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($typecontent);
	        $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        return $this->redirect($this->generateUrl('admin_typecontent_show', array('typecontent_id' => $typecontent->getId())));
	    }
	    
	    return array(
	        'form'   => $form->createView(),
	    );
    }

    /**
     * @Route("/typecontent/edit/{typecontent_id}", name="admin_typecontent_edit", requirements={"typecontent_id" = "\d+"})
	 * @ParamConverter("typecontent", options={"mapping": {"typecontent_id": "id"}})
	 * @Template("AdminBundle:TypeContent:edit.html.twig")
	 */
    public function editAction(TypeContent $typecontent, Request $request)
    {
	    $form = $this->createForm(new TypeContentType(), $typecontent);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
	       $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_typecontent_show', array('typecontent_id' => $typecontent->getId())));
	    }
	
	    return array(
	       'form'   => $form->createView(),
	    );
    }
     
  
    /**
     * @Route("/typecontent/delete/{typecontent_id}", name="admin_typecontent_delete", requirements={"typecontent_id" = "\d+"})
	 * @ParamConverter("typecontent", options={"mapping": {"typecontent_id": "id"}})
	 */
	public function deleteAction(TypeContent $typecontent, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($typecontent);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_typecontent_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = TypeContentController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('ContentBundle:TypeContent')
	      ->getTypeContents($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
		  'action'      => $action,
	    );
    }
}
