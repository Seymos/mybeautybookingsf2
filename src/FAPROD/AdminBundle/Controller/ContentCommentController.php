<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\ContentBundle\Form\ContentCommentType;
use FAPROD\ContentBundle\Entity\ContentComment;

class ContentCommentController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/contentcomment/index/{page}/{sort}/{sens}", name="admin_contentcomment_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:ContentComment:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
	    return $this->initPagination($page, $sort, $sens, '', 'admin_contentcomment_index');
    }
    
    /**
     * @Route("/contentcomment/avalider/{page}/{sort}/{sens}", name="admin_contentcomment_avalider", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:ContentComment:index.html.twig")
     */
    public function avaliderAction($page, $sort, $sens)
    {
    	$repository_comment = $this->getDoctrine()->getManager()->getRepository('ContentBundle:ContentComment');
    	$queryBuilder = $repository_comment->createQueryBuilder('c');
		$queryBuilder->where('c.validate is null or c.validate = 0');
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_contentcomment_avalider');
    }
    
    /**
     * @Route("/contentcomment/enligne/{page}/{sort}/{sens}", name="admin_contentcomment_enligne", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:ContentComment:index.html.twig")
     */
    public function enligneAction($page, $sort, $sens)
    {
    	$repository_comment = $this->getDoctrine()->getManager()->getRepository('ContentBundle:ContentComment');
    	$queryBuilder = $repository_comment->createQueryBuilder('c');
		$queryBuilder->where('c.validate = 1');
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_contentcomment_enligne');
    }
    
    /**
     * @Route("/contentcomment/refuse/{page}/{sort}/{sens}", name="admin_contentcomment_refuse", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:ContentComment:index.html.twig")
     */
    public function refuseAction($page, $sort, $sens)
    {
    	$repository_comment = $this->getDoctrine()->getManager()->getRepository('ContentBundle:ContentComment');
    	$queryBuilder = $repository_comment->createQueryBuilder('c');
		$queryBuilder->where('c.validate = -1');
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_contentcomment_refuse');
    }
    
    /**
     * @Route("/contentcomment/show/{comment_id}", name="admin_contentcomment_show", requirements={"comment_id" = "\d+"})
	 * @ParamConverter("comment", options={"mapping": {"comment_id": "id"}})
	 * @Template("AdminBundle:ContentComment:show.html.twig")
	 */
	public function showAction(ContentComment $comment)
	{	
	  	return array(
	    	'comment' => $comment,
	  	);
	}
	
	/**
     * @Route("/contentcomment/accepter/{comment_id}", name="admin_contentcomment_accepter", requirements={"comment_id" = "\d+"})
	 * @ParamConverter("comment", options={"mapping": {"comment_id": "id"}})
	 */
	public function accepterAction(ContentComment $comment, Request $request)
	{	
		$em = $this->getDoctrine()->getManager();
		$comment->setValidate(1);
	    $em->flush();
	    
	    $vendeur = $comment->getVendeur();
	    
	    if ($vendeur){
	    	
	    	$vendeur->updateNotes();
	    	$em->persist($vendeur);
	    	$em->flush();
		    
		    if ($vendeur){
		    	$mailBody = $this->renderView('AdminBundle:ContentComment:mail_accepter.html.twig', array('comment' => $comment));
		        $message = \Swift_Message::newInstance()
			        ->setSubject($this->get('translator')->trans('Un nouveau commentaire !'))
			        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
			        ->setTo($vendeur->getEmail())
			        ->setBody($mailBody, 'text/html');
			    //echo $mailBody;exit;
			    $this->get('mailer')->send($message);
		    }
			
			$mailBody = $this->renderView('AdminBundle:ContentComment:mail_accepter2.html.twig', array('comment' => $comment));
			$message = \Swift_Message::newInstance()
				->setSubject($this->get('translator')->trans('Un nouveau commentaire !'))
				->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
				->setTo($comment->getEmail())
				->setBody($mailBody, 'text/html');
			//echo $mailBody;exit;
			$this->get('mailer')->send($message);
    	}
	    
	  	$request->getSession()->getFlashBag()->add('message', 'Commentaire publié.');
	
	    return $this->redirect($this->generateUrl('admin_contentcomment_index'));
	}
	
	/**
     * @Route("/contentcomment/refuser/{comment_id}", name="admin_contentcomment_refuser", requirements={"comment_id" = "\d+"})
	 * @ParamConverter("comment", options={"mapping": {"comment_id": "id"}})
	 */
	public function refuserAction(ContentComment $comment, Request $request)
	{	
		$em = $this->getDoctrine()->getManager();
		$comment->setValidate(-1);
	    $em->flush();
	    
	    $vendeur = $comment->getVendeur();
	    
	    if ($vendeur){	
	    	$vendeur->updateNotes();
	    	$em->persist($vendeur);
	    	$em->flush();
    	}
	    
	  	$request->getSession()->getFlashBag()->add('message', 'Commentaire refusée.');
	
	    return $this->redirect($this->generateUrl('admin_contentcomment_index'));
	}

    /**
     * @Route("/contentcomment/edit/{comment_id}", name="admin_contentcomment_edit", requirements={"comment_id" = "\d+"})
	 * @ParamConverter("comment", options={"mapping": {"comment_id": "id"}})
	 * @Template("AdminBundle:ContentComment:edit.html.twig")
	 */
    public function editAction(ContentComment $comment, Request $request)
    {
	    $form = $this->createForm(new ContentCommentType(), $comment);
	    $form->remove('ip_address');
	    $form->remove('user');
	    $form->remove('content');
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
           $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_contentcomment_show', array('comment_id' => $comment->getId())));
	    }
	
	    return array(
	       'form'        => $form->createView(),
	    );
    }
     
  
    /**
     * @Route("/contentcomment/delete/{comment_id}", name="admin_contentcomment_delete", requirements={"comment_id" = "\d+"})
	 * @ParamConverter("comment", options={"mapping": {"comment_id": "id"}})
	 */
	public function deleteAction(ContentComment $comment, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($comment);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_contentcomment_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = ContentController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('ContentBundle:ContentComment')
	      ->getComments($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    
	    $nb_result = $this->getDoctrine()
							->getManager()
						      ->getRepository('ContentBundle:ContentComment')->getCommentsCount($queryBuilder);
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
		  'nb_result'   => $nb_result,
	    );
    }
}
