<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\CoreBundle\Lib\MyConst;

use FAPROD\MessageBundle\Form\DiscussionType;
use FAPROD\MessageBundle\Entity\Discussion;

class DiscussionController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/discussion/index/{page}/{sort}/{sens}", name="admin_discussion_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Discussion:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
	    return $this->initPagination($page, $sort, $sens, '', 'admin_discussion_index');
    }
    
    /**
     * @Route("/discussion/show/{discussion_id}", name="admin_discussion_show", requirements={"discussion_id" = "\d+"})
	 * @ParamConverter("discussion", options={"mapping": {"discussion_id": "id"}})
	 * @Template("AdminBundle:Discussion:show.html.twig")
	 */
	public function showAction(Discussion $discussion)
	{	
	  	return array(
	    	'discussion' => $discussion,
	  	);
	}
    
	/**
     * @Route("/discussion/delete/{discussion_id}", name="admin_discussion_delete", requirements={"discussion_id" = "\d+"})
	 * @ParamConverter("discussion", options={"mapping": {"discussion_id": "id"}})
	 */
	public function deleteAction(Discussion $discussion, Request $request)
	{	
		$em = $this->getDoctrine()->getManager();
	    $em->remove($discussion);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_discussion_index'));
	}
    
	
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = DiscussionController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('MessageBundle:Discussion')
	      ->getDiscussions($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    
	    $nb_result = $this->getDoctrine()
							->getManager()
						      ->getRepository('MessageBundle:Discussion')->getDiscussionsCount($queryBuilder);
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
		  'nb_result'   => $nb_result,
	    );
    }
}
