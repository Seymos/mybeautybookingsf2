<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\CoreBundle\Lib\MyConst;
use Ob\HighchartsBundle\Highcharts\Highchart;
use MangoPay\MangoPayApi;

use FAPROD\ContactBundle\Form\ContactType;
use FAPROD\UserBundle\Form\ClienteType;
use FAPROD\UserBundle\Form\ProfessionnelType;
use FAPROD\UserBundle\Entity\User;
use FAPROD\UserBundle\Entity\Image;
use FAPROD\CoreBundle\Lib\Tools\FichierExcel;

class UserController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/user/index/{page}/{sort}/{sens}", name="admin_user_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:User:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
    	$repository_user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User');
    	$queryBuilder = $repository_user->createQueryBuilder('u');
    	
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_user_index');
    }
	
	/**
     * @Route("/user/contact/{user_id}", name="admin_user_contact", requirements={"user_id" = "\d+"})
	 * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 * @Template("AdminBundle:User:contact.html.twig")
	 */
	public function contactAction(User $user, Request $request)
	{
		$form = $this->createForm(new ContactType());
		$form->remove("first_name");
		$form->remove("last_name");
		$form->add("sujet", "text");
    	
    	if ($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);
	    	
	    	$contact = $form->getData();
	        
	        $mailBody = $this->renderView('AdminBundle:User:mail_contact.html.twig', array('message' => $contact['message']));
	        $message = \Swift_Message::newInstance()
		        ->setSubject($contact['sujet'])
		        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		        ->setTo($contact['email'])
		        ->setBody($mailBody, 'text/html');
		    //echo $mailBody;exit;
		    $this->get('mailer')->send($message);
	    
	        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Message envoyé.'));
	    
	        return $this->redirect($this->generateUrl('admin_user_show', array('user_id' => $user->getId())));
	    }
    	
        return array(
				'form'   => $form->createView(),
				'user'   => $user,
				);
	}
	
	/**
     * @Route("/user/mango/{user_id}", name="admin_user_mango", requirements={"user_id" = "\d+"})
	 * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 */
	public function mangoAction(User $user, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
	    $em->persist($user);
			
		// CREATION COMPTE MAGO PAY
		require_once __DIR__ . '/../../BookingBundle/mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];
	
		$user_mango = new \MangoPay\UserNatural();
		$user_mango->PersonType = "NATURAL";
		$user_mango->FirstName = $user->getLastName();
		$user_mango->LastName = $user->getFirstName();
		$user_mango->Birthday = strtotime("1979/09/24");//$this->getUser()->getBirthdate()->getTimestamp();
		$user_mango->Nationality = "FR";//$this->getUser()->getCountry();
		$user_mango->CountryOfResidence = "FR";//$this->getUser()->getCountry();
		$user_mango->Email = $user->getEmail();
		if ($user->getType() == 1){
			$user_mango->Tag = 'CLIENTE n°'.$user->getId();
		} else {
			$user_mango->Tag = 'PRO n°'.$user->getId();
		}		
		
		$result = $mangoPayApi->Users->Create($user_mango);
		
		if ($result) {
			
			$mangoId = $result->Id;
			
			$user->setMangoId($mangoId);
			$em->flush();
			
			$Wallet = new \MangoPay\Wallet();
			$Wallet->Owners = array($mangoId);
			$Wallet->Description = $user->getLastName().' '.$user->getFirstName();
			$Wallet->Currency = "EUR";
			if ($user->getType() == 1){
				$Wallet->Tag = 'CLIENTE n°'.$user->getId();
			} else {
				$Wallet->Tag = 'PRO n°'.$user->getId();
			}
			$result = $mangoPayApi->Wallets->Create($Wallet);
			
		}
		
		return $this->redirect($this->generateUrl('admin_user_show', array('user_id' => $user->getId())));
	}
    
    /**
     * @Route("/user/show/{user_id}", name="admin_user_show", requirements={"user_id" = "\d+"})
	 * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 * @Template("AdminBundle:User:show.html.twig")
	 */
	public function showAction(User $user)
	{	
	  	$nb_jours = 40;
    	
    	$repository_stat = $this->getDoctrine()->getManager()->getRepository('UserBundle:Stat');
    	$queryBuilder = $repository_stat->createQueryBuilder('s');
		$queryBuilder->where('s.user = :user')
					 ->andWhere('s.date >= :date')
					 ->setParameter('user', $user)
					 ->setParameter('date', $date = new \DateTime("- ".$nb_jours." days"));
    	$stats_user = $repository_stat->getStats(null, null, 'id', 0, $queryBuilder);
    	
    	$dates_temp = array();
    	$vues_temp = array();
    	$contacts_temp = array();
    	
    	for ($i = $nb_jours; $i >=0; $i--){
    		$date = new \DateTime("- ".$i." days");
    		$dates_temp[$date->format('d/m')] = $date->format('d/m');
    		$vues_temp[$date->format('d/m')] = 0;
    		$contacts_temp[$date->format('d/m')] = 0;
    	}
    	
    	foreach ($stats_user as $stat){
    		$vues_temp[$stat->getDate('d/m')] += $stat->getVue();
    		$contacts_temp[$stat->getDate('d/m')] += $stat->getContact();
    	}
    	
    	$dates = array();
    	$vues = array();
    	$contacts = array();
    	foreach ($dates_temp as $date)$dates[] = $date;
    	foreach ($vues_temp as $vue)$vues[] = $vue;
    	foreach ($contacts_temp as $contact)$contacts[] = $contact;
    	
        $History = array(
            array(
                 "name" => "Vues", 
                 "data" => $vues
            ),
            array(
                 "name" => "Contacts", 
                 "data" => $contacts
            ),
            
        );
        
        $ob = new Highchart();
        $ob->chart->renderTo('linechart');  
        $ob->title->text($this->get('translator')->trans('Statistiques'));
        $ob->yAxis->min(0);
        //$ob->yAxis->title(array('text' => "Ventes (milliers d'unité)"));
        //$ob->xAxis->title(array('text'  => "Date du jours"));
        $ob->xAxis->categories($dates);
        $ob->xAxis->minTickInterval(count($dates)/10);

        $ob->series($History);
        
	  	return array(
	  		'chart'  => $ob,
	    	'user'   => $user,
	  	);
	}
	
	/**
     * @Route("/user/add", name="admin_user_add")
     * @Template("AdminBundle:User:add.html.twig")
	 */
    public function addAction(Request $request)
    {
    	
	    $user = new User();
		$user->setType(2);
	    $statut = MyConst::getStatutFiscal();
		$distance = MyConst::getDistance();
		$metier = MyConst::getMetier();
		$form = $this->createForm(new ProfessionnelType(), $user, array(
																	'statut'    => $statut,
																	'distance'  => $distance,
																	'metiers'   => $metier,
																	));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($user);
			
			if ($user->getType() == 2 and $form->get('gmap_lat')->getData())$user->setLat($form->get('gmap_lat')->getData());
	        if ($user->getType() == 2 and $form->get('gmap_lng')->getData())$user->setLng($form->get('gmap_lng')->getData());
	       
	       $em->flush();
	       
	       if ($request->request->get('images')){
		   	   foreach ($request->request->get('images') as $image_name){
		   	   		$image = new Image();
		   	   		$user->addImage($image);
		   	   		$em->persist($image);
		   	   		$em->flush();
		   	   		$image->upload($image_name, $user->getShortUsername());
		   	   }
		   	   $em->flush();
	   	   }
	        
	        if ($request->request->get('image')){
		       	if ($user->getImage()){
		       		$image = $user->getImage();
		       		$image->upload($request->request->get('image'), $user->getShortUsername());
		       		$em->flush();
		       	}	       		
	       } elseif ($request->request->get('image_old') == '') {
	       	  	$image = $user->getImage();
	       	  	$image->setFilename('no_image.png');
	       	  	$em->flush();
	       }
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        return $this->redirect($this->generateUrl('admin_user_show', array('user_id' => $user->getId())));
	    }
	        
	    $_SESSION['is_adminsf2_log'] = true;
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$cheveux = $repository_category->find(165);
		$peau = $repository_category->find(199);
		$preferences = $repository_category->find(216);
	
	    return array(
	       'form'          => $form->createView(),
		   'cheveux'       => $cheveux,
		   'peau'          => $peau,
		   'preferences'   => $preferences,
	    );
    }

    /**
     * @Route("/user/edit/{user_id}", name="admin_user_edit", requirements={"user_id" = "\d+"})
	 * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 * @Template("AdminBundle:User:edit.html.twig")
	 */
    public function editAction(User $user, Request $request)
    {
    	$originalImages = new ArrayCollection();
		
		foreach ($user->getImages() as $image) {
			$originalImages->add($image);
		}	

		if ($user->getType() == 1){
			$form = $this->createForm(new ClienteType(), $user);
		} else {
			$statut = MyConst::getStatutFiscal();
			$distance = MyConst::getDistance();
			$metier = MyConst::getMetier();
			$form = $this->createForm(new ProfessionnelType(), $user, array(
																	'statut'    => $statut,
																	'distance'  => $distance,
																	'metiers'   => $metier,
																	));
		}
	    
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
		   
	       if ($user->getType() == 2 and $form->get('gmap_lat')->getData())$user->setLat($form->get('gmap_lat')->getData());
	       if ($user->getType() == 2 and $form->get('gmap_lng')->getData())$user->setLng($form->get('gmap_lng')->getData());
	       
	       $em->flush();
	       
	       if ($request->request->get('images')){
		   	   foreach ($request->request->get('images') as $image_name){
		   	   		$image = new Image();
		   	   		$user->addImage($image);
		   	   		$em->persist($image);
		   	   		$em->flush();
		   	   		$image->upload($image_name, $user->getShortUsername());
		   	   }
		   	   $em->flush();
	   	   }
		   
           foreach ($originalImages as $image) {
            if (!in_array($image->getId(), is_array($request->request->get('images_old')) ? $request->request->get('images_old') : array())) {
                $em->remove($image);
            }
           } 
	       
	       $em->flush();
	       
	       foreach ($user->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
	       
	       if ($request->request->get('image')){
		       	if ($user->getImage()){
		       		$image = $user->getImage();
		       		$image->upload($request->request->get('image'), $user->getShortUsername());
		       		$em->flush();
		       	}	       		
	       } elseif ($request->request->get('image_old') == '') {
	       	  	$image = $user->getImage();
	       	  	$image->setFilename('no_image.png');
	       	  	$em->flush();
	       }
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_user_show', array('user_id' => $user->getId())));
	    }
	        
	    $_SESSION['is_adminsf2_log'] = true;
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$cheveux = $repository_category->find(165);
		$peau = $repository_category->find(199);
		$preferences = $repository_category->find(216);
	
	    return array(
	       'form'          => $form->createView(),
		   'cheveux'       => $cheveux,
		   'peau'          => $peau,
		   'preferences'   => $preferences,
	    );
    }
     
  
    /**
     * @Route("/user/delete/{user_id}", name="admin_user_delete", requirements={"user_id" = "\d+"})
	 * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 */
	public function deleteAction(User $user, Request $request)
	{	
		$em = $this->getDoctrine()->getManager();	
		
		$stmt = $em->getConnection()
					->prepare("update message set destinataire_id = null where `destinataire_id`= :destinataire_id");
		$stmt->bindValue('destinataire_id',$user->getId());
		$stmt->execute();
		
		$stmt = $em->getConnection()
					->prepare("update message set user_id = null where `user_id`= :user_id");
		$stmt->bindValue('user_id',$user->getId());
		$stmt->execute();
		
		$stmt = $em->getConnection()
					->prepare("update discussion set destinataire_id = null where `destinataire_id`= :destinataire_id");
		$stmt->bindValue('destinataire_id',$user->getId());
		$stmt->execute();
		
		$stmt = $em->getConnection()
					->prepare("update discussion set user_id = null where `user_id`= :user_id");
		$stmt->bindValue('user_id',$user->getId());
		$stmt->execute();
		
	    $em->remove($user);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_user_index'));
	}
	
	/**
     * @Route("/user/export", name="admin_user_export")
	 */
	public function exportAction(Request $request)
	{	
		$users = $this->getDoctrine()
	        ->getManager()
	        ->getRepository('UserBundle:User')
	        ->findBy(
			    array(),
			    array('id' => 'desc')
		    );

	  	$fichier = new FichierExcel();
		$fichier->Colonne(utf8_decode("Nom;Prénom;Entreprise;SIRET;Email;Tél;Ville;Compte actif;Date"));
		
		foreach ($users as $user){
			$fichier->Insertion(utf8_decode($user->getFirstName().";".$user->getLastName().";".$user->getEntreprise().";".$user->getSiren().";".$user->getEmail().";".$user->getPhone().";".$user->getCity().";".$user->getValidateLabel().";".$user->getUpdatedAt('d/m/Y')));
		}
		
		$fichier->output('Export');
	}
	
	/**
     * @Route("/user/gosearch", name="admin_user_gosearch")
	 */
	public function gosearchAction(Request $request)
	{	
		if ($request->getMethod() == 'POST'){
			$request->getSession()->set('search_user', $request->request->get('search_user'));
		}

        return $this->redirect($this->generateUrl('admin_user_search'));
	}
	
	/**
     * @Route("/user/search/{page}/{sort}/{sens}", name="admin_user_search", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:User:index.html.twig")
     */
    public function searchAction($page, $sort, $sens, Request $request)
    {
		$mot = $request->getSession()->get('search_user');
		
		$mot = strtolower($mot);
	    $mot = $this->container->get('FAPROD.MyTools')->netoyage($mot);
	    $mots = explode(" ",$mot);
	    $nombre_mots=count($mots);
	    $z=1;
		
	    $queryBuilder = $this->getDoctrine()
						      ->getManager()
						      ->getRepository('UserBundle:User')
						      ->createQueryBuilder('u');
	    
		$queryBuilder->where('u.id LIKE :mot_0 or u.first_name LIKE :mot_0 or u.last_name LIKE :mot_0 or u.email LIKE :mot_0 or u.entreprise LIKE :mot_0');
		$queryBuilder->setParameter('mot_0', "%".$mots[0]."%");
		
		while($z<$nombre_mots)
    	{
    		$queryBuilder->orWhere('u.id LIKE :mot_'.$z.' or u.first_name LIKE :mot_'.$z.' or u.last_name LIKE :mot_'.$z.' or u.email LIKE :mot_'.$z.' or u.entreprise LIKE :mot_'.$z);
    		$queryBuilder->setParameter('mot_'.$z, "%".$mots[$z]."%");
    		$z++;
		}

		return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_user_search');
    }
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = UserController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('UserBundle:User')
	      ->getUsers($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
		
		/*
		$em = $this->getDoctrine()->getManager();	
		foreach ($liste as $user){
			$image = new \FAPROD\UserBundle\Entity\Image();
			$user->setImage($image);
			$em->persist($user);
		}
		$em->flush();
		*/
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    
	    $nb_result = $this->getDoctrine()
							->getManager()
						      ->getRepository('UserBundle:User')->getUsersCount($queryBuilder);
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
		  'nb_result'   => $nb_result,
	    );
    }
}
