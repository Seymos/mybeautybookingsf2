<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use FAPROD\ContentBundle\Form\ContentType;
use FAPROD\ContentBundle\Entity\Content;
use FAPROD\ContentBundle\Entity\Image;

class ContentController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/content/index/{page}/{sort}/{sens}", name="admin_content_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Content:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
	    return $this->initPagination($page, $sort, $sens, '', 'admin_content_index');
    }
    
    /**
     * @Route("/content/show/{content_id}", name="admin_content_show", requirements={"content_id" = "\d+"})
	 * @ParamConverter("content", options={"mapping": {"content_id": "id"}})
	 * @Template("AdminBundle:Content:show.html.twig")
	 */
	public function showAction(Content $content)
	{
	  	return array(
	    	'content' => $content,
	  	);
	}
	
	/**
     * @Route("/content/add", name="admin_content_add")
     * @Template("AdminBundle:Content:add.html.twig")
     * @I18nDoctrine
	 */
    public function addAction(Request $request)
    {
    	$langues = $this->container->get('FAPROD.Configs')->getBoLangues();
		
	    $content = new Content();
	    $form = $this->createForm(new ContentType, $content, array(
	    															'langues' => $langues,
	    															));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        //$em->persist($content);
	        
	        $new_content = new Content();
	        $em->persist($new_content);
	        $em->flush();
	        
	        if ($request->request->get('image_principale')){
		       	if ($new_content->getImagePrincipale()){
		       		$image_principale = $new_content->getImagePrincipale();
		       		$image_principale->upload($request->request->get('image_principale'), $form->get('translations')['fr']['title']->getData());
		       		$em->flush();
		       	}	       		
	       } elseif ($request->request->get('image_principale_old') == '') {
	       	  	$image_principale = $new_content->getImagePrincipale();
	       	  	$image_principale->setFilename('no_image.png');
	       	  	$em->flush();
	       }
		    
		   if ($request->request->get('images')){
		   	   foreach ($request->request->get('images') as $image_name){
		   	   		$image = new Image();
		   	   		$new_content->addImage($image);
		   	   		$em->persist($image);
		   	   		$em->flush();
		   	   		$image->upload($image_name, $form->get('translations')['fr']['title']->getData());
		   	   }
		   	   $em->flush();
	   	   }
	       
	       foreach ($new_content->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
           
           $new_content->setIsPublish($form->get('is_publish')->getData());
           $new_content->setEnligneDate($form->get('enligne_date')->getData());
           $new_content->setTypeContent($form->get('type_content')->getData());
           $new_content->setTitle($form->get('translations')['fr']['title']->getData());
           $new_content->setDescription($form->get('translations')['fr']['description']->getData());
           $new_content->setHtml($form->get('translations')['fr']['html']->getData());
           $new_content->setMetaTitle($form->get('translations')['fr']['meta_title']->getData());
           $new_content->setMetaDescription($form->get('translations')['fr']['meta_description']->getData());
           $new_content->setMetaKeywords($form->get('translations')['fr']['meta_keywords']->getData());
           
           $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        return $this->redirect($this->generateUrl('admin_content_show', array('content_id' => $new_content->getId())));
	    }
	    
	    $_SESSION['is_adminsf2_log'] = true;
	    
	    $repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$queryBuilder = $repository_category->createQueryBuilder('c');
		$queryBuilder->where('c.level = 0');
    	$categorys = $repository_category->getCategorys(null, null, 'no_order', 1, $queryBuilder);
	    
	    return array(
	       'form'        => $form->createView(),
	       'langues'     => $langues,
	       'categorys'   => $categorys,
	    );
    }

    /**
     * @Route("/content/edit/{content_id}", name="admin_content_edit", requirements={"content_id" = "\d+"})
	 * @ParamConverter("content", options={"mapping": {"content_id": "id"}})
	 * @Template("AdminBundle:Content:edit.html.twig")
	 * @I18nDoctrine
	 */
    public function editAction(Content $content, Request $request)
    {
    	$originalImages = new ArrayCollection();
		foreach ($content->getImages() as $image) {
			$originalImages->add($image);
		}
		
    	$langues = $this->container->get('FAPROD.Configs')->getBoLangues();
    	
	    $form = $this->createForm(new ContentType, $content, array(
	    															'langues' => $langues,
	    															));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();	       
	       $em->flush();
           
           if ($request->request->get('image_principale')){
		       	if ($content->getImagePrincipale()){
		       		$image = $content->getImagePrincipale();
		       		$image->upload($request->request->get('image_principale'), $content->getTitle());
		       		$em->flush();
		       	}	       		
	       } elseif ($request->request->get('image_principale_old') == '') {
	       	  	$image = $content->getImagePrincipale();
	       	  	$image->setFilename('no_image.png');
	       	  	$em->flush();
	       }
		    
		   if ($request->request->get('images')){
		   	   foreach ($request->request->get('images') as $image_name){
		   	   		$image = new Image();
		   	   		$content->addImage($image);
		   	   		$em->persist($image);
		   	   		$em->flush();
		   	   		$image->upload($image_name, $content->getTitle());
		   	   }
		   	   $em->flush();
	   	   }
		   
           foreach ($originalImages as $image) {
            if (!in_array($image->getFilename(), is_array($request->request->get('images_old')) ? $request->request->get('images_old') : array())) {
                $em->remove($image);
            }
           } 
	       
	       $em->flush();
	       
	       foreach ($content->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
           
           $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_content_show', array('content_id' => $content->getId())));
	    }
	
	    $_SESSION['is_adminsf2_log'] = true;
	    
	    $repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$queryBuilder = $repository_category->createQueryBuilder('c');
		$queryBuilder->where('c.level = 0');
    	$categorys = $repository_category->getCategorys(null, null, 'no_order', 1, $queryBuilder);
	    
	    return array(
	       'form'        => $form->createView(),
	       'langues'     => $langues,
	       'categorys'   => $categorys,
	    );
    }
     
  
    /**
     * @Route("/content/delete/{content_id}", name="admin_content_delete", requirements={"content_id" = "\d+"})
	 * @ParamConverter("content", options={"mapping": {"content_id": "id"}})
	 */
	public function deleteAction(Content $content, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($content);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_content_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = ContentController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('ContentBundle:Content')
	      ->getContents($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
	    );
    }
}
