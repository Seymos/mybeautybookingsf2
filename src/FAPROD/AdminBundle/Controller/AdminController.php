<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\AdminBundle\Form\AdminType;
use FAPROD\AdminBundle\Entity\Admin;
use FAPROD\CoreBundle\Lib\Tools\FichierExcel;

class AdminController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/admin/index/{page}/{sort}/{sens}", name="admin_admin_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Admin:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
	    return $this->initPagination($page, $sort, $sens, '', 'admin_admin_index');
    }
    
    /**
     * @Route("/admin/show/{admin_id}", name="admin_admin_show", requirements={"admin_id" = "\d+"})
	 * @ParamConverter("admin", options={"mapping": {"admin_id": "id"}})
	 * @Template("AdminBundle:Admin:show.html.twig")
	 */
	public function showAction(Admin $admin)
	{	
	  	return array(
	    	'admin' => $admin,
	  	);
	}
	
	/**
     * @Route("/admin/add", name="admin_admin_add")
     * @Template("AdminBundle:Admin:add.html.twig")
	 */
    public function addAction(Request $request)
    {
	    $admin = new Admin();
	    $form = $this->createForm(new AdminType(), $admin);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($admin);
	        $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        return $this->redirect($this->generateUrl('admin_admin_show', array('admin_id' => $admin->getId())));
	    }
	    
	    return array(
	        'form'   => $form->createView(),
	    );
    }

    /**
     * @Route("/admin/edit/{admin_id}", name="admin_admin_edit", requirements={"admin_id" = "\d+"})
	 * @ParamConverter("admin", options={"mapping": {"admin_id": "id"}})
	 * @Template("AdminBundle:Admin:edit.html.twig")
	 */
    public function editAction(Admin $admin, Request $request)
    {
	    $form = $this->createForm(new AdminType(), $admin);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
	       $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_admin_show', array('admin_id' => $admin->getId())));
	    }
	
	    return array(
	       'form'   => $form->createView(),
	    );
    }
     
  
    /**
     * @Route("/admin/delete/{admin_id}", name="admin_admin_delete", requirements={"admin_id" = "\d+"})
	 * @ParamConverter("admin", options={"mapping": {"admin_id": "id"}})
	 */
	public function deleteAction(Admin $admin, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($admin);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_admin_index'));
	}
	
	/**
     * @Route("/admin/export", name="admin_admin_export")
	 */
	public function exportAction(Request $request)
	{	
		$admins = $this->getDoctrine()
	        ->getManager()
	        ->getRepository('AdminBundle:Admin')
	        ->findBy(
			    array(),
			    array('id' => 'desc')
		    );

	  	$fichier = new FichierExcel();
		$fichier->Colonne(utf8_decode("Nom;Prénom;Adresse;Ville;Code postal;Email;Tél;Pro;Date"));
		
		foreach ($admins as $admin){
			$fichier->Insertion(utf8_decode($admin->getFirstName().";".$admin->getLastName().";".$admin->getAddress1().' '.$admin->getAddress2().";".$admin->getCity().";".$admin->getPostalCode().";".$admin->getEmail().";".$admin->getPhone().";".$admin->getProLabel().";".$admin->getUpdatedAt('d/m/Y')));
		}
		
		$fichier->output('Export');
	}
	
	/**
     * @Route("/admin/gosearch", name="admin_admin_gosearch")
	 */
	public function gosearchAction(Request $request)
	{	
		if ($request->request->get('search_update')){
			$request->getSession()->set('search_admin', $request->request->get('search_admin'));
		}

        return $this->redirect($this->generateUrl('admin_admin_search'));
	}
	
	/**
     * @Route("/admin/search/{page}/{sort}/{sens}", name="admin_admin_search", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Admin:index.html.twig")
     */
    public function searchAction($page, $sort, $sens, Request $request)
    {
		$mot = $request->getSession()->get('search_admin');
		
		$mot = strtolower($mot);
	    $mot = $this->container->get('FAPROD.MyTools')->netoyage($mot);
	    $mots = explode(" ",$mot);
	    $nombre_mots=count($mots);
	    $z=1;
		
	    $queryBuilder = $this->getDoctrine()
						      ->getManager()
						      ->getRepository('AdminBundle:Admin')
						      ->createQueryBuilder('u');
	    
		$queryBuilder->where('u.first_name LIKE :mot_0 or u.last_name LIKE :mot_0 or u.email LIKE :mot_0 or u.pro_rs LIKE :mot_0');
		$queryBuilder->setParameter('mot_0', "%".$mots[0]."%");
		
		while($z<$nombre_mots)
    	{
    		$queryBuilder->orWhere('u.first_name LIKE :mot_'.$z.' or u.last_name LIKE :mot_'.$z.' or u.email LIKE :mot_'.$z.' or u.pro_rs LIKE :mot_'.$z);
    		$queryBuilder->setParameter('mot_'.$z, "%".$mots[$z]."%");
    		$z++;
		}

		return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_admin_search');
    }
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = AdminController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('AdminBundle:Admin')
	      ->getAdmins($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
	    );
    }
}
