<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\RememberMe\TokenBasedRememberMeServices;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LoginController extends Controller
{
	/**
     * @Route("/login", name="admin_login_index")
     * @Template("AdminBundle:Login:index.html.twig")
	 */
	public function indexAction(Request $request)
	{
		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			$user = $this->get('security.context')->getToken()->getUser();
			
			$admin = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Admin')
		      ->findOneBy(array(
		      	"username" => $user->getUserName(),
		      	"password" => $user->getPassword(),
		      	"validate" => 1,
				));
		
			if ($admin){
				
		        $token = new UsernamePasswordToken($admin, $admin->getPassword(), 'admin_area', array('ROLE_ADMIN'));
				$this->get("security.context")->setToken($token);
				return $this->redirect($this->generateUrl('admin_index'));
			}
	    }
	    
		return array();
	}
	
	/**
     * @Route("/login/logout", name="admin_login_logout")
	 */
	public function logoutAction(Request $request)
	{
		$this->get('security.context')->setToken(null);
		//$this->get('request')->getSession()->invalidate();
		$response = new RedirectResponse($this->generateUrl('admin_login_index'));
		$response->headers->clearCookie('REMEMBERME_ADMIN');
		
		return $response;
	}
	
	/**
     * @Route("/login/login", name="admin_login_login")
     * @Template("AdminBundle:Login:index.html.twig")
	 */
    public function loginAction(Request $request)
	{
	    $user = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Admin')
		      ->findOneBy(array(
		      	"username" => $request->get('username'),
		      	"password" => md5($request->get('password')),
		      	"validate" => 1,
				));
		
		if ($user){
			$token = new UsernamePasswordToken($user, $user->getPassword(), 'admin_area', array('ROLE_ADMIN'));
			$this->get("security.context")->setToken($token);
        		
			$response = new RedirectResponse($this->generateUrl('admin_index'));
			
			if ($request->get('_remember_me') == 'on'){
				
				// write cookie for persistent session storing
				$providerKey = 'admin'; // defined in security.yml
				$securityKey = $this->container->getParameter('secret');// defined in security.yml
				
				$rememberMeService = new TokenBasedRememberMeServices(array($this->getDoctrine()->getManager()->getRepository('AdminBundle:Admin')), $securityKey, $providerKey, array(
				                'path' => '/',
				                'name' => 'REMEMBERME_ADMIN',
				                'domain' => null,
				                'secure' => false,
				                'httponly' => true,
				                'lifetime' => 2592000, // 30 days
				                'always_remember_me' => true,
				                'remember_me_parameter' => '_remember_me')
				            );
				
				$rememberMeService->loginSuccess($request, $response, $token);
			} else {
				$response->headers->clearCookie('REMEMBERME_ADMIN');
			}
			
			$event = new InteractiveLoginEvent($request, $token);
        	$this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
        	
        	return $response;
		} else {
			$this->get('session')->getFlashBag()->add('error', 'Authentification invalide');
		}
		
		return array();
	}
}
