<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
	/**
     * @Route("/", name="admin_index")
     * @Template("AdminBundle:Home:index.html.twig")
	 */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * @Route("/cache", name="admin_cache")
	 */
    public function cacheAction(Request $request)
    {
    	$filesystem   = $this->container->get('filesystem');
		$realCacheDir = $this->container->getParameter('kernel.cache_dir')."/http_cache";

		$this->container->get('cache_clearer')->clear($realCacheDir);
		$filesystem->remove($realCacheDir);

        $request->getSession()->getFlashBag()->add('message', 'Le site est actualisé.');

        return $this->redirect($this->generateUrl('admin_index'));
    }
    
    /**
     * @Route("/media", name="admin_media")
     * @Template("AdminBundle:Home:media.html.twig")
	 */
    public function mediaAction()
    {
    	$_SESSION['is_adminsf2_log'] = true;
        return array();
    }
    
    /**
     * @Route("/infosheader", name="admin_infosheader")
     * @Template("AdminBundle:Home:infosheader.html.twig")
	 */
    public function infosheaderAction()
    {
    	$nb_user = $this->getDoctrine()->getManager()->getRepository('UserBundle:User')
    								->createQueryBuilder('u')
    								->select('COUNT(u)')
									->getQuery()
									->getSingleScalarResult();
					 
        return array(
        			"nb_user"                => $nb_user,
        		);
    }
}
