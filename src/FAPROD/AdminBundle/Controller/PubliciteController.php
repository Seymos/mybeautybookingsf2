<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Ob\HighchartsBundle\Highcharts\Highchart;

use FAPROD\PubBundle\Form\PubliciteType;
use FAPROD\PubBundle\Entity\Publicite;

class PubliciteController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/publicite/index/{page}/{sort}/{sens}", name="admin_publicite_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Publicite:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
    	$repository_pub = $this->getDoctrine()->getManager()->getRepository('PubBundle:Publicite');
    	$queryBuilder = $repository_pub->createQueryBuilder('p');
    	
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_publicite_index');
    }
    
    /**
     * @Route("/publicite/show/{publicite_id}", name="admin_publicite_show", requirements={"publicite_id" = "\d+"})
	 * @ParamConverter("publicite", options={"mapping": {"publicite_id": "id"}})
	 * @Template("AdminBundle:Publicite:show.html.twig")
	 */
	public function showAction(Publicite $publicite)
	{	
		$nb_jours = 20;
		
		$repository_stat = $this->getDoctrine()->getManager()->getRepository('PubBundle:Stat');
    	$queryBuilder = $repository_stat->createQueryBuilder('s');
		$queryBuilder->where('s.publicite = :publicite')
					 ->andWhere('s.date >= :date')
					 ->setParameter('publicite', $publicite)
					 ->setParameter('date', $date = new \DateTime("- ".$nb_jours." days"));
    	$stats = $repository_stat->getStats(null, null, 'id', 0, $queryBuilder);
    	
    	$dates_temp = array();
    	$vues_temp = array();
    	$clics_temp = array();
    	
    	for ($i = $nb_jours; $i >=0; $i--){
    		$date = new \DateTime("- ".$i." days");
    		$dates_temp[$date->format('d/m')] = $date->format('d/m');
    		$vues_temp[$date->format('d/m')] = 0;
    		$clics_temp[$date->format('d/m')] = 0;
    	}
    	
    	foreach ($stats as $stat){
    		$vues_temp[$stat->getDate('d/m')] = $stat->getVue();
    		$clics_temp[$stat->getDate('d/m')] = $stat->getClic();
    	}
    	
    	$dates = array();
    	$vues = array();
    	$clics = array();
    	foreach ($dates_temp as $date)$dates[] = $date;
    	foreach ($vues_temp as $vue)$vues[] = $vue;
    	foreach ($clics_temp as $clic)$clics[] = $clic;
    	
        $History = array(
            array(
                 "name" => "Vues", 
                 "data" => $vues
            ),
                        array(
                 "name" => "Clics", 
                 "data" => $clics
            ),
            
        );
        
        $ob = new Highchart();
        $ob->chart->renderTo('linechart');  
        $ob->title->text($this->get('translator')->trans('Statistiques'));
        $ob->yAxis->min(0);
        //$ob->yAxis->title(array('text' => "Ventes (milliers d'unité)"));
        //$ob->xAxis->title(array('text'  => "Date du jours"));
        $ob->xAxis->categories($dates);

        $ob->series($History);
        
	  	return array(
	    	'publicite'  => $publicite,
	    	'chart'      => $ob,
	  	);
	}
	
	/**
     * @Route("/publicite/add", name="admin_publicite_add")
     * @Template("AdminBundle:Publicite:add.html.twig")
	 */
    public function addAction(Request $request)
    {
    	$status = $this->container->get('FAPROD.MyConst')->getStatutPub();
    	$types = $this->container->get('FAPROD.MyConst')->getTypesPub();
    	$cibles = $this->container->get('FAPROD.MyConst')->getCiblesPub();
    	
    	$originalImages = new ArrayCollection();
    	
	    $publicite = new Publicite();
	    $form = $this->createForm(new PubliciteType(), $publicite, array(
	    																'status'  => $status,
	    																'types'   => $types,
	    																'cibles'  => $cibles,
	    																));
	    																
	    $form->add('url', 'url', array('required' => false));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($publicite);
		    
		   foreach ($originalImages as $image) {
            if (false === $publicite->getImages()->contains($image)) {
                $em->remove($image);
            }
           } 
	       
	       $em->flush();
	       
	       foreach ($publicite->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
           
           $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        return $this->redirect($this->generateUrl('admin_publicite_show', array('publicite_id' => $publicite->getId())));
	    }
	    
	    return array(
	        'form'   => $form->createView(),
	    );
    }

    /**
     * @Route("/publicite/edit/{publicite_id}", name="admin_publicite_edit", requirements={"publicite_id" = "\d+"})
	 * @ParamConverter("publicite", options={"mapping": {"publicite_id": "id"}})
	 * @Template("AdminBundle:Publicite:edit.html.twig")
	 */
    public function editAction(Publicite $publicite, Request $request)
    {
    	$status = $this->container->get('FAPROD.MyConst')->getStatutPub();
    	$types = $this->container->get('FAPROD.MyConst')->getTypesPub();
    	$cibles = $this->container->get('FAPROD.MyConst')->getCiblesPub();
    	
    	$originalImages = new ArrayCollection();
		foreach ($publicite->getImages() as $image) {
			$originalImages->add($image);
		}
    	
	    $form = $this->createForm(new PubliciteType(), $publicite, array(
	    																'status'  => $status,
	    																'types'   => $types,
	    																'cibles'  => $cibles,
	    																));
	    																
	    $form->add('url', 'url', array('required' => false));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
		    
		   foreach ($originalImages as $image) {
            if (false === $publicite->getImages()->contains($image)) {
                $em->remove($image);
            }
           } 
	       
	       $em->flush();
	       
	       foreach ($publicite->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
           
           $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_publicite_show', array('publicite_id' => $publicite->getId())));
	    }
	
	    return array(
	       'form'   => $form->createView(),
	    );
    }
     
  
    /**
     * @Route("/publicite/delete/{publicite_id}", name="admin_publicite_delete", requirements={"publicite_id" = "\d+"})
	 * @ParamConverter("publicite", options={"mapping": {"publicite_id": "id"}})
	 */
	public function deleteAction(Publicite $publicite, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($publicite);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_publicite_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = PubliciteController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('PubBundle:Publicite')
	      ->getPublicites($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
		  'action'      => $action,
	    );
    }
}
