<?php
 
namespace FAPROD\AdminBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\BookingBundle\Entity\Booking;
use MangoPay\MangoPayApi;


class BookingController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/booking/index/{page}/{sort}/{sens}", name="admin_booking_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "created_at", "sens" = ""})
     * @Template("AdminBundle:Booking:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
    	$queryBuilder = $repository_booking->createQueryBuilder('b');
		$queryBuilder->where('b.statut IS NOT NULL');

        return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_booking_index');   	
    }
	
	/**
     * @Route("/booking/realise/{booking_id}", name="admin_booking_realise", requirements={"booking_id" = "\d+"})
     * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
     */
    public function bookingrerealiseAction(Booking $booking, Request $request)
    {
		
		require_once __DIR__ . '/../../BookingBundle/mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
		
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];
		
		$client_mango_id = $booking->getUser()->getMangoId();
		$Pagination = new \MangoPay\Pagination();
		$wallet_client = $mangoPayApi->Users->GetWallets($client_mango_id, $Pagination);
		if ($wallet_client){
			$wallet_client = end($wallet_client);
		}
		
		$seller_mango_id = $booking->getSeller()->getMangoId();
		$Pagination = new \MangoPay\Pagination();
		$wallet_seller = $mangoPayApi->Users->GetWallets($seller_mango_id, $Pagination);
		if ($wallet_seller){
			$wallet_seller = end($wallet_seller);
		}
		
		$result = '';
		
		if ($wallet_seller and $wallet_client) {
				
			$CardPreAuthorizationId = $booking->getPreauthorizationId();
			$CardId = $booking->getCardId();
			$result = $mangoPayApi->CardPreAuthorizations->Get($CardPreAuthorizationId);
			
			try {

				$Transfer = new \MangoPay\Transfer();
				$Transfer->Tag = "Transfer Booking N°".$booking->getId();
				$Transfer->AuthorId = $client_mango_id;
				$Transfer->CreditedUserId = $seller_mango_id;
				$Transfer->DebitedFunds = new \MangoPay\Money();
				$Transfer->DebitedFunds->Currency = "EUR";
				$Transfer->DebitedFunds->Amount = $booking->getPrice()*100;
				$Transfer->Fees = new \MangoPay\Money();
				$Transfer->Fees->Currency = "EUR";
				$Transfer->Fees->Amount = 0;
				$Transfer->DebitedWalletId = $wallet_client->Id;
				$Transfer->CreditedWalletId = $wallet_seller->Id;

				$Result = $mangoPayApi->Transfers->Create($Transfer);

			} catch (\MangoPay\Libraries\ResponseException $e) {
				//echo $e->getMessage();exit;
				$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur dans la réservation'));
				return $this->redirect($this->generateUrl('booking_NOK'));
			}
			
		}
		
		if ($result and $result->Status == 'SUCCEEDED') {
			$em = $this->getDoctrine()->getManager();
			$booking->setStatut(4);
			$em->persist($booking);
			$em->flush();
			
			$account= null;				
			$Pagination = new \MangoPay\Pagination();
			$result = $mangoPayApi->Users->GetBankAccounts($seller_mango_id, $Pagination);
			if ($result){
				$account = end($result);
			}
			
			if ($account){
			
				try{
					//echo $result->DebitedFunds->Amount;exit;
					//Build the parameters for the request
					$PayOut = new \MangoPay\PayOut();
					$PayOut->Tag = "Paiement Booking N°".$booking->getId();
					$PayOut->AuthorId = $seller_mango_id;
					$PayOut->DebitedFunds = new \MangoPay\Money();
					$PayOut->DebitedFunds->Currency = "EUR";
					$PayOut->DebitedFunds->Amount = $booking->getPrice()*100;
					$PayOut->Fees = new \MangoPay\Money();
					$PayOut->Fees->Currency = "EUR";
					$PayOut->Fees->Amount = 0;
					$PayOut->DebitedWalletId = $wallet_seller->Id;
					$PayOut->BankWireRef = $account->Details->IBAN;	
					$PayOut->PaymentType = "BANK_WIRE";
					$PayOut->MeanOfPaymentDetails = new \MangoPay\PayOutPaymentDetailsBankWire();
					$PayOut->MeanOfPaymentDetails->BankAccountId = $account->Id;					

					$result = $mangoPayApi->PayOuts->Create($PayOut);					

				} catch (\MangoPay\Libraries\ResponseException $e) {
					//echo $e->getMessage();exit;
				}
			
			}
		
		}
		
		return $this->redirect($this->generateUrl('admin_booking_show', array('booking_id' => $booking->getId())));
	}


    /**
     * @Route("/booking/booking1/{page}/{sort}/{sens}", name="admin_booking_1", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Booking:index.html.twig")
     */
    public function booking1Action($page, $sort, $sens)
    {
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
    	$queryBuilder = $repository_booking->createQueryBuilder('b');
		$queryBuilder->where('b.statut = 1');
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_booking_1');
    }
    
    /**
     * @Route("/booking/booking2/{page}/{sort}/{sens}", name="admin_booking_2", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Booking:index.html.twig")
     */
    public function booking2Action($page, $sort, $sens)
    {
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
    	$queryBuilder = $repository_booking->createQueryBuilder('b');
		$queryBuilder->where('b.statut = 2');
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_booking_2');
    }
	
    /**
     * @Route("/booking/booking3/{page}/{sort}/{sens}", name="admin_booking_3", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Booking:index.html.twig")
     */
    public function booking3Action($page, $sort, $sens)
    {
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
    	$queryBuilder = $repository_booking->createQueryBuilder('b');
		$queryBuilder->where('b.statut = 3');
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_booking_3');
    }
	
    /**
     * @Route("/booking/booking4/{page}/{sort}/{sens}", name="admin_booking_4", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Booking:index.html.twig")
     */
    public function booking4Action($page, $sort, $sens)
    {
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
    	$queryBuilder = $repository_booking->createQueryBuilder('b');
		$queryBuilder->where('b.statut = 4');
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_booking_4');
    }
	
	/**
     * @Route("/booking/booking5/{page}/{sort}/{sens}", name="admin_booking_5", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Booking:index.html.twig")
     */
    public function booking5Action($page, $sort, $sens)
    {
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
    	$queryBuilder = $repository_booking->createQueryBuilder('b');
		$queryBuilder->where('b.statut = 5');
		
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_booking_5');
    }
    
    /**
     * @Route("/booking/annulee/{page}/{sort}/{sens}", name="admin_booking_canceled", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Booking:index.html.twig")
     */
    public function canceledAction($page, $sort, $sens)
    {
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
    	$queryBuilder = $repository_booking->createQueryBuilder('b');
		$queryBuilder->where('b.statut = -2');
					 
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_booking_canceled');
    }
	
    /**
     * @Route("/booking/show/{booking_id}", name="admin_booking_show", requirements={"booking_id" = "\d+"})
	 * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
	 * @Template("AdminBundle:Booking:show.html.twig")
	 */
	public function showAction(Booking $booking)
	{	
	  	return array(
	    	'booking' => $booking,
	  	);
	}
	
	/**
     * @Route("/booking/pay/{booking_id}", name="booking_paid", requirements={"booking_id" = "\d+"})
	 * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
	 */
    public function bookingpaidAction(Booking $booking, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
		$booking->setStatut(3);
        $em->flush();
		
		if ($booking->getAdvert()->getStructure()->getId() == "88"){
			$to = $booking->getAdvert()->getUser()->getEmail();			
		}else{
			$to = $booking->getAdvert()->getStructure()->getEmail();
		}
		$mailBody = "réservation payée / pro";
		$message = \Swift_Message::newInstance()
			->setSubject('Votre réservation ZeeMono a été payée')
			->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
			->setTo($to)
			->setBody($mailBody, 'text/html');
		//$this->get('mailer')->send($message);

        $request->getSession()->getFlashBag()->add('message', 'Réservation Payée!');

		$monUrl = $_SERVER["HTTP_REFERER"];

        return $this->redirect($monUrl);
    }
	
	/**
     * @Route("/booking/cancel/{booking_id}", name="booking_cancel", requirements={"booking_id" = "\d+"})
	 * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
	 */
    public function bookingcancelAction(Booking $booking, Request $request)
    {

		require_once __DIR__ . '/../../BookingBundle/mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
			
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];
	
		if ($booking->getStatut() == 0){
	
			//Build the parameters for the request
			$CardPreAuthorizationId = $booking->getPreauthorizationId();
			$CardPreAuthorization = $mangoPayApi->CardPreAuthorizations->Get($CardPreAuthorizationId);
			$CardPreAuthorization->PaymentStatus = "CANCELED";
			$CardPreAuthorization->tag = "Reservation annulée à la demande du client";

			//Send the request
			$result = $mangoPayApi->CardPreAuthorizations->Update($CardPreAuthorization);

			if ($result->Status == 'SUCCEEDED') {
			
				$em = $this->getDoctrine()->getManager();
				$booking->setStatut(-2);
				$em->flush();
				
				if ($booking->getAdvert()->getStructure()->getId() == "88"){
					$to = $booking->getAdvert()->getUser()->getEmail();			
				}else{
					$to = $booking->getAdvert()->getStructure()->getEmail();
				}
				$mailBody = "réservation annulée / pro";
				$message = \Swift_Message::newInstance()
					->setSubject('Votre réservation ZeeMono a été annulée')
					->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
					->setTo($to)
					->setBody($mailBody, 'text/html');
				//$this->get('mailer')->send($message);

				$request->getSession()->getFlashBag()->add('message', 'Réservation annulée!');
			}else{
				
				$request->getSession()->getFlashBag()->add('error', "Oups! Problème dans l'annulation");
				
			}
		}else{
			
			$em = $this->getDoctrine()->getManager();
			$booking->setStatut(-2);
			$em->flush();
				
			if ($booking->getAdvert()->getStructure()->getId() == "88"){
				$to = $booking->getAdvert()->getUser()->getEmail();			
			}else{
				$to = $booking->getAdvert()->getStructure()->getEmail();
			}
			$mailBody = "réservation annulée / pro";
			$message = \Swift_Message::newInstance()
				->setSubject('Votre réservation ZeeMono a été annulée')
				->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
				->setTo($to)
				->setBody($mailBody, 'text/html');
			//$this->get('mailer')->send($message);

			$request->getSession()->getFlashBag()->add('message', "Réservation annulée! N'oubliez pas de procéder au remboursement!");
			
		}

		$monUrl = $_SERVER["HTTP_REFERER"];

        return $this->redirect($monUrl);
    }
	
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = BookingController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('BookingBundle:Booking')
	      ->getBookings($page, $nbPerPage, $sort, $sens, $queryBuilder);
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
	    );
    }
}
