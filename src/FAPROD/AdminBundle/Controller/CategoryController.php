<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use FAPROD\UserBundle\Form\CategoryType;
use FAPROD\UserBundle\Entity\Category;

class CategoryController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/category/index/{page}/{sort}/{sens}", name="admin_category_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "no_order", "sens" = "1"})
     * @Template("AdminBundle:Category:index.html.twig")
     */
    public function indexAction($page, $sort, $sens, Request $request)
    {
    	$QueryBuilder = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category')->createQueryBuilder('c');
    	$QueryBuilder->where('c.level = :level')
			  		  ->setParameter('level', 0);
    	
	    return $this->initPagination($page, $sort, $sens, $QueryBuilder, 'admin_category_index');
    }
    
    /**
     * @Route("/category/show/{category_id}", name="admin_category_show", requirements={"category_id" = "\d+"})
	 * @ParamConverter("category", options={"mapping": {"category_id": "id"}})
	 * @Template("AdminBundle:Category:show.html.twig")
	 */
	public function showAction(Category $category, Request $request)
	{	
	  	return array(
	    	'category' => $category,
	  	);
	}

	/**
     * @Route("/category/add", name="admin_category_add")
     * @Template("AdminBundle:Category:add.html.twig")
     * @I18nDoctrine
	 */
    public function addAction(Request $request)
    {
    	$langues = $this->container->get('FAPROD.Configs')->getBoLangues();
		
    	$category = new Category();
    	
		if ($request->query->get('id_ref')){
			$parent = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category')->find($request->query->get('id_ref'));
			$category->setParent($parent);
			$category->setLevel($parent->getLevel() + 1);
		}
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$queryBuilder = $repository_category->createQueryBuilder('c');
    	$queryBuilder->select('MAX(c.no_order)');
		$queryBuilder->where('c.level = :level')->setParameter('level', $category->getLevel());
		if ($category->getParent())$queryBuilder->andWhere('c.parent = :parent')->setParameter('parent', $category->getParent());
    	$category->setNoOrder($queryBuilder->getQuery()->getSingleScalarResult()+1);
	    
	    $form = $this->createForm(new CategoryType, $category, array('langues' => $langues));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($category);
	        $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        if ($category->getParent()){
	       		return $this->redirect($this->generateUrl('admin_category_show', array('category_id' => $category->getParent()->getId())));
	        } else {
	       		return $this->redirect($this->generateUrl('admin_category_show', array('category_id' => $category->getId())));
	        }
	    }
	    
	    return array(
	        'form'       => $form->createView(),
	        'langues'    => $langues,
	    );
    }
    
    /**
     * @Route("/category/edit/{category_id}", name="admin_category_edit", requirements={"category_id" = "\d+"})
	 * @ParamConverter("category", options={"mapping": {"category_id": "id"}})
	 * @Template("AdminBundle:Category:edit.html.twig")
	 * @I18nDoctrine
	 */
    public function editAction(Category $category, Request $request)
    {
    	$langues = $this->container->get('FAPROD.Configs')->getBoLangues();
    	
	    $form = $this->createForm(new CategoryType(), $category, array('langues' => $langues));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
	       $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       if ($category->getParent()){
	       		return $this->redirect($this->generateUrl('admin_category_show', array('category_id' => $category->getParent()->getId())));
	       } else {
	       		return $this->redirect($this->generateUrl('admin_category_show', array('category_id' => $category->getId())));
	       }
	    }
	
	    return array(
	       'form'       => $form->createView(),
	       'langues'    => $langues,
	    );
    }
     
  
    /**
     * @Route("/category/delete/{category_id}", name="admin_category_delete", requirements={"category_id" = "\d+"})
	 * @ParamConverter("category", options={"mapping": {"category_id": "id"}})
	 */
	public function deleteAction(Category $category, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($category);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_category_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = CategoryController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('UserBundle:Category')
	      ->getCategorys($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }

	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
	    );
    }
}
