<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use A2lix\I18nDoctrineBundle\Annotation\I18nDoctrine;

use FAPROD\ContentBundle\Form\ZoneType;
use FAPROD\ContentBundle\Entity\Zone;

class ZoneController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/zone/index/{page}/{sort}/{sens}", name="admin_zone_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "title", "sens" = "1"})
     * @Template("AdminBundle:Zone:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
    	$repository_zone = $this->getDoctrine()->getManager()->getRepository('ContentBundle:Zone');
    	$queryBuilder = $repository_zone->createQueryBuilder('z');
    	
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'admin_zone_index');
    }
    
    /**
     * @Route("/zone/show/{zone_id}", name="admin_zone_show", requirements={"zone_id" = "\d+"})
	 * @ParamConverter("zone", options={"mapping": {"zone_id": "id"}})
	 * @Template("AdminBundle:Zone:show.html.twig")
	 */
	public function showAction(Zone $zone)
	{
	  	return array(
	    	'zone' => $zone,
	  	);
	}
	
	/**
     * @Route("/zone/add", name="admin_zone_add")
     * @Template("AdminBundle:Zone:add.html.twig")
     * @I18nDoctrine
	 */
    public function addAction(Request $request)
    {
    	$langues = $this->container->get('FAPROD.Configs')->getBoLangues();

	    $zone = new Zone();
	    $form = $this->createForm(new ZoneType, $zone, array('langues' => $langues));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($zone);
	        $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        return $this->redirect($this->generateUrl('admin_zone_show', array('zone_id' => $zone->getId())));
	    }
	    
	    $_SESSION['is_adminsf2_log'] = true;
	    
	    return array(
	        'form'      => $form->createView(),
	        'langues'   => $langues,
	    );
    }

    /**
     * @Route("/zone/edit/{zone_id}", name="admin_zone_edit", requirements={"zone_id" = "\d+"})
	 * @ParamConverter("zone", options={"mapping": {"zone_id": "id"}})
	 * @Template("AdminBundle:Zone:edit.html.twig")
	 * @I18nDoctrine
	 */
    public function editAction(Zone $zone, Request $request)
    {
    	$langues = $this->container->get('FAPROD.Configs')->getBoLangues();
    	
	    $form = $this->createForm(new ZoneType(), $zone, array('langues' => $langues));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
	       $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_zone_show', array('zone_id' => $zone->getId())));
	    }
	
	    $_SESSION['is_adminsf2_log'] = true;
	    
	    return array(
	       'form'      => $form->createView(),
	       'langues'   => $langues,
	    );
    }
     
  
    /**
     * @Route("/zone/delete/{zone_id}", name="admin_zone_delete", requirements={"zone_id" = "\d+"})
	 * @ParamConverter("zone", options={"mapping": {"zone_id": "id"}})
	 */
	public function deleteAction(Zone $zone, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($zone);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_zone_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = ZoneController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('ContentBundle:Zone')
	      ->getZones($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
	    );
    }
}
