<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\BookingBundle\Form\ReducType;
use FAPROD\BookingBundle\Entity\Reduc;

class ReducController extends Controller
{
	const ITEMS_PAR_PAGE = 40;
	
	/**
     * @Route("/reduc/index/{page}/{sort}/{sens}", name="admin_reduc_index", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("AdminBundle:Reduc:index.html.twig")
     */
    public function indexAction($page, $sort, $sens)
    {
	    return $this->initPagination($page, $sort, $sens, '', 'admin_reduc_index');
    }
    
    /**
     * @Route("/reduc/show/{reduc_id}", name="admin_reduc_show", requirements={"reduc_id" = "\d+"})
	 * @ParamConverter("reduc", options={"mapping": {"reduc_id": "id"}})
	 * @Template("AdminBundle:Reduc:show.html.twig")
	 */
	public function showAction(Reduc $reduc)
	{	
	  	return array(
	    	'reduc' => $reduc,
	  	);
	}
	
	/**
     * @Route("/reduc/add", name="admin_reduc_add")
     * @Template("AdminBundle:Reduc:add.html.twig")
	 */
    public function addAction(Request $request)
    {
	    $reduc = new Reduc();
	    $form = $this->createForm(new ReducType(), $reduc);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($reduc);
	        $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	    
	        return $this->redirect($this->generateUrl('admin_reduc_show', array('reduc_id' => $reduc->getId())));
	    }
	    
	    return array(
	        'form'   => $form->createView(),
	    );
    }

    /**
     * @Route("/reduc/edit/{reduc_id}", name="admin_reduc_edit", requirements={"reduc_id" = "\d+"})
	 * @ParamConverter("reduc", options={"mapping": {"reduc_id": "id"}})
	 * @Template("AdminBundle:Reduc:edit.html.twig")
	 */
    public function editAction(Reduc $reduc, Request $request)
    {
	    $form = $this->createForm(new ReducType(), $reduc);
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
	       $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', 'Enregistrement effectué.');
	
	       return $this->redirect($this->generateUrl('admin_reduc_show', array('reduc_id' => $reduc->getId())));
	    }
	
	    return array(
	       'form'   => $form->createView(),
	    );
    }
     
  
    /**
     * @Route("/reduc/delete/{reduc_id}", name="admin_reduc_delete", requirements={"reduc_id" = "\d+"})
	 * @ParamConverter("reduc", options={"mapping": {"reduc_id": "id"}})
	 */
	public function deleteAction(Reduc $reduc, Request $request)
	{	
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($reduc);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $this->redirect($this->generateUrl('admin_reduc_index'));
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = ReducController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('BookingBundle:Reduc')
	      ->getReducs($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
	    );
    }
}
