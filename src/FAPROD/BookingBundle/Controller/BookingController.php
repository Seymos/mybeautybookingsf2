<?php

namespace FAPROD\BookingBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Validator\Constraints\Null;

use MangoPay\MangoPayApi;
use MangoPay\CardPreAuthorization;
use MangoPay\Money;
use MangoPay\PayIn;
use MangoPay\PayInPaymentDetailsPreAuthorized;

use FAPROD\CoreBundle\Lib\MyConst;
use FAPROD\ContentBundle\Form\ContentCommentType;
use FAPROD\UserBundle\Form\UserType;
use FAPROD\UserBundle\Entity\User;
use FAPROD\BookingBundle\Entity\Booking;
use FAPROD\BookingBundle\Form\BookingType;
use FAPROD\UserBundle\Form\AgendaType;
use FAPROD\UserBundle\Entity\Agenda;
use FAPROD\UserBundle\Entity\Stat as StatUser;
use FAPROD\BookingBundle\Form\PaiementFiltre;
use FAPROD\BookingBundle\Entity\ReducLog;
use FAPROD\BookingBundle\Entity\Reduc;


class BookingController extends Controller {

	const ITEMS_PAR_PAGE = 10;
	
	function addLog($txt) {
		$fichier = __DIR__.'/../../../../mangolog.txt';
		if (!file_exists($fichier)) file_put_contents($fichier, "");
		file_put_contents($fichier,date("[j/m/y H:i:s]")." - $txt \r\n".file_get_contents($fichier));
	 }
	 
	/**
     * @Route("/booking/newbooking", name="booking_newbooking")
	 */
    public function newbookingAction(Request $request)
    {
	    
	    if ($request->getMethod() == 'POST')
        {
			$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
			$queryBuilder = $repository_booking->createQueryBuilder('b')->select('COUNT(b)');
			$queryBuilder->leftJoin('b.user', 'u');
			$queryBuilder->where('u = :user')->setParameter(':user', $this->getUser());
			$nb_booking = $queryBuilder->getQuery()->getSingleScalarResult();
		
			$booking = new Booking();
			
	    	$em = $this->getDoctrine()->getManager();
			
			$reduction = 0;
			
			if ($request->get("date_select")){
				$date = explode("_", $request->get("date_select"));
				$booking->setDateRdv(\DateTime::createFromFormat("Y-m-d", date($date[2]."-".$date[1]."-".$date[0])));
				
				$heure_select_string = explode('h', $date[3]);
				$heure_select = sprintf('%d', floor($heure_select_string[0]));
				$minute_select = $heure_select_string[1] ? $heure_select_string[1] : 0;
			
				$booking->setHeureRdv($heure_select);
				$booking->setMinuteRdv($minute_select);
			}
			
			if ($request->get("offer")){
				$booking->setOffer($this->getDoctrine()->getManager()->getRepository('UserBundle:Offer')->find($request->get("offer")));
			}
			
			if ($request->request->get('code_reduction')){
				$code = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Reduc')->findOneBy(array('code' => $request->request->get('code_reduction')));
				
				if ($code and $code->getActif()){
					
					$log = $this->getDoctrine()->getManager()->getRepository('BookingBundle:ReducLog')->findOneBy(array('code' => $code->getCode(), 'user_id' => $this->getUser()->getId()));
						
					if (!$log){

						$code->setNbUsed($code->getNbUsed() + 1);
						
						$booking->setCode($code->getCode());
						
						$reduction = round(($booking->getOffer()->getPrice() * $code->getValue()) / 100,2);
						
						$booking->setCodeReduc($reduction);
						
						$reducLog = new ReducLog();
						$reducLog->setUserId($this->getUser()->getId());
						$reducLog->setReducId($code->getId());
						$reducLog->setCode($code->getCode());
						
						$em->persist($reducLog);
					}
				}
			}
			

			$booking->setUser($this->getUser());
			$booking->setSeller($booking->getOffer()->getUser());
			
			$booking->setStatut(1);
			$booking->setPrice($booking->getOffer()->getPrice());
			$taux_tva = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Configs')->getTva();
			$taux_com = $this->getDoctrine()->getManager()->getRepository('AdminBundle:Configs')->getCommission();

			$commission = ceil (($booking->getPrice() * $taux_com) / 100);
			$tva = ceil ((($commission + $booking->getPrice()) * $taux_tva) / 100);
			
			$price_ttc = $booking->getPrice() + $commission + $tva - $reduction;
			$booking->setPriceTtc($price_ttc);
			$booking->setCommission($commission);
			$booking->setTva($tva);		
			
			$em->persist($booking);
			$em->flush();
			
			if (!$nb_booking) {
				$date = new \DateTime('now');
				$date->add(new \DateInterval('P20D'));
				
				$codeuser = $this->getUser()->getLastName();
				$codeuser = str_replace("","", $codeuser);
				$codeuser = strtoupper($codeuser).$this->getUser()->getId();
				
				$code_reduc = new Reduc();
				$code_reduc->setCode($codeuser);
				$code_reduc->setValue(10);
				$code_reduc->setValide(true);
				$code_reduc->setBeginDate(new \DateTime('now'));
				$code_reduc->setEndDate($date);
				
				$em->persist($code_reduc);
				$em->flush();
				
				$mailBody = $this->renderView('BookingBundle:Mail:nouveau_code.html.twig', array('user' => $this->getUser(), 'code_reduc' => $code_reduc));
				$message = \Swift_Message::newInstance()
					->setSubject($this->get('translator')->trans('Un code de réduction pour votre prochain booking !'))
					->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
					->setTo($this->getUser()->getEmail())
					->setBody($mailBody, 'text/html');
				//echo $mailBody;exit;
				$this->get('mailer')->send($message);
			}       

			$seller = $booking->getSeller();
			$now = \DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d 00:00:00"));
		
			$repository_stat = $this->getDoctrine()->getManager()->getRepository('UserBundle:Stat');
			$queryBuilder = $repository_stat->createQueryBuilder('s');
			$queryBuilder->where('s.user = :user')
						 ->andWhere('s.date = :date')
						 ->setParameter('user', $seller)
						 ->setParameter('date', $now);
			$stat = $queryBuilder->setMaxResults('1')->getQuery()->getOneOrNullResult();
			
			$em = $this->getDoctrine()->getManager();
			
			if ($stat){
				$stat->setContact($stat->getContact() + 1);
			} else {
				$stat = new StatUser();
				$stat->setUser($seller);
				$stat->setVue(0);
				$stat->setContact(1);
				$stat->setDate($now);
				
				$em->persist($stat);
			}
			
			$em->persist($seller);
			$seller->setContacts($seller->getContacts() + 1);
			$em->flush();

	        /*
	        $mailBody = $this->renderView('MessageBundle:Mail:alerte_message.html.twig', array('user' => $discussion->getDestinataire()));
	        $message = \Swift_Message::newInstance()
		        ->setSubject($this->get('translator')->trans('Nouveau message'))
		        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		        ->setTo($discussion->getDestinataire()->getEmail())
		        ->setBody($mailBody, 'text/html');
		    //echo $mailBody;exit;
		    $this->get('mailer')->send($message);
	        */
			
	        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Réservation en attente du paiement.'));
	        
	        return $this->redirect($this->generateUrl('booking_show', array('booking_id' => $booking->getId())));
    	}
    	
        return $this->redirect($this->generateUrl('booking_index'));
    }
	
	/**
     * @Route("/booking/liste/{page}/{sort}/{sens}", name="booking_index", requirements={"page" = "\d+"}, defaults={"page" = "1"})
     * @Template("BookingBundle:Booking:index.html.twig")
	 */
    public function indexAction(Request $request, $page, $sort = "updated_at", $sens = 0)
    {
		$user = $this->getUser();
    	
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
	    $queryBuilder = $repository_booking->createQueryBuilder('b');
		if ($user->getType() == 1){
			$queryBuilder->where('u = :user')->setParameter(':user', $user);
		} else {
			$queryBuilder->where('s = :seller')->setParameter(':seller', $user);
		}		
	    
        return $this->initPagination($page, "updated_at", 0, $queryBuilder, 'booking_index');
    }
	
	/**
     * @Route("/booking/popup", name="booking_popup")
	 * @Template("BookingBundle:Booking:show_booking.html.twig")
	 */
    public function showbookingAction(Request $request)
    {
		$user = $this->getUser();
    	
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
	    $queryBuilder = $repository_booking->createQueryBuilder('b');
		if ($user->getType() == 1){
			$queryBuilder->where('u = :user')->setParameter(':user', $user);
		} else {
			$queryBuilder->where('s = :seller')->setParameter(':seller', $user);
		}
		
		$liste = $this->getDoctrine()
						  ->getManager()
						  ->getRepository('BookingBundle:Booking')
						  ->getBookings(1, 15, "updated_at", 0, $queryBuilder)
						;
						
		return array(
					"liste" => $liste,
					);
	}
	
	/**
     * @Route("/booking/paiements/{page}/{sort}/{sens}", name="booking_paiements", requirements={"page" = "\d+"}, defaults={"page" = "1"})
     * @Template("BookingBundle:Booking:paiements.html.twig")
	 */
    public function paiementsAction(Request $request, $page, $sort = "updated_at", $sens = 0)
    {
		$user = $this->getUser();
    	
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
	    $queryBuilder = $repository_booking->createQueryBuilder('b');
		if ($user->getType() == 1){
			$queryBuilder->where('u = :user')->setParameter(':user', $user);
		} else {
			$queryBuilder->where('s = :seller')->setParameter(':seller', $user);
		}
		$queryBuilder->andWhere('b.statut >= 4');	

		$form = $this->createForm(new PaiementFiltre());
		$form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
			
			$begin_date = $form->get('begin_date')->getData();
			$end_date = $form->get('end_date')->getData();	
			
			if ($begin_date){
				$queryBuilder->andWhere('b.date_rdv >= :begin_date')->setParameter('begin_date', $begin_date);
			}
			if ($end_date){
				$queryBuilder->andWhere('b.date_rdv <= :end_date')->setParameter('end_date', $end_date);
			}

		}			
	    
        return $this->initPagination($page, "updated_at", 0, $queryBuilder, 'booking_paiements', $form);
    }
	
	/**
     * @Route("/booking/paiements/popup", name="booking_paiementspopup")
	 * @Template("BookingBundle:Booking:show_paiement.html.twig")
	 */
    public function showpaiementsAction(Request $request)
    {
		$user = $this->getUser();
    	
    	$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
	    $queryBuilder = $repository_booking->createQueryBuilder('b');
		if ($user->getType() == 1){
			$queryBuilder->where('u = :user')->setParameter(':user', $user);
		} else {
			$queryBuilder->where('s = :seller')->setParameter(':seller', $user);
		}
		$queryBuilder->andWhere('b.statut >= 4');
		
		$liste = $this->getDoctrine()
						  ->getManager()
						  ->getRepository('BookingBundle:Booking')
						  ->getBookings(1, 15, "updated_at", 0, $queryBuilder)
						;
						
		return array(
					"liste" => $liste,
					);
	}
	
	/**
     * @Route("/booking/voir/{booking_id}", name="booking_show", requirements={"booking_id" = "\d+"})
	 * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
	 * @Template("BookingBundle:Booking:show.html.twig")
	 */
	public function showAction(Booking $booking, Request $request)
	{
		if ($booking->getUser()->getId() != $this->getUser()->getId() and $booking->getSeller()->getId() != $this->getUser()->getId()){
			return $this->redirect($this->generateUrl('booking_index'));
		}
		
		return array(
					'booking' => $booking,
					);
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action, $form = ''){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = BookingController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('BookingBundle:Booking')
	      ->getBookings($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    
	    $nb_result = $this->getDoctrine()
							->getManager()
						      ->getRepository('BookingBundle:Booking')->getBookingsCount($queryBuilder);
		
		if (!$form)$form = $this->createForm(new PaiementFiltre());
		
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
		  'form'        => $form->createView(),
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
		  'nb_result'   => $nb_result,
	    );
    }
	
	
	/**
     * @Route("/booking/note/{booking_id}", name="booking_note", requirements={"booking_id" = "\d+"})
     * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
	 * @Template("BookingBundle:Booking:note.html.twig")
     */
    public function noteAction(Booking $booking, Request $request)
    {
		$form_comment = $this->createForm(new ContentCommentType());
		
		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED') and $request->getMethod() == 'POST' and $this->getUser()->getId())
        {
			$vendeur = $booking->getOffer()->getUser();
			
            $repository_comment = $this->getDoctrine()->getManager()->getRepository('ContentBundle:ContentComment');
	    	$queryBuilder = $repository_comment->createQueryBuilder('c')
								    				  ->join('c.user', 'u')->addSelect('u')
									    			  ->join('c.booking', 'b')->addSelect('b');
								    				  
	    	$queryBuilder->where('u.id = :user_id')->setParameter('user_id', $this->getUser()->getId());
	    	$queryBuilder->andWhere('b.id = :booking_id')->setParameter('booking_id', $booking->getId());
	    	$comment = $queryBuilder->getQuery()->getOneOrNullResult();
    	
	    	if ($comment){
	    		$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Vous avez déjà noté cette réservation.'));
	    	} else {
	            $form_comment->handleRequest($request);
	            $comment = $form_comment->getData();
	            $comment->setUser($this->getUser());
				$comment->setBooking($booking);
	            $comment->setVendeur($vendeur);
	            $comment->setIpAddress($_SERVER['REMOTE_ADDR']);
				$comment->setValidate(1);
				
				$em = $this->getDoctrine()->getManager();
		        $em->persist($comment);
				
				$booking->setStatut(5);

				$em->persist($booking);
			
		        $em->flush();
				
				$vendeur->updateNotes();
				$em->persist($vendeur);
				$em->flush();
		    
		        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Commentaire enregistré.'));
        	}
			
			return $this->redirect($this->generateUrl('booking_show', array('booking_id' => $booking->getId())));
	    }
		
		return array(
			'booking'		  => $booking,
			'form_comment'    => $form_comment->createView(),
		);
	}

	
	/**
     * @Route("/booking/payer/{booking_id}", name="booking_payer", requirements={"booking_id" = "\d+"})
     * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
	 * @Template("BookingBundle:Booking:pay.html.twig")
     */
    public function payerAction(Booking $booking, Request $request)
    {
		$currency = $this->get('FAPROD.MyConst')->getCurrency();
		
		//demande card registration MangoPay
		require_once __DIR__ . '/../mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];

		if ($this->getUser()->getMangoId()){
			$user_mango_id = $this->getUser()->getMangoId();
			
			$CardRegistration = new \MangoPay\CardRegistration();
			$CardRegistration->UserId = $user_mango_id;
			$CardRegistration->Currency = $currency;
			$result = $mangoPayApi->CardRegistrations->Create($CardRegistration);
		}
		
		$request->getSession()->set('ID', $result->Id);
		$request->getSession()->set('booking', $booking->getId());
		$request->getSession()->set('data', $result->PreregistrationData);
		
		$returnURL = $this->generateUrl('booking_suitepayer', array(), true);
		
		return array(
			'result'		=> $result,			
			'currency'		=> $currency,
			'price_ttc'		=> $booking->getPriceTtc(),
			'returnURL'		=> $returnURL,
		);
	}
	
	 /**
     * @Route("/booking/suitepayer", name="booking_suitepayer")
     */
	public function suitepayerAction(Request $request)
    {
		
		require_once __DIR__ . '/../mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];
		
		try {
			// update register card with registration data from Payline service
			
			$booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:booking')->find($request->getSession()->get("booking"));
			
			$cardRegister = $mangoPayApi->CardRegistrations->Get($request->getSession()->get("ID"));
			$data = isset($_GET['data']) ? $_GET['data'] : '';
			$cardRegister->RegistrationData = "data=" . $data;

			if (!isset($cardRegister->CardId)){
			
				$updatedCardRegister = $mangoPayApi->CardRegistrations->Update($cardRegister);

				if ($updatedCardRegister->Status != 'VALIDATED' || !isset($updatedCardRegister->CardId)){
					$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur dans la réservation'));
					return $this->redirect($this->generateUrl('booking_NOK'));	
				}

				// get created virtual card object
				$card = $mangoPayApi->Cards->Get($updatedCardRegister->CardId);
				$cardId = $updatedCardRegister->CardId;
			
			} else {
				$cardId = $cardRegister->CardId;
			}
			
			$user_mango_id = $this->getUser()->getMangoId();

			// preauthorisation card
			$CardPreAuthorization = new CardPreAuthorization();
			$CardPreAuthorization->AuthorId = $user_mango_id;
			$CardPreAuthorization->DebitedFunds = new Money();
			$CardPreAuthorization->DebitedFunds->Currency = "EUR";
			$CardPreAuthorization->DebitedFunds->Amount = $booking->getPriceTtc()*100;
			//$CardPreAuthorization->SecureMode = "FORCE";
			$CardPreAuthorization->SecureMode = "DEFAULT";
			$CardPreAuthorization->CardId = $cardId;
			$returnURL = $this->generateUrl('check_3ds', array(), true);

			$CardPreAuthorization->SecureModeReturnURL = $returnURL;

			$result = $mangoPayApi->CardPreAuthorizations->Create($CardPreAuthorization);
			
			if ($result->Status == 'SUCCEEDED') {
				$booking->setCardId($result->CardId);
				$booking->setPreauthorizationId($result->Id);
				$booking->setStatut(2);

				$em = $this->getDoctrine()->getManager();
				$em->persist($booking);
				$em->flush();

				return $this->redirect($this->generateUrl('booking_show', array('booking_id' => $booking->getId())));
			}
		
			if ($result->Status != 'CREATED') {
				$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur dans la réservation'));
				return $this->redirect($this->generateUrl('booking_NOK'));	
			}
			
		} catch (\MangoPay\Libraries\ResponseException $e) {
				//echo $e->getMessage();exit; 
				
				$error = $e->GetErrorDetails()->Errors;
				$this->addLog("Booking ".$booking->getId()." : ".implode(' - ', (array) $error));
			
				$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur dans la réservation'));
				return $this->redirect($this->generateUrl('booking_NOK'));
		}
		
		return $this->redirect($result->SecureModeRedirectURL);
		
	}
	
	/**
     * @Route("/booking/check_3ds", name="check_3ds")
     */
	public function check_3ds(Request $request)
    {
		$user = $this->getUser();
		
		require_once __DIR__ . '/../mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
		
		$booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:booking')->find($request->getSession()->get("booking"));
			
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];
		
		$CardPreAuthorizationId = $request->query->get('preAuthorizationId');
		$result = $mangoPayApi->CardPreAuthorizations->Get($CardPreAuthorizationId);
		
		if ($result->Status == 'SUCCEEDED') {

			$booking->setCardId($result->CardId);
			$booking->setPreauthorizationId($result->Id);
			$booking->setStatut(2);

			$em = $this->getDoctrine()->getManager();
			$em->persist($booking);
			$em->flush();

			return $this->redirect($this->generateUrl('booking_show', array('booking_id' => $booking->getId())));
			
		}else{
			
			//anulation preauthorisation
			
			$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur dans la réservation'));
			
			return $this->redirect($this->generateUrl('booking_NOK'));	
			
		}
		
	}
	
	
	/**
     * @Route("/booking/accept/{booking_id}", name="booking_accept", requirements={"booking_id" = "\d+"})
     * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
     */
    public function bookingacceptAction(Booking $booking, Request $request)
    {
		
		$em = $this->getDoctrine()->getManager();
		
		$user = $this->getUser();	
		
		require_once __DIR__ . '/../mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
		
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];
		
		$user_mango_id = $booking->getUser()->getMangoId();
		
		$Pagination = new \MangoPay\Pagination();
		$wallet = $mangoPayApi->Users->GetWallets($user_mango_id, $Pagination);
		if ($wallet){
			$wallet = end($wallet);
		}
		
		//var_dump($result);
		//	exit;
					
		$CardPreAuthorizationId = $booking->getPreauthorizationId();
		$CardId = $booking->getCardId();
		 
		//Send the request
		$result = $mangoPayApi->CardPreAuthorizations->Get($CardPreAuthorizationId);
		
		try{
			//echo $result->DebitedFunds->Amount;exit;
			//Build the parameters for the request
			$PayIn = new PayIn();
			$PayIn->AuthorId = $user_mango_id;
			$PayIn->CardId = $CardId;
			$PayIn->PaymentType = "PREAUTHORIZED";
			$PayIn->PaymentDetails = new PayInPaymentDetailsPreAuthorized();
			$PayIn->PaymentDetails->PreauthorizationId = $CardPreAuthorizationId;
			$PayIn->DebitedFunds = new Money();
			$PayIn->DebitedFunds->Currency = $result->DebitedFunds->Currency;
			$PayIn->DebitedFunds->Amount = $result->DebitedFunds->Amount;
			$PayIn->Fees = new Money();
			$PayIn->Fees->Currency = $result->DebitedFunds->Currency;
			$PayIn->Fees->Amount = ($booking->getCommission()+$booking->getTva())*100;
			$PayIn->CreditedWalletId = $wallet->Id;
			$PayIn->ExecutionType = "DIRECT";
			$PayIn->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
			$PayIn->Tag = "Paiement Booking N°".$booking->getId();
			
			$returnURL = $this->generateUrl('booking_show', array('booking_id' => $booking->getId()), true);
			
			$PayIn->SecureModeReturnURL = $returnURL;

			$result = $mangoPayApi->PayIns->Create($PayIn);
			//var_dump($result);
			//exit;
		} catch (\MangoPay\Libraries\ResponseException $e) {
			$error = $e->GetErrorDetails()->Errors;
			$this->addLog("Booking ".$booking->getId()." : ".implode(' - ', (array) $error));
			
			$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur dans la réservation'));
			return $this->redirect($this->generateUrl('booking_NOK'));
		}
		
		if ($result->Status == 'SUCCEEDED') {
		
			$booking->setStatut(3);
			$em->persist($booking);
			$em->flush();		

			$repository_agenda = $this->getDoctrine()->getManager()->getRepository('UserBundle:Agenda');
			$offer_duree = $booking->getOffer()->getDuree();
			$now = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, $booking->getDateRdv('m'), $booking->getDateRdv('d'), $booking->getDateRdv('Y'))));
			$now_agenda = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d H:i:00', mktime($booking->getHeureRdv(), $booking->getMinuteRdv(), 0, $booking->getDateRdv('m'), $booking->getDateRdv('d'), $booking->getDateRdv('Y'))));
				
			for ($i = 1; $i <= $offer_duree; $i++){

				$queryBuilder = $repository_agenda->createQueryBuilder('a');
				$queryBuilder->where('a.user = :user')
							 ->andWhere('a.date = :date')
							 ->andWhere('a.heure = :heure')
							 ->andWhere('a.minute = :minute')
							 ->setParameter('user', $user)
							 ->setParameter('heure', $now_agenda->format('H'))
							 ->setParameter('minute', $now_agenda->format('i'))
							 ->setParameter('date', $now);
				$agenda = $queryBuilder->getQuery()->getOneOrNullResult();
				
				if (!$agenda){
					$agenda = new Agenda();
					$agenda->setBooking($booking);
					$agenda->setUser($user);
					$agenda->setHeure($now_agenda->format('H'));
					$agenda->setMinute($now_agenda->format('i'));
					$agenda->setDate($now);
					
					$em->persist($agenda);
				}

				$now_agenda->add(new \DateInterval('PT30M'));
			
			}

			$em->flush();

			$email_destinataire = $booking->getUser()->getEmail();
		
			$mailBody = $this->renderView('BookingBundle:Mail:booking_ok.html.twig', array(
																					'booking' => $booking,
																					));
																					
			$message = \Swift_Message::newInstance()
				->setSubject($this->get('translator')->trans('Nouveau message'))
				->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
				->setTo($email_destinataire)
				->setBody($mailBody, 'text/html');
			//echo $mailBody;exit;
			$this->get('mailer')->send($message);

			$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Reservation acceptée.'));

			return $this->redirect($this->generateUrl('booking_show', array('booking_id' => $booking->getId())));

		}else{
			
			$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans("OUPS! Une erreur s'est produite, veuillez réesayer"));

			return $this->redirect($this->generateUrl('booking_show', array('booking_id' => $booking->getId())));
		}
		
    }
	
	/**
     * @Route("/booking/refuse/{booking_id}", name="booking_refuse", requirements={"booking_id" = "\d+"})
     * @ParamConverter("booking", options={"mapping": {"booking_id": "id"}})
     */
    public function bookingrefuseAction(Booking $booking, Request $request)
    {
		
		require_once __DIR__ . '/../mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
		
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];
		 
		try{
			//Build the parameters for the request
			$CardPreAuthorizationId = $booking->getPreauthorizationId();
			$CardPreAuthorization = $mangoPayApi->CardPreAuthorizations->Get($CardPreAuthorizationId);
			$CardPreAuthorization->PaymentStatus = "CANCELED";
			$CardPreAuthorization->tag = "Reservation annulée par le pro";

			//Send the request
			$result = $mangoPayApi->CardPreAuthorizations->Update($CardPreAuthorization);
			//echo $mailBody;exit;
		
		} catch (\MangoPay\Libraries\ResponseException $e) {
			//echo $e->getMessage();exit;
			
			$error = $e->GetErrorDetails()->Errors;
			$this->addLog("Booking ".$booking->getId()." : ".implode(' - ', (array) $error));
			
			$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur dans la réservation'));
			return $this->redirect($this->generateUrl('booking_NOK'));
		}
		
		if ($result->Status == 'SUCCEEDED') {
		
			$em = $this->getDoctrine()->getManager();
			$booking->setStatut(-1);
			$em->persist($booking);
			$em->flush();
			
			$stmt = $em->getConnection()
					->prepare("delete from agenda where `booking_id` = :booking_id");
			$stmt->bindValue('booking_id',$booking->getId());
			$stmt->execute();
		
			
			$mailBody = $this->renderView('BookingBundle:Mail:booking_ko.html.twig', array(
																					'booking' => $booking,
																					));
			$message = \Swift_Message::newInstance()
				->setSubject('Votre réservation a été annulée (n°' . $booking->getId() . ")")
				->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
				->setTo($booking->getUser()->getEmail())
				->setBody($mailBody, 'text/html');
			$this->get('mailer')->send($message);

			$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Reservation annulée.'));
		
		}else{
			
			$request->getSession()->getFlashBag()->add('error', "Oups! Problème dans l'annulation. Si le problème persiste; contactez notre équipe");
			
		}
		
		return $this->redirect($this->generateUrl('booking_show', array('booking_id' => $booking->getId())));
    }
	
	/**
     * @Route("/booking/NOK", name="booking_NOK")
     * @Template("BookingBundle:Booking:booking_nok.html.twig")
     */
	public function booking_NOK(Request $request)
    {
		
	}
}
