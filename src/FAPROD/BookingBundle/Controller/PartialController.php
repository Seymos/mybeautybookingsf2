<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\BookingBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\UserBundle\Entity\User;

class PartialController extends Controller
{
	
	/**
     * @Template("BookingBundle:Booking:nbpro.html.twig")
	 */
	public function bookingproAction()
	{             
		$user = $this->getUser();
		
		$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
	    $queryBuilder = $repository_booking->createQueryBuilder('b')->select('COUNT(b)');
		$queryBuilder->leftJoin('b.seller', 's'); 				
		$queryBuilder->where('s = :seller')->setParameter(':seller', $user);
		$queryBuilder->andWhere('b.statut = 2');
		$result = $queryBuilder->getQuery()->getSingleScalarResult();
    
	  	return array(
	  	    'result' => $result,
        );
	}
	
	/**
     * @Template("BookingBundle:Booking:nbcliente.html.twig")
	 */
	public function bookingclienteAction()
	{              
		$user = $this->getUser();
		
		$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
	    $queryBuilder = $repository_booking->createQueryBuilder('b')->select('COUNT(b)');
		$queryBuilder->leftJoin('b.user', 'u');  
		$queryBuilder->where('u = :user')->setParameter(':user', $user);
		$queryBuilder->andWhere('b.statut = 3');
		$result = $queryBuilder->getQuery()->getSingleScalarResult();
    
	  	return array(
	  	    'result' => $result,
        );
	}

}
