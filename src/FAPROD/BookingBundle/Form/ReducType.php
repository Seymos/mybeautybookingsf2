<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\BookingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReducType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', 'text', array('required' => true))
            ->add('value', 'text', array('required' => true))
            ->add('nb_max')
            ->add('order_min')
            ->add('valide')
            ->add('begin_date', 'date', array('input'  => 'datetime','widget' => 'choice', 'empty_value' => '', 'required' => true))
            ->add('end_date', 'date', array('input'  => 'datetime','widget' => 'choice', 'empty_value' => '', 'required' => true))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FAPROD\BookingBundle\Entity\Reduc'
        ));
    }
    
    public function getDefaultOptions(array $options)
	{
	    return array(
	    	'data_class' => 'FAPROD\BookingBundle\Entity\Reduc',
	    			);
	}

    /**
     * @return string
     */
    public function getName()
    {
        return 'faprod_bookingbundle_reduc';
    }
}
