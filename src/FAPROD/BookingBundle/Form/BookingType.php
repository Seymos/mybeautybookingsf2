<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\BookingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', 'entity_hidden', array(
                'class' => 'FAPROD\UserBundle\Entity\User'))
			->add('day', 'date', array(
					'input'  => 'datetime',
					'widget' => 'choice', 
					'years'=> range(date("Y"),date("Y")+1), 
					'empty_value' => '', 
					'required' => true
				)
			)
            ->add('message', 'textarea', array(
                    'required'  => true,
                )
			)
            ->add('statut');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FAPROD\BookingBundle\Entity\Booking',
        ));
    }
	
	public function getDefaultOptions(array $options)
    {
        return array(
			'data_class'      => 'FAPROD\BookingBundle\Entity\Booking',
        );
    }
    
    public function getName()
    {
        return 'faprod_bookingbundle_booking';
    }
}
