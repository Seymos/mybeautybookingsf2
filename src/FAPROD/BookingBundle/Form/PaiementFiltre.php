<?php

/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\BookingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PaiementFiltre extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('begin_date', 'date', array('input'  => 'datetime','widget' => 'choice', 'empty_value' => '', 'required' => false))
			->add('end_date', 'date', array('input'  => 'datetime','widget' => 'choice', 'empty_value' => '', 'required' => false))
        ;
    }
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
						'begin_date'     => new \DateTime('now'),
						'end_date'       => new \DateTime('now'),
        			));
    }
    
    public function getDefaultOptions(array $options)
	{
	    return array(
					'begin_date'     => new \DateTime('now'),
						'end_date'       => new \DateTime('now'),
	    			);
	}

    public function getName()
    {
        return 'paiement_filtre';
    }
}
