<?php

namespace FAPROD\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use FAPROD\CoreBundle\Lib\MyConst;


/**
 * @ORM\Entity(repositoryClass="FAPROD\BookingBundle\Entity\BookingRepository")
 * @ORM\HasLifecycleCallbacks() 
 * @ORM\Table(name="booking")
 */
class Booking
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User", inversedBy="bookings1")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
	
	/**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User", inversedBy="bookings2")
     * @ORM\JoinColumn(name="seller_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $seller;
	
	/**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\Offer")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $offer;
        
	/**
     * @ORM\Column(name="statut", type="integer", nullable=true)
     */
    private $statut;
	
	/**
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;
	
	/**
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    private $code;
    
    /**
     * @ORM\Column(name="code_reduc", type="float", nullable=true)
     */
    private $code_reduc;
	
	/**
     * @ORM\Column(name="commission", type="float", nullable=true)
     */
    private $commission;
	
	/**
     * @ORM\Column(name="tva", type="float", nullable=true)
     */
    private $tva;
	
	/**
     * @ORM\Column(name="price_ttc", type="float", nullable=true)
     */
    private $price_ttc;
	
	/**
     * @ORM\Column(name="PreauthorizationId", type="integer", nullable=true)
     */
    private $PreauthorizationId;
	
	/**
     * @ORM\Column(name="CardId", type="integer", nullable=true)
     */
    private $CardId;
	
	/**
     * @ORM\Column(name="date_rdv", type="datetime", nullable=true)
     */
    private $date_rdv;
	
	/**
     * @ORM\Column(name="heure_rdv", type="integer", nullable=true)
     */
    private $heure_rdv;
	
	/**
     * @ORM\Column(name="minute_rdv", type="integer", nullable=true)
     */
    private $minute_rdv;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;
    
    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;


    // Les getters et setters
    
    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));
    
        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }


    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
            return $this->updated_at->format($format);
        } else {
            return $this->updated_at;
        }      
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
        if($this->created_at and $this->created_at instanceof \DateTime and $format){
            return $this->created_at->format($format);
        } else {
            return $this->created_at;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
    
    public function setSeller(\FAPROD\UserBundle\Entity\User $seller)
    {
        $this->seller = $seller;

        return $this;
    }

    public function getSeller()
    {
        return $this->seller;
    }
	
	public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    public function getStatut()
    {
        return $this->statut;
    }
	
	public function getStatutLabel()
    {
        return MyConst::getBookingStatutLabel($this->statut);
    }
	
	public function setPreauthorizationId($PreauthorizationId)
    {
        $this->PreauthorizationId = $PreauthorizationId;

        return $this;
    }

    public function getPreauthorizationId()
    {
        return $this->PreauthorizationId;
    }
	
	public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }
	
	public function getPriceLabel()
    {
       if ($this->getPrice())
          return number_format($this->getPrice(), 0, '.', ' ').' €';
       else return '';
    }
	
    public function setOffer(\FAPROD\UserBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    public function getOffer()
    {
        return $this->offer;
    }

    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    public function getCommission()
    {
        return $this->commission;
    }
	
	public function getCommissionLabel()
    {
       if ($this->getCommission())
          return number_format($this->getCommission(), 0, '.', ' ').' €';
       else return '';
    }

    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    public function getTva()
    {
        return $this->tva;
    }
	
	public function getTvaLabel()
    {
       if ($this->getTva())
          return number_format($this->getTva(), 0, '.', ' ').' €';
       else return '';
    }

    public function setPriceTtc($priceTtc)
    {
        $this->price_ttc = $priceTtc;

        return $this;
    }

    public function getPriceTtc()
    {
        return $this->price_ttc;
    }
	
	public function getPriceTtcLabel()
    {
       if ($this->getPriceTtc())
          return number_format($this->getPriceTtc(), 0, '.', ' ').' €';
       else return '';
    }

    public function setDateRdv($dateRdv)
    {
        $this->date_rdv = $dateRdv;

        return $this;
    }
	
	public function getDateRdv($format = '')
    {
    	if($this->date_rdv and $this->date_rdv instanceof \DateTime and $format){
		    return $this->date_rdv->format($format);
		} else {
    		return $this->date_rdv;
    	}
    }

    public function setHeureRdv($heureRdv)
    {
        $this->heure_rdv = $heureRdv;

        return $this;
    }

    public function getHeureRdv()
    {
        return $this->heure_rdv;
    }
	
	public function getHeureRdvLabel()
    {
        return $this->heure_rdv.'h'.$this->minute_rdv;
    }

    public function setCardId($cardId)
    {
        $this->CardId = $cardId;

        return $this;
    }

    public function getCardId()
    {
        return $this->CardId;
    }

    public function setMinuteRdv($minuteRdv)
    {
        $this->minute_rdv = $minuteRdv;

        return $this;
    }

    public function getMinuteRdv()
    {
        return $this->minute_rdv;
    }
	
	public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCodeReduc($codeReduc)
    {
        $this->code_reduc = $codeReduc;

        return $this;
    }

    public function getCodeReduc()
    {
        return $this->code_reduc;
    }
    
    public function getCodeReducLabel()
    {
       if ($this->getCodeReduc())
          return number_format($this->getCodeReduc(), 2, '.', ' ').' €';
       else return '';
    }
}
