<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\BookingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="FAPROD\BookingBundle\Entity\ReducRepository")
 * @ORM\Table(name="reduc")
 */
class Reduc
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    private $code;
    
    /**
     * @ORM\Column(name="value", type="float", nullable=true)
     */
    private $value;
    
    /**
     * @ORM\Column(name="nb_max", type="integer", nullable=true)
     */
    private $nb_max;
    
    /**
     * @ORM\Column(name="nb_used", type="integer", nullable=true)
     */
    private $nb_used;
    
    /**
     * @ORM\Column(name="order_min", type="integer", nullable=true)
     */
    private $order_min;
  
    /**
     * @ORM\Column(name="valide", type="boolean", nullable=true)
     */
    private $valide;
    
    /**
     * @ORM\Column(name="begin_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $begin_date;
    
    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $end_date;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {
    	$this->valide = true;
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
	
	public function __toString()
    {
    	$title = $this->getCode();
		return $title;
    }

    public function setValide($valide)
    {
        $this->valide = $valide;

        return $this;
    }

    public function getValide()
    {
        return $this->valide;
    }
    
    public function getValideLabel()
    {
        if ($this->getValide())
        {
            return "Oui";
        } else {
            return "Non";
        }
    }
    
    public function getActif()
    {
    	if ($this->getNbMax() and $this->getNbUsed() >= $this->getNbMax()){
    		return false;
    	}
    	
        if ($this->getValide() and $this->getBeginDate() <= new \DateTime('now') and $this->getEndDate() >= new \DateTime('now')){
        	return true;
        } else {
        	return false;
        }
    }
    
    public function getActifLabel()
    {
        if ($this->getActif())
        {
            return "Oui";
        } else {
            return "Non";
        }
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }
    
    public function getValueLabel()
    {
       if ($this->getValue())
          return $this->getValue().' %';
       else return '';
    }

    public function setNbMax($nbMax)
    {
        $this->nb_max = $nbMax;

        return $this;
    }

    public function getNbMax()
    {
        return $this->nb_max;
    }

    public function setNbUsed($nbUsed)
    {
        $this->nb_used = $nbUsed;

        return $this;
    }

    public function getNbUsed()
    {
        return $this->nb_used;
    }

    public function setOrderMin($orderMin)
    {
        $this->order_min = $orderMin;

        return $this;
    }

    public function getOrderMin()
    {
        return $this->order_min;
    }
    
    public function getOrderMinLabel()
    {
       if ($this->getOrderMin())
          return number_format($this->getOrderMin(), 2, '.', ' ').' €';
       else return '';
    }

    public function setBeginDate($beginDate)
    {
        $this->begin_date = $beginDate;

        return $this;
    }
    
    public function getBeginDate($format = '')
    {
        if($this->begin_date and $this->begin_date instanceof \DateTime and $format){
		    return $this->begin_date->format($format);
		} else {
    		return $this->begin_date;
    	}
    }

    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }
    
    public function getEndDate($format = '')
    {
        if($this->end_date and $this->end_date instanceof \DateTime and $format){
		    return $this->end_date->format($format);
		} else {
    		return $this->end_date;
    	}
    }
}
