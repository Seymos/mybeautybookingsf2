<?php
 
namespace FAPROD\BookingBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * BookingRepository
 *
 */
class BookingRepository extends EntityRepository
{
    public function getBookings($page = null, $nbPerPage = null, $sort = 'id', $sens = 0, $QueryBuilder = '')
    {
        if (!$QueryBuilder)$QueryBuilder = $this->createQueryBuilder('b');
        
        $query = $QueryBuilder
    				->leftJoin('b.user', 'u')->addSelect('u')
    				->leftJoin('b.seller', 's')->addSelect('s')
                    ->orderBy('b.'.$sort, $sens ? 'ASC' : 'DESC')
                    ->getQuery();
    
		$query = $QueryBuilder->getQuery();
	
	    if ($page and $nbPerPage){
		    $query
		      ->setFirstResult(($page-1) * $nbPerPage)
		      ->setMaxResults($nbPerPage)
		    ;
		    
		    return new Paginator($query, true);
    	} else {
    		return $query->getResult();
    	}
    }
	
	public function getBookingsCount($QueryBuilder = '')
    {
    	if (!$QueryBuilder)$QueryBuilder = $this->createQueryBuilder('b');
    	
    	$query = $QueryBuilder->getQuery();
	
	    $paginator = new Paginator($query);
        return count($paginator);
    }
}
