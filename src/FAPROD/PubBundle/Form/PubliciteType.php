<?php

/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\PubBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PubliciteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('url', 'url')
            ->add('status', 'choice', array(
            						'choices'   => $options['status'],
	            					'multiple'  => false,
	            					'expanded'  => false,
	            					'required'  => false,
            						)
            )
            ->add('type', 'choice', array(
            						'choices'   => $options['types'],
	            					'multiple'  => false,
	            					'expanded'  => false,
	            					'required'  => false,
            						)
            )
            ->add('cible', 'choice', array(
            						'choices'   => $options['cibles'],
	            					'multiple'  => false,
	            					'expanded'  => false,
	            					'required'  => false,
            						)
            )
            ->add('enligne_date', 'date', array('input'  => 'datetime','widget' => 'choice', 'empty_value' => '', 'required' => false))
            ->add('end_date', 'date', array('input'  => 'datetime','widget' => 'choice', 'empty_value' => '', 'required' => false))
            ->add('user')
            ->add('images', 'collection', array(
            								'type'         => new ImageType(),
                                            'allow_add'    => true,
								            'by_reference' => false,
								            'allow_delete' => true,
								            'prototype'    => true,
								            'label'        => false,
            							    'options'      => array('label' => false),
										   ))
			->add('html')
        ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
			            'data_class'     => 'FAPROD\PubBundle\Entity\Publicite',
			            'types'          => array(),
			            'status'         => array(),
			            'cibles'         => array(),
        			));
    }
    
    public function getDefaultOptions(array $options)
	{
	    return array(
	    			'data_class'      => 'FAPROD\PubBundle\Entity\Publicite',
	    			'types'           => null,
	    			'status'          => null,
	    			'cibles'          => null,
	    			);
	}

    public function getName()
    {
        return 'faprod_pubbundle_publicite';
    }
}
