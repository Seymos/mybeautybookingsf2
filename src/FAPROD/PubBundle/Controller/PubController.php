<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\PubBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\CoreBundle\Lib\MyConst;
use Ob\HighchartsBundle\Highcharts\Highchart;

use FAPROD\PubBundle\Form\PubliciteType;
use FAPROD\PubBundle\Entity\Publicite;
use FAPROD\OrderBundle\Entity\Order;
use FAPROD\OrderBundle\Entity\Orderitem;

class PubController extends Controller
{
    /**
     * @Route("/publicite", name="publicite_index")
     * @Template("PubBundle:Pub:index.html.twig")
	 */
    public function indexAction(Request $request)
    {		
    	$repository_pub = $this->getDoctrine()->getManager()->getRepository('PubBundle:Publicite');
		$queryBuilder = $repository_pub->createQueryBuilder('p');
		$queryBuilder->where('p.user = :user')->setParameter('user', $this->getUser());					 
    	$publicites = $repository_pub->getPublicites(null, null, 'id', 0, $queryBuilder);
	    
        return array(
        			"publicites" => $publicites,
        		);
    }
    
    /**
     * @Route("/publicite/show/{publicite_id}", name="publicite_show", requirements={"publicite_id" = "\d+"})
	 * @ParamConverter("publicite", options={"mapping": {"publicite_id": "id"}})
	 * @Template("PubBundle:Pub:show.html.twig")
	 */
	public function showAction(Publicite $publicite)
	{	
		if ($publicite->getUser()->getId() != $this->getUser()->getId()){
			return $this->redirect($this->generateUrl('publicite_index'));
		}
		
		$nb_jours = 20;
		
		$repository_stat = $this->getDoctrine()->getManager()->getRepository('PubBundle:Stat');
    	$queryBuilder = $repository_stat->createQueryBuilder('s');
		$queryBuilder->where('s.publicite = :publicite')
					 ->andWhere('s.date >= :date')
					 ->setParameter('publicite', $publicite)
					 ->setParameter('date', $date = new \DateTime("- ".$nb_jours." days"));
    	$stats = $repository_stat->getStats(null, null, 'id', 0, $queryBuilder);
    	
    	$dates_temp = array();
    	$vues_temp = array();
    	$clics_temp = array();
    	
    	for ($i = $nb_jours; $i >=0; $i--){
    		$date = new \DateTime("- ".$i." days");
    		$dates_temp[$date->format('d/m')] = $date->format('d/m');
    		$vues_temp[$date->format('d/m')] = 0;
    		$clics_temp[$date->format('d/m')] = 0;
    	}
    	
    	foreach ($stats as $stat){
    		$vues_temp[$stat->getDate('d/m')] = $stat->getVue();
    		$clics_temp[$stat->getDate('d/m')] = $stat->getClic();
    	}
    	
    	$dates = array();
    	$vues = array();
    	$clics = array();
    	foreach ($dates_temp as $date)$dates[] = $date;
    	foreach ($vues_temp as $vue)$vues[] = $vue;
    	foreach ($clics_temp as $clic)$clics[] = $clic;
    	
        $History = array(
            array(
                 "name" => "Vues", 
                 "data" => $vues
            ),
                        array(
                 "name" => "Clics", 
                 "data" => $clics
            ),
            
        );
        
        $ob = new Highchart();
        $ob->chart->renderTo('linechart');  
        $ob->title->text($this->get('translator')->trans('Statistiques'));
        $ob->yAxis->min(0);
        //$ob->yAxis->title(array('text' => "Ventes (milliers d'unité)"));
        //$ob->xAxis->title(array('text'  => "Date du jours"));
        $ob->xAxis->categories($dates);

        $ob->series($History);
		
	  	return array(
	    	'publicite' => $publicite,
	    	'chart' => $ob,
	  	);
	}
	
	/**
     * @Route("/publicite/edit/{publicite_id}", name="publicite_edit", requirements={"publicite_id" = "\d+"})
	 * @Template("PubBundle:Pub:edit.html.twig")
	 */
    public function editAction($publicite_id, Request $request)
    {
    	if ($publicite_id){
    		$publicite = $this->getDoctrine()->getManager()->getRepository('PubBundle:Publicite')->findOneById($publicite_id);
    		
    		$originalImages = new ArrayCollection();
			foreach ($publicite->getImages() as $image) {
				$originalImages->add($image);
			}
    	} else {
    		$publicite = new Publicite();
			$publicite->setUser($this->getUser());
			$publicite->setStatus(0);
			
			$originalImages = new ArrayCollection();
    	}
		
    	if ($publicite->getUser()->getId() != $this->getUser()->getId()){
			return $this->redirect($this->generateUrl('publicite_index'));
		}
		    
	    $form = $this->createForm(new PubliciteType(), $publicite);
	    $form->remove('status');
	    $form->remove('type');
	    $form->remove('cible');
	    $form->remove('enligne_date');
	    $form->remove('end_date');
	    $form->remove('html');
	    $form->remove('sites');
	    $form->add('user', 'entity_hidden', array(
                'class' => 'FAPROD\UserBundle\Entity\User'
				    ));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	    	
	       $em = $this->getDoctrine()->getManager();
	       $em->persist($publicite);
		    
		   foreach ($originalImages as $image) {
            if (false === $publicite->getImages()->contains($image)) {
                $em->remove($image);
            }
           } 
	       
	       $em->flush();
	       
	       foreach ($publicite->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
           
           $em->flush();
           
           if ($request->request->get('publicite_option')){
           	  $order = new Order();
           	  $order->setUser($this->getUser());
           	  $order->setType(MyConst::ORDER_PUBLICITE);
           	  $order->setCibleId($publicite->getId());
           	  
           	  $repository_option = $this->getDoctrine()->getManager()->getRepository('OrderBundle:Option');
           	  $taux_tva = $this->get('FAPROD.Configs')->getTva();
           	  
           	  $total_ht = 0;
           	  
       	  	  $option = $repository_option->findOneById($request->request->get('publicite_option'));
       	  	  $total_ht += $option->getPriceHt();
       	  	 
       	  	  $item = new Orderitem;
       	  	  $item->setTitle($option->getTitle());
       	  	  $item->setOptionId($option->getId());
       	  	  $item->setPrice($option->getPriceHt());
       	  	  $item->setQuantity(1);
       	  	  $item->setTotal($option->getPriceHt());
       	  	 
       	  	  $order->addItem($item);
           	  
           	  $tva = round(($total_ht * $taux_tva) / 100, 2);
           	  $total_ttc = $total_ht + $tva;
           	  
           	  $order->setTotalHt($total_ht);
           	  $order->setTotalTtc($total_ttc);
           	  $order->setTva($tva);
           	  $order->setStatus(0);
           	  $order->setTxnId('');
           	  
           	  $em->persist($order);
           	  $em->flush(); 
           	  
           	  $order->setOrderId(date('dmY').'.'.$order->getId());
           	  $order->buildFacture();
           	  $em->flush();
           	  
           	  $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Enregistrement effectué.'));
	
	       	  return $this->redirect($this->generateUrl('order_show', array('order_id' => $order->getId())));
       	   }
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Enregistrement effectué.'));
	
	       return $this->redirect($this->generateUrl('publicite_show', array('publicite_id' => $publicite->getId())));
	    }
	    
	    $repository_option = $this->getDoctrine()->getManager()->getRepository('OrderBundle:Option');
    	$queryBuilder = $repository_option->createQueryBuilder('o');
		$queryBuilder->where('o.is_publish = 1');
		$queryBuilder->andWhere('o.product = :product')->setParameter('product', MyConst::ORDER_PUBLICITE);
		if ($this->getUser()->getPro()){
			$queryBuilder->andWhere('o.cible = 1 or o.cible = 3');
		} else {
			$queryBuilder->andWhere('o.cible = 1 or o.cible = 2');
		}
    	$options = $repository_option->getOptions(null, null, 'no_order', 1, $queryBuilder);
    	
    	$taux_tva = $this->get('FAPROD.Configs')->getTva();
	
	    return array(
	       'form'        => $form->createView(),
	       'options'     => $options,
		   'taux_tva'    => $taux_tva,
	    );
    }
  
    /**
     * @Route("/publicite/delete/{publicite_id}", name="publicite_delete", requirements={"publicite_id" = "\d+"})
	 * @ParamConverter("publicite", options={"mapping": {"publicite_id": "id"}})
	 */
	public function deleteAction(Publicite $publicite, Request $request)
	{	
		if ($publicite->getUser()->getId() != $this->getUser()->getId()){
			return $this->redirect($this->generateUrl('publicite_index'));
		}
		
	    $em = $this->getDoctrine()->getManager();
	    $em->remove($publicite);
	    $em->flush();

	    $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Suppression effectuée.'));

        return $this->redirect($this->generateUrl('publicite_index'));
	}
}
