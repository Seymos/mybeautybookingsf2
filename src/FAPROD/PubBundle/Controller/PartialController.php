<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\PubBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FAPROD\PubBundle\Entity\Stat;

class PartialController extends Controller
{	
	public function publiciteAction($type = 1, $action_name, Request $request)
	{	
		$repository_pub = $this->getDoctrine()->getManager()->getRepository('PubBundle:Publicite');
		$queryBuilder = $repository_pub->createQueryBuilder('p');
		$queryBuilder->where('p.status = 2')
					  ->andWhere('p.enligne_date IS NULL or p.enligne_date <= :begin_date')
					  ->andWhere('p.end_date IS NULL or p.end_date >= :end_date')
					  ->setParameter('begin_date', new \DateTime('now'))
					  ->setParameter('end_date', new \DateTime('now'));
		
		$queryBuilder->andWhere('p.type = :type')->setParameter('type', $type);

		if ($action_name == 'Home/index'){
			$queryBuilder->andWhere('p.cible = 1 or p.cible = 3');
		} else {
			$queryBuilder->andWhere('p.cible = 2 or p.cible = 3');
		}

    	$publicites = $repository_pub->getPublicites(null, null, 'id', 0, $queryBuilder);
    	
    	$trie = array();
	    foreach ($publicites as $publicite)$trie[] = $publicite;
	    shuffle($trie);
	    
	    if (count($trie)){
	    	$publicite = $trie[0];
	    	
	    	$now = \DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d 00:00:00"));
	    	
	    	$repository_stat = $this->getDoctrine()->getManager()->getRepository('PubBundle:Stat');
	    	$queryBuilder = $repository_stat->createQueryBuilder('s');
			$queryBuilder->where('s.publicite = :publicite')
						 ->andWhere('s.date = :date')
						 ->setParameter('publicite', $publicite)
						 ->setParameter('date', $now);
	    	$stat = $queryBuilder->setMaxResults('1')->getQuery()->getOneOrNullResult();
	    	
	    	$em = $this->getDoctrine()->getManager();
	    	
	    	if ($stat){
	    		$stat->setVue($stat->getVue() + 1);
	    		$em->flush();
	    	} else {
	    		$stat = new Stat();
	    		$stat->setPublicite($publicite);
	    		$stat->setVue(1);
	    		$stat->setClic(0);
	    		$stat->setDate($now);
	    		
	    		$em->persist($stat);
	    		$em->flush();
	    	}
	    	
	    	if (count($publicite->getImages())){
	    		$index_image = rand(0, count($publicite->getImages())-1);
	    	} else {
	    		$index_image = -1;
	    	}	    	
	    	
	    } else {
	    	$publicite = null;
	    	$index_image = -1;
	    }
    	
    	return $this->render('PubBundle:Partial:publicite.html.twig', array(
																   'publicite'     => $publicite,
																   'index_image'   => $index_image,
																   'type'          => $type,
															    ));
	}
}
