<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\PubBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="FAPROD\PubBundle\Entity\StatRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="statpublicite")
 */
class Stat
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
  
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\PubBundle\Entity\Publicite")
     * @ORM\JoinColumn(name="publicite_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $publicite;
    
    /**
     * @ORM\Column(name="vue", type="integer", nullable=true)
     */
    private $vue;
    
    /**
     * @ORM\Column(name="clic", type="integer", nullable=true)
     */
    private $clic;
    
    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $date;
  
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {

    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}

    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setVue($vue)
    {
        $this->vue = $vue;

        return $this;
    }

    public function getVue()
    {
        return $this->vue;
    }

    public function setClic($clic)
    {
        $this->clic = $clic;

        return $this;
    }

    public function getClic()
    {
        return $this->clic;
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getDate($format = '')
    {
    	if($this->date and $this->date instanceof \DateTime and $format){
		    return $this->date->format($format);
		} else {
    		return $this->date;
    	}
    }

    public function setPublicite(\FAPROD\PubBundle\Entity\Publicite $publicite = null)
    {
        $this->publicite = $publicite;

        return $this;
    }

    public function getPublicite()
    {
        return $this->publicite;
    }
}
