<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\PubBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping\OrderBy;

use FAPROD\CoreBundle\Lib\MyConst;

/**
 * @ORM\Entity(repositoryClass="FAPROD\PubBundle\Entity\PubliciteRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="publicite")
 */
class Publicite
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
  
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
    
    /**
     * @ORM\OneToMany(targetEntity="FAPROD\PubBundle\Entity\Image", mappedBy="publicite", cascade={"persist"})
     * @OrderBy({"id" = "DESC"})
     * @Assert\Valid()
     */
    private $images;
    
    /**
     * @ORM\Column(name="html", type="text", nullable=true)
     */
    private $html;
    
    /**
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;
    
    /**
     * @ORM\Column(name="url", type="string", length=200, nullable=true)
     */
    private $url;
    
    /**
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;
    
    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;
    
    /**
     * @ORM\Column(name="cible", type="integer", nullable=true)
     */
    private $cible;
    
    /**
     * @ORM\Column(name="enligne_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $enligne_date;
  
    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $end_date;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {

    }
    
    public function __toString()
    {
		return $this->getTitle();
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}

    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
    
    public function getStatusLabel()
    {
       return MyConst::getStatutPubLabel($this->getStatus());
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }
    
    public function getTypeLabel()
    {
       return MyConst::getTypesPubLabel($this->getType());
    }

    public function setEnligneDate($enligneDate)
    {
        $this->enligne_date = $enligneDate;

        return $this;
    }

    public function getEnligneDate($format = '')
    {
        if($this->enligne_date and $this->enligne_date instanceof \DateTime and $format){
		    return $this->enligne_date->format($format);
		} else {
    		return $this->enligne_date;
    	}
    }

    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }

    public function getEndDate($format = '')
    {
        if($this->end_date and $this->end_date instanceof \DateTime and $format){
		    return $this->end_date->format($format);
		} else {
    		return $this->end_date;
    	}
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setCible($cible)
    {
        $this->cible = $cible;

        return $this;
    }

    public function getCible()
    {
        return $this->cible;
    }
    
    public function getCibleLabel()
    {
       return MyConst::getCiblesPubLabel($this->getCible());
    }

    public function addImage(\FAPROD\PubBundle\Entity\Image $image)
    {
        $this->images[] = $image;
        
        $image->setPublicite($this);

        return $this;
    }

    public function removeImage(\FAPROD\PubBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    public function getHtml()
    {
        return $this->html;
    }
}
