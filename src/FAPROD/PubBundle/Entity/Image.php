<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\PubBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

use FAPROD\CoreBundle\Lib\MyConst;

/**
 * @ORM\Entity(repositoryClass="FAPROD\UserBundle\Entity\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="publiciteimage")
 */
class Image
{
	
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
      * @ORM\ManyToOne(targetEntity="Publicite", inversedBy="images", cascade={"persist"})
      * @ORM\JoinColumn(name="publicite_id", referencedColumnName="id", onDelete="CASCADE")
      */
    private $publicite;
    
    /**
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    private $filename;
    
    /**
     * @ORM\Column(name="ext", type="string", length=25, nullable=true)
     */
    private $ext;
    
    /**
     * @ORM\Column(name="alt", type="string", length=255, nullable=true)
     */
    private $alt;
    
    /**
    * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "La taille maximale est de 5MB.",
     *     mimeTypesMessage = "Type invalide (jpg, gif, png, tiff autorisés)."
     * )
	 */
    private $file;
    
    private $tempFilename;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    public function __toString()
    {
		return $this->getFilename();
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
	
	public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }
    
    public function setFile(UploadedFile $file)
    {
      $this->file = $file;

      // On vérifie si on avait déjà un fichier pour cette entité
      if (null !== $this->filename) {
        // On sauvegarde l'extension du fichier pour le supprimer plus tard
        $this->tempFilename = $this->filename;

        // On réinitialise les valeurs des attributs url et alt
        $this->filename = null;
        $this->alt = null;
      }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
      if (null === $this->file) {
        return;
      }
      $this->filename = sha1($this->file->getClientOriginalName().date('dhis')).'.'.pathinfo((string)$this->file->getClientOriginalName(), PATHINFO_EXTENSION);
      $this->ext = $this->file->guessExtension();
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
      // Si jamais il n'y a pas de fichier (champ facultatif)
      if (null === $this->file) {
        return;
      }

	  $fichier_destination = 'p'.$this->id.'.'.sha1($this->file->getClientOriginalName().date('dhis')).'.'.pathinfo((string)$this->file->getClientOriginalName(), PATHINFO_EXTENSION);
	  
      // Si on avait un ancien fichier, on le supprime
      if (null !== $this->tempFilename and $this->tempFilename != 'no_image.png') {
        $oldFile = $this->getUploadRootDir().'/thumb/'.$fichier_destination;
        if (file_exists($oldFile)) {
          unlink($oldFile);
        }
      }

      // On déplace le fichier envoyé dans le répertoire de notre choix
      $this->file->move(
        $this->getUploadRootDir().'/thumb', // Le répertoire de destination
        $fichier_destination   // Le nom du fichier à créer, ici « id.extension »
      );

    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
      // On sauvegarde temporairement le nom du fichier, car il dépend de l'id
      $this->tempFilename = $this->getUploadRootDir().'/thumb/'.$this->id.'.'.$this->filename;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
      // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
      if (file_exists($this->tempFilename) and $this->tempFilename != 'no_image.png') {
        // On supprime le fichier
        unlink($this->tempFilename);
      }
    }

    public function getUploadDir()
    {
      // On retourne le chemin relatif vers l'image pour un navigateur
      return 'uploads';
    }

    protected function getUploadRootDir()
    {
      // On retourne le chemin relatif vers l'image pour notre code PHP
      return __DIR__.'/../../../../'.MyConst::getWebDir().'/'.$this->getUploadDir();
    }
    
    public function getUrl()
    {
    	return MyConst::getSiteUrl().$this->getWebPath();
	}
  
    public function getWebPath()
    {
    	if ($this->getFilename() and $this->getFilename() != 'no_image.png'){
    		return $this->getUploadDir().'/thumb/'.'p'.$this->id.'.'.$this->getFilename();
    	} else {
    		return $this->getUploadDir().'/no_image.png';
    	}
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    public function getAlt()
    {
        return $this->alt;
    }

    public function setExt($ext)
    {
        $this->ext = $ext;

        return $this;
    }

    public function getExt()
    {
        return $this->ext;
    }

    public function setPublicite(\FAPROD\PubBundle\Entity\Publicite $publicite = null)
    {
        $this->publicite = $publicite;

        return $this;
    }

    public function getPublicite()
    {
        return $this->publicite;
    }
}
