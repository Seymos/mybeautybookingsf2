<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\CoreBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\UserBundle\Entity\User;
use FAPROD\UserBundle\Entity\Category;

class PartialController extends Controller
{
	
	/**
     * @Route("agenda/{user_id}", name="home_agenda", requirements={"user_id" = "\d+"})
     * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 * @Template("CoreBundle:Partial:agenda.html.twig")
	*/
	public function agendaAction(User $user, Request $request)
	{	
	
		$date_select = $request->get('date_select');
		
		if ($date_select){
			$date_select_string = explode('_', $date_select);
			$heure_select_string = explode('h', $date_select_string[3]);

			$heure_select = sprintf('%d', floor($heure_select_string[0]));
			$minute_select = $heure_select_string[1] ? $heure_select_string[1] : 0;
			
			$duree = 0;
			if ($request->get('offer_id')){
				$offer = $this->getDoctrine()->getManager()->getRepository('UserBundle:Offer')->find($request->get('offer_id'));
				if ($offer)$duree = $offer->getDuree() * 30;
			}

			$begin_prestation = \DateTime::createFromFormat("Y-m-d H:i:s", date($date_select_string[2]."-".$date_select_string[1]."-".$date_select_string[0]." ".sprintf('%02d', floor($heure_select)).":".sprintf('%02d', floor($minute_select)).":00"));
			$end_prestation   = \DateTime::createFromFormat("Y-m-d H:i:s", date($date_select_string[2]."-".$date_select_string[1]."-".$date_select_string[0]." ".sprintf('%02d', floor($heure_select)).":".sprintf('%02d', floor($minute_select)).":00"));
			$end_prestation->add(new \DateInterval('PT' . $duree . 'M'));
		}
		
		$semaine = array("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche");
		$datas = array();
		
		$heure_min = 24;
		$heure_max = 0;
		
		foreach ($user->getHoraires() as $horaire){
			if ($horaire->getHeureDeb() < $heure_min){
				$heure_min = $horaire->getHeureDeb();
			}
			if ($horaire->getHeureFin() > $heure_max){
				$heure_max = $horaire->getHeureFin();
			}
		}
		
		$ouvertures = array();
		$ouvertures[1] = $user->getHorairesLundi();
		$ouvertures[2] = $user->getHorairesMardi();
		$ouvertures[3] = $user->getHorairesMercredi();
		$ouvertures[4] = $user->getHorairesJeudi();
		$ouvertures[5] = $user->getHorairesVendredi();
		$ouvertures[6] = $user->getHorairesSamedi();
		$ouvertures[7] = $user->getHorairesDimanche();
		
		$param_begin = $request->get('date') ? $request->get('date') : date('Y-m-d');
		
		
		$begin = \DateTime::createFromFormat("Y-m-d H:i:s", date($param_begin." 00:00:00"));
		$end = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, explode('-',$param_begin)[1], explode('-',$param_begin)[2]+6, explode('-',$param_begin)[0])));

		
		$repository_agenda = $this->getDoctrine()->getManager()->getRepository('UserBundle:Agenda');
    	$queryBuilder = $repository_agenda->createQueryBuilder('a');
		$queryBuilder->where('a.user = :user')
					 ->andWhere('a.date >= :begin and a.date <= :end')
					 ->setParameter('user', $user)
					 ->setParameter('begin', $begin)
					 ->setParameter('end', $end);
    	$agendas = $repository_agenda->getAgendas(null, null, "id", 0, $queryBuilder);
		
		$indisponibles = array();
		foreach ($agendas as $agenda){
			
			if ($agenda->getMinute()){
				$string = $agenda->getDate('d_m_Y').'_'.$agenda->getHeure().'.5';
			} else {
				$string = $agenda->getDate('d_m_Y').'_'.$agenda->getHeure();
			}
			
			$indisponibles[$string] = $string;
		}
		
		
		// entete du tableau
		$entete = array();
		
		for($jour = $begin; $jour <= $end; $jour->modify('+1 day')){
			
			$entete[] = array(
						"jour" => $semaine[$jour->format("N")-1],
						"date" => $jour->format("d/m/Y"),
						);
		}
		
		
		// agenda
		$datas = array();
		$nb_case_select = 0;
		
		for ($heure = $heure_min; $heure <= $heure_max; $heure += 0.5){
						
			$begin = \DateTime::createFromFormat("Y-m-d H:i:s", date($param_begin." 00:00:00"));
			$end = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, explode('-',$param_begin)[1], explode('-',$param_begin)[2]+6, explode('-',$param_begin)[0])));
			$jours = array();
			
			for($jour = $begin; $begin <= $end; $jour->modify('+1 day')){
				
				$ok = false;
				
				foreach ($ouvertures[$jour->format("N")] as $horaire){
					$heure_deb = $horaire->getMinuteDeb() == 30 ? $horaire->getHeureDeb() + 0.5 : $horaire->getHeureDeb();
					$heure_fin = $horaire->getMinuteFin() == 30 ? $horaire->getHeureFin() + 0.5 : $horaire->getHeureFin();
					if ($heure >= $heure_deb and $heure < $heure_fin){
						$ok = true;
					}
				}
				
				if ($ok){
				
					if (in_array($jour->format("d_m_Y").'_'.$heure, $indisponibles)){
						$jours[] = array(
									"ouverture" => -1,
									"date" => $jour->format("d_m_Y"),
									);
					} else {
						
						$case_select = false;
						if ($date_select){
							$heure_temp = floor($heure);
							$minute_temp = strpos($heure, ".") ? 30 : 0;
							$begin_temp = \DateTime::createFromFormat("Y-m-d H:i:s", date($jour->format("Y")."-".$jour->format("m")."-".$jour->format("d")." ".sprintf('%02d', floor($heure_temp)).":".sprintf('%02d', floor($minute_temp)).":00"));
							
							if ($begin_temp >= $begin_prestation and $begin_temp < $end_prestation){
								$case_select = true;
								$nb_case_select+=30;
							}
						}
						
						$jours[] = array(
									"ouverture"     => 1,
									"date"          => $jour->format("d_m_Y"),
									"case_select"   => $case_select,
									);
					}
				
				} else {
					$jours[] = array(
									"ouverture" => 0,
									);
				}
			}
			
			$datas[] = array(
							"heure"  => strpos($heure, ".") ? sprintf('%02d', floor($heure)).'h30' : sprintf('%02d', $heure).'h',
							"jours"  => $jours,
							);

		}
		
		$last_date = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, explode('-',$param_begin)[1], explode('-',$param_begin)[2]-7, explode('-',$param_begin)[0])));
		$next_date = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, explode('-',$param_begin)[1], explode('-',$param_begin)[2]+7, explode('-',$param_begin)[0])));

		$selection_valide = 0;
		if ($date_select)$selection_valide = $nb_case_select == $duree ? 1 : -1;	
		
    	return array(
				   'duree'             => $request->get('duree'),
				   'datas'             => $datas,
				   'entete'            => $entete,
				   'user'              => $user,
				   'last_date'         => $last_date->format('Y-m-d'),
				   'next_date'         => $next_date->format('Y-m-d'),
				   'date_agenda'       => $param_begin,
				   'selection_valide'  => $selection_valide,
				   'date_select'       => $date_select,
				);
	}
	
	public function rightAction()
	{    	
		$repository_content = $this->getDoctrine()->getManager()->getRepository('ContentBundle:Content');

	    $queryBuilder = $repository_content->createQueryBuilder('c');
	    $queryBuilder->where('c.is_publish = 1');
	    $queryBuilder->andWhere('tc.id = 4');
        $contents = $repository_content->getContents(1, 3, "id", 0, $queryBuilder);
    	
		$response = $this->render('CoreBundle:Partial:right.html.twig', array(
																			'contents' => $contents,
																			));
		$response->setSharedMaxAge(10000);
    	$response->setPublic();
        $response->setMaxAge(10000);
    
	  	return $response;
	}
	
	public function homeAction()
	{	
	
		$repository_avis = $this->getDoctrine()->getManager()->getRepository('ContentBundle:ContentComment');

    	$queryBuilder = $repository_avis->createQueryBuilder('c');
		$queryBuilder->where('c.validate = 1 AND c.note >= 3');
    	$avis = $repository_avis->getComments(1, 9, 'id', 0, $queryBuilder);
		
		$response = $this->render('CoreBundle:Partial:home.html.twig', array(
																			'avis' => $avis,
																			));
																			
																			
		$response->setSharedMaxAge(10000);
    	$response->setPublic();
        $response->setMaxAge(10000);
    
	  	return $response;
	}
	
	public function searchAction(Request $request)
	{	
	
		$search_category = $request->getSession()->get('search_category');
		
		$repository_prestation = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation');
    	$queryBuilder = $repository_prestation->createQueryBuilder('p');
		$queryBuilder->where('p.level = 0');
    	$metiers = $repository_prestation->getPrestations(null, null, 'no_order', 1, $queryBuilder);
		
		if ($search_category){
			$repository_prestation = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation');
			$queryBuilder = $repository_prestation->createQueryBuilder('p');
			$queryBuilder->where('p.level = 1');
			$queryBuilder->leftJoin('p.parent', 'm_parent')->addSelect('m_parent');
			$queryBuilder->andWhere('m_parent.id = :metier_id')->setParameter('metier_id', $search_category);
			$prestations = $repository_prestation->getPrestations(null, null, 'title', 1, $queryBuilder);
		} else {
			$prestations = array();
		}
	    	
    	return $this->render('CoreBundle:Partial:search.html.twig', array(
																   'metiers'       => $metiers,
																   'prestations'   => $prestations,
															    ));
	}
	
	/**
     * @Route("ajaxphonepro/{user_id}", name="home_ajaxphonepro", requirements={"user_id" = "\d+"})
     * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 * @Template("CoreBundle:Partial:ajaxphonepro.html.twig")
	 */
	public function ajaxphoneproAction(User $user)
	{
		if ($_SERVER['HTTP_USER_AGENT'] and stripos($_SERVER['HTTP_USER_AGENT'], 'bot') === false){
			
			$now = \DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d 00:00:00"));
		    
		    $repository_stat = $this->getDoctrine()->getManager()->getRepository('UserBundle:Stat');
	    	$queryBuilder = $repository_stat->createQueryBuilder('s');
			$queryBuilder->where('s.user = :user')
						 ->andWhere('s.date = :date')
						 ->setParameter('user', $user)
						 ->setParameter('date', $now);
	    	$stat = $queryBuilder->setMaxResults('1')->getQuery()->getOneOrNullResult();
	    	
	    	$em = $this->getDoctrine()->getManager();
	    	
	    	if ($stat){
	    		$stat->setContact($stat->getContact() + 1);
	    		$em->flush();
	    	} else {
	    		$stat = new StatUser();
	    		$stat->setUser($user);
	    		$stat->setVue(0);
	    		$stat->setContact(1);
	    		$stat->setDate($now);
	    		
	    		$em->persist($stat);
	    		$em->flush();
	    	}
    	}
    	
		return array(
	    	'user' => $user,
	  	);
	}
	
	/**
     * @Route("/prestation", name="home_prestation")
     * @Template("CoreBundle:Partial:prestation.html.twig")
     */
	public function prestationAction(Request $request){
		
		$repository_prestation = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation');
		
    	$queryBuilder = $repository_prestation->createQueryBuilder('p');
		$queryBuilder->where('p.level = 1');
		$queryBuilder->leftJoin('p.parent', 'm_parent')->addSelect('m_parent');
		$queryBuilder->andWhere('m_parent.id = :metier_id')->setParameter('metier_id', $request->get('metier_id'));
    	$prestations = $repository_prestation->getPrestations(null, null, 'no_order', 1, $queryBuilder);
    	
		return array(
					"prestations"  => $prestations,
				);
	}

}
