<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\CoreBundle\Controller;

require_once __DIR__ . '/../Lib/Tools/autoload.php';

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Exception\HttpException;

use FAPROD\CoreBundle\Lib\MyConst;

use FAPROD\UserBundle\Entity\User;
use FAPROD\UserBundle\Entity\Stat;
use FAPROD\PubBundle\Entity\Publicite;
use FAPROD\ContentBundle\Form\ContentCommentType;
use FAPROD\MessageBundle\Form\DiscussionType;
use FAPROD\MessageBundle\Entity\Discussion;


use FAPROD\ContactBundle\Form\ContactType;


class HomeController extends Controller
{
	const ITEMS_PAR_PAGE = 20;
	
	public function preExecute(Request $request)
    {        
	
		/*
        if ($request->query->get('parrain')){
        	$this->container->get('session')->set('parrain', $request->query->get('parrain'));
        }
		
		
		if (!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '109.190.24.64', '90.127.136.65', '82.253.90.86', '86.245.223.201'))) {
			
			$regexp = "#::([a-zA-Z]*)Action#";
			$results = array();
			preg_match($regexp, $request->get('_controller'), $results);
			$action = isset($results[1]) ? $results[1] : '';
			
			if ($action != "welcome") {
				throw new HttpException(307, null, null, array('Location' => $this->generateUrl('home_welcome')));
			}
		}
		*/
		
    }
	
	/**
     * @Route("/invitation", name="home_invitation")
     * @Template("CoreBundle:Home:invitation.html.twig")
	 */
    public function invitationAction(Request $request)
    {
    	if ($request->getMethod() == 'POST')
        {
        	$validator = $this->container->get('validator');
        	$constraints = array(
		        new \Symfony\Component\Validator\Constraints\Email(),
		        new \Symfony\Component\Validator\Constraints\NotBlank()
		    );
        	
        	$emails = explode(',', str_replace(' ','',$request->request->get('emails')));
        	
        	foreach ($emails as $email){
        	
	        	try{
	        		
	        		$error = $validator->validateValue($email, $constraints);
					
					if (count($error) == 0) {
	        		
			        	$mailBody = $this->renderView('CoreBundle:Mail:invitation.html.twig', array('prenom' => $request->request->get('prenom')));
			        	//echo $mailBody;exit;
				        $message = \Swift_Message::newInstance()
					        ->setSubject($this->get('translator')->trans('Invitation sur').' '.$this->get('FAPROD.MyConst')->getSiteName())
					        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
					        ->setTo($email)
					        ->setBody($mailBody, 'text/html');
					    
					    $this->get('mailer')->send($message);
					    
				    }
				    
				} catch (\Exception $e) {
	
				}
			
			}
	    
	        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Message envoyé.'));
	        
	        return $this->redirect($this->generateUrl('home_invitation'));
    	}
	}
	
	/**
     * @Route("/pros/partage/{user_id}", name="home_partage", requirements={"user_id" = "\d+"})
     * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
     * @Template("CoreBundle:Home:partage.html.twig")
	 */
    public function partageAction(User $user, Request $request)
    {
    	if ($request->getMethod() == 'POST')
        {
        	$validator = $this->container->get('validator');
        	$constraints = array(
		        new \Symfony\Component\Validator\Constraints\Email(),
		        new \Symfony\Component\Validator\Constraints\NotBlank()
		    );
        	
        	$emails = explode(',', str_replace(' ','',$request->request->get('emails')));
        	
        	foreach ($emails as $email){
        	
	        	try{
	        		
	        		$error = $validator->validateValue($email, $constraints);
					
					if (count($error) == 0) {

			        	$mailBody = $this->renderView('CoreBundle:Mail:partage.html.twig', array('user' => $user, 'prenom' => $request->request->get('prenom')));
			        	//echo $mailBody;exit;
				        $message = \Swift_Message::newInstance()
					        ->setSubject($request->request->get('prenom').' '.$this->get('translator')->trans('souhaite vous faire découvrir').' '.$this->get('FAPROD.MyConst')->getSiteName())
					        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
					        ->setTo($email)
					        ->setBody($mailBody, 'text/html');
					    
					    $this->get('mailer')->send($message);
					    
				    }
				    
				} catch (\Exception $e) {
	
				}
			
			}
	    
	        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Message envoyé.'));
	        
	        return $this->redirect($this->generateUrl('home_partage', array('user_id' => $user->getId())));
    	}
    	
    	return array(
    				'user' => $user,
    				);
	}
	
   /**
   * @Route("/upload/process/{resize}", name="upload_media")
   */
   public function uploadAction(Request $request, $resize = "")
   {
   		if ($request->getMethod() == 'POST')
        {
	   		$temp = $resize ? explode("_",$resize) : array();
	   		$traitements = array();
	   		for ($i = 0; $i < count($temp);$i = $i + 3){
	   			$traitements[] = array($temp[$i],$temp[$i+1],$temp[$i+2]);
	   		}
	   		
		    $request = $this->get('request');
		    $files = $request->files;       
	
	   	    $directory = $this->get('kernel')->getRootDir() . '/../' . MyConst::getWebDir() . '/uploads/temp/';
	
	   	   	foreach ($files as $uploadedFile) {
	       		$name = sha1($uploadedFile->getClientOriginalName().date('dhis')).'.'.pathinfo((string)$uploadedFile->getClientOriginalName(), PATHINFO_EXTENSION);
	       		
	       		$file = $uploadedFile->move($directory, '1_'.$name);
	       		for ($i = 2; $i <= count($traitements); $i++){
	       			copy($directory.'1_'.$name, $directory.$i.'_'.$name);
       			}
	       		
	       		$i = 1;
	       		foreach ($traitements as $traitement){
	       			$image = new \EscapeWork\Resize\Resize($directory.$i.'_'.$name);
	       			if ($traitement[0] == 1){
	       				$image->setWidth($traitement[1])->setHeight($traitement[2])->crop();
	       			} else {
	       				$image->setWidth($traitement[1])->setHeight($traitement[2])->resize();
	       			}
	       			$i++;		    	
	       		}
	   		}
	   		
	   		return new JsonResponse(["name" => $name]);
   		}
   		
   		return new JsonResponse([]);

   }
   
   /**
     * @Route("/clic/{publicite_id}", name="home_clic", requirements={"publicite_id" = "\d+"})
     * @ParamConverter("publicite", options={"mapping": {"publicite_id": "id"}})
	 */
    public function clicAction(Publicite $publicite)
    {		
    	$now = \DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d 00:00:00"));
    	
    	$repository_stat = $this->getDoctrine()->getManager()->getRepository('PubBundle:Stat');
    	$queryBuilder = $repository_stat->createQueryBuilder('s');
		$queryBuilder->where('s.publicite = :publicite')
					 ->andWhere('s.date = :date')
					 ->setParameter('publicite', $publicite)
					 ->setParameter('date', $now);
    	$stat = $queryBuilder->getQuery()->getOneOrNullResult();
    	
    	$em = $this->getDoctrine()->getManager();
    	
    	if ($stat){
    		$stat->setClic($stat->getClic() + 1);
    		$em->flush();
    	} else {
    		$stat = new Stat();
    		$stat->setPublicite($publicite);
    		$stat->setVue(0);
    		$stat->setClic(1);
    		$stat->setDate($now);
    		
    		$em->persist($stat);
    		$em->flush();
    	}
	    
        return $this->redirect($publicite->getUrl());
    }
   
	/**
     * @Route("/", name="home_index")
     * @Template("CoreBundle:Home:index.html.twig")
	 */
    public function indexAction(Request $request)
    {
    	if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED') and !$this->get('security.context')->isGranted('ROLE_USER')){
    		return $this->redirect($this->generateUrl('register_index'));
    	}
		
		/*
    	if ($request->getSession()->get('popupcnil')) {
			$popup = 1;
			$request->getSession()->set('popupcnil', 0);
		} elseif (isset($_COOKIE['popupcnil'])) {
			$popup = 0;
		}
		
		if (!isset($_COOKIE['popupcnil'])){
			$request->getSession()->set('popupcnil', 1);
			
	        $cookie_info = array(
	            'name'  => 'popupcnil',
	            'value' => 1,
	            'time'  => time() + 3600 * 24 * 15
	        );
	        $cookie = new Cookie($cookie_info['name'], $cookie_info['value'], $cookie_info['time']);
	        //$response = new Response();
	        
	        
	        $response = new RedirectResponse($this->generateUrl('home_index'));
	        $response->headers->setCookie($cookie);
	        $response->send();
	        
    		return $response;
    	}
		*/
		
		$popup = 0;

	  	return array(
	  		'popup' => $popup,
	  		);
    }
	
	/**
     * @Route("/welcome", name="home_welcome")
     * @Template("CoreBundle:Home:welcome.html.twig")
	 */
    public function welcomeAction(Request $request)
    {
		$success = false;
		
		if ((isset($_POST['g-recaptcha-response']))) {
			$recaptcha = new \ReCaptcha\ReCaptcha("6LetBCwUAAAAAM1WCAhcSATFwbsiBvQ6pf1ppFn_");
			$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
			if ($resp->isSuccess()) {
				$success = true;
			}
		}
		
		if ($request->getMethod() == 'POST')
		{

			if ($request->get('email') and $success)
			{
				$message = \Swift_Message::newInstance()
					->setSubject($this->get('translator')->trans('Une inscription'))
					->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
					->setTo($this->get('FAPROD.MyConst')->getSiteEmail())
					->setBody($request->get('email'), 'text/html');
				//echo $request->get('email');exit;
				$this->get('mailer')->send($message);

				$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Inscription envoyée.'));

				return $this->redirect($this->generateUrl('home_welcome'));
			} else {
				$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Formulaire incorrect.'));
				
				return $this->redirect($this->generateUrl('home_welcome'));
			}
			
		}
    	
    	
    }
    
    /**
     * @Route("/endcb", name="home_endcb")
     * @Template("CoreBundle:Home:endcb.html.twig")
	 */
    public function endcbAction(Request $request)
    {
	}
	
	/**
     * @Route("/gosearch", name="home_gosearch")
	 */
	public function gosearchAction(Request $request)
	{	
		if ($request->getMethod() == 'POST' or $request->get('update')){
			$request->getSession()->set('search_ou', $request->request->get('search_ou'));
			$request->getSession()->set('search_ou_location', $request->request->get('search_ou_location'));
			$request->getSession()->set('search_date', $request->request->get('search_date'));
			$request->getSession()->set('search_category', $request->get('search_category'));
			$request->getSession()->set('search_prestation', $request->request->get('search_prestation'));
		}

        return $this->redirect($this->generateUrl('home_search'));
	}
	
	/**
     * @Route("/recherche/{page}/{sort}/{sens}", name="home_search", requirements={"page" = "\d+", "sens" = "\d+"}, defaults={"page" = "1", "sort" = "id", "sens" = ""})
     * @Template("CoreBundle:Home:search.html.twig")
     */
    public function searchAction($page, $sort, $sens, Request $request)
    {
	    $nbPerPage = HomeController::ITEMS_PAR_PAGE;
	    $queryBuilder = $this->search($request);

        $sort = "id";
        $sens = 0;
        
	    return $this->initPagination($page, $sort, $sens, $queryBuilder, 'home_search', $request);
    }
    
    public function search(Request $request){
    	
    	$search_filtre               = $request->getSession()->get('search_filtre');
    	$search_trie 				 = $request->getSession()->get('search_trie');
    	
    	$search_ou                   = $request->getSession()->get('search_ou');
    	$search_ou_location          = $request->getSession()->get('search_ou_location');
    	$search_date                 = $request->getSession()->get('search_date');
    	$search_category             = $request->getSession()->get('search_category');
		$search_prestation           = $request->getSession()->get('search_prestation');
		
		$queryBuilder = $this->getDoctrine()
						      ->getManager()
						      ->getRepository('UserBundle:User')
						      ->createQueryBuilder('u');
							  
		$queryBuilder->where('u.validate = 1 AND u.type = 2');
		
		if ($search_category){
			$category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation')->find($search_category);
			
			if ($category) {
				if (count($category->getMetiers())) {
					$sql = '';
					
					foreach ($category->getMetiers() as $metier) {
						if ($sql)$sql .= ' OR ';
						$sql .= 'u.metiers LIKE :metier'.$metier;
					}
					$queryBuilder->andWhere($sql);
					foreach ($category->getMetiers() as $metier) {
						$queryBuilder->setParameter('metier'.$metier, '%'.$metier.'%');
					}
					
				} else {
					$queryBuilder->andWhere('u.metiers = 9999');
				}				
			}
		}
		
		if ($search_prestation){
			$queryBuilder->join('u.offers', 'offers')->addSelect('offers');
			$queryBuilder->join('offers.prestation', 'prestation')->addSelect('prestation');
			$queryBuilder->leftJoin('prestation.parent', 'prestation_parent')->addSelect('prestation_parent');
			$queryBuilder->andWhere($queryBuilder->expr()->in('prestation.id', $search_prestation).' OR '.$queryBuilder->expr()->in('prestation_parent.id', $search_prestation));
		}
		
		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			
			$user = $this->getUser();
			
			if ($search_category == 1){
				foreach ($user->getCheveux() as $cheveux){
					if ($cheveux->getParent()->getMatching()){
						$queryBuilder->leftjoin('u.cheveux', 'cheveux'.$cheveux->getId())->addSelect('cheveux'.$cheveux->getId());
						$queryBuilder->andWhere($queryBuilder->expr()->in('cheveux'.$cheveux->getId().'.id', $cheveux->getId()));
					}
				}
			}
			
			if ($search_category == 20){
				foreach ($user->getPeau() as $peau){
					if ($peau->getParent()->getMatching()){
						$queryBuilder->leftjoin('u.peau', 'peau'.$peau->getId())->addSelect('peau'.$peau->getId());
						$queryBuilder->andWhere($queryBuilder->expr()->in('peau'.$peau->getId().'.id', $peau->getId()));
					}
				}
			}
			
		}

		if ($search_ou and !$search_ou_location){ 
			$search_mots = strtolower($search_ou);
		    $search_mots = $this->container->get('FAPROD.MyTools')->netoyage($search_mots);
		    $mots = explode(" ",$search_mots);
		    $nombre_mots=count($mots);
	    	
		    $string_query = 'u.postal_code LIKE :mot_0 OR u.city LIKE :mot_0 OR u.description LIKE :mot_0';
			
			$z=1;
			while($z<$nombre_mots) {
				$string_query .= ' OR u.postal_code LIKE :mot_'.$z.' OR u.city LIKE :mot_'.$z.' OR u.description LIKE :mot_'.$z;
	    		$z++;
			}
			
			$queryBuilder->andWhere($string_query);
			$queryBuilder->setParameter('mot_0', "%".$mots[0]."%");
			
			$z=1;
			while($z<$nombre_mots) {
	    		$queryBuilder->setParameter('mot_'.$z, "%".$mots[$z]."%");
	    		$z++;
			}
			$queryBuilder->addSelect('0 as distance');
		} else if ($search_ou and $search_ou_location){        	
			$array_suppr = array("(",")", " ");
			$gps = explode (",", str_replace($array_suppr, "", $search_ou_location));
			$latitude = $gps[0];
			$longitude = $gps[1];
			
			$request->getSession()->set('search_ou_latitude', $latitude);
			$request->getSession()->set('search_ou_longitude', $longitude);
			
			$formule = '( 6371 * acos(cos(radians(' . $latitude . '))' .
		                '* cos( radians( u.lat ) )' .
		                '* cos( radians( u.lng )' .
		                '- radians(' . $longitude . ') )' .
		                '+ sin( radians(' . $latitude . ') )' .
		                '* sin( radians( u.lat ) ) ) )';
			
			$queryBuilder
				->addSelect($formule.' as distance')
				->andWhere($formule.' <= u.distance');
		} else {
			$queryBuilder->addSelect('0 as distance');
			$request->getSession()->set('search_ou', '');
			$request->getSession()->set('search_ou_location', '');
			$request->getSession()->set('search_ou_latitude', '');
			$request->getSession()->set('search_ou_longitude', '');
			if (!$search_trie)$request->getSession()->set('search_trie', 1);
		}
		
		return $queryBuilder;
    }
	
	/**
     * @Route("/pro/newcomment/{user_id}", name="home_comment", requirements={"user_id" = "\d+"})
	 * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 */
	public function commentAction(User $user, Request $request){
		
		$form = $this->createForm(new ContentCommentType());
		$form->handleRequest($request);
		
		$repository_comment = $this->getDoctrine()->getManager()->getRepository('ContentBundle:ContentComment');
		$queryBuilder = $repository_comment->createQueryBuilder('c')
												  ->join('c.vendeur', 'v')->addSelect('v');

		$queryBuilder->where('c.email = :email')->setParameter('email', $form->get('email')->getData());
		$queryBuilder->andWhere('v.id = :vendeur_id')->setParameter('vendeur_id', $user->getId());
		$comment = $queryBuilder->setMaxResults('1')->getQuery()->getOneOrNullResult();
	
		if ($comment){
			$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Vous avez déjà noté ce vendeur.'));
		} else {
			
			if ($form->isSubmitted() and $form->isValid() and $request->request->get('bot') == 3) {
				$comment = $form->getData();
				$comment->setVendeur($user);
				$comment->setIpAddress($_SERVER['REMOTE_ADDR']);
				$comment->setValidate(0);
				
				$em = $this->getDoctrine()->getManager();
				$em->persist($comment);
				$em->flush();
			
				$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Commentaire enregistré.'));
			} else {
				$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur dans le formulaire.'));
			}
		}
	    
	    return $this->redirect($this->generateUrl('home_show', array('user_id' => $user->getId(), 't' => $user->getStrippedName())));
	}
	
	/**
     * @Route("/pro/{t}/{user_id}", name="home_show", requirements={"user_id" = "\d+"})
	 * @ParamConverter("user", options={"mapping": {"user_id": "id"}})
	 * @Template("CoreBundle:Home:show.html.twig")
	 */
	public function showAction(User $user, Request $request)
	{		
		
		if (!$this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
			return $this->redirect($this->generateUrl('register_cliente'));
		}
		
		if ($this->getUser()->getType() == 2) {
			return $this->redirect($this->generateUrl('register_cliente'));
		}
		
		$now = \DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d 00:00:00"));
		
		$repository_stat = $this->getDoctrine()->getManager()->getRepository('UserBundle:Stat');
    	$queryBuilder = $repository_stat->createQueryBuilder('s');
		$queryBuilder->where('s.user = :user')
					 ->andWhere('s.date = :date')
					 ->setParameter('user', $user)
					 ->setParameter('date', $now);
    	$stat = $queryBuilder->setMaxResults('1')->getQuery()->getOneOrNullResult();
    	
    	$em = $this->getDoctrine()->getManager();
    	
    	if ($stat){
    		$stat->setVue($stat->getVue() + 1);
    	} else {
    		$stat = new Stat();
    		$stat->setUser($user);
    		$stat->setVue(1);
    		$stat->setContact(0);
    		$stat->setDate($now);
    		
    		$em->persist($stat);
    	}
    	
    	$user->setVues($user->getVues() + 1);
    	$em->flush();
		
		$repository_offer = $this->getDoctrine()->getManager()->getRepository('UserBundle:Offer');
        $queryBuilder = $repository_offer->createQueryBuilder('o');
        $queryBuilder->where('s = :user')
                ->setParameter(':user', $user);
        $offers = $repository_offer->getOffers(null, null, 'id', 0, $queryBuilder);
    	
	  	return array(
	    	'user'            => $user,
			'offers'          => $offers,
	  	);
	}
	
	public function initPagination($page, $sort, $sens, $queryBuilder, $action, $request){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = HomeController::ITEMS_PAR_PAGE;
		
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('UserBundle:User')
	      ->getUsers($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;

	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    
	    $nb_result = $this->getDoctrine()
							->getManager()
						      ->getRepository('UserBundle:User')->getUsersCount($queryBuilder);
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
		  'nb_result'   => $nb_result,
	    );
    }

}
