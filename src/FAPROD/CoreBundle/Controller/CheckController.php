<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\CoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use FAPROD\CoreBundle\Lib\MyConst;

use FAPROD\OrderBundle\Entity\Log;

class CheckController extends Controller
{
	/**
     * @Route("/check", name="check_index")
     * @Template("CoreBundle:Check:index.html.twig")
	 */
    public function indexAction(Request $request)
    {
    	// lire le formulaire provenant du système PayPal et ajouter 'cmd'
	    $req = 'cmd=_notify-validate';
	    $log = '';
	    
	    foreach ($_POST as $key => $value) {
	    $log .= "$key=$value ";
	    $value = urlencode(stripslashes($value));
	    $req .= "&$key=$value";
	    }
	    
	    // renvoyer au système PayPal pour validation
	    $header = "";
	    $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
	    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
	    $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
	    $fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);
	
	    // affecter les variables du formulaire aux variables locales
	    $item_name = isset($_POST['item_name']) ? $_POST['item_name'] : "";
	    $item_number = isset($_POST['item_number']) ? $_POST['item_number'] : "";
	    $payment_status = isset($_POST['payment_status']) ? $_POST['payment_status'] : "";
	    $payment_amount = isset($_POST['mc_gross']) ? $_POST['mc_gross'] : "";
	    $payment_currency = isset($_POST['mc_currency']) ? $_POST['mc_currency'] : "";
	    $txn_id = isset($_POST['txn_id']) ? $_POST['txn_id'] : "";
	    $receiver_email = isset($_POST['receiver_email']) ? $_POST['receiver_email'] : "";
	    $payer_email = isset($_POST['payer_email']) ? $_POST['payer_email'] : "";
	    
	    $invoice = isset($_POST['invoice']) ? $_POST['invoice'] : "";
	    
	    $em = $this->getDoctrine()->getManager();
	    
	    if (!$fp) {
	    
	        $dblog = new Log();
	        $dblog->setLabel('HTTP ERROR');
	        $dblog->setDescription($log);
	        $dblog->setRef($invoice);

	        $em->persist($dblog);
	        $em->flush();
	    
	    } else {
			fputs ($fp, $header . $req);
			while (!feof($fp)) {
		    $res = fgets ($fp, 1024);
		    if (strcmp ($res, "VERIFIED") == 0) {
		    // vérifier que payment_status a la valeur Completed
		    // vérifier que txn_id n'a pas été précédemment traité
		    // vérifier que receiver_email est votre adresse email PayPal principale
		    // vérifier que payment_amount et payment_currency sont corrects
		    // traiter le paiement
		    
	        $dblog = new Log();
	        $dblog->setLabel('VERIFIED');
	        $dblog->setDescription($log);
	        $dblog->setRef($invoice);
	        
	        $em->persist($dblog);
	        $em->flush();
	        
	        if ($payment_status == 'Completed'){
		        	
		        $repository_order = $this->getDoctrine()->getManager()->getRepository('OrderBundle:Order');
    			$queryBuilder = $repository_order->createQueryBuilder('o');
				$queryBuilder->where('o.order_id = :order_id')->setParameter('order_id', $invoice);
	            $order = $queryBuilder->getQuery()->getOneOrNullResult();
	            
	            if ($order) {
	                
	                if ($receiver_email == $this->get('FAPROD.MyConst')->getPaypalEmail() and $payment_amount == $order->getTotalTtc() and $payment_currency == 'CHF'){
	            
	                    $order->setTxnId($txn_id);
	                    $order->setPaiement(2);
	                    
	                    $repository_option = $this->getDoctrine()->getManager()->getRepository('OrderBundle:Option');
    	
				    	$user = $order->getUser();
		
						$em->persist($user);
				    	
				    	if (count($order->getItems())){
    	
							$option = $repository_option->find($order->getItems()[0]->getOptionId());
							
							if ($user->getAboEndDate() < new \DateTime("now")){
								$user->setAboEndDate(new \DateTime("+ ".$option->getDuree()." months"));
							} else {
								$date = $user->getAboEndDate()->add(new \DateInterval('P'.$option->getDuree().'M'));
								$user->setAboEndDate(new \DateTime($date->format('Y-m-d')));
							}
											
							$mailBody = $this->renderView('AdminBundle:Order:mail_boutique.html.twig', array('user' => $user));
							$message = \Swift_Message::newInstance()
								->setSubject($this->get('translator')->trans('Votre abonnement est activé !'))
								->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
								->setTo($user->getEmail())
								->setBody($mailBody, 'text/html');
							//echo $mailBody;exit;
							$this->get('mailer')->send($message);
						
						}
						
						$order->setStatus(MyConst::ORDER_STATUS_TRAITEE);
	                    
	                    $dblog = new Log();
	                    $dblog->setLabel('CB OK');
	                    $dblog->setDescription('payment_status = '.$payment_status.'  receiver_email = '.$receiver_email.'  payment_amount = '.$payment_amount.'  payment_currency = '.$payment_currency);
	                    $dblog->setRef($invoice);
	                    
	                    $em->persist($dblog);
	                    $em->flush();
	                    
	                    $mailBody = $this->renderView('OrderBundle:Mail:paiement.html.twig');
				        $message = \Swift_Message::newInstance()
					        ->setSubject($this->get('translator')->trans('Paiement effectué'))
					        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
					        ->setTo($order->getUser()->getEmail())
					        ->setBody($mailBody, 'text/html');
					    //echo $mailBody;exit;
					    $this->get('mailer')->send($message);
	                    
	                } else {
	                    $dblog = new Log();
	                    $dblog->setLabel('FRAUDE');
	                    $dblog->setDescription('payment_status = '.$payment_status.'  receiver_email = '.$receiver_email.'  payment_amount = '.$payment_amount.'  payment_currency = '.$payment_currency);
	                    $dblog->setRef($invoice);
	                    $em->persist($dblog);
	                    $em->flush();
	                }
	
	            } else {
	                $dblog = new Log();
	                $dblog->setLabel('ORDER NOT FOUND');
	                $dblog->setDescription('');
	                $dblog->setRef($invoice);
	                $em->persist($dblog);
	        		$em->flush();
	            }
	            
	        } else {
	            $dblog = new Log();
	            $dblog->setLabel('PAYMENT ERROR');
	            $dblog->setDescription('payment_status = '.$payment_status);
	            $dblog->setRef($invoice);
	            $em->persist($dblog);
	        	$em->flush();
	        }
	    
	    }
	    else if (strcmp ($res, "INVALID") == 0) {
	    // consigner pour étude manuelle
	
	        $dblog = new Log();
	        $dblog->setLabel('INVALID');
	        $dblog->setDescription($log);
	        $dblog->setRef($invoice);
	        $em->persist($dblog);
	        $em->flush();
	        
	    }
	    }
	    fclose ($fp);
	    }
    }
}
