<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2009 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\CoreBundle\Lib;

class MyConst
{

const SITE_NAME = "my beauty booking";
const SITE_EMAIL = "contact@mybeautybooking.fr";
const PAYPAL_EMAIL = "contact@mybeautybooking.fr";
const SITE_URL = "https://www.mybeautybooking.fr/";

const KEY_GMAP = 'AIzaSyDXdduZyWHMEMK28MQlgDsfKRqLjE7d0z0';

const KEY_GMAP_DEV = 'AIzaSyD-sEM_-n6d8ZY3YTt--5ZHYmdZZ3P5_aI';

// PRODUCTION
const FACEBOOK_API = '348125272265930';
const FACEBOOK_SECRET = '18dc9cc5b775b71e5278a84bbeffffd6';
// TEST
const FACEBOOK_API_DEV = '126479931232442';
const FACEBOOK_SECRET_DEV = '40ffb508fc9dadb163612df9cfcdd1b2';

const FACEBOOK_SCOPE = 'public_profile,email';//publish_stream,publish_actions

const DUREE_PUBLICATION = 12;

const OPTION_ABONNEMENT = 1;
const OPTION_REABONNEMENT = 2;

const ORDER_ANNONCE = 1;
const ORDER_PUBLICITE = 2;
const ORDER_MEMBRE = 3;

const ORDER_STATUS_PAIEMENT_KO = 0;
const ORDER_STATUS_PAIEMENT_OK = 1;
const ORDER_STATUS_TRAITEE = 2;


// ------ MANGOPAY ---------------------------------------------------------

public static function getMangoConf(){
	$mangoconf = array();
	if ($_SERVER['HTTP_HOST'] == "127.0.0.1:8080") {		
		$mangoconf[0] = 'faprod';
		$mangoconf[1] = 'HHOVoPFZ2CGWh9RDGi8sHAyycc1mJCGfKDpaCCbmsWwoRH8TNL';
		$mangoconf[2] = __DIR__ . '/mango';
		$mangoconf[3] = 'https://api.sandbox.mangopay.com';
	}else{
		$mangoconf[0] = 'mybbprod2017';
		$mangoconf[1] = 'vr6QNugEUfYwvtkPiHsoWdHhQjExhjqVFuPhwdDpBeWTOCfdT1';
		$mangoconf[2] = __DIR__ . '/mango';
		$mangoconf[3] ='https://api.mangopay.com';
	}
	
	return $mangoconf;
}
public static function getCurrency(){
	
	$currency = 'EUR'; 
	return $currency;
}


//------ GLOBAL --------------------------------------------------------------------


public static function getKeyGmap(){
	
	if ($_SERVER['HTTP_HOST'] == "127.0.0.1:8080") {
    	return MyConst::KEY_GMAP_DEV;
    } else {
    	return MyConst::KEY_GMAP;
    }
}

public static function getWebDir()
{
	if ($_SERVER['HTTP_HOST'] == "mbbooking.faprod.com" or $_SERVER['HTTP_HOST'] == "127.0.0.1:8080") {
    	return 'web';
    } else {
    	return 'www';
    }
}

public static function getDureePublication(){
	return MyConst::DUREE_PUBLICATION;
}

public static function getSiteName(){
	return MyConst::SITE_NAME;
}

public static function getSiteEmail(){
	return MyConst::SITE_EMAIL;
}

public static function getPaypalEmail(){
	return MyConst::PAYPAL_EMAIL;
}

public static function getSiteUrl(){
	if ($_SERVER['HTTP_HOST'] == "127.0.0.1:8080") {
    	return "http://127.0.0.1:8080/projects/mbbooking/web/";
    } else {
    	return MyConst::SITE_URL;
    }
}

// ----- DOCUMENT ---------------------------------------------------------------

public static function getTypeDocument()
{
    $option = array();
	$options[1] = "Pièce d'identité";
	$options[2] = "RIB";
	$options[3] = "Assurance Pro";
	$options[4] = "Kbis ou relevé Insee";
	$options[5] = "Diplôme";
	$options[6] = "Book";
    
    return $options;
}

public static function getTypeDocumentLabel($status)
{
    $options = MyConst::getTypeDocument();  
    
    return isset($options[$status]) ? $options[$status] : '';
}

// ----- BOOKING ----------------------------------------------------------------


public static function getLength()
{
    $option = array();
    $options["1"] = '30 mins';
    $options["2"] = '1 heure';
    $options["3"] = '1 h 30 mins';
    $options["4"] = '2 heures';
    $options["5"] = '2 h 30 mins';
    $options["6"] = '3 heures';
    $options["7"] = '3 h 30 mins';
    $options["8"] = '4 heures';
 
    return $options;
}

public static function getLengthLabel($status)
{
    $options = MyConst::getLength();  
    
    return isset($options[$status]) ? $options[$status] : '';
}

public static function getBookingStatut()
{
    $option = array();
	$options["-1"] = 'Réservation annulée';
    $options["1"] = 'En attente du paiement';
    $options["2"] = 'En attente de validation';
    $options["3"] = 'Prestation à réaliser';
	$options["4"] = 'En attente du témoignage';
	$options["5"] = 'Prestation clôturée';
 
    return $options;
}

public static function getBookingStatutLabel($status)
{
    $options = MyConst::getBookingStatut();  
    
    return isset($options[$status]) ? $options[$status] : '';
}

//----- FACEBOOK ---------------------------------------------------------------------

public static function getFacebookApi()
{
	if ($_SERVER['HTTP_HOST'] == "mbbooking.faprod.com") {
    	return MyConst::FACEBOOK_API_DEV;
    } else {
    	return MyConst::FACEBOOK_API;
    }
}
public static function getFacebookSecret()
{
	if ($_SERVER['HTTP_HOST'] == "mbbooking.faprod.com") {
    	return MyConst::FACEBOOK_SECRET_DEV;
    } else {
    	return MyConst::FACEBOOK_SECRET;
    }
}

public static function getFacebookScope(){
	return MyConst::FACEBOOK_SCOPE;
}


//----- PUBLICITE ---------------------------------------------------------------------

public static function getTypesPub()
{
    $options = array();
    $options[1] = 'Horizontal 728 X 90';
    $options[2] = 'Vertical 120 X 600';
    
    return $options;
}

public static function getTypesPubLabel($status)
{
    $options = MyConst::getTypesPub();  
    
    return isset($options[$status]) ? $options[$status] : '';
}

public static function getCiblesPub()
{
    $options = array();
    $options[1] = 'Page accueil';
    $options[2] = 'Pages intérieures';
    $options[3] = 'Toutes les pages';
    
    return $options;
}

public static function getCiblesPubLabel($status)
{
    $options = MyConst::getCiblesPub();  
    
    return isset($options[$status]) ? $options[$status] : '';
}

public static function getStatutPub()
{
    $options = array();
    $options[0] = 'En attente';
    $options[2] = 'En production';
    
    return $options;
}

public static function getStatutPubLabel($status)
{
    $options = MyConst::getStatutPub();  
    
    return isset($options[$status]) ? $options[$status] : '';
}


//----- USER ---------------------------------------------------------------------

public static function getHeures()
{
    $options = array();
	$options['0'] = '00';
	$options['1'] = '01';
	$options['2'] = '02';
	$options['3'] = '03';
	$options['4'] = '04';
	$options['5'] = '05';
	$options['6'] = '06';
	$options['7'] = '07';
    $options['8'] = '08';
    $options['9'] = '09';
	$options['10'] = '10';
	$options['11'] = '11';
	$options['12'] = '12';
	$options['13'] = '13';
    $options['14'] = '14';
    $options['15'] = '15';
	$options['16'] = '16';
	$options['17'] = '17';
	$options['18'] = '18';
	$options['19'] = '19';
	$options['20'] = '20';
	$options['21'] = '21';
	$options['22'] = '22';
	$options['23'] = '23';
    
    return $options;
}

public static function getMinutes()
{
    $options = array();
	
	$options['0'] = '00';
	$options['30'] = '30';
    
    return $options;
}

public static function getStatutFiscal()
{
    $options = array();
	$options['1'] = 'Auto-Entrepreneur';
	$options['2'] = 'Salariée';
	$options['3'] = 'Autre';
    
    return $options;
}

public static function getStatutFiscalLabel($status)
{
    $options = MyConst::getStatutFiscal();  
    
    return isset($options[$status]) ? $options[$status] : '';
}

public static function getDistance()
{
    $options = array();
	$options['5'] = '5 Km';
	$options['10'] = '10 Km';
	$options['15'] = '15 Km';
	$options['25'] = '25 Km';
	$options['50'] = '50 Km';
	$options['100'] = '100 Km';
    
    return $options;
}

public static function getDistanceLabel($status)
{
    $options = MyConst::getDistance();  
    
    return isset($options[$status]) ? $options[$status] : '';
}

public static function getTypeUser()
{
    $options = array();
    $options['1'] = 'Cliente';
    $options['2'] = 'Professionnel';
    
    return $options;
}

public static function getTypeUserLabel($status)
{
    $options = MyConst::getTypeUser();  
    
    return isset($options[$status]) ? $options[$status] : '';
}

public static function getMetier()
{	
	$options = array();
	$options['1'] = 'Hair Artist Pro';
	$options['2'] = 'Hair Artist Design';
	$options['3'] = 'Make-up Artist';
	$options['4'] = 'Eyes Artist';
	$options['5'] = 'Nails Artist';
	$options['6'] = 'Body Artist Esthétic';
	$options['7'] = 'Body Artist Massage';
    
    return $options;
}

public static function getMetierLabel($status)
{
    $options = MyConst::getMetier();  
    
    return isset($options[$status]) ? $options[$status] : '';
}




}