<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2009 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\CoreBundle\Lib\Form;
 
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
 
class EntityToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;
 
    /**
     * @var string
     */
    protected $class;
 
    public function __construct(ObjectManager $objectManager, $class)
    {
        $this->objectManager = $objectManager;
        $this->class = $class;
    }
 
    public function transform($entity)
    {
        if (null === $entity) {
            return;
        }
 
        return $entity->getId();
    }
 
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
 
        $entity = $this->objectManager
                       ->getRepository($this->class)
                       ->find($id);
 
        if (null === $entity) {
            throw new TransformationFailedException();
        }
 
        return $entity;
    }
}