<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2009 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\CoreBundle\Lib\Twig;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TwigExtension extends \Twig_Extension
{
	protected $container;
	protected $request;
	protected $environment;
	
	public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
        if (PHP_SAPI != 'cli' )$this->request = $this->container->get("request");
    }
	
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('base64_encode', array($this, 'base64Encode')),
            new \Twig_SimpleFilter('truncate', array($this, 'truncate')),
            new \Twig_SimpleFilter('strip', array($this, 'strip')),
        );
    }
    
    public function getFunctions()
    {
        return array(
           'base64_encode'        => new \Twig_Function_Method($this, 'base64Encode'),
           'getFoLangues'         => new \Twig_Function_Method($this, 'getFoLangues'),
           'getBoLangues'         => new \Twig_Function_Method($this, 'getBoLangues'),
           'getSiteName'          => new \Twig_Function_Method($this, 'getSiteName'),
           'getSiteEmail'         => new \Twig_Function_Method($this, 'getSiteEmail'),
           'getPaypalEmail'       => new \Twig_Function_Method($this, 'getPaypalEmail'),
           'getSiteUrl'           => new \Twig_Function_Method($this, 'getSiteUrl'),
           'getControllerName'    => new \Twig_Function_Method($this, 'getControllerName'),
           'getActionName'        => new \Twig_Function_Method($this, 'getActionName'),
           'getControllerAction'  => new \Twig_Function_Method($this, 'getControllerAction'),
           'isHomeIndex'          => new \Twig_Function_Method($this, 'isHomeIndex'),
           'getMetaTitle'         => new \Twig_Function_Method($this, 'getMetaTitle'),
           'getMetaDescription'   => new \Twig_Function_Method($this, 'getMetaDescription'),
           'getMetaKeywords'      => new \Twig_Function_Method($this, 'getMetaKeywords'),
           'getGmapApi'           => new \Twig_Function_Method($this, 'getGmapApi'),
           'getFacebookApi'       => new \Twig_Function_Method($this, 'getFacebookApi'),
           'getFacebookSecret'    => new \Twig_Function_Method($this, 'getFacebookSecret'),
           'getFacebookScope'     => new \Twig_Function_Method($this, 'getFacebookScope'),
		   'getHeures'            => new \Twig_Function_Method($this, 'getHeures'),
		   'getMinutes'           => new \Twig_Function_Method($this, 'getMinutes'),
        );
	}
	
	public function getHeures()
    {
    	return $this->container->get('FAPROD.MyConst')->getHeures();
	}
	
	public function getMinutes()
    {
    	return $this->container->get('FAPROD.MyConst')->getMinutes();
	}
	
	public function getGmapApi()
    {
    	return $this->container->get('FAPROD.MyConst')->getKeyGmap();
	}
	
	public function getFacebookApi()
    {
    	return $this->container->get('FAPROD.MyConst')->getFacebookApi();
	}
	
	public function getFacebookSecret()
    {
    	return $this->container->get('FAPROD.MyConst')->getFacebookSecret();
	}
	
	public function getFacebookScope()
    {
    	return $this->container->get('FAPROD.MyConst')->getFacebookScope();
	}
	
	public function truncate($string, $max)
    {
    	return $this->container->get('FAPROD.MyTools')->trunc($string, $max);
	}
	
	public function strip($string)
    {
    	return $this->container->get('FAPROD.MyTools')->stripText($string);
	}
	
	public function getMetaTitle()
    {
    	$meta = $this->container->get('session')->get('meta_title');
    	
    	if (!$meta){
	        $meta = $this->container->get('doctrine')
	        		->getManager()
	        		->getRepository('AdminBundle:Configs')
		        	->getMetaTitle();
		        	
		    $this->container->get('session')->set('meta_title', $meta);
    	}
    	
	    return $meta;
    }
    
    public function getMetaDescription()
    {
    	$meta = $this->container->get('session')->get('meta_description');
    	
    	if (!$meta){
	        $meta =  $this->container->get('doctrine')
	        		->getManager()
	        		->getRepository('AdminBundle:Configs')
		        	->getMetaDescription();
		    
		    $this->container->get('session')->set('meta_description', $meta);
    	}
	    
	    return $meta;
    }
    
    public function getMetaKeywords()
    {
    	$meta = $this->container->get('session')->get('meta_keywords');
    	
    	if (!$meta){
	        $meta = $this->container->get('doctrine')
	        		->getManager()
	        		->getRepository('AdminBundle:Configs')
		        	->getMetaKeywords();
		        	
		    $this->container->get('session')->set('meta_keywords', $meta);
    	}
	    
	    return $meta;
    }
	
	public function getControllerName()
	{
		$regexp = "#Controller\\\([a-zA-Z]*)Controller#";
		$results = array();
		preg_match($regexp, $this->request->get('_controller'), $results);
		
		return isset($results[1]) ? $results[1] : '';
	}
	
	public function getActionName()
    {
	    $regexp = "#::([a-zA-Z]*)Action#";
	    $results = array();
	    preg_match($regexp, $this->request->get('_controller'), $results);
	    
	    return isset($results[1]) ? $results[1] : '';
    }
    
    public function getControllerAction()
    {
    	return $this->getControllerName().'/'.$this->getActionName();
	}

    public function base64Encode($string)
    {
        return base64_encode($string);
    }
    
    public function getFoLangues()
    {
        return $this->container->get('doctrine')
        		->getManager()
        		->getRepository('AdminBundle:Configs')
	        	->getFoLangues();
    }
    
    public function getBoLangues()
    {
        return $this->container->get('doctrine')
        		->getManager()
        		->getRepository('AdminBundle:Configs')
	        	->getBoLangues();
    }
    
    public function getSiteName(){
    	return $this->container->get('FAPROD.MyConst')->getSiteName();
    }
    
    public function getSiteEmail(){
    	return $this->container->get('FAPROD.MyConst')->getSiteEmail();
    }
    
    public function getPaypalEmail(){
    	return $this->container->get('FAPROD.MyConst')->getPaypalEmail();
    }
    
    public function getSiteUrl(){
    	return $this->container->get('FAPROD.MyConst')->getSiteUrl();
    }
    
    public function isHomeIndex(){
    	if ($this->getControllerAction() == "Home/index"){
    		return true;
    	} else {
    		return false;
		}
    }

    public function getName()
    {
        return 'FAPROD.TwigExtension';
    }
}