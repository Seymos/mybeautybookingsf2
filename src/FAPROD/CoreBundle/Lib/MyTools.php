<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2009 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\CoreBundle\Lib;

class MyTools
{
	


public static function getCodeYoutube($text) {
    // No youtube? Not worth processing the text.
    if ((stripos($text, 'youtube.') === false) && (stripos($text, 'youtu.be') === false)) {
        return $text;
    }

    $search = '~
        # Match non-linked youtube URL in the wild. (Rev:20130823)
        (?:https?://)?    # Optional scheme.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube         # or youtube.com or
          (?:-nocookie)?  # youtube-nocookie.com
          \.com           # followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\s-]        # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w-]{11})       # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w-]|$)      # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w.-]*    # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w.-]*      # Consume any URL (query) remainder.
        ~ix';

    $replace = '$1';
    $text = preg_replace($search, $replace, $text);

    return $text;
}

public static function generateVideoEmbeds($text, $width = 560, $height = 315) {
	$text = MyTools::getCodeYoutube($text);
    $replace = '<iframe width="'.$width.'" height="'.$height.'" src="https://www.youtube.com/embed/'.$text.'" frameborder="0" allowfullscreen></iframe>';
    return $replace;
}
public static function generateVideoImg($text, $width = 80, $height = 60) {
	$text = MyTools::getCodeYoutube($text);
    $replace = '<img style="margin:5px;" width="'.$width.'" height="'.$height.'" src="'.MyTools::generateVideoImgUrl($text).'">';
    return $replace;
}
public static function generateVideoImgUrl($text) {
	return "http://img.youtube.com/vi/".MyTools::getCodeYoutube($text)."/0.jpg";
}

public static function rotateImage($file, $angle){
	 if (!strpos(strtolower($file),'.jpg')===false or !strpos(strtolower($file),'.jpeg')===false){
		$image = imagecreatefromjpeg($file);
		$blanc = imagecolorallocate($image, 128, 117, 122);
		$image = imagerotate($image, $angle, $blanc);
		imagejpeg($image,$file);
	} elseif (!strpos(strtolower($file),'.png')===false){
		$image = imagecreatefrompng($file);
		$blanc = imagecolorallocate($image, 128, 117, 122);
		$image = imagerotate($image, $angle, $blanc);
		imagepng($image,$file);
	} elseif (!strpos(strtolower($file),'.gif')===false){
		$image = imagecreatefromgif($file);
		$blanc = imagecolorallocate($image, 128, 117, 122);
		$image = imagerotate($image, $angle, $blanc);
		imagegif($image,$file);
	}
}

public static function GetExtensionName($File, $Dot)
{
  if ($Dot == true) { $Ext = strtolower(substr($File, strrpos($File, '.')));}
  else                     { $Ext = strtolower(substr($File, strrpos($File, '.') + 1));}
  return $Ext;
}

public static function format_tel($tel)
{
   //$tel=preg_replace("[^0-9]","",$tel); 
   //$tel = wordwrap ($tel, 2, '.', 1);
   return $tel;    
} 

public static function changeDate($date){
	$date = trim($date);
	$date = explode('/',$date);
	if (count($date) == 3){
		return $date[2].'/'.$date[1].'/'.$date[0];
	} else {
		return date('Y/m/d');
	}
	
}

public static function netoyage($mot)
{

if (substr($mot,0,4) == "les ")
{
   $mot = substr($mot,4);
}
if (substr($mot,0,3) == "le ")
{
   $mot = substr($mot,3);
}
if (substr($mot,0,3) == "la ")
{
   $mot = substr($mot,3);
}
if (substr($mot,0,3) == "de ")
{
   $mot = substr($mot,3);
}
if (substr($mot,0,4) == "des ")
{
   $mot = substr($mot,4);
}
if (substr($mot,0,3) == "un ")
{
   $mot = substr($mot,3);
}
if (substr($mot,0,4) == "une ")
{
   $mot = substr($mot,4);
}


		$mot = str_replace(";", "", $mot);
		//$mot = str_replace("-", "", $mot);
		$mot = str_replace("\"", "", $mot);
		$mot = str_replace("<", "", $mot);
		$mot = str_replace(">", "", $mot);
		$mot = str_replace("%", "", $mot);
		$mot = str_replace("?", "", $mot);
		$mot = str_replace("&", "", $mot);
		$mot = str_replace(":", "", $mot);
		$mot = str_replace("...", "", $mot);
		$mot = str_replace(".", "", $mot);
		$mot = str_replace("!", "", $mot);
		$mot = str_replace("?", "", $mot);
		$mot = str_replace(",", "", $mot);
		$mot = str_replace("d'", "", $mot);
		$mot = str_replace("l'", "", $mot);
		$mot = str_replace("s'", "", $mot);
		$mot = str_replace("c'", "", $mot);
		$mot = str_replace(' le ', ' ', $mot);
		$mot = str_replace(' en ', ' ', $mot);
		$mot = str_replace(' la ', ' ', $mot);
		$mot = str_replace(' les ', ' ', $mot);
		$mot = str_replace(' à ', ' ', $mot);
		$mot = str_replace(' a ', ' ', $mot);
		$mot = str_replace(' au ', ' ', $mot);
		$mot = str_replace(' de ', ' ', $mot);
		$mot = str_replace(' des ', ' ', $mot);
		$mot = str_replace(' vos ', ' ', $mot);
		$mot = str_replace(' et ', ' ', $mot);
		$mot = str_replace(' aux ', ' ', $mot);
		$mot = str_replace(' sa ', ' ', $mot);
		$mot = str_replace(' ces ', ' ', $mot);
		$mot = str_replace(' ses ', ' ', $mot);
		$mot = str_replace(' son ', ' ', $mot);
		$mot = str_replace(' dans ', ' ', $mot);
		$mot = str_replace(' pour ', ' ', $mot);
		$mot = str_replace(' du ', ' ', $mot);
		$mot = str_replace(' un ', ' ', $mot);
		$mot = str_replace(' une ', ' ', $mot);
		$mot = str_replace(' sur ', ' ', $mot);
		$mot = str_replace(' est ', ' ', $mot);
		$mot = str_replace(' avec ', ' ', $mot);
		$mot = str_replace(' ont ', ' ', $mot);
		$mot = str_replace(' été ', ' ', $mot);
		$mot = str_replace(' mon ', ' ', $mot);
		$mot = str_replace(' ta ', ' ', $mot);
		$mot = str_replace(' tes ', ' ', $mot);
		$mot = str_replace(' mes ', ' ', $mot);
		$mot = str_replace(' il ', ' ', $mot);
		$mot = str_replace(' elle ', ' ', $mot);
		$mot = str_replace(' je ', ' ', $mot);
		$mot = str_replace(' tu ', ' ', $mot);
		$mot = str_replace(' nous ', ' ', $mot);
		$mot = str_replace(' vous ', ' ', $mot);
		$mot = str_replace(' ils ', ' ', $mot);
		$mot = str_replace(' elles ', ' ', $mot);
		$mot = str_replace(' leur ', ' ', $mot);
		$mot = str_replace(' que ', ' ', $mot);
		$mot = str_replace(' par ', ' ', $mot);
		
		$mot = str_replace("'", "", $mot);
		
return $mot;
}

public static function stripText($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}


/**
 *  Formate une chaîne de caractère pour la tronquer si besoin
 *
 *  Si la longueur de la chaîne est dépassé, le fonction coupe celle-ci puis
 *  ajoute la séquence "..." 
 *
 *  @param string $string Le texte à tronquer
 *  @param int    $max    Le nombre de caractères maximum
 *
 *  @return string        Le texte tronqué si besoin
 *
 **/
public static function trunc($string, $max)
{
    if (strlen($string) > $max) {
        $string = substr($string, 0, $max).'...';
    }
    return $string;
}



}


