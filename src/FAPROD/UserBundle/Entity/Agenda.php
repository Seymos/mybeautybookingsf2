<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="FAPROD\UserBundle\Entity\AgendaRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="agenda")
 */
class Agenda
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
  
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
	
	/**
     * @ORM\ManyToOne(targetEntity="FAPROD\BookingBundle\Entity\Booking")
     * @ORM\JoinColumn(name="booking_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $booking;
    
    /**
     * @ORM\Column(name="heure", type="integer", nullable=true)
     */
    private $heure;
	
	/**
     * @ORM\Column(name="minute", type="integer", nullable=true)
     */
    private $minute;
    
    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $date;
  
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {

    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}

    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getDate($format = '')
    {
    	if($this->date and $this->date instanceof \DateTime and $format){
		    return $this->date->format($format);
		} else {
    		return $this->date;
    	}
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setHeure($heure)
    {
        $this->heure = $heure;

        return $this;
    }

    public function getHeure()
    {
        return $this->heure;
    }

    public function setMinute($minute)
    {
        $this->minute = $minute;

        return $this;
    }

    public function getMinute()
    {
        return $this->minute;
    }

    public function setBooking(\FAPROD\BookingBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    public function getBooking()
    {
        return $this->booking;
    }
}
