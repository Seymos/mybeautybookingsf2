<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="categorytranslation")
 */
class CategoryTranslation extends AbstractTranslation
{
	
	/**
     * @Prezent\Translatable(targetEntity="Category")
     */
    protected $translatable;
    
	/**
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;
	
	/**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=150, nullable=true)
     */
    private $stripped_title;
    
    public static function getTranslationEntityClass()
    {
        return 'CategoryTranslation';
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setStrippedTitle($strippedTitle)
    {
        $this->stripped_title = $strippedTitle;

        return $this;
    }

    public function getStrippedTitle()
    {
        return $this->stripped_title;
    }
	
	public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

}
