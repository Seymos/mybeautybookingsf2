<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @prestation   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * PrestationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PrestationRepository extends EntityRepository
{
	public function getPrestations($page = null, $nbPerPage = null, $sort = 'id', $sens = 0, $QueryBuilder = '')
    {
    	if (!$QueryBuilder)$QueryBuilder = $this->createQueryBuilder('p');
    	
    	$QueryBuilder
				->leftJoin('p.children', 'sub')->addSelect('sub')
				->leftJoin('sub.translations', 'sub_trans')->addSelect('sub_trans')
    			->join('p.translations', 'trans')->addSelect('trans');

	    if ($sort == 'title'){
	    	$QueryBuilder->orderBy('trans.'.$sort, $sens ? 'ASC' : 'DESC');
	    } else {
	    	$QueryBuilder->orderBy('p.'.$sort, $sens ? 'ASC' : 'DESC');
	    }
		    		
		$query = $QueryBuilder->getQuery();
		
		if ($page and $nbPerPage){
		    $query
		      ->setFirstResult(($page-1) * $nbPerPage)
		      ->setMaxResults($nbPerPage)
		    ;
		    
		    return new Paginator($query, true);
    	} else {
    		return $query->getResult();
    	}   
    }
}
