<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use FAPROD\CoreBundle\Lib\MyConst;
use FAPROD\CoreBundle\Lib\MyTools;

/**
 * @ORM\Entity(repositoryClass="FAPROD\UserBundle\Entity\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="userimage")
 */
class Image
{
	const IMG_THUMB_WIDTH = 200;
	const IMG_THUMB_HEIGHT = 130;
	const IMG_PHOTO_WIDTH = 250;
	const IMG_PHOTO_HEIGHT = 150;
	const IMG_ZOOM_WIDTH = 600;
	const IMG_ZOOM_HEIGHT = 600;
	
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
      * @ORM\ManyToOne(targetEntity="User", inversedBy="images", cascade={"persist"})
      * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
      */
    private $user;
	
	/**
      * @ORM\ManyToOne(targetEntity="Offer", inversedBy="images", cascade={"persist"})
      * @ORM\JoinColumn(name="offer_id", referencedColumnName="id", onDelete="CASCADE")
      */
    private $offer;
    
    /**
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    private $filename;
    
    /**
     * @ORM\Column(name="ext", type="string", length=25, nullable=true)
     */
    private $ext;
    
    /**
     * @ORM\Column(name="alt", type="string", length=255, nullable=true)
     */
    private $alt;
    
    private $tempFilename;
    private $tempFilename_photo;
    private $tempFilename_zoom;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    public function __toString()
    {
		return $this->getFilename();
    }
    
    public function __construct()
    {
    	$this->filename = 'no_image.png';
	}
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
	
	public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }
    
    public function rotateImage($angle, $title){
    	
    	$new_filename = $title.'.'.date('is').MyTools::GetExtensionName($this->getFilename(), true);
    	
    	$tempFilename = $this->getUploadRootDir().'/thumb/u'.$this->id.'.'.$this->getFilename();
        //$tempFilename_photo = $this->getUploadRootDir().'/photo/photo_u'.$this->id.'.'.$this->getFilename();
        $tempFilename_zoom = $this->getUploadRootDir().'/zoom/zoom_u'.$this->id.'.'.$this->getFilename();
        
        $new_tempFilename = $this->getUploadRootDir().'/thumb/u'.$this->id.'.'.$new_filename;
        //$new_tempFilename_photo = $this->getUploadRootDir().'/photo/photo_u'.$this->id.'.'.$new_filename;
        $new_tempFilename_zoom = $this->getUploadRootDir().'/zoom/zoom_u'.$this->id.'.'.$new_filename;
        
        MyTools::rotateImage($tempFilename, $angle);
        //MyTools::rotateImage($tempFilename_photo, $angle);
        MyTools::rotateImage($tempFilename_zoom, $angle);
      
    	copy($tempFilename,$new_tempFilename);
		unlink($tempFilename);
		//copy($tempFilename_photo,$new_tempFilename_photo);
		//unlink($tempFilename_photo);
		copy($tempFilename_zoom,$new_tempFilename_zoom);
		unlink($tempFilename_zoom);
		
		$this->setFilename($new_filename);
	}

    public function upload($file_temp, $title)
    {

	  	if (file_exists($this->getUploadRootDir().'/temp/1_'.$file_temp) and file_exists($this->getUploadRootDir().'/temp/2_'.$file_temp)) {
	  	
	  		$filename = MyTools::stripText($title).'.'.date('is').MyTools::GetExtensionName($file_temp, true);
			$fichier_destination = 'u'.$this->id.'.'.$filename;
			  
		    // Si on avait un ancien fichier, on le supprime
		    $oldFile = $this->getUploadRootDir().'/thumb/'.$fichier_destination;
	        if (file_exists($oldFile)) {
	          unlink($oldFile);
	        }
	        //$oldFile = $this->getUploadRootDir().'/photo/photo_'.$fichier_destination;
	        //if (file_exists($oldFile)) {
	        //  unlink($oldFile);
	        //}
	        $oldFile = $this->getUploadRootDir().'/zoom/zoom_'.$fichier_destination;
	        if (file_exists($oldFile)) {
	          unlink($oldFile);
	        }
		      
		    copy($this->getUploadRootDir().'/temp/1_'.$file_temp, $this->getUploadRootDir().'/thumb/'.$fichier_destination);	      
		    //copy($this->getUploadRootDir().'/temp/2_'.$file_temp, $this->getUploadRootDir().'/photo/photo_'.$fichier_destination);
		    copy($this->getUploadRootDir().'/temp/2_'.$file_temp, $this->getUploadRootDir().'/zoom/zoom_'.$fichier_destination);
		    
		    //on efface le fichier temporaire
		    unlink($this->getUploadRootDir().'/temp/1_'.$file_temp);
		    unlink($this->getUploadRootDir().'/temp/2_'.$file_temp);
		    //unlink($this->getUploadRootDir().'/temp/3_'.$file_temp);
		    
		    $this->filename = $filename;
		    $this->ext = MyTools::GetExtensionName($file_temp, false);
      
  		}
    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
      $new_tempFilename = $this->getUploadRootDir().'/thumb/u'.$this->id.'.'.$this->filename;
      $new_tempFilename_zoom = $this->getUploadRootDir().'/zoom/zoom_u'.$this->id.'.'.$this->filename;
		
      // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
      if (file_exists($new_tempFilename) and $this->filename != 'no_image.png') {
        // On supprime le fichier
        unlink($new_tempFilename);
      }
      if (file_exists($new_tempFilename_zoom) and $this->filename != 'no_image.png') {
        // On supprime le fichier
        unlink($new_tempFilename_zoom);
      }
    }

    public function getUploadDir()
    {
      // On retourne le chemin relatif vers l'image pour un navigateur
      return 'uploads';
    }

    public function getUploadRootDir()
    {
      // On retourne le chemin relatif vers l'image pour notre code PHP
      return __DIR__.'/../../../../'.MyConst::getWebDir().'/'.$this->getUploadDir();
    }
    
    public function getUrl()
    {
    	return MyConst::getSiteUrl().$this->getWebPath();
	}
	
	public function getUrlPhoto()
    {
    	return MyConst::getSiteUrl().$this->getWebPathPhoto();
	}
	
	public function getUrlZoom()
    {
    	return MyConst::getSiteUrl().$this->getWebPathZoom();
	}
  
    public function getWebPath()
    {
    	if ($this->getFilename() and $this->getFilename() != 'no_image.png'){
    		return $this->getUploadDir().'/thumb/'.'u'.$this->id.'.'.$this->getFilename();
    	} else {
    		return $this->getUploadDir().'/no_image.png';
    	}
    }
    
    public function getWebPathPhoto()
    {
    	if ($this->getFilename() and $this->getFilename() != 'no_image.png'){
    		return $this->getUploadDir().'/photo/photo_'.'u'.$this->id.'.'.$this->getFilename();
    	} else {
    		return $this->getUploadDir().'/no_image.png';
    	}
    }
    
    public function getWebPathZoom()
    {
    	if ($this->getFilename() and $this->getFilename() != 'no_image.png'){
    		return $this->getUploadDir().'/zoom/zoom_'.'u'.$this->id.'.'.$this->getFilename();
    	} else {
    		return $this->getUploadDir().'/no_image.png';
    	}
    }

    public function getId()
    {
        return $this->id;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    public function getAlt()
    {
        return $this->alt;
    }

    public function setExt($ext)
    {
        $this->ext = $ext;

        return $this;
    }

    public function getExt()
    {
        return $this->ext;
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setOffer(\FAPROD\UserBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    public function getOffer()
    {
        return $this->offer;
    }
}
