<?php
 
namespace FAPROD\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslatable;
use FAPROD\CoreBundle\Lib\MyConst;
use Doctrine\ORM\Mapping\OrderBy;


/**
 * @Gedmo\Tree(type="nested")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="FAPROD\UserBundle\Entity\OfferRepository")
 * @ORM\Table(name="offer")
 */
class Offer extends AbstractTranslatable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
	/**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User", inversedBy="offers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
	
	/**
     * @ORM\OneToMany(targetEntity="FAPROD\BookingBundle\Entity\Booking", mappedBy="user")
     * @OrderBy({"id" = "DESC"})
     */
    private $booking;
	
	/**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\Prestation")
     * @ORM\JoinColumn(name="prestation_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $prestation;
	
	/**
     * @ORM\Column(name="metier", type="integer", nullable=true)
     */
    private $metier;
	
	/**
     * @Prezent\Translations(targetEntity="OfferTranslation")
     */
    protected $translations;
	
	/**
     * @Prezent\CurrentLocale
     */
    private $currentLocale;
	
    /**
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;
	
	/**
     * @ORM\Column(name="duree", type="integer", nullable=true)
     */
    private $duree;
	
	/**
     * @ORM\OneToMany(targetEntity="FAPROD\UserBundle\Entity\Image", mappedBy="offer", cascade={"persist"})
     * @OrderBy({"id" = "DESC"})
     * @Assert\Valid()
     */
    private $images;
	
	/**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;
	
    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    private $currentTranslation; // Cache current translation. Useful in Doctrine 2.4+
	
	public function __construct()
    {
		$this->translations = new ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString()
    {
		return $this->getTitle();
    }
	
    public function setTitle($title)
    {
    	if (!$title)$title = $this->getTitle('fr');
        return $this;
    }

    public function getTitle($locale = null)
    {
        if ($this->prestation){
			return $this->prestation->getTitle();
		} else {
			return "NC";
		}
    }
	
    public function setDescription($description)
    {
        $this->translate()->setDescription($description);
        return $this;
    }

    public function getDescription($locale = null)
    {
        return $this->translate($locale)->getDescription();
    }
	
	/**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}

    public function getId()
    {
        return $this->id;
    }
	
	public function setPrice($price)
    {
        //$commission = round($com,2);
        $commission = .13 * $price;
        // tva
        $tva = .2*$commission;
        //ht
        $ht = $price + $commission;
        // ttc
        $ttc = number_format($ht + $tva, 2);
        // set
        $this->price = $ttc;
    }

    public function getPrice()
    {
        return $this->price;
    }
	
	public function getPriceLabel()
    {
       if ($this->getPrice())
          return number_format($this->getPrice(), 2, '.', ' ').' €';
       else return '';
    }
	
	public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
            return $this->updated_at->format($format);
        } else {
            return $this->updated_at;
        }
    }

	public function getCreatedAt($format = '')
    {
        if($this->created_at and $this->created_at instanceof \DateTime and $format){
            return $this->created_at->format($format);
        } else {
            return $this->created_at;
        }
    }
	
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
        return $this;
    }
	
    public static function getTranslationEntityClass()
    {
        return 'FAPROD\UserBundle\Entity\OfferTranslation';
    }
    
    public function translate($locale = null)
    {
        if (null === $locale) {
            $locale = $this->currentLocale;
        }

        if (!$locale) {
            throw new \RuntimeException('No locale has been set and currentLocale is empty');
        }

        if ($this->currentTranslation && $this->currentTranslation->getLocale() === $locale) {
            return $this->currentTranslation;
        }

        if (!$translation = $this->translations->get($locale)) {
            $translation = new AdvertTranslation();
            $translation->setLocale($locale);
            $this->addTranslation($translation);
        }

        $this->currentTranslation = $translation;
        return $translation;
    }
	
	public function addBooking(\FAPROD\BookingBundle\Entity\Booking $booking)
    {
        $this->bookings[] = $booking;
        
		$booking->setUser($this);

        return $this;
    }

    public function removeBooking(\FAPROD\BookingBundle\Entity\Booking $bookings)
    {
        $this->bookings->removeElement($bookings);
    }

    public function getBookings()
    {
        return $this->bookings;
    }

    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    public function getDuree()
    {
        return $this->duree;
    }
	
	public function getDureeLabel()
    {
        return MyConst::getLengthLabel($this->duree);
    }

    public function getBooking()
    {
        return $this->booking;
    }
	
	public function addImage(\FAPROD\UserBundle\Entity\Image $image)
    {
        $this->images[] = $image;

		$image->setOffer($this);
		
        return $this;
    }

    public function removeImage(\FAPROD\UserBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setPrestation(\FAPROD\UserBundle\Entity\Prestation $prestation = null)
    {
        $this->prestation = $prestation;

        return $this;
    }

    public function getPrestation()
    {
        return $this->prestation;
    }

    public function setMetier($metier)
    {
        $this->metier = $metier;

        return $this;
    }

    public function getMetier()
    {
        return $this->metier;
    }
}
