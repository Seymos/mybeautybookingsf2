<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity(repositoryClass="FAPROD\UserBundle\Entity\HoraireRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="horaire")
 */
class Horaire
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User", inversedBy="horaires")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
	
	/**
     * @ORM\Column(name="jour", type="integer", nullable=true)
     */
    private $jour;
    
    /**
     * @ORM\Column(name="heure_deb", type="integer", nullable=true)
     */
    private $heure_deb;
    
    /**
     * @ORM\Column(name="minute_deb", type="integer", nullable=true)
     */
    private $minute_deb;
	
	/**
     * @ORM\Column(name="heure_fin", type="integer", nullable=true)
     */
    private $heure_fin;
    
    /**
     * @ORM\Column(name="minute_fin", type="integer", nullable=true)
     */
    private $minute_fin;
  
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {

    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}

    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setJour($jour)
    {
        $this->jour = $jour;

        return $this;
    }

    public function getJour()
    {
        return $this->jour;
    }

    public function setHeureDeb($heureDeb)
    {
        $this->heure_deb = $heureDeb;

        return $this;
    }

    public function getHeureDeb()
    {
        return $this->heure_deb;
    }

    public function setMinuteDeb($minuteDeb)
    {
        $this->minute_deb = $minuteDeb;

        return $this;
    }

    public function getMinuteDeb()
    {
        return $this->minute_deb;
    }

    public function setHeureFin($heureFin)
    {
        $this->heure_fin = $heureFin;

        return $this;
    }

    public function getHeureFin()
    {
        return $this->heure_fin;
    }

    public function setMinuteFin($minuteFin)
    {
        $this->minute_fin = $minuteFin;

        return $this;
    }

    public function getMinuteFin()
    {
        return $this->minute_fin;
    }
}
