<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @prestation   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslatable;

use FAPROD\CoreBundle\Lib\MyConst;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="FAPROD\UserBundle\Entity\PrestationRepository")
 * @ORM\Table(name="prestation")
 */
class Prestation extends AbstractTranslatable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Prezent\Translations(targetEntity="PrestationTranslation")
     */
    protected $translations;
    
    /**
     * @Prezent\CurrentLocale
     */
    private $currentLocale;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="no_order", type="integer", nullable=true)
     */
    private $no_order;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Prestation", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Prestation", mappedBy="parent")
     * @ORM\OrderBy({"no_order" = "ASC"})
     */
    private $children;
	
	/**
     * @ORM\Column(name="metiers", type="string", length=100, nullable=true)
     */
    private $metiers;
  
    /**
     * @ORM\Column(name="is_publish", type="boolean", nullable=true)
     */
    private $is_publish;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    // Getters et setters

    private $currentTranslation; // Cache current translation. Useful in Doctrine 2.4+
    

    public function __construct()
    {
		$this->translations = new ArrayCollection();
		$this->level = 0;
		$this->no_order = 1;
		$this->is_publish = true;
    }
    
    public static function getTranslationEntityClass()
    {
        return 'FAPROD\UserBundle\Entity\PrestationTranslation';
    }
    
    public function translate($locale = null)
    {
        if (null === $locale) {
            $locale = $this->currentLocale;
        }

        if (!$locale) {
            throw new \RuntimeException('No locale has been set and currentLocale is empty');
        }

        if ($this->currentTranslation && $this->currentTranslation->getLocale() === $locale) {
            return $this->currentTranslation;
        }

        if (!$translation = $this->translations->get($locale)) {
            $translation = new PrestationTranslation();
            $translation->setLocale($locale);
            $this->addTranslation($translation);
        }

        $this->currentTranslation = $translation;
        return $translation;
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
	
	public function __toString()
    {
    	$title = $this->getTitle();
    	if (!$title)$title = $this->getTitle('fr');
		return $title;
    }
	
	public function getTitle($locale = null)
    {
        return $this->translate($locale)->getTitle();
    }

    public function setTitle($title)
    {
        $this->translate()->setTitle($title);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setIsPublish($isPublish)
    {
        $this->is_publish = $isPublish;

        return $this;
    }

    public function getIsPublish()
    {
        return $this->is_publish;
    }
    
    public function getIsPublishLabel()
    {
        if ($this->getIsPublish())
        {
            return "Oui";
        } else {
            return "";
        }
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setNoOrder($noOrder)
    {
        $this->no_order = $noOrder;

        return $this;
    }

    public function getNoOrder()
    {
        return $this->no_order;
    }

    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setParent(\FAPROD\UserBundle\Entity\Prestation $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function addChild(\FAPROD\UserBundle\Entity\Prestation $children)
    {
        $this->children[] = $children;

        return $this;
    }

    public function removeChild(\FAPROD\UserBundle\Entity\Prestation $children)
    {
        $this->children->removeElement($children);
    }

    public function getChildren()
    {
        return $this->children;
    }
	
	public function getDescription($locale = null)
    {
        return $this->translate($locale)->getDescription();
    }

    public function setDescription($content)
    {
        $this->translate()->setDescription($content);
        return $this;
    }

    public function setMetiers($metiers)
    {
        $this->metiers = implode("_",$metiers);

        return $this;
    }

    public function getMetiers()
    {
        return $this->metiers ? explode("_",$this->metiers) : array();
    }
	
	public function getMetiersLabel()
    {
		$metiers_label = '';
		
		foreach ($this->getMetiers() as $metier) {
			$metiers_label .= MyConst::getMetierLabel($metier).', ';
		}
		
        return $metiers_label;
    }
}
