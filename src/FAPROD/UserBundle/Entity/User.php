<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping\OrderBy;

use FAPROD\UserBundle\Validator\Constraints\ContainsAlphanumeric;
use FAPROD\CoreBundle\Lib\MyConst;
use FAPROD\CoreBundle\Lib\MyTools;

/**
 * @ORM\Entity(repositoryClass="FAPROD\UserBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks() 
 * @UniqueEntity(fields="email", message="Un compte a déjà été créé avec cette adresse email.")
 * @ORM\Table(name="user")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
     * @ORM\Column(name="ip_address", type="string", length=15, nullable=true)
     */
    private $ip_address;
	
	/**
     * @ORM\Column(name="mangoId", type="integer", length=10, nullable=true)
     */
    private $mangoId;
    
    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;
	
	/**
     * @ORM\Column(name="genre", type="integer", nullable=true)
     */
    private $genre;
	
	/**
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    private $distance;
	
	/**
     * @ORM\Column(name="statut", type="integer", nullable=true)
     */
    private $statut;
	
	/**
     * @ORM\Column(name="metiers", type="string", length=100, nullable=true)
     */
    private $metiers;
    
    /**
     * @ORM\Column(name="entreprise", type="string", length=100, nullable=true)
     */
    private $entreprise;
	
	/**
     * @ORM\Column(name="siren", type="string", length=100, nullable=true)
     */
    private $siren;
	
	/**
     * @ORM\Column(name="first_name", type="string", length=50)
     */
    private $first_name;
  
    /**
     * @ORM\Column(name="last_name", type="string", length=50)
     */
    private $last_name;
	
	/**
     * @ORM\Column(name="facebook_id", type="string", length=40, nullable=true)
     */
    private $facebook_id;

    /**
     * @ORM\Column(name="lat", type="float", nullable=true)
     */
    private $lat;
    
    /**
     * @ORM\Column(name="lng", type="float", nullable=true)
     */
    private $lng;
	
	/**
     * @ORM\Column(name="address_1", type="string", length=100, nullable=true)
     */
    private $address_1;
  
    /**
     * @ORM\Column(name="address_2", type="string", length=100, nullable=true)
     */
    private $address_2;
  
    /**
     * @ORM\Column(name="postal_code", type="string", length=10, nullable=true)
     */
    private $postal_code;
  
    /**
     * @ORM\Column(name="city", type="string", length=50, nullable=true)
     */
    private $city;
	
	/**
     * @ORM\Column(name="email", type="string", length=70, unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(name="password", type="string", length=40, nullable=true)
     */
    private $password;
  
    /**
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     */
    private $phone;
  
    /**
     * @ORM\Column(name="mobile", type="string", length=20, nullable=true)
     */
    private $mobile;
	
	/**
     * @ORM\ManyToMany(targetEntity="FAPROD\UserBundle\Entity\Category")
     * @ORM\JoinColumn(nullable=true)
	 * @ORM\JoinTable(name="user_cheveux")
     */
    private $cheveux;
	
	/**
     * @ORM\ManyToMany(targetEntity="FAPROD\UserBundle\Entity\Category")
     * @ORM\JoinColumn(nullable=true)
	 * @ORM\JoinTable(name="user_peau")
     */
    private $peau;
	
	/**
     * @ORM\ManyToMany(targetEntity="FAPROD\UserBundle\Entity\Category")
     * @ORM\JoinColumn(nullable=true)
	 * @ORM\JoinTable(name="user_preferences")
     */
    private $preferences;
    
    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\OneToOne(targetEntity="FAPROD\UserBundle\Entity\Image", cascade={"persist"})
     * @Assert\Valid()
     */
    private $image;
	
	/**
     * @ORM\OneToOne(targetEntity="FAPROD\UserBundle\Entity\Image", cascade={"persist"})
     * @Assert\Valid()
     */
    private $image_cheveux;
	
	/**
     * @ORM\OneToOne(targetEntity="FAPROD\UserBundle\Entity\Image", cascade={"persist"})
     * @Assert\Valid()
     */
    private $image_peau;
    
    /**
     * @ORM\OneToMany(targetEntity="FAPROD\UserBundle\Entity\Image", mappedBy="user", cascade={"persist"})
     * @OrderBy({"id" = "DESC"})
     * @Assert\Valid()
     */
    private $images;
    
    /**
     * @ORM\OneToMany(targetEntity="FAPROD\MessageBundle\Entity\Discussion", mappedBy="user")
     * @OrderBy({"id" = "DESC"})
     */
    private $discussions1;
    
    /**
     * @ORM\OneToMany(targetEntity="FAPROD\MessageBundle\Entity\Discussion", mappedBy="destinataire")
     * @OrderBy({"id" = "DESC"})
     */
    private $discussions2;
	
	/**
     * @ORM\OneToMany(targetEntity="FAPROD\ContentBundle\Entity\ContentComment", mappedBy="vendeur", cascade={"persist"})
     * @OrderBy({"id" = "DESC"})
     */
    private $comments;
	
	/**
     * @ORM\Column(name="note", type="integer", nullable=true)
     */
    private $note;
    
    /**
     * @ORM\Column(name="nb_note", type="integer", nullable=true)
     */
    private $nb_note;
  
    /**
     * @ORM\Column(name="newsletter", type="boolean", nullable=true)
     */
    private $newsletter;
  
    /**
     * @ORM\Column(name="cgu", type="boolean", nullable=true)
     */
    private $cgu;
	
	/**
     * @ORM\Column(name="charte", type="boolean", nullable=true)
     */
    private $charte;
  
    /**
     * @ORM\Column(name="validate", type="integer", nullable=true)
     */
    private $validate;
    
    /**
     * @ORM\Column(name="vues", type="integer", nullable=true)
     */
    private $vues;
    
    /**
     * @ORM\Column(name="contacts", type="integer", nullable=true)
     */
    private $contacts;
	
	/**
     * @ORM\ManyToMany(targetEntity="FAPROD\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     * @OrderBy({"id" = "DESC"})
     */
    private $favoris;
	
	/**
     * @ORM\OneToMany(targetEntity="FAPROD\UserBundle\Entity\Horaire", mappedBy="user")
     * @OrderBy({"jour" = "ASC", "heure_deb" = "ASC", "minute_deb" = "ASC"})
     */
    private $horaires;
	
	/**
     * @ORM\OneToMany(targetEntity="FAPROD\UserBundle\Entity\Offer", mappedBy="user")
     * @OrderBy({"id" = "DESC"})
     */
    private $offers;
	
	/**
     * @ORM\OneToMany(targetEntity="FAPROD\BookingBundle\Entity\Booking", mappedBy="user")
     * @OrderBy({"id" = "DESC"})
     */
    private $bookings1;
	
	/**
     * @ORM\OneToMany(targetEntity="FAPROD\BookingBundle\Entity\Booking", mappedBy="seller")
     * @OrderBy({"id" = "DESC"})
     */
    private $bookings2;
	
	/**
     * @ORM\OneToMany(targetEntity="FAPROD\UserBundle\Entity\Document", mappedBy="user", cascade={"persist"})
     * @OrderBy({"id" = "DESC"})
     */
    private $documents;
	
	/**
     * @ORM\Column(name="iban", type="string", length=100, nullable=true)
     */
    private $iban;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;
    
    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;

    private $salt;

    private $roles = array();

    // Les getters et setters
    
    public function __toString()
    {
		return $this->getUsername();
    }
    
    public function __construct()
    {
        $this->image = new \FAPROD\UserBundle\Entity\Image();
		$this->image_cheveux = new \FAPROD\UserBundle\Entity\Image();
		$this->image_peau = new \FAPROD\UserBundle\Entity\Image();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
		$this->cheveux = new \Doctrine\Common\Collections\ArrayCollection();
		$this->peau = new \Doctrine\Common\Collections\ArrayCollection();
		$this->preferences = new \Doctrine\Common\Collections\ArrayCollection();
		$this->horaires = new \Doctrine\Common\Collections\ArrayCollection();
		$this->offers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->vues = 0;
        $this->contacts = 0;
		$this->distance = 25;
    }
    
    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->salt,
        ) = unserialize($serialized);
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
	
	/**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
		
		if ($this->image){
		  $new_tempFilename = $this->image->getUploadRootDir().'/thumb/u'.$this->image->getId().'.'.$this->image->getFilename();
			$new_tempFilename_zoom = $this->image->getUploadRootDir().'/zoom/zoom_u'.$this->image->getId().'.'.$this->image->getFilename();
			
		  // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
		  if (file_exists($new_tempFilename) and $this->image->getFilename() != 'no_image.png') {
			// On supprime le fichier
			unlink($new_tempFilename);
		  }
		  if (file_exists($new_tempFilename_zoom) and $this->image->getFilename() != 'no_image.png') {
			// On supprime le fichier
			unlink($new_tempFilename_zoom);
		  }
	  }
	  
	  foreach ($this->images as $image){
		  $new_tempFilename = $image->getUploadRootDir().'/thumb/u'.$image->getId().'.'.$image->getFilename();
			$new_tempFilename_zoom = $image->getUploadRootDir().'/zoom/zoom_u'.$image->getId().'.'.$image->getFilename();
			
		  // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
		  if (file_exists($new_tempFilename) and $image->getFilename() != 'no_image.png') {
			// On supprime le fichier
			unlink($new_tempFilename);
		  }
		  if (file_exists($new_tempFilename_zoom) and $image->getFilename() != 'no_image.png') {
			// On supprime le fichier
			unlink($new_tempFilename_zoom);
		  }
	  }
    }

    public function eraseCredentials()
    {
    }
  
    public function getAllUsername()
    {
    	if ($this->getLastName() or $this->getFirstName()){
    		return $this->getLastName()." ".strtoupper($this->getFirstName());
		} else {
        	return $this->getShortUsername;
    	}
    }
	
	public function getEntrepriseStripped()
    {
    	return MyTools::stripText($this->getEntreprise());
    }
    
    public function getShortUsername()
    {
    	if ($this->getEntreprise()){
    		return $this->getEntreprise();
		} elseif ($this->getLastName()){
			return ucfirst($this->getLastName());
		} else {
        	return $this->email;
    	}
    }
	
	public function getShortUsername2()
    {
    	return $this->getLastName()." ".strtoupper(substr($this->getFirstName(), 0, 1));
    }
    
    public function getUsername()
    {
    	return $this->email;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setPassword($password)
    {
		if ($password != '')
		{
	       $this->password = md5($password);
    	}
    	
        return $this;
    }
    
    public function initPassword($password)
	{
		$this->password = $password;
		
		return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    public function getLastName()
    {
        return $this->last_name;
    }
    
    public function getStrippedName()
    {
        return MyTools::stripText($this->getShortUsername());
    }

    public function setPostalCode($postalCode)
    {
        $this->postal_code = $postalCode;

        return $this;
    }

    public function getPostalCode()
    {
        return $this->postal_code;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }
    
    public function getPhoneLabel()
    {
        return MyTools::format_tel($this->phone);
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getMobile()
    {
        return $this->mobile;
    }
	
	public function getMobileLabel()
    {
        return MyTools::format_tel($this->mobile);
    }

    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getNewsletter()
    {
        return $this->newsletter;
    }
    
    public function getNewsletterLabel()
    {
    	if ($this->newsletter){
    		return "Oui";
    	} else {
    		return "Non";
    	}
    }

    public function setCgu($cgu)
    {
        $this->cgu = $cgu;

        return $this;
    }

    public function getCgu()
    {
        return $this->cgu;
    }
    
    public function getCguLabel()
    {
    	if ($this->cgu){
    		return "Oui";
    	} else {
    		return "Non";
    	}
    }

    public function setValidate($validate)
    {
        $this->validate = $validate;

        return $this;
    }

    public function getValidate()
    {
        return $this->validate;
    }
    
    public function getValidateLabel()
    {
        switch ($this->getValidate()) {
            case 1:
                return 'Oui';
            break;
            case 0:
                return 'Non';
            break;
            case -1:
                return 'Blacklisté';
            break;
            default:
                return 'Non';
        }
    }
    
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
    	if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}      
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
        if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setImage(\FAPROD\UserBundle\Entity\Image $image = null)
    {
        $this->image = $image;
		$image->setFilename('no_image.png');
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }
    
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }
    
    public function getTypeLabel()
    {
        return MyConst::getTypeUserLabel($this->type);
    }

    public function setDateLog($dateLog)
    {
        $this->date_log = $dateLog;

        return $this;
    }

    public function getDateLog($format = '')
    {
        if($this->date_log and $this->date_log instanceof \DateTime and $format){
		    return $this->date_log->format($format);
		} else {
    		return $this->date_log;
    	}
    }
    
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    public function getLng()
    {
        return $this->lng;
    }

    public function setVues($vues)
    {
        $this->vues = $vues;

        return $this;
    }

    public function getVues()
    {
        return $this->vues;
    }

    public function setContacts($contacts)
    {
        $this->contacts = $contacts;

        return $this;
    }

    public function getContacts()
    {
        return $this->contacts;
    }

    public function addImage(\FAPROD\UserBundle\Entity\Image $image)
    {
        $this->images[] = $image;

		$image->setUser($this);
		
        return $this;
    }

    public function removeImage(\FAPROD\UserBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setIpAddress($ipAddress)
    {
        $this->ip_address = $ipAddress;

        return $this;
    }

    public function getIpAddress()
    {
        return $this->ip_address;
    }

    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function getEntreprise()
    {
        return $this->entreprise;
    }

    public function setCharte($charte)
    {
        $this->charte = $charte;

        return $this;
    }

    public function getCharte()
    {
        return $this->charte;
    }
	
	public function getCharteLabel()
    {
		if ($this->charte){
			return "Oui";
		} else {
			return "Non";
		}
    }

    public function setSiren($siren)
    {
        $this->siren = $siren;

        return $this;
    }

    public function getSiren()
    {
        return $this->siren;
    }
	
	public function getValidComments(){
		
		$criteria = Criteria::create()
		    ->where(Criteria::expr()->eq("validate", 1))
		    ->orderBy(array("id" => Criteria::DESC))
		;
		
		return $this->comments->matching($criteria);
    }

    public function addDiscussions1(\FAPROD\MessageBundle\Entity\Discussion $discussions1)
    {
        $this->discussions1[] = $discussions1;

        return $this;
    }

    public function removeDiscussions1(\FAPROD\MessageBundle\Entity\Discussion $discussions1)
    {
        $this->discussions1->removeElement($discussions1);
    }

    public function getDiscussions1()
    {
        return $this->discussions1;
    }

    public function addDiscussions2(\FAPROD\MessageBundle\Entity\Discussion $discussions2)
    {
        $this->discussions2[] = $discussions2;

        return $this;
    }

    public function removeDiscussions2(\FAPROD\MessageBundle\Entity\Discussion $discussions2)
    {
        $this->discussions2->removeElement($discussions2);
    }

    public function getDiscussions2()
    {
        return $this->discussions2;
    }

    public function addComment(\FAPROD\ContentBundle\Entity\ContentComment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    public function removeComment(\FAPROD\ContentBundle\Entity\ContentComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    public function getComments()
    {
        return $this->comments;
    }
	
	public function updateNotes(){
	    $total = 0;
	    $comments = $this->getValidComments();
	    
	    foreach ($comments as $comment){
	    	$total += $comment->getNote();
	    }
	    
	    if (count($comments) > 0){
	    	$note = round($total / count($comments));
	    	$this->setNote($note);
	    	$this->setNbNote(count($comments));
	    } else {
	    	$this->setNote(null);
	    	$this->setNbNote(0);
	    }
    }

    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setNbNote($nbNote)
    {
        $this->nb_note = $nbNote;

        return $this;
    }

    public function getNbNote()
    {
        return $this->nb_note;
    }
	
	public function getVideoIframe($width = 560, $height = 315){
    	return MyTools::generateVideoEmbeds($this->youtube, $width, $height);
    }
    
    public function getVideoPreview($width = 80, $height = 60){
    	return MyTools::generateVideoImg($this->youtube, $width, $height);
    }
    
    public function getVideoPreviewUrl(){
    	return MyTools::generateVideoImgUrl($this->youtube);
    }
	
	public function addFavori(\FAPROD\UserBundle\Entity\User $favori)
    {
        $this->favoris[] = $favori;

        return $this;
    }

    public function removeFavori(\FAPROD\UserBundle\Entity\User $favori)
    {
        $this->favoris->removeElement($favori);
    }

    public function getFavoris()
    {
        return $this->favoris;
    }

    public function setAddress1($address1)
    {
        $this->address_1 = $address1;

        return $this;
    }

    public function getAddress1()
    {
        return $this->address_1;
    }

    public function setAddress2($address2)
    {
        $this->address_2 = $address2;

        return $this;
    }

    public function getAddress2()
    {
        return $this->address_2;
    }

    public function setActivites($activites)
    {
        $this->activites = $activites;

        return $this;
    }

    public function getActivites()
    {
        return $this->activites;
    }
	
	public function setMangoId($mangoId)
    {
        $this->mangoId = $mangoId;

        return $this;
    }

    public function getMangoId()
    {
        return $this->mangoId;
    }

    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    public function getGenre()
    {
        return $this->genre;
    }
	
	public function getGenreLabel()
    {
        if ($this->genre == 1){
			return "Femme";
		} else {
			return "Homme";
		}
    }

    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    public function getStatut()
    {
        return $this->statut;
    }
	
	public function getStatutLabel()
    {
        return MyConst::getStatutFiscalLabel($this->statut);
    }

    public function addHoraire(\FAPROD\UserBundle\Entity\Horaire $horaire)
    {
        $this->horaires[] = $horaire;
		
		$horaire->setUser($this);

        return $this;
    }

    public function removeHoraire(\FAPROD\UserBundle\Entity\Horaire $horaire)
    {
        $this->horaires->removeElement($horaire);
    }

    public function getHoraires()
    {
        return $this->horaires;
    }
	
	public function getHorairesLundi()
    {
		$result = array();
        foreach ($this->horaires as $horaire){
			if ($horaire->getJour() == 1){
				$result[] = $horaire;
			}
		}
		return $result;
    }
	
	public function getHorairesMardi()
    {
		$result = array();
        foreach ($this->horaires as $horaire){
			if ($horaire->getJour() == 2){
				$result[] = $horaire;
			}
		}
		return $result;
    }
	
	public function getHorairesMercredi()
    {
		$result = array();
        foreach ($this->horaires as $horaire){
			if ($horaire->getJour() == 3){
				$result[] = $horaire;
			}
		}
		return $result;
    }
	
	public function getHorairesJeudi()
    {
		$result = array();
        foreach ($this->horaires as $horaire){
			if ($horaire->getJour() == 4){
				$result[] = $horaire;
			}
		}
		return $result;
    }
	
	public function getHorairesVendredi()
    {
		$result = array();
        foreach ($this->horaires as $horaire){
			if ($horaire->getJour() == 5){
				$result[] = $horaire;
			}
		}
		return $result;
    }
	
	public function getHorairesSamedi()
    {
		$result = array();
        foreach ($this->horaires as $horaire){
			if ($horaire->getJour() == 6){
				$result[] = $horaire;
			}
		}
		return $result;
    }
	
	public function getHorairesDimanche()
    {
		$result = array();
        foreach ($this->horaires as $horaire){
			if ($horaire->getJour() == 7){
				$result[] = $horaire;
			}
		}
		return $result;
    }
	
	public function addDocument(\FAPROD\UserBundle\Entity\Document $document)
    {
        $this->documents[] = $document;
		
		$document->setUser($this);

        return $this;
    }

    public function removeDocument(\FAPROD\UserBundle\Entity\Document $document)
    {
        $this->documents->removeElement($document);
    }

    public function getDocuments()
    {
        return $this->documents;
    }
	
	public function checkProfil()
    {
        $result = "successko";
		
		if (count($this->cheveux) and count($this->peau) and count($this->preferences) and ($this->last_name and $this->first_name and $this->email and $this->address_1 and $this->postal_code and $this->city and ($this->phone or $this->mobile))){
			$result = "success";
		}
		
		return $result;
    }
	
	public function getScore()
    {
		$result = 0;
		
		if ($this->getType() == 1){
			if ($this->checkMesinfos() == "success")$result += 20;
			if ($this->checkPhotos() == "success")$result += 20;
			if ($this->checkCheveux() == "success")$result += 20;
			if ($this->checkPeau() == "success")$result += 20;
			if ($this->checkPreferences() == "success")$result += 20;
		} else {
			if ($this->checkProfilPro() == "success")$result += 20;
			if ($this->checkDisponibilites() == "success")$result += 20;
			if ($this->checkDocuments() == "success")$result += 20;
			if ($this->checkCompetences() == "success")$result += 20;
			if ($this->checkServices() == "success")$result += 20;
		}
		
		return $result.'%';
	}
	
	public function checkMesinfos()
    {
        $result = "successko";
		
		if ($this->last_name and $this->first_name and $this->email and $this->address_1 and $this->postal_code and $this->city and ($this->phone or $this->mobile)){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkCompetences()
    {
        $result = "successko";
		
		if (count($this->cheveux) or count($this->peau)){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkCheveux()
    {
        $result = "successko";
		
		if (count($this->cheveux)){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkPeau()
    {
        $result = "successko";
		
		if (count($this->peau)){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkPreferences()
    {
        $result = "successko";
		
		if (count($this->preferences)){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkProfilPro()
    {
        $result = "successko";
		
		if ($this->genre and count($this->getMetiers()) and $this->statut and $this->entreprise){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkDisponibilites()
    {
        $result = "successko";
		
		if (count($this->horaires)){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkPhotos()
    {
        $result = "successko";
		
		if ($this->image->getFilename() != 'no_image.png'){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkServices()
    {
        $result = "successko";
		
		if (count($this->getOffers())){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkDocuments()
    {
        $result = "successko";
		
		if (count($this->documents)){
			$result = "success";
		}
		
		return $result;
    }
	
	public function checkCompteiban()
    {
        $result = "successko";
		
		if ($this->iban){
			$result = "success";
		}
		
		return $result;
    }

    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    public function getIban()
    {
        return $this->iban;
    }

    public function addOffer(\FAPROD\UserBundle\Entity\Offer $offer)
    {
        $this->offers[] = $offer;
		
		$offer->setUser($this);

        return $this;
    }

    public function removeOffer(\FAPROD\UserBundle\Entity\Offer $offer)
    {
        $this->offers->removeElement($offer);
    }

    public function getOffers()
    {
        return $this->offers;
    }

    public function addBookings1(\FAPROD\BookingBundle\Entity\Booking $bookings1)
    {
        $this->bookings1[] = $bookings1;
		
		$bookings1->setUser($this);

        return $this;
    }

    public function removeBookings1(\FAPROD\BookingBundle\Entity\Booking $bookings1)
    {
        $this->bookings1->removeElement($bookings1);
    }

    public function getBookings1()
    {
        return $this->bookings1;
    }

    public function addBookings2(\FAPROD\BookingBundle\Entity\Booking $bookings2)
    {
        $this->bookings2[] = $bookings2;
		
		$bookings2->setSeller($this);

        return $this;
    }

    public function removeBookings2(\FAPROD\BookingBundle\Entity\Booking $bookings2)
    {
        $this->bookings2->removeElement($bookings2);
    }

    public function getBookings2()
    {
        return $this->bookings2;
    }

    public function addCheveux(\FAPROD\UserBundle\Entity\Category $cheveux)
    {
        $this->cheveux[] = $cheveux;

        return $this;
    }

    public function removeCheveux(\FAPROD\UserBundle\Entity\Category $cheveux)
    {
        $this->cheveux->removeElement($cheveux);
    }

    public function getCheveux()
    {
        return $this->cheveux;
    }
	
	public function getCheveuxLabel(){
		$string = '';
		
		foreach ($this->getCheveux() as $category)
		{
			if ($string)$string .= ', ';
			$string .= $category->getTitle();
		}
		
		return $string;
	}

    public function addPeau(\FAPROD\UserBundle\Entity\Category $peau)
    {
        $this->peau[] = $peau;

        return $this;
    }

    public function removePeau(\FAPROD\UserBundle\Entity\Category $peau)
    {
        $this->peau->removeElement($peau);
    }

    public function getPeau()
    {
        return $this->peau;
    }
	
	public function getPeauLabel(){
		$string = '';
		
		foreach ($this->getPeau() as $category)
		{
			if ($string)$string .= ', ';
			$string .= $category->getTitle();
		}
		
		return $string;
	}

    public function addPreference(\FAPROD\UserBundle\Entity\Category $preference)
    {
        $this->preferences[] = $preference;

        return $this;
    }

    public function removePreference(\FAPROD\UserBundle\Entity\Category $preference)
    {
        $this->preferences->removeElement($preference);
    }

    public function getPreferences()
    {
        return $this->preferences;
    }
	
	public function getPreferencesLabel(){
		$string = '';
		
		foreach ($this->getPreferences() as $category)
		{
			if ($string)$string .= ', ';
			$string .= $category->getTitle();
		}
		
		return $string;
	}

    public function setImageCheveux(\FAPROD\UserBundle\Entity\Image $imageCheveux = null)
    {
        $this->image_cheveux = $imageCheveux;
		$imageCheveux->setFilename('no_image.png');

        return $this;
    }

    public function getImageCheveux()
    {
        return $this->image_cheveux;
    }

    public function setImagePeau(\FAPROD\UserBundle\Entity\Image $imagePeau = null)
    {
		$this->image_peau = $imagePeau;
		$imagePeau->setFilename('no_image.png');

        return $this;
    }

    public function getImagePeau()
    {
        return $this->image_peau;
    }

    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    public function getDistance()
    {
        return $this->distance ? $this->distance : 25;
    }
	
	public function getDistanceLabel()
    {
        return MyConst::getDistance($this->distance);
    }

    public function setMetiers($metiers)
    {
        $this->metiers = implode("_",$metiers);

        return $this;
    }

    public function getMetiers()
    {
        return $this->metiers ? explode("_",$this->metiers) : array();
    }
	
	public function getMetiersLabel()
    {
		$metiers_label = '';
		
		foreach ($this->getMetiers() as $metier) {
			if ($metiers_label)$metiers_label .= ', ';
			$metiers_label .= MyConst::getMetierLabel($metier);
		}
		
        return $metiers_label;
    }
	
	
	
}
