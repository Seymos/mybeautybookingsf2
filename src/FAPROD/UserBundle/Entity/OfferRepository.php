<?php

namespace FAPROD\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * OfferRepository
 *
 */
class OfferRepository extends EntityRepository
{
	public function getOffers($page = null, $nbPerPage = null, $sort = 'id', $sens = 0, $QueryBuilder = '')
    {
    	if (!$QueryBuilder)$QueryBuilder = $this->createQueryBuilder('o');
    	
    	$query = $QueryBuilder
    	            ->leftJoin('o.user', 's')->addSelect('s')
    	            ->leftJoin('o.translations', 'cat_trans')->addSelect('cat_trans')
		    		->orderBy('o.'.$sort, $sens ? 'ASC' : 'DESC')
			      	->getQuery();
		
		return $query->getResult();
    }	
}
