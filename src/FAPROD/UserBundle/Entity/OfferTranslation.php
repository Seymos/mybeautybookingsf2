<?php

namespace FAPROD\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="offertranslation")
 */
class OfferTranslation extends AbstractTranslation
{
	/**
     * @Prezent\Translatable(targetEntity="Offer")
     */
    protected $translatable;
    
	/**
     * @ORM\Column(name="title", type="string", length=150, nullable=true)
     */
    private $title;
    
    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
        
    public static function getTranslationEntityClass()
    {
        return 'OfferTranslation';
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    public function getLocale()
    {
        return $this->locale;
    }


}
