<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', 'a2lix_translations', array(
				    'locales' => $options['langues'],
				    //'required_locales' => array('fr'),      // [2]
				    'fields' => array(                      // [3]
				    	'title' => array( 
				        	'field_type' => 'text'
				        ),
				        'stripped_title' => array( 
				        	'display' => false
				        ),
						'description' => array( 
				        	'field_type' => 'textarea',
				        	'required'   => false,
				        ),
				    )
				)
			)
            ->add('parent', 'entity_hidden', array(
                'class' => 'FAPROD\UserBundle\Entity\Category'
		    ))
            ->add('level', 'hidden')
            ->add('no_order', 'integer', array('required' => false))
            ->add('is_publish')
			->add('matching')
        ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
	    $resolver->setDefaults(array('data_class' => 'FAPROD\UserBundle\Entity\Category',
	    							  'langues' => array('fr'),
	    						));
	}
	    
	public function getDefaultOptions(array $options)
	{
	    return array('data_class' => 'FAPROD\UserBundle\Entity\Category',
	    			'langues' => null,
	    			);
	}

    /**
     * @return string
     */
    public function getName()
    {
        return 'faprod_userbundle_category';
    }
}
