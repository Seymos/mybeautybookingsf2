<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfessionnelType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
		    ->add('first_name')
            ->add('last_name')
            ->add('email')
            ->add('password', 'password', array('required' => false))
            ->add('phone', 'text', array('attr' => array('maxlength' => 10)))
            ->add('mobile', 'text', array('attr' => array('maxlength' => 10)))
            ->add('newsletter')
			->add('genre', 'choice', array(
								'choices'   => array('1' => 'Femme', '2' => 'Homme'),
								'required'  => true,
								'expanded'  => true,
								))
			->add('statut', 'choice', array(
								'choices'   => $options['statut'],
								'required'  => true,
								'expanded'  => true,
								))
			->add('distance', 'choice', array(
								'choices'   => $options['distance'],
								'required'  => true,
								'expanded'  => false,
								))
			->add('metiers', 'choice', array(
            						'choices'       => $options['metiers'],
	            					'multiple'      => true,
	            					'expanded'      => true,
	            					'required'      => false,
            						)
            )
            ->add('cgu', 'checkbox', array('required'  => true))
			->add('siren', 'text', array('attr' => array('maxlength' => 14)))
            ->add('entreprise')
            ->add('description')
			->add('charte', 'checkbox', array('required'  => true))
            ->add('image', 'hidden', array('mapped' => false))
            ->add('image_old', 'hidden', array('mapped' => false))
            ->add('images', 'hidden', array('mapped' => false))
            ->add('images_old', 'hidden', array('mapped' => false))
            ->add('validate')
			->add('address_1')
            ->add('address_2')
			->add('postal_code', 'text', array('attr' => array('maxlength' => 5)))
			->add('city')
			->add('cheveux', 'entity', array(
												'multiple' => true,
												'expanded' => true,
												'required' => true,
												'class'    => 'FAPROD\UserBundle\Entity\Category',
												))
			->add('peau', 'entity', array(
												'multiple' => true,
												'expanded' => true,
												'required' => true,
												'class'    => 'FAPROD\UserBundle\Entity\Category',
												))
			->add('preferences', 'entity', array(
												'multiple' => true,
												'expanded' => true,
												'required' => true,
												'class'    => 'FAPROD\UserBundle\Entity\Category',
												))
			->add('gmap_lat', 'hidden', array('mapped' => false))
            ->add('gmap_lng', 'hidden', array('mapped' => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
					            'data_class'       => 'FAPROD\UserBundle\Entity\User',
								'statut'           => array(),
								'metiers'          => array(),
								'distance'         => array(),
					        ));
    }
    
    public function getDefaultOptions(array $options)
	{
	    return array('data_class'       => 'FAPROD\UserBundle\Entity\User',
					'statut'            => null,
					'metiers'           => null,
					'distance'          => null,
	    			);
	}

    public function getName()
    {
        return 'faprod_userbundle_user';
    }
}
