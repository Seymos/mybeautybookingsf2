<?php

namespace FAPROD\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class BankType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('owner_name')
            ->add('owner_address')
            ->add('postcode', 'text', array('attr' => array('maxlength' => 5)))
            ->add('city')
			->add('IBAN', 'text', array('constraints' => array(new Assert\Iban(array('message' => 'IBAN invalide')))))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
					        ));
    }
    
    public function getDefaultOptions(array $options)
	{
	    return array(
	    			);
	}

    public function getName()
    {
        return 'faprod_userbundle_bank';
    }
}
