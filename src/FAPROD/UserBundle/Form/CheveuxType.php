<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CheveuxType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('cheveux', 'entity', array(
												'multiple' => true,
												'expanded' => true,
												'required' => true,
												'class'    => 'FAPROD\UserBundle\Entity\Category',
												))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
					            'data_class'       => 'FAPROD\UserBundle\Entity\User',
					        ));
    }
    
    public function getDefaultOptions(array $options)
	{
	    return array('data_class'       => 'FAPROD\UserBundle\Entity\User',
	    			);
	}

    public function getName()
    {
        return 'faprod_userbundle_user';
    }
}
