<?php
namespace FAPROD\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OfferType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', 'a2lix_translations', array(
				    'locales' => $options['langues'],
				    'fields' => array(
                        'description' => array(
                            'field_type' => 'textarea',
                            'required'   => false,
                            'attr' => array('rows' => '3','cols' => '60'),
                        ),
                    )
                )
            )
			->add('prestation')
			->add('metier')
			->add('duree', 'choice', array(
                    'choices'   => $options['duree'],
                    'required'  => true,
                )
            )
			->add('price', 'integer', array(
					'attr' => array(
							'min' =>"1", 
						)
				)
			)
            ->add('user', 'entity_hidden', array(
                'class' => 'FAPROD\UserBundle\Entity\User'
			))
			->add('images', 'hidden', array('mapped' => false))
            ->add('images_old', 'hidden', array('mapped' => false))
			;
    }
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
									'data_class'     => 'FAPROD\UserBundle\Entity\Offer',
									'langues'        => array('fr'),
									'duree'          => array(),
								));
    }

    public function getDefaultOptions(array $options)
    {
        return array('data_class'      => 'FAPROD\UserBundle\Entity\Offer',
            'langues'                  => null,
            'duree'                    => null,
        );
    }

    public function getName()
    {
        return 'faprod_userbundle_offer';
    }
}
