<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
 
namespace FAPROD\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('type', 'choice', array(
            						'choices'       => $options['type'],
	            					'multiple'      => false,
	            					'expanded'      => false,
	            					'required'      => true,
									'empty_data'    => "",
									'empty_value'   => "Type de document",
            						)
            )
            ->add('file', 'file', array(
            							'label'      => false,
            							'required'   => false
            							)
            )
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'FAPROD\UserBundle\Entity\Document',
			'type'          => array(),
        ));
    }
    
    public function getDefaultOptions(array $options)
	{
	    return array('data_class'      => 'FAPROD\UserBundle\Entity\Document',
	    			 'type'            => null,
	    			);
	}

    public function getName()
    {
        return 'faprod_userbundle_document';
    }
}
