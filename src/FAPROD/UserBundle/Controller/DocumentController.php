<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\UserBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;


use FAPROD\UserBundle\Form\DocumentType;
use FAPROD\UserBundle\Entity\Document;
use FAPROD\UserBundle\Entity\Image;

use FAPROD\CoreBundle\Lib\MyConst;
use FAPROD\CoreBundle\Lib\MyTools;

class DocumentController extends Controller
{
    /**
     * @Route("/documents", name="document_index")
     * @Template("UserBundle:Document:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $repository_document = $this->getDoctrine()->getManager()->getRepository('UserBundle:Document');
        $queryBuilder = $repository_document->createQueryBuilder('d');
        $queryBuilder->where('d.user = :user')
                ->setParameter(':user', $this->getUser());
        $documents = $repository_document->getDocuments(null, null, 'id', 0, $queryBuilder);
		
		$documents_type = array();
		foreach ($documents as $document){
			$documents_type[$document->getType()] = $document->getType();
		}
		
		$types = $this->container->get('FAPROD.MyConst')->getTypeDocument();	

        return array(
            "documents_type"  => $documents_type,
			"documents"       => $documents,
			"types"           => $types,
        );
    }
	
	/**
     * @Route("/documents/popup", name="user_showdocuments")
	 * @Template("UserBundle:Document:show_documents.html.twig")
	 */
    public function showdocumentsAction(Request $request)
    {
		$repository_document = $this->getDoctrine()->getManager()->getRepository('UserBundle:Document');
        $queryBuilder = $repository_document->createQueryBuilder('d');
        $queryBuilder->where('d.user = :user')
                ->setParameter(':user', $this->getUser());
        $documents = $repository_document->getDocuments(null, null, 'id', 0, $queryBuilder);
		
		$documents_type = array();
		foreach ($documents as $document){
			$documents_type[$document->getType()] = $document->getType();
		}
		
		$types = $this->container->get('FAPROD.MyConst')->getTypeDocument();	

        return array(
            "documents_type"  => $documents_type,
			"documents"       => $documents,
			"types"           => $types,
        );
	}

    /**
     * @Route("/documents/show/{document_id}", name="document_show", requirements={"document_id" = "\d+"})
     * @ParamConverter("document", options={"mapping": {"document_id": "id"}})
     * @Template("UserBundle:Document:show.html.twig")
     */
    public function showAction(Document $document)
    {

        return array(
            'document' => $document,
        );
    }

    /**
     * @Route("/document/edit/{document_id}", name="document_edit", requirements={"document_id" = "\d+"})
	 * @Template("UserBundle:Document:edit.html.twig")
	 */
    public function editAction($document_id, Request $request)
    {

    	if ($document_id){
    		$document = $this->getDoctrine()->getManager()->getRepository('UserBundle:Document')->findOneById($document_id);
    	} else {
    		$document = new Document();
			$document->setType($request->get('type'));
			$document->setUser($this->getUser());
			
    	}
		    
		$type = $this->container->get('FAPROD.MyConst')->getTypeDocument();	
		
	    $form = $this->createForm(new DocumentType(), $document, array(
																	'type' => $type,
																	));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	    	
	       $em = $this->getDoctrine()->getManager();
	    	
	       $em->persist($document);
		   
		   $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Enregistrement effectué.'));
	
	       return $this->redirect($this->generateUrl('document_show', array('document_id' => $document->getId(), 'np' => 1)));
	    }
	
	    return array(
	       'form'              => $form->createView(),
	    );
    }
	
    /**
     * @Route("/documents/delete/{document_id}", name="document_delete", requirements={"document_id" = "\d+"})
     * @ParamConverter("document", options={"mapping": {"document_id": "id"}})
     */
    public function deleteAction(Document $document, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($document);
        $em->flush();

        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Suppression effectuée.'));

        return $this->redirect($this->generateUrl('document_index'));
    }
}
