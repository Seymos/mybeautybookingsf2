<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\UserBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;

use FAPROD\CoreBundle\Lib\MyConst;
use Ob\HighchartsBundle\Highcharts\Highchart;

use FAPROD\UserBundle\Form\MesDocumentsType;
use FAPROD\UserBundle\Form\ProfilType;
use FAPROD\UserBundle\Form\CheveuxType;
use FAPROD\UserBundle\Form\CompetencesType;
use FAPROD\UserBundle\Form\PeauType;
use FAPROD\UserBundle\Form\PreferencesType;
use FAPROD\UserBundle\Form\ProfessionnelType;
use FAPROD\UserBundle\Form\PasswordType;
use FAPROD\UserBundle\Entity\User;
use FAPROD\UserBundle\Entity\Image;
use FAPROD\OrderBundle\Entity\Order;
use FAPROD\OrderBundle\Entity\Orderitem;
use FAPROD\UserBundle\Form\BankType;
use FAPROD\UserBundle\Entity\Horaire;

use MangoPay\MangoPayApi;
use MangoPay\BankAccountDetails;
use MangoPay\BankAccountDetailsIBAN;
use MangoPay\BankAccount;
use MangoPay\Pagination;
use MangoPay\UserNatural;
use MangoPay\Address;


class UserController extends Controller
{

    /**
     * @Route("/avis", name="user_avis")
     * @Template("UserBundle:User:avis.html.twig")
     */
    public function avisAction(Request $request)
    {
		$user = $this->getUser();
		
        $repository_avis = $this->getDoctrine()->getManager()->getRepository('ContentBundle:ContentComment');
        $queryBuilder = $repository_avis->createQueryBuilder('c');
		if ($user->getType() == 1){
			$queryBuilder->where('c.user = :user')->setParameter(':user', $this->getUser());
		} else {
			$queryBuilder->where('c.vendeur = :vendeur')->setParameter(':vendeur', $this->getUser());
		}
        $avis = $repository_avis->getComments(null, null, 'id', 0, $queryBuilder);

        return array(
            "avis" => $avis,
        );
    }
	
	/**
     * @Route("/profil/avis", name="user_showavis")
	 * @Template("UserBundle:User:show_avis.html.twig")
	 */
    public function showavisAction(Request $request)
    {
		$user = $this->getUser();
		
		$repository_avis = $this->getDoctrine()->getManager()->getRepository('ContentBundle:ContentComment');
        $queryBuilder = $repository_avis->createQueryBuilder('c');
		if ($user->getType() == 1){
			$queryBuilder->where('c.user = :user')->setParameter(':user', $this->getUser());
		} else {
			$queryBuilder->where('c.vendeur = :vendeur')->setParameter(':vendeur', $this->getUser());
		}
        $avis = $repository_avis->getComments(1, 5, 'id', 0, $queryBuilder);
						
		return array(
					"avis" => $avis,
					);
	}
	
	
	/**
     * @Route("/addfavoris/{user_id}", name="user_addfavoris", requirements={"user_id" = "\d+"})
	 * @ParamConverter("partenaire", options={"mapping": {"user_id": "id"}})
	 */
    public function addfavorisAction(User $partenaire, Request $request)
    {
    	$user = $this->getUser();
    	if (false === $user->getFavoris()->contains($partenaire)) {
	    	$em = $this->getDoctrine()->getManager();
		    $em->persist($user);
		    $user->addFavori($partenaire);
	    	$em->flush();
	    	
	    	$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Ajouté dans vos favoris.'));
		}
    	return $this->redirect($this->generateUrl('home_show', array(
	        																'user_id' => $partenaire->getId(),
	        																't'         => $partenaire->getStrippedName(),
	        																)));
	}
	
	/**
     * @Route("/deletefavoris/{user_id}", name="user_deletefavoris", requirements={"user_id" = "\d+"})
	 * @ParamConverter("partenaire", options={"mapping": {"user_id": "id"}})
	 */
    public function deletefavorisAction(User $partenaire, Request $request)
    {
    	$user = $this->getUser();
    	if ($user->getFavoris()->contains($partenaire)) {
	    	$em = $this->getDoctrine()->getManager();
		    $em->persist($user);
		    $user->removeFavori($partenaire);
	    	$em->flush();
	    	
	    	$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Retiré de vos favoris.'));
		}
    	return $this->redirect($this->generateUrl('user_favoris'));
	}
	
	/**
     * @Route("/favoris", name="user_favoris")
	 * @Template("UserBundle:User:favoris.html.twig")
	 */
    public function favorisAction(Request $request)
    {
    	
	}
	
	/**
     * @Route("/profil/favoris", name="user_showfavoris")
	 * @Template("UserBundle:User:show_favoris.html.twig")
	 */
    public function showfavorisAction(Request $request)
    {

	}
	
	/**
     * @Route("/rotateimg", name="user_rotateimg")
     * @Template("UserBundle:User:rotateimg.html.twig")
     */
    public function rotateimgAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();	
    	$image = $em->getRepository('UserBundle:Image')->find($request->get('image_id'));
    	$image->rotateImage($request->get('rotate'), $request->get('title'));
    	$em->flush();
    	return array(
    				"image" => $image,
    				"type"  => $request->get('type'),
    				"title"  => $request->get('title'),
    				);
	}
	
	/**
     * @Route("/delcompte", name="user_delcompte")
	 * @Template("UserBundle:User:delcompte.html.twig")
	 */
    public function delcompteAction(Request $request)
    {
    	
	}
	
	/**
     * @Route("/delcomptego", name="user_delcomptego")
	 */
	public function delcomptegoAction(Request $request)
	{	
		$user = $this->getUser();
		
	    $em = $this->getDoctrine()->getManager();	
		
		$stmt = $em->getConnection()
					->prepare("update message set destinataire_id = null where `destinataire_id`= :destinataire_id");
		$stmt->bindValue('destinataire_id',$user->getId());
		$stmt->execute();
		
		$stmt = $em->getConnection()
					->prepare("update message set user_id = null where `user_id`= :user_id");
		$stmt->bindValue('user_id',$user->getId());
		$stmt->execute();
		
		$stmt = $em->getConnection()
					->prepare("update discussion set destinataire_id = null where `destinataire_id`= :destinataire_id");
		$stmt->bindValue('destinataire_id',$user->getId());
		$stmt->execute();
		
		$stmt = $em->getConnection()
					->prepare("update discussion set user_id = null where `user_id`= :user_id");
		$stmt->bindValue('user_id',$user->getId());
		$stmt->execute();
		
	    $em->remove($user);
	    $em->flush();

		$this->get('security.context')->setToken(null);
		//$this->get('request')->getSession()->invalidate();
		$response = new RedirectResponse($this->generateUrl('home_index'));
		$response->headers->clearCookie('REMEMBERME');
		
	    $request->getSession()->getFlashBag()->add('message', 'Suppression effectuée.');

        return $response;
	}
	
	/**
     * @Route("/", name="user_index")
     * @Template("UserBundle:User:index.html.twig")
	 */
    public function indexAction(Request $request)
    {
		
		if ($this->getUser()->getType() == 1){
			return $this->redirect($this->generateUrl('user_show'));
		}
		
		return array(
	       'user'        => $this->getUser(),
	    );
		
	}
	
	/**
     * @Route("/statistique", name="user_statistique")
     * @Template("UserBundle:User:statistique.html.twig")
	 */
    public function statistiqueAction(Request $request)
    {
		
		if ($this->getUser()->getType() == 1){
			return $this->redirect($this->generateUrl('user_show'));
		}
		
    	$nb_jours = 60;
    	
    	$repository_stat = $this->getDoctrine()->getManager()->getRepository('UserBundle:Stat');
    	$queryBuilder = $repository_stat->createQueryBuilder('s');
		$queryBuilder->where('s.user = :user')
					 ->andWhere('s.date >= :date')
					 ->setParameter('user', $this->getUser())
					 ->setParameter('date', $date = new \DateTime("- ".$nb_jours." days"));
    	$stats_user = $repository_stat->getStats(null, null, 'id', 0, $queryBuilder);
    	
    	$dates_temp = array();
    	$vues_temp = array();
    	$contacts_temp = array();
    	
    	for ($i = $nb_jours; $i >=0; $i--){
    		$date = new \DateTime("- ".$i." days");
    		$dates_temp[$date->format('d/m')] = $date->format('d/m');
    		$vues_temp[$date->format('d/m')] = 0;
    		$contacts_temp[$date->format('d/m')] = 0;
    	}
    	
    	foreach ($stats_user as $stat){
    		$vues_temp[$stat->getDate('d/m')] += $stat->getVue();
    		$contacts_temp[$stat->getDate('d/m')] += $stat->getContact();
    	}
    	
    	$dates = array();
    	$vues = array();
    	$contacts = array();
    	foreach ($dates_temp as $date)$dates[] = $date;
    	foreach ($vues_temp as $vue)$vues[] = $vue;
    	foreach ($contacts_temp as $contact)$contacts[] = $contact;
    	
        $History = array(
            array(
                 "name" => "Nombre de vues", 
                 "data" => $vues
            ),
            array(
                 "name" => "Nombre de bookings", 
                 "data" => $contacts
            ),
            
        );
        
        $ob = new Highchart();
        $ob->chart->renderTo('linechart');  
        $ob->title->text($this->get('translator')->trans('Statistiques'));
        $ob->yAxis->min(0);
        //$ob->yAxis->title(array('text' => "Ventes (milliers d'unité)"));
        //$ob->xAxis->title(array('text'  => "Date du jours"));
        $ob->xAxis->categories($dates);
        $ob->xAxis->minTickInterval(count($dates)/10);
        $ob->chart->backgroundColor("#EEEEEE");

        $ob->series($History);
		
	  	return array(
	    	'chart'    => $ob,
	  	);
    }
    
    /**
     * @Route("/boutique", name="user_boutique")
     * @Template("UserBundle:User:boutique.html.twig")
	 */
    public function boutiqueAction(Request $request)
    {
    	if ($request->getMethod() == 'POST')
        {
        	if ($request->request->get('pro_option')){
				
				$repository_option = $this->getDoctrine()->getManager()->getRepository('OrderBundle:Option');
				$option = $repository_option->findOneById($request->request->get('pro_option'));
        		
				if ($option->getPriceHt()){
					  $em = $this->getDoctrine()->getManager();
					  
					  $order = new Order();
					  $order->setUser($this->getUser());
					  
					  
					  $taux_tva = $this->get('FAPROD.Configs')->getTva();
					  
					  $total_ht = 0;
					  
					  
					  $total_ht += $option->getPriceHt();
					 
					  $item = new Orderitem;
					  $item->setTitle($option->getTitle());
					  $item->setOptionId($option->getId());
					  $item->setPrice($option->getPriceHt());
					  $item->setCredits($option->getCredits());
					  $item->setQuantity(1);
					  $item->setTotal($option->getPriceHt());
					 
					  $order->addItem($item);
					  
					  $order->setType(MyConst::ORDER_MEMBRE);
					  
					  $tva = round(($total_ht * $taux_tva) / 100, 2);
					  $total_ttc = $total_ht + $tva;
					  
					  $order->setTotalHt($total_ht);
					  $order->setTotalTtc($total_ttc);
					  $order->setCredits($option->getCredits());
					  $order->setTva($tva);
					  $order->setStatus(0);
					  $order->setTxnId('');
					  $order->setPaiement(1);
					  
					  $em->persist($order);
					  $em->flush(); 
					  
					  $order->setOrderId(date('dmY').'.'.$order->getId());
					  $order->buildFacture();
					  $em->flush();
					  
					  $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Enregistrement effectué.'));
			
					  return $this->redirect($this->generateUrl('order_show', array('order_id' => $order->getId(), 'np' => 1)));
					  
				} else {
					  $em = $this->getDoctrine()->getManager();
					  
					  $user = $this->getUser();
					  
					  $user->setAboEndDate(new \DateTime("+ ".$option->getDuree()." months"));
					  
					  $em->persist($user);
					  $em->flush();
					  
					  return $this->redirect($this->generateUrl('user_index'));
			    }
       	    }
    	}
    	
    	$repository_option = $this->getDoctrine()->getManager()->getRepository('OrderBundle:Option');
    	$queryBuilder = $repository_option->createQueryBuilder('o');
		$queryBuilder->where('o.is_publish = 1');
		
		if ($this->getUser()->getAboActif()){
			$queryBuilder->andWhere('o.type = 2');
		} else {
			$queryBuilder->andWhere('o.type = 1');
		}
		
    	$options = $repository_option->getOptions(null, null, 'no_order', 1, $queryBuilder);
    	
    	$taux_tva = $this->get('FAPROD.Configs')->getTva();
	
	    return array(
	       'options'     => $options,
		   'taux_tva'    => $taux_tva,
	    );
    }
    
	/**
     * @Route("/editpassword", name="user_editpassword")
	 * @Template("UserBundle:User:editpassword.html.twig")
	 */
    public function editpasswordAction(Request $request)
    {
    	$user = $this->getUser();
    	
	    $form = $this->createForm(new PasswordType());
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted()) {
	    	
	    	if ($form->isValid()){
	    	
		       $datas = $form->getData();
		       
		       if (md5($datas['password_old']) == $this->getUser()->getPassword()){
			       $em = $this->getDoctrine()->getManager();
			       $user->setPassword($datas['password']);
			       $em->flush();
			
			       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Modification effectuée'));
			
			       return $this->redirect($this->generateUrl('user_index'));
	       	   }
	       	   
	       	} else {
	       		$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Erreur de saisie'));
	       	}
	    }
	
	    return array(
	       'form'        => $form->createView(),
	    );
    }
	
	
	/**
     * @Route("/competences", name="user_competences")
	 * @Template("UserBundle:User:competences.html.twig")
	 */
    public function competencesAction(Request $request)
    {
		$user = $this->getUser();
		$form = $this->createForm(new CompetencesType(), $user);
		
		$form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();

		   $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Modification effectuée'));
		   
		   return $this->redirect($this->generateUrl('user_show'));
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$cheveux = $repository_category->find(165);
    	$peau = $repository_category->find(199);
	
	    return array(
	       'form'        => $form->createView(),
	       'cheveux'     => $cheveux,
		   'peau'        => $peau,
	    );
		   
	}
	
	/**
     * @Route("/cheveux", name="user_cheveux")
	 * @Template("UserBundle:User:cheveux.html.twig")
	 */
    public function cheveuxAction(Request $request)
    {
		$user = $this->getUser();
		$form = $this->createForm(new CheveuxType(), $user);
		
		$form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();

		   $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Modification effectuée'));
		   
		   return $this->redirect($this->generateUrl('user_show'));
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$cheveux = $repository_category->find(165);
	
	    return array(
	       'form'        => $form->createView(),
	       'cheveux'     => $cheveux,
	    );
		   
	}
	
	/**
     * @Route("/peau", name="user_peau")
	 * @Template("UserBundle:User:peau.html.twig")
	 */
    public function peauAction(Request $request)
    {
		$user = $this->getUser();
		$form = $this->createForm(new PeauType(), $user);
		
		$form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();

		   $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Modification effectuée'));
		   
		   return $this->redirect($this->generateUrl('user_show'));
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$peau = $repository_category->find(199);
	
	    return array(
	       'form'        => $form->createView(),
	       'peau'        => $peau,
	    );
		   
	}
	
	/**
     * @Route("/preferences", name="user_preferences")
	 * @Template("UserBundle:User:preferences.html.twig")
	 */
    public function preferencesAction(Request $request)
    {
		$user = $this->getUser();
		$form = $this->createForm(new PreferencesType(), $user);
		
		$form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();

		   $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Modification effectuée'));
		   
		   return $this->redirect($this->generateUrl('user_show'));
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$preferences = $repository_category->find(216);
		
		$lisse = $repository_category->find(168);
		$boucle = $repository_category->find(169);
		$crepu = $repository_category->find(170);
		$frise = $repository_category->find(171);
		
		$is_crepu = $user->getCheveux()->contains($crepu) ? true : false;
		$is_lisse = $user->getCheveux()->contains($lisse) ? true : false;
		$is_boucle = ($user->getCheveux()->contains($boucle) or $user->getCheveux()->contains($frise)) ? true : false;
	
	    return array(
	       'form'            => $form->createView(),
	       'preferences'     => $preferences,
		   'is_crepu'        => $is_crepu,
		   'is_lisse'        => $is_lisse,
		   'is_boucle'       => $is_boucle,
	    );
		   
	}
    
    /**
     * @Route("/edit", name="user_edit")
	 * @Template("UserBundle:User:edit.html.twig")
	 */
    public function editAction(Request $request)
    {    	
    	$user = $this->getUser();
			
    	if ($user->getType() == 1){
			$form = $this->createForm(new ProfilType(), $user);
		} else {
			$statut = MyConst::getStatutFiscal();
			$distance = MyConst::getDistance();
			$form = $this->createForm(new ProfessionnelType(), $user, array(
																	'statut'    => $statut,
																	'distance'  => $distance,
																	));
		}
		
	    $form->remove('cgu');
	    $form->remove('validate');
	    $form->remove('abo_end_date');
		$form->remove('charte');
		$form->remove('cheveux');
		$form->remove('peau');
		$form->remove('metiers');
		$form->remove('preferences');
	    $form->handleRequest($request);
	    $erreur = '';
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	       $em = $this->getDoctrine()->getManager();
		   
		   if ($user->getType() == 2){
			   
			   if ($request->request->get('image')){
					if ($user->getImage()){
						$image = $user->getImage();
						$image->upload($request->request->get('image'), $user->getShortUsername());
						$em->flush();
					}	       		
			   } elseif ($request->request->get('image_old') == '') {
					$image = $user->getImage();
					$image->setFilename('no_image.png');
					$em->flush();
			   }
			   
			   //$user->setPrestations($user->getCategorysLabel());

			   if ($form->get('gmap_lat')->getData())$user->setLat($form->get('gmap_lat')->getData());
			   if ($form->get('gmap_lng')->getData())$user->setLng($form->get('gmap_lng')->getData());
		   }
       	   
	       $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Modification effectuée'));
	
	       return $this->redirect($this->generateUrl('user_show'));
	    }
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$queryBuilder = $repository_category->createQueryBuilder('c');
		$queryBuilder->where('c.level = 0');
    	$categorys = $repository_category->getCategorys(null, null, 'no_order', 1, $queryBuilder);
	
	    return array(
	       'form'        => $form->createView(),
	       'categorys'   => $categorys,
	    );
    }
	
	/**
     * @Route("/profil", name="user_show")
	 * @Template("UserBundle:User:show.html.twig")
	 */
    public function showAction(Request $request)
    {	
	
		if ($this->getUser()->getType() == 2){
			return $this->redirect($this->generateUrl('user_index'));
		}
		
	    return array(
	       'user'        => $this->getUser(),
	    );
	}
	
	/**
     * @Route("/profil/infos", name="user_showinfos")
	 * @Template("UserBundle:User:show_infos.html.twig")
	 */
    public function showinfosAction(Request $request)
    {
	    return array(
	       'user'        => $this->getUser(),
	    );
	}
	
	/**
     * @Route("/profil/competences", name="user_showcompetences")
	 * @Template("UserBundle:User:show_competences.html.twig")
	 */
    public function showcompetencesAction(Request $request)
    {
		$cheveux = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category')->find(165);
		$peau = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category')->find(199);
		
	    return array(
	       'user'        => $this->getUser(),
		   'cheveux'     => $cheveux,
		   'peau'        => $peau,
	    );
	}
	
	/**
     * @Route("/profil/cheveux", name="user_showcheveux")
	 * @Template("UserBundle:User:show_cheveux.html.twig")
	 */
    public function showcheveuxAction(Request $request)
    {
		$cheveux = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category')->find(165);
		
	    return array(
	       'user'        => $this->getUser(),
		   'cheveux'     => $cheveux,
	    );
	}
	
	/**
     * @Route("/profil/peau", name="user_showpeau")
	 * @Template("UserBundle:User:show_peau.html.twig")
	 */
    public function showpeauAction(Request $request)
    {
		$peau = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category')->find(199);
		
	    return array(
	       'user'        => $this->getUser(),
		   'peau'        => $peau,
	    );
	}
	
	/**
     * @Route("/profil/preferences", name="user_showpreferences")
	 * @Template("UserBundle:User:show_preferences.html.twig")
	 */
    public function showpreferencesAction(Request $request)
    {
		$preferences = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category')->find(216);
		
	    return array(
	       'user'          => $this->getUser(),
		   'preferences'   => $preferences,
	    );
	}
	
	/**
     * @Route("/show/bank", name="user_bank")
     * @Template("UserBundle:User:bank.html.twig")
     */
	public function bankAction(Request $request)
    {
		require_once __DIR__ . '/../../BookingBundle/mangopay/MangoPay/Autoloader.php';
		$mangoPayApi = new MangoPayApi();
		$conf = $this->get('FAPROD.MyConst')->getMangoConf();
		$mangoPayApi->Config->ClientId = $conf[0];
		$mangoPayApi->Config->ClientPassword = $conf[1];
		$mangoPayApi->Config->TemporaryFolder = $conf[2];
		$mangoPayApi->Config->BaseUrl = $conf[3];
		
		$mangoId = $this->getUser()->getMangoId();
		$form = $this->createForm(new BankType());
		$account= null;
		
		if ($mangoId){
			
			$Pagination = new \MangoPay\Pagination();
			$result = $mangoPayApi->Users->GetBankAccounts($mangoId, $Pagination);
			if ($result){
				
				$account = end($result);
				$form->get('owner_name')->setData($account->OwnerName);
				$form->get('owner_address')->setData($account->OwnerAddress->AddressLine1);
				$form->get('city')->setData($account->OwnerAddress->City);
				$form->get('postcode')->setData($account->OwnerAddress->PostalCode);
				$form->get('IBAN')->setData($account->Details->IBAN);

			}

		}

        $form->handleRequest($request);
		
        if ($form->isSubmitted() and $form->isValid()) {
			
			if (!$account or ($form->get('owner_name')->getData() != $account->OwnerName) or ($form->get('owner_address')->getData() != $account->OwnerAddress->AddressLine1) or ($form->get('city')->getData() != $account->OwnerAddress->City) or ($form->get('postcode')->getData() != $account->OwnerAddress->PostalCode) or ($form->get('IBAN')->getData() != $account->Details->IBAN)){
				
				$BankAccount = new \MangoPay\BankAccount();
				$BankAccount->UserId = $mangoId;
				$BankAccount->Type = "IBAN";
				$BankAccount->OwnerName = $form->get('owner_name')->getData();
				$BankAccount->OwnerAddress = new \MangoPay\Address();
				$BankAccount->OwnerAddress->AddressLine1 = $form->get('owner_address')->getData();
				$BankAccount->OwnerAddress->City = $form->get('city')->getData();
				$BankAccount->OwnerAddress->PostalCode = $form->get('postcode')->getData();
				$BankAccount->OwnerAddress->Country = substr($form->get('IBAN')->getData(), 0, 2);
				$BankAccount->Details = new \MangoPay\BankAccountDetailsIBAN();
				$BankAccount->Details->IBAN = $form->get('IBAN')->getData();
			 
				$account = $mangoPayApi->Users->CreateBankAccount($mangoId, $BankAccount);
				
				$user = $this->getUser();
				$em = $this->getDoctrine()->getManager();
				$em->persist($user);
				
				$user->setIban($form->get('IBAN')->getData());
				
				$em->flush();
				
			}

			if (!$account) {
				$request->getSession()->getFlashBag()->add('error', $this->get('translator')->trans('Les information renseignée'));
				return $this->redirect($this->generateUrl('user_bank'));
			}else{
				$request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Compte bancaire enregistré'));
				return $this->redirect($this->generateUrl('user_bank'));
			}
		}

        return array(
            "account" 	=> $account,
            "form" 	=> $form->createView(),
        );    
	}
	
	/**
     * @Route("/photos", name="user_photos")
	 * @Template("UserBundle:User:photos.html.twig")
	 */
    public function photosAction(Request $request)
    {    	
    	$user = $this->getUser();
    	
    	$originalImages = new ArrayCollection();
		
		foreach ($user->getImages() as $image) {
			$originalImages->add($image);
		}	
    	
	    $form = $this->createForm(new ProfessionnelType(), $user);
	    $erreur = '';
	    
	    if ($request->getMethod() == 'POST'){
	       $em = $this->getDoctrine()->getManager();
	       
	       if ($request->request->get('images')){
		   	   foreach ($request->request->get('images') as $image_name){
		   	   		$image = new Image();
		   	   		$user->addImage($image);
		   	   		$em->persist($image);
		   	   		$em->flush();
		   	   		$image->upload($image_name, $user->getShortUsername());
		   	   }
		   	   $em->flush();
	   	   }
		   
           foreach ($originalImages as $image) {
            if (!in_array($image->getId(), is_array($request->request->get('images_old')) ? $request->request->get('images_old') : array())) {
                $em->remove($image);
            }
           } 
	       
	       $em->flush();
	       
	       foreach ($user->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
	       
	       if ($request->request->get('image')){
		       	if ($user->getImage()){
		       		$image = $user->getImage();
		       		$image->upload($request->request->get('image'), $user->getShortUsername());
		       		$em->flush();
		       	}	       		
	       } elseif ($request->request->get('image_old') == '') {
	       	  	$image = $user->getImage();
	       	  	$image->setFilename('no_image.png');
	       	  	$em->flush();
	       }
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Modification effectuée'));
	
	       return $this->redirect($this->generateUrl('user_index'));
	    }
	
	    return array(
	       'form'        => $form->createView(),
	    );
    }
	
	/**
     * @Route("/photospro", name="user_photospro")
	 * @Template("UserBundle:User:photospro.html.twig")
	 */
    public function photosproAction(Request $request)
    {    	
    	$user = $this->getUser();
    	
    	$originalImages = new ArrayCollection();
		
		foreach ($user->getImages() as $image) {
			$originalImages->add($image);
		}	
    	
	    $form = $this->createForm(new ProfessionnelType(), $user);
	    $erreur = '';
	    
	    if ($request->getMethod() == 'POST'){
	       $em = $this->getDoctrine()->getManager();
	       
	       if ($request->request->get('images')){
		   	   foreach ($request->request->get('images') as $image_name){
		   	   		$image = new Image();
		   	   		$user->addImage($image);
		   	   		$em->persist($image);
		   	   		$em->flush();
		   	   		$image->upload($image_name, $user->getShortUsername());
		   	   }
		   	   $em->flush();
	   	   }
		   
           foreach ($originalImages as $image) {
            if (!in_array($image->getId(), is_array($request->request->get('images_old')) ? $request->request->get('images_old') : array())) {
                $em->remove($image);
            }
           } 
	       
	       $em->flush();
	       
	       foreach ($user->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
	       
	       if ($request->request->get('image')){
		       	if ($user->getImage()){
		       		$image = $user->getImage();
		       		$image->upload($request->request->get('image'), $user->getShortUsername());
		       		$em->flush();
		       	}	       		
	       } elseif ($request->request->get('image_old') == '') {
	       	  	$image = $user->getImage();
	       	  	$image->setFilename('no_image.png');
	       	  	$em->flush();
	       }
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Modification effectuée'));
	
	       return $this->redirect($this->generateUrl('user_index'));
	    }
	
	    return array(
	       'form'        => $form->createView(),
	    );
    }

}
