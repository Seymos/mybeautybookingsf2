<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\UserBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;


use FAPROD\UserBundle\Form\OfferType;
use FAPROD\UserBundle\Entity\Offer;
use FAPROD\UserBundle\Entity\Image;

use FAPROD\CoreBundle\Lib\MyConst;
use FAPROD\CoreBundle\Lib\MyTools;

class OfferController extends Controller
{
    /**
     * @Route("/offers", name="offer_index")
     * @Template("UserBundle:Offer:index.html.twig")
     */
    public function indexAction(Request $request)
    {

    }
	
	/**
     * @Route("/offers/prestations", name="user_showprestations")
	 * @Template("UserBundle:Offer:show_prestations.html.twig")
	 */
    public function showprestationsAction(Request $request)
    {

	}

    /**
     * @Route("/offers/show/{offer_id}", name="offer_show", requirements={"offer_id" = "\d+"})
     * @ParamConverter("offer", options={"mapping": {"offer_id": "id"}})
     * @Template("UserBundle:Offer:show.html.twig")
     */
    public function showAction(Offer $offer)
    {

        return array(
            'offer' => $offer,
        );
    }

    /**
     * @Route("/offer/edit/{offer_id}", name="offer_edit", requirements={"offer_id" = "\d+"})
	 * @Template("UserBundle:Offer:edit.html.twig")
	 */
    public function editAction($offer_id, Request $request)
    {

    	if ($offer_id){
    		$offer = $this->getDoctrine()->getManager()->getRepository('UserBundle:Offer')->findOneById($offer_id);
    	} else {
    		$offer = new Offer();
			$offer->setUser($this->getUser());
    	}
		
		$originalImages = new ArrayCollection();
		
		foreach ($offer->getImages() as $image) {
			$originalImages->add($image);
		}	
		
    	if ($offer->getUser()->getId() != $this->getUser()->getId()){
			return $this->redirect($this->generateUrl('offer_index'));
		}
		
		$langues_fo = $this->container->get('FAPROD.Configs')->getFoLangues();
		$duree = $this->container->get('FAPROD.MyConst')->getLength();
		    
	    $form = $this->createForm(new OfferType(), $offer, array(
															'duree'		=> $duree,
															'langues'   => $langues_fo,
														));
	    $form->handleRequest($request);
	    
	    if ($form->isSubmitted() and $form->isValid()) {
	    	
	       $em = $this->getDoctrine()->getManager();
	    	
	       $em->persist($offer);
		   
		   $offer->setTitle($offer->getPrestation()->getTitle());
		   
		   $em->flush();
		   
		   if ($request->request->get('images')){
		   	   foreach ($request->request->get('images') as $image_name){
		   	   		$image = new Image();
		   	   		$offer->addImage($image);
		   	   		$em->persist($image);
		   	   		$em->flush();
		   	   		$image->upload($image_name, "offre");
		   	   }
		   	   $em->flush();
	   	   }
		   
           foreach ($originalImages as $image) {
            if (!in_array($image->getId(), is_array($request->request->get('images_old')) ? $request->request->get('images_old') : array())) {
                $em->remove($image);
            }
           } 
	       
	       $em->flush();
	       
	       foreach ($offer->getImages() as $image) {
            if (!$image->getFilename()) {
                $em->remove($image);
            }
           }
		   
		   $em->flush();
	
	       $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Enregistrement effectué.'));
	
	       return $this->redirect($this->generateUrl('offer_index'));
	    }

		$repository_prestation = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation');
		$queryBuilder = $repository_prestation->createQueryBuilder('p');
		$queryBuilder->where('p.level = 0');
		
		if (count($this->getUser()->getMetiers())) {
			$sql = '';
			
			foreach ($this->getUser()->getMetiers() as $metier) {
				if ($sql)$sql .= ' OR ';
				$sql .= 'p.metiers LIKE :metier'.$metier;
			}
			$queryBuilder->andWhere($sql);
			foreach ($this->getUser()->getMetiers() as $metier) {
				$queryBuilder->setParameter('metier'.$metier, '%'.$metier.'%');
			}
			
		} else {
			$queryBuilder->andWhere('p.metiers = 9999');
		}				
				
		$prestations = $repository_prestation->getPrestations(null, null, 'title', 1, $queryBuilder);
	
	    return array(
	       'form'              => $form->createView(),
		   'langues'           => $langues_fo,
		   'prestations'       => $prestations,
	    );
    }
	
	/**
     * @Route("/offers/prestation", name="offer_prestation")
     * @Template("UserBundle:Offer:prestation.html.twig")
     */
	public function prestationAction(Request $request){
		
		$repository_prestation = $this->getDoctrine()->getManager()->getRepository('UserBundle:Prestation');
		
    	$queryBuilder = $repository_prestation->createQueryBuilder('p');
		$queryBuilder->where('p.level = 1');
		$queryBuilder->leftJoin('p.parent', 'm_parent')->addSelect('m_parent');
		$queryBuilder->andWhere('m_parent.id = :metier_id')->setParameter('metier_id', $request->get('metier_id'));
    	$prestations = $repository_prestation->getPrestations(null, null, 'title', 1, $queryBuilder);
    	
		return array(
					"prestations"  => $prestations,
				);
	}
	
    /**
     * @Route("/offers/delete/{offer_id}", name="offer_delete", requirements={"offer_id" = "\d+"})
     * @ParamConverter("offer", options={"mapping": {"offer_id": "id"}})
     */
    public function deleteAction(Offer $offer, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($offer);
        $em->flush();

        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Suppression effectuée.'));

        return $this->redirect($this->generateUrl('offer_index'));
    }
}
