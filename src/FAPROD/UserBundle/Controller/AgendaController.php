<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\UserBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;

use FAPROD\CoreBundle\Lib\MyConst;

use FAPROD\UserBundle\Entity\Agenda;
use FAPROD\UserBundle\Entity\Horaire;

class AgendaController extends Controller
{
	
	/**
     * @Route("/agenda", name="agenda_index")
	 * @Template("UserBundle:Agenda:index.html.twig")
	 */
    public function indexAction(Request $request)
    {
		$user = $this->getUser();
		
		if ($request->getMethod() == 'POST'){
			
	       $em = $this->getDoctrine()->getManager();
		   
			if ($user->getType() == 2){
	   
			    foreach ($user->getHoraires() as $horaire){
				   $em->remove($horaire);
			    }		   
			   
			    for ($jour = 1; $jour <= 7; $jour++){
			   
				   $cnt = $request->get('cnt_'.$jour);
				   
				   $debut_h = $request->get('jour_'.$jour.'_deb_h');
				   $debut_m = $request->get('jour_'.$jour.'_deb_m');
				   $fin_h = $request->get('jour_'.$jour.'_fin_h');
				   $fin_m = $request->get('jour_'.$jour.'_fin_m');
				   
				   for ($i = 0; $i < $cnt; $i++){
					   if ($debut_h[$i] != '' and $debut_m[$i] != '' and $fin_h[$i] != '' and $fin_m[$i] != ''){
						  $horaire = new Horaire();
						  $horaire->setJour($jour);
						  $horaire->setHeureDeb($debut_h[$i]);
						  $horaire->setMinuteDeb($debut_m[$i]);
						  $horaire->setHeureFin($fin_h[$i]);
						  $horaire->setMinuteFin($fin_m[$i]);
						  $user->addHoraire($horaire);
						  
						  $em->persist($horaire);
					   }
				   }
			    }
				   
			}
				
			$em->flush();
		}
	}
	
	/**
     * @Route("disponibilite", name="agenda_disponibilite")
	 * @Template("UserBundle:Agenda:disponibilite.html.twig")
	*/
	public function disponibiliteAction(Request $request)
	{
		$user = $this->getUser();
		$date = $request->get('date');
		$date_string = explode('_', $request->get('date'));
		$heure_string = explode('h', $date_string[3]);

		$heure = sprintf('%d', floor($heure_string[0]));
		$minute = $heure_string[1];
		
		$now = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, $date_string[1], $date_string[0], $date_string[2])));
    	
    	$repository_agenda = $this->getDoctrine()->getManager()->getRepository('UserBundle:Agenda');
    	$queryBuilder = $repository_agenda->createQueryBuilder('a');
		$queryBuilder->where('a.user = :user')
					 ->andWhere('a.date = :date')
					 ->andWhere('a.heure = :heure')
					 ->andWhere('a.minute = :minute')
					 ->setParameter('user', $user)
					 ->setParameter('heure', $heure)
					 ->setParameter('minute', $minute)
					 ->setParameter('date', $now);
    	$agenda = $queryBuilder->getQuery()->getOneOrNullResult();
    	
    	$em = $this->getDoctrine()->getManager();
    	
		$disponible = false;
    	if ($agenda){
    		$em->remove($agenda);
    		$em->flush();
			$disponible = false;
    	} else {
    		$agenda = new Agenda();
    		$agenda->setUser($user);
    		$agenda->setHeure($heure);
			$agenda->setMinute($minute);
    		$agenda->setDate($now);
    		
    		$em->persist($agenda);
    		$em->flush();
			$disponible = true;
    	}
		
		return array(
					'disponible' => $disponible,
					'date'       => $date,
					);
	}
	
	/**
     * @Route("getagenda", name="agenda_getagenda")
	 * @Template("UserBundle:Agenda:agenda.html.twig")
	*/
	public function getagendaAction(Request $request)
	{	
		$user = $this->getUser();
		
		$semaine = array("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche");
		$datas = array();
		
		$heure_min = 24;
		$heure_max = 0;
		
		foreach ($user->getHoraires() as $horaire){
			if ($horaire->getHeureDeb() < $heure_min){
				$heure_min = $horaire->getHeureDeb();
			}
			if ($horaire->getHeureFin() > $heure_max){
				$heure_max = $horaire->getHeureFin();
			}
		}
		
		$ouvertures = array();
		$ouvertures[1] = $user->getHorairesLundi();
		$ouvertures[2] = $user->getHorairesMardi();
		$ouvertures[3] = $user->getHorairesMercredi();
		$ouvertures[4] = $user->getHorairesJeudi();
		$ouvertures[5] = $user->getHorairesVendredi();
		$ouvertures[6] = $user->getHorairesSamedi();
		$ouvertures[7] = $user->getHorairesDimanche();
		
		$param_begin = $request->get('date') ? $request->get('date') : date('Y-m-d');
		
		$begin = \DateTime::createFromFormat("Y-m-d H:i:s", date($param_begin." 00:00:00"));
		$end = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, explode('-',$param_begin)[1], explode('-',$param_begin)[2]+7, explode('-',$param_begin)[0])));

		$repository_agenda = $this->getDoctrine()->getManager()->getRepository('UserBundle:Agenda');
    	$queryBuilder = $repository_agenda->createQueryBuilder('a');
		$queryBuilder->where('a.user = :user')
					 ->andWhere('a.date >= :begin and a.date <= :end')
					 ->setParameter('user', $user)
					 ->setParameter('begin', $begin)
					 ->setParameter('end', $end);
    	$agendas = $repository_agenda->getAgendas(null, null, "id", 0, $queryBuilder);
		
		$indisponibles = array();
		$agendas_string = array();
		foreach ($agendas as $agenda){
			if ($agenda->getMinute()){
				$string = $agenda->getDate('d_m_Y').'_'.$agenda->getHeure().'.5';
			} else {
				$string = $agenda->getDate('d_m_Y').'_'.$agenda->getHeure();
			}
			
			$indisponibles[$string] = $string;
			if ($agenda->getBooking()){
				$agendas_string[$string] = array($agenda->getBooking()->getId(), $agenda->getBooking()->getUser()->getShortUsername());
			}
		}	
		
		// entete du tableau
		$entete = array();
		
		for($jour = $begin; $begin <= $end; $jour->modify('+1 day')){
			
			$entete[] = array(
						"jour" => $semaine[$jour->format("N")-1],
						"date" => $jour->format("d/m/Y"),
						);
		}
		
		
		// agenda
		$datas = array();
		
		for ($heure = $heure_min; $heure <= $heure_max; $heure += 0.5){
						
			$begin = \DateTime::createFromFormat("Y-m-d H:i:s", date($param_begin." 00:00:00"));
			$end = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, explode('-',$param_begin)[1], explode('-',$param_begin)[2]+7, explode('-',$param_begin)[0])));
			$jours = array();
			
			for($jour = $begin; $begin <= $end; $jour->modify('+1 day')){
				
				$ok = false;
				
				foreach ($ouvertures[$jour->format("N")] as $horaire){
					$heure_deb = $horaire->getMinuteDeb() == 30 ? $horaire->getHeureDeb() + 0.5 : $horaire->getHeureDeb();
					$heure_fin = $horaire->getMinuteFin() == 30 ? $horaire->getHeureFin() + 0.5 : $horaire->getHeureFin();
					if ($heure >= $heure_deb and $heure < $heure_fin){
						$ok = true;
					}
				}
				
				if ($ok){
				
					if (in_array($jour->format("d_m_Y").'_'.$heure, $indisponibles)){
						$jours[] = array(
									"ouverture"     => -1,
									"date"          => $jour->format("d_m_Y"),
									"booking_id"    => isset($agendas_string[$jour->format("d_m_Y").'_'.$heure]) ? $agendas_string[$jour->format("d_m_Y").'_'.$heure][0] : "",
									"booking_user"  => isset($agendas_string[$jour->format("d_m_Y").'_'.$heure]) ? $agendas_string[$jour->format("d_m_Y").'_'.$heure][1] : "",
									);
					} else {
						$jours[] = array(
									"ouverture"     => 1,
									"date"          => $jour->format("d_m_Y"),
									"booking_id"    => "",
									"booking_user"  => "",									
									);
					}
				
				} else {
					$jours[] = array(
									"ouverture"     => 0,
									"booking_id"    => "",
									"booking_user"  => "",	
									);
				}
			}
			
			$datas[] = array(
							"heure"  => strpos($heure, ".") ? sprintf('%02d', floor($heure)).'h30' : sprintf('%02d', $heure).'h',
							"jours"  => $jours,
							);

		}
		
		$last_date = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, explode('-',$param_begin)[1], explode('-',$param_begin)[2]-8, explode('-',$param_begin)[0])));
		$next_date = \DateTime::createFromFormat("Y-m-d H:i:s", date('Y-m-d 00:00:00', mktime(0, 0, 0, explode('-',$param_begin)[1], explode('-',$param_begin)[2]+8, explode('-',$param_begin)[0])));
	    	
    	return array(
				   'datas'       => $datas,
				   'entete'      => $entete,
				   'user'        => $user,
				   'last_date'   => $last_date->format('Y-m-d'),
				   'next_date'   => $next_date->format('Y-m-d'),
				);
	}
	
	

}
