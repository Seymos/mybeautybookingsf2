<?php

/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\CronBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\CoreBundle\Lib\MyTools;
use FAPROD\CoreBundle\Lib\MyConst;


class DailyController extends Controller
{
	
	/**
     * @Route("/daily", name="cron_daily")
     * @Template("CronBundle:Daily:index.html.twig")
	 */
    public function indexAction(Request $request)
    {     	
    	$this->reservationExpire();
    }
	
	public function reservationExpire()
	{
		$repository_booking = $this->getDoctrine()->getManager()->getRepository('BookingBundle:Booking');
	    $queryBuilder = $repository_booking->createQueryBuilder('b');
		$queryBuilder->where('b.statut = 2');	
		$queryBuilder->andWhere('b.updated_at >= :end_date_1 and b.updated_at <= :end_date_2')
					 ->setParameter('end_date_1', new \DateTime('- 6 days'))
					 ->setParameter('end_date_2', new \DateTime('- 5 days'));
		
		$liste = $repository_booking->getBookings(null, null, "updated_at", 0, $queryBuilder);
		$em = $this->getDoctrine()->getManager();
		
		foreach ($liste as $booking){
			
			$booking->setStatut(-1);
			$em->persist($booking);
			$em->flush();
			
			$stmt = $em->getConnection()
					->prepare("delete from agenda where `booking_id` = :booking_id");
			$stmt->bindValue('booking_id',$booking->getId());
			$stmt->execute();		
			
			$mailBody = $this->renderView('BookingBundle:Mail:booking_ko.html.twig', array(
																					'booking' => $booking,
																					));
			
			$message = \Swift_Message::newInstance()
				->setSubject('Votre réservation a été annulée (n°' . $booking->getId() . ")")
				->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
				->setTo($booking->getUser()->getEmail())
				->setBody($mailBody, 'text/html');
			$this->get('mailer')->send($message);
		}

	}
    
   
}
