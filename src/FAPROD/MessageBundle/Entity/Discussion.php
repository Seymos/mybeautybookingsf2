<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\MessageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="FAPROD\MessageBundle\Entity\DiscussionRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="discussion")
 */
class Discussion
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User", inversedBy="discussions1")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User", inversedBy="discussions2")
     * @ORM\JoinColumn(name="destinataire_id", referencedColumnName="id", nullable=true)
     */
    private $destinataire;
    
    /**
     * @ORM\OneToMany(targetEntity="FAPROD\MessageBundle\Entity\Message", mappedBy="discussion", cascade={"remove","persist"})
     * @OrderBy({"id" = "ASC"})
     */
    private $messages;
    
    /**
     * @ORM\Column(name="nb_message", type="integer", nullable=true)
     */
    private $nb_message;
    
    /**
     * @ORM\Column(name="nb_noread_user", type="integer", nullable=true)
     */
    private $nb_noread_user;
    
    /**
     * @ORM\Column(name="nb_noread_destinataire", type="integer", nullable=true)
     */
    private $nb_noread_destinataire;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {
		$this->nb_message = 0;
		$this->nb_noread_user = 0;
		$this->nb_noread_destinataire = 0;
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
  
	public function __toString()
    {
		return $this->getTitle();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setDestinataire(\FAPROD\UserBundle\Entity\User $destinataire = null)
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    public function getDestinataire()
    {
        return $this->destinataire;
    }

    public function addMessage(\FAPROD\MessageBundle\Entity\Message $message)
    {
        $this->messages[] = $message;
        
        $message->setDiscussion($this);

        return $this;
    }

    public function removeMessage(\FAPROD\MessageBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function setNbMessage($nbMessage)
    {
        $this->nb_message = $nbMessage;

        return $this;
    }

    public function getNbMessage()
    {
        return $this->nb_message;
    }

    public function setNbNoreadUser($nbNoreadUser)
    {
        $this->nb_noread_user = $nbNoreadUser;

        return $this;
    }

    public function getNbNoreadUser()
    {
        return $this->nb_noread_user;
    }

    public function setNbNoreadDestinataire($nbNoreadDestinataire)
    {
        $this->nb_noread_destinataire = $nbNoreadDestinataire;

        return $this;
    }

    public function getNbNoreadDestinataire()
    {
        return $this->nb_noread_destinataire;
    }

   
}
