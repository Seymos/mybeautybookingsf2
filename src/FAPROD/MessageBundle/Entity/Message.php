<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\MessageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping\OrderBy;

/**
 * @ORM\Entity(repositoryClass="FAPROD\MessageBundle\Entity\MessageRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="message")
 */
class Message
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\MessageBundle\Entity\Discussion", inversedBy="messages")
     * @ORM\JoinColumn(name="discussion_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $discussion;
  
    /**
     * @ORM\Column(name="texte", type="text", nullable=true)
     */
    private $texte;
    
    /**
     * @ORM\Column(name="lu", type="boolean", nullable=true)
     */
    private $lu;

    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="destinataire_id", referencedColumnName="id", nullable=true)
     */
    private $destinataire;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {
		$this->lu = false;
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
  
	public function __toString()
    {
		return $this->getTexte() ? $this->getTexte() : $this->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setTexte($texte)
    {
        $this->texte = $texte;

        return $this;
    }

    public function getTexte()
    {
        return $this->texte;
    }

    public function setDestinataire(\FAPROD\UserBundle\Entity\User $destinataire = null)
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    public function getDestinataire()
    {
        return $this->destinataire;
    }

    public function setLu($lu)
    {
        $this->lu = $lu;

        return $this;
    }

    public function getLu()
    {
        return $this->lu;
    }
    
    public function getLuLabel()
    {
        if ($this->lu){
        	return 'Oui';
        } else {
        	return 'Non';
        }
    }

    public function setDiscussion(\FAPROD\MessageBundle\Entity\Discussion $discussion = null)
    {
        $this->discussion = $discussion;

        return $this;
    }

    public function getDiscussion()
    {
        return $this->discussion;
    }
}
