<?php

namespace FAPROD\MessageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiscussionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('texte', 'textarea', array('required' => true, 'mapped' => false, 'attr' => array('cols' => '5', 'rows' => '3')))
            ->add('user', 'entity_hidden', array(
                'class'    => 'FAPROD\UserBundle\Entity\User',
                'required' => true,
		    ))
		    ->add('destinataire', 'entity_hidden', array(
                'class'    => 'FAPROD\UserBundle\Entity\User',
                'required' => true,
		    ))
        ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FAPROD\MessageBundle\Entity\Discussion'
        ));
    }

    public function getName()
    {
        return 'faprod_messagebundle_discussion';
    }
}
