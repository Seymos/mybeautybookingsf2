<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\MessageBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\UserBundle\Entity\User;

class PartialController extends Controller
{
	
	/**
     * @Template("MessageBundle:Message:nbmessages.html.twig")
	 */
	public function usermessagesAction()
	{              
		$repository_discussion = $this->getDoctrine()->getManager()->getRepository('MessageBundle:Discussion');
		
    	$queryBuilder = $repository_discussion->createQueryBuilder('d');
		$result1 = $queryBuilder->select('SUM(d.nb_noread_user)')
								 ->where('d.user = :user')
								 ->setParameter('user', $this->getUser())
								 ->getQuery()->getSingleScalarResult();
		
		$queryBuilder = $repository_discussion->createQueryBuilder('d');
		$result2 = $queryBuilder->select('SUM(d.nb_noread_destinataire)')
								 ->where('d.destinataire = :user')
								 ->setParameter('user', $this->getUser())
								 ->getQuery()->getSingleScalarResult();
		
		$result = 0;
		$result += (int)$result1;
		$result += (int)$result2;
    
	  	return array(
					'result' => $result,
					);
	}

}
