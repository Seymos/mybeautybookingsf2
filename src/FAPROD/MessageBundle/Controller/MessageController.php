<?php

/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */


namespace FAPROD\MessageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\UserBundle\Entity\User;
use FAPROD\MessageBundle\Entity\Message;
use FAPROD\MessageBundle\Form\MessageType;
use FAPROD\MessageBundle\Form\DiscussionType;
use FAPROD\MessageBundle\Entity\Discussion;
use FAPROD\UserBundle\Entity\Stat as StatUser;
use FAPROD\UserBundle\Entity\Agenda;
use FAPROD\BookingBundle\Entity\Booking;

class MessageController extends Controller
{
	const ITEMS_PAR_PAGE = 10;
	
	/**
     * @Route("/messages/{page}/{sort}/{sens}", name="message_index", requirements={"page" = "\d+"}, defaults={"page" = "1"})
     * @Template("MessageBundle:Message:index.html.twig")
	 */
    public function indexAction(Request $request, $page, $sort = "updated_at", $sens = 0)
    {
		$user = $this->getUser();
    	
    	$repository_discussion = $this->getDoctrine()->getManager()->getRepository('MessageBundle:Discussion');
	    $queryBuilder = $repository_discussion->createQueryBuilder('d');
		$queryBuilder->where('destinataire = :user OR user = :user')->setParameter(':user', $user);
	    
        return $this->initPagination($page, "updated_at", 0, $queryBuilder, 'message_index');
    }
	
	/**
     * @Route("/messages/popup", name="message_popup")
     * @Template("MessageBundle:Message:show_messagerie.html.twig")
	 */
    public function popupAction(Request $request)
    {
		$user = $this->getUser();
    	
    	$repository_discussion = $this->getDoctrine()->getManager()->getRepository('MessageBundle:Discussion');
	    $queryBuilder = $repository_discussion->createQueryBuilder('d');
		$queryBuilder->where('destinataire = :user OR user = :user')->setParameter(':user', $user);
	    
        $liste = $this->getDoctrine()
					  ->getManager()
					  ->getRepository('MessageBundle:Discussion')
					  ->getDiscussions(1, 5, "updated_at", 0, $queryBuilder)
					;
					
		return array(
					"liste" => $liste,
					);
    }
    
    /**
     * @Route("/messages/newdiscussion/{destinataire_id}", name="message_newdiscussion", requirements={"destinataire_id" = "\d+"})
	 * @ParamConverter("destinataire", options={"mapping": {"destinataire_id": "id"}})
	 */
    public function newdiscussionAction(User $destinataire, Request $request)
    {
    	$discussion = new Discussion();
			
		$em = $this->getDoctrine()->getManager();
		$discussion->setUser($this->getUser());
		$discussion->setDestinataire($destinataire);
		$discussion->setTitle("Nouvelle discussion"); 
		$em->persist($discussion);

		$discussion->setNbMessage(0);
		$discussion->setNbNoreadDestinataire(0);
		
		$em->flush();
		
		return $this->redirect($this->generateUrl('message_show', array('discussion_id' => $discussion->getId())));

    }
	
	
	/**
     * @Route("/messages/discussion/{discussion_id}", name="message_show", requirements={"discussion_id" = "\d+"})
	 * @ParamConverter("discussion", options={"mapping": {"discussion_id": "id"}})
	 * @Template("MessageBundle:Message:show.html.twig")
	 */
	public function showAction(Discussion $discussion, Request $request)
	{
		if ($discussion->getUser()->getId() != $this->getUser()->getId() and $discussion->getDestinataire()->getId() != $this->getUser()->getId()){
			return $this->redirect($this->generateUrl('message_index'));
		}
		
		$em = $this->getDoctrine()->getManager();
		
		$message = new Message();
		$message->setUser($this->getUser());
		if ($discussion->getUser() == $this->getUser()){
			$message->setDestinataire($discussion->getDestinataire());
		} else {
			$message->setDestinataire($discussion->getUser());
		}

		$form = $this->createForm(new MessageType(), $message);
		$form->handleRequest($request);
		
		if ($form->isSubmitted() and $form->isValid()) {
	        $em->persist($message);
			$em->persist($discussion);
	        $discussion->addMessage($message);
	        $discussion->setNbMessage($discussion->getNbMessage() + 1);
	        if ($discussion->getUser() == $this->getUser()){
		        $discussion->setNbNoreadDestinataire($discussion->getNbNoreadDestinataire() + 1);
		        $email_destinataire = $discussion->getDestinataire()->getEmail();
		        $destinataire = $discussion->getDestinataire();
	        } else {
	        	$discussion->setNbNoreadUser($discussion->getNbNoreadUser() + 1);
	        	$email_destinataire = $discussion->getUser()->getEmail();
	        	$destinataire = $discussion->getUser();
	        }
	        $em->flush();
	        
	        $mailBody = $this->renderView('MessageBundle:Mail:alerte_message.html.twig', array('user' => $destinataire));
	        $message = \Swift_Message::newInstance()
		        ->setSubject($this->get('translator')->trans('Nouveau message'))
		        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		        ->setTo($email_destinataire)
		        ->setBody($mailBody, 'text/html');
		    //echo $mailBody;exit;
		    $this->get('mailer')->send($message);
	        
	        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Message envoyé.'));
	        
	        return $this->redirect($this->generateUrl('message_show', array('discussion_id' => $discussion->getId())));
    	}
    	
    	if ($discussion->getUser() == $this->getUser()){
    		$discussion->setNbNoreadUser(0);
		} else {
			$discussion->setNbNoreadDestinataire(0);
		}
		
		$em->flush();
		
		return array(
					'discussion' => $discussion,
					'form'       => $form->createView(),
					);
	}
    
    public function initPagination($page, $sort, $sens, $queryBuilder, $action){
    	
    	if ($page < 1) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    $nbPerPage = MessageController::ITEMS_PAR_PAGE;
	    
    	$liste = $this->getDoctrine()
	      ->getManager()
	      ->getRepository('MessageBundle:Discussion')
	      ->getDiscussions($page, $nbPerPage, $sort, $sens, $queryBuilder)
	    ;
	    
	    $nbPages = ceil(count($liste)/$nbPerPage);

	    if ($page > $nbPages and $nbPages > 0) {
	      throw $this->createNotFoundException("La page ".$page." n'existe pas.");
	    }
	    
	    $nb_result = $this->getDoctrine()
							->getManager()
						      ->getRepository('MessageBundle:Discussion')->getDiscussionsCount($queryBuilder);
	
	    return array(
	      'liste'       => $liste,
	      'nbPages'     => $nbPages,
	      'page'        => $page,
	      'sort'        => $sort,
	      'sens'        => $sens,
	      'sens_inv'    => $sens ? 0 : 1,
		  'action'      => $action,
		  'nb_result'   => $nb_result,
	    );
    }
}
