<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\ContentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use FAPROD\ContentBundle\Entity\Content;
use FAPROD\ContentBundle\Entity\TypeContent;
use FAPROD\ContentBundle\Form\ContentCommentType;

class ContentController extends Controller
{
	const ITEMS_PAR_PAGE = 16;
	
	/**
     * @Route("/mag/partage/{content_id}", name="content_partage", requirements={"content_id" = "\d+"})
     * @ParamConverter("content", options={"mapping": {"content_id": "id"}})
     * @Template("ContentBundle:Content:partage.html.twig")
	 */
    public function partageAction(Content $content, Request $request)
    {
    	if ($request->getMethod() == 'POST')
        {
        	$validator = $this->container->get('validator');
        	$constraints = array(
		        new \Symfony\Component\Validator\Constraints\Email(),
		        new \Symfony\Component\Validator\Constraints\NotBlank()
		    );
        	
        	$emails = explode(',', str_replace(' ','',$request->request->get('emails')));
        	
        	foreach ($emails as $email){
        	
	        	try{
	        		
	        		$error = $validator->validateValue($email, $constraints);
					
					if (count($error) == 0) {

			        	$mailBody = $this->renderView('ContentBundle:Mail:partage.html.twig', array('content' => $content, 'prenom' => $request->request->get('prenom')));
			        	//echo $mailBody;exit;
				        $message = \Swift_Message::newInstance()
					        ->setSubject($request->request->get('prenom').' '.$this->get('translator')->trans('souhaite vous faire découvrir').' '.$this->get('FAPROD.MyConst')->getSiteName())
					        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
					        ->setTo($email)
					        ->setBody($mailBody, 'text/html');
					    
					    $this->get('mailer')->send($message);
					    
				    }
				    
				} catch (\Exception $e) {
	
				}
			
			}
	    
	        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Message envoyé.'));
	        
	        return $this->redirect($this->generateUrl('content_partage', array('content_id' => $content->getId())));
    	}
    	
    	return array(
    				'content' => $content,
    				);
	}
	
	/**
     * @Route("/mag/comment/{content_id}", name="content_comment", requirements={"content_id" = "\d+"})
	 * @ParamConverter("content", options={"mapping": {"content_id": "id"}})
	 */
	public function commentAction(Content $content, Request $request){
		
		$form = $this->createForm(new ContentCommentType());
		
		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED') and $request->getMethod() == 'POST' and $this->getUser()->getId())
        {
            $form->handleRequest($request);
            $comment = $form->getData();
            $comment->setUser($this->getUser());
            $comment->setContent($content);
            $comment->setIpAddress($_SERVER['REMOTE_ADDR']);
			$comment->setValidate(1);
			
			$em = $this->getDoctrine()->getManager();
	        $em->persist($comment);
	        $em->flush();
	    
	        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Commentaire enregistré.'));
	    }
	    
	    return $this->redirect($this->generateUrl('content_show', array('content_id' => $content->getId(), 't' => $content->getStrippedTitle())));
	}
	
	/**
     * @Route("/mag/gosearch", name="content_gosearch")
	 */
	public function gosearchAction(Request $request)
	{	
		if ($request->getMethod() == 'POST' or $request->query->get('update')){
			$request->getSession()->set('search_mots', $request->get('search_mots'));
			$request->getSession()->set('search_blog_category', $request->request->get('search_category') ? $request->request->get('search_category') : $request->query->get('search_category'));
		}

        return $this->redirect($this->generateUrl('content_search'));
	}
	
	/**
     * @Route("/mag/recherche", name="content_search")
     * @Template("ContentBundle:Content:index.html.twig")
     */
    public function searchAction(Request $request)
    {
	    $nbPerPage = ContentController::ITEMS_PAR_PAGE;
	    $queryBuilder = $this->search($request);
	    
	    $repository_content = $this->getDoctrine()
								->getManager()
								     ->getRepository('ContentBundle:Content');
	    
    	$contents = $repository_content->getContents(1, $nbPerPage, "id", 0, $queryBuilder);
	      
	    $nb_result = $repository_content->getContentsCount($queryBuilder);
		
	    return array(
	      'contents'       => $contents,
		  'nb_result'      => $nb_result,
		  'nbPerPage'      => $nbPerPage,
		);
    }
    
    public function search(Request $request){
    	
    	$search_mots                 = $request->getSession()->get('search_mots');
    	$search_category             = $request->getSession()->get('search_blog_category');
		
		$queryBuilder = $this->getDoctrine()
						      ->getManager()
						      ->getRepository('ContentBundle:Content')
						      ->createQueryBuilder('c');
						      
		$queryBuilder->where('c.is_publish = 1');
		
		if ($search_category){
			$queryBuilder->leftJoin('c.category', 'cat')->addSelect('cat');
			$queryBuilder->leftJoin('cat.parent', 'c_parent')->addSelect('c_parent');
			$queryBuilder->andWhere('cat.id = :category_id OR c_parent.id = :category_id')->setParameter('category_id', $search_category);
		}
		if ($search_mots){
			$search_mots = strtolower($search_mots);
		    $search_mots = $this->container->get('FAPROD.MyTools')->netoyage($search_mots);
		    $mots = explode(" ",$search_mots);
		    $nombre_mots=count($mots);
	    	
		    $string_query = 'trans.title LIKE :mot_0 OR trans.html LIKE :mot_0';
			
			$z=1;
			while($z<$nombre_mots) {
				$string_query .= ' OR trans.title LIKE :mot_'.$z.' OR trans.html LIKE :mot_'.$z;
	    		$z++;
			}
			
			$queryBuilder->andWhere($string_query);
			$queryBuilder->setParameter('mot_0', "%".$mots[0]."%");
			
			$z=1;
			while($z<$nombre_mots) {
	    		$queryBuilder->setParameter('mot_'.$z, "%".$mots[$z]."%");
	    		$z++;
			}
		}
		
		return $queryBuilder;
    }
	
	/**
     * @Route("/mag", name="content_index")
	 * @Template("ContentBundle:Content:index.html.twig")
	 */
    public function indexAction(Request $request)
    {
    	$nbPerPage = ContentController::ITEMS_PAR_PAGE;
    	
	    $repository_content = $this->getDoctrine()->getManager()->getRepository('ContentBundle:Content');
	    $queryBuilder = $repository_content->createQueryBuilder('c');
	    $queryBuilder->where('c.is_publish = 1');
        $contents = $repository_content->getContents(1, $nbPerPage, "id", 0, $queryBuilder);
	      
	    $nb_result = $this->getDoctrine()
							->getManager()
						      ->getRepository('ContentBundle:Content')->getContentsCount($queryBuilder);
		
	    return array(
	      'contents'       => $contents,
		  'nb_result'      => $nb_result,
		  'nbPerPage'      => $nbPerPage,
		);
    }
    
    /**
     * @Route("/mag/ajaxcontents", name="content_ajaxcontents")
     * @Template("ContentBundle:Partial:contents.html.twig")
     */
    public function ajaxcontentsAction(Request $request) 
    {
        $nbPerPage = ContentController::ITEMS_PAR_PAGE;
        $page = $request->query->get('page');
	    $sort = $request->query->get('sort') ? $request->query->get('sort') : "id";
	    $sens = $request->query->get('sens');
	    
    	$repository_content = $this->getDoctrine()->getManager()->getRepository('ContentBundle:Content');
        $contents = $repository_content->getContents($page, $nbPerPage, $sort, $sens, $this->search($request));
         
	    return array('contents' => $contents);
	}
    
    /**
     * @Route("/mag/article/{t}/{content_id}", name="content_show", requirements={"content_id" = "\d+"})
	 * @ParamConverter("content", options={"mapping": {"content_id": "id"}})
	 * @Template("ContentBundle:Content:show.html.twig")
	 */
	public function showAction(Content $content)
	{		
		$form_comment = $this->createForm(new ContentCommentType());
		
	  	return array(
	    	'content'         => $content,
	    	'form_comment'    => $form_comment->createView(),
	  	);
	}

    /**
     * @Route("/mentions-legales", name="mentions_legales")
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function legal()
    {
        return $this->render(
            '@Content/Content/mentions_legales.html.twig'
        );
    }
}
