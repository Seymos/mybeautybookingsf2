<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\ContentBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use FAPROD\ContentBundle\Entity\Zone;

class PartialController extends Controller
{	
	/**
	 * @ParamConverter("zone")
	 */
	public function zoneAction(Zone $zone)
	{
		$response = $this->render('ContentBundle:Partial:zone.html.twig', array('zone' => $zone	));
    	$response->setSharedMaxAge(10000);
    	$response->setPublic();
        $response->setMaxAge(10000);
    
	  	return $response;
	}
	
	public function searchAction(Request $request)
	{	
		
		$repository_category = $this->getDoctrine()->getManager()->getRepository('UserBundle:Category');
    	$queryBuilder = $repository_category->createQueryBuilder('c');
		$queryBuilder->where('c.level = 0 AND c.blog = 1');
    	$categorys = $repository_category->getCategorys(null, null, 'no_order', 1, $queryBuilder);
	    	
    	return $this->render('ContentBundle:Partial:search.html.twig', array(
																   'categorys'     => $categorys,
															    ));
	}
}
