<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\ContentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContentType extends AbstractType
{
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', 'a2lix_translations', array(
				    'locales' => $options['langues'],
				    //'required_locales' => array('fr'),      // [2]
				    'fields' => array(                      // [3]
				    	'title' => array( 
				        	'field_type' => 'text'
				        ),
				        'description' => array(                   // [3.a]
				            'field_type' => 'text',                 // [4]
				            'required'   => false,
				            //'label' => 'descript.',                     // [4]
				            //'locale_options' => array(            // [3.b]
				            //    'es' => array(
				            //        'label' => 'descripción'            // [4]
				            //    ),
				            //    'fr' => array(
				            //        'display' => false                  // [4]
				            //    )
				            //)
				        ),
				        'stripped_title' => array( 
				        	'display' => false
				        ),
				        'html' => array( 
				        	'field_type' => 'textarea',
				        	'required'   => false,
				        ),
				        'meta_title' => array( 
				        	'field_type' => 'text',
				        	'required'   => false,
				        ),
				        'meta_description' => array( 
				        	'field_type' => 'text',
				        	'required'   => false,
				        ),
				        'meta_keywords' => array( 
				        	'field_type' => 'text',
				        	'required'   => false,
				        ),
				    )
				)
			)
			->add('type_content')
            ->add('is_publish')
            ->add('category')
            ->add('enligne_date', 'date', array('input'  => 'datetime','widget' => 'choice', 'empty_value' => '', 'required' => false))
            ->add('image_principale', 'hidden', array('mapped' => false))
            ->add('image_principale_old', 'hidden', array('mapped' => false))
            ->add('images', 'hidden', array('mapped' => false))
            ->add('images_old', 'hidden', array('mapped' => false))
        ;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
	    $resolver->setDefaults(array('data_class'  => 'FAPROD\ContentBundle\Entity\Content',
	    							  'langues'    => array('fr'),
	    						));
	}
	    
	public function getDefaultOptions(array $options)
	{
	    return array('data_class' => 'FAPROD\ContentBundle\Entity\Content',
	    			'langues'     => null,
	    			);
	}

    /**
     * @return string
     */
    public function getName()
    {
        return 'faprod_contentbundle_content';
    }
}
