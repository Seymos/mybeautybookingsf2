<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslatable;

use FAPROD\CoreBundle\Lib\MyConst;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="zone")
 * @ORM\Entity(repositoryClass="FAPROD\ContentBundle\Entity\ZoneRepository")
 */
class Zone extends AbstractTranslatable
{
	
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Prezent\Translations(targetEntity="ZoneTranslation")
     */
    protected $translations;
    
    /**
     * @Prezent\CurrentLocale
     */
    private $currentLocale;
    
    /**
     * @ORM\Column(name="is_publish", type="boolean", nullable=true)
     */
    private $is_publish;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    private $currentTranslation; // Cache current translation. Useful in Doctrine 2.4+
    

    public function __construct()
    {
		$this->translations = new ArrayCollection();
    }
    
    public static function getTranslationEntityClass()
    {
        return 'FAPROD\ContentBundle\Entity\ZoneTranslation';
    }
    
    public function translate($locale = null)
    {
        if (null === $locale) {
            $locale = $this->currentLocale;
        }

        if (!$locale) {
            throw new \RuntimeException('No locale has been set and currentLocale is empty');
        }

        if ($this->currentTranslation && $this->currentTranslation->getLocale() === $locale) {
            return $this->currentTranslation;
        }

        if (!$translation = $this->translations->get($locale)) {
            $translation = new ZoneTranslation();
            $translation->setLocale($locale);
            $this->addTranslation($translation);
        }

        $this->currentTranslation = $translation;
        return $translation;
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}


    // Proxy getters and setters

    public function getTitle($locale = null)
    {
        return $this->translate($locale)->getTitle();
    }

    public function setTitle($title)
    {
        $this->translate()->setTitle($title);
        return $this;
    }
    
    public function getHtml($locale = null)
    {
        return $this->translate($locale)->getHtml();
    }

    public function setHtml($zone)
    {
        $this->translate()->setHtml($zone);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setEnligneDate($enligneDate)
    {
        $this->enligne_date = $enligneDate;

        return $this;
    }

    public function getEnligneDate($format = '')
    {
        if($this->enligne_date and $this->enligne_date instanceof \DateTime and $format){
		    return $this->enligne_date->format($format);
		} else {
    		return $this->enligne_date;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
        if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
    	if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}      
    }

    public function setIsPublish($isPublish)
    {
        $this->is_publish = $isPublish;

        return $this;
    }

    public function getIsPublish()
    {
        return $this->is_publish;
    }
    
    public function getIsPublishLabel()
    {
        if ($this->getIsPublish())
        {
            return "Oui";
        } else {
            return "";
        }
    }
}
