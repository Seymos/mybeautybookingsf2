<?php

namespace FAPROD\ContentBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
class ContentCommentRepository extends EntityRepository
{
	public function getComments($page = null, $nbPerPage = null, $sort = 'id', $sens = 0, $QueryBuilder = '')
    {
    	if (!$QueryBuilder)$QueryBuilder = $this->createQueryBuilder('c');
    	
    	$query = $QueryBuilder
    	            ->leftJoin('c.user', 'user')->addSelect('user')
    	            ->leftJoin('c.vendeur', 'vendeur')->addSelect('vendeur')
		    		->orderBy('c.'.$sort, $sens ? 'ASC' : 'DESC')
			      	->getQuery();
	
	    if ($page and $nbPerPage){
		    $query
		      ->setFirstResult(($page-1) * $nbPerPage)
		      ->setMaxResults($nbPerPage)
		    ;
		    
		    return new Paginator($query, true);
    	} else {
    		return $query->getResult();
    	}
    }
    
    public function getCommentsCount($QueryBuilder = '')
    {
    	if (!$QueryBuilder)$QueryBuilder = $this->createQueryBuilder('c');
    	
    	$query = $QueryBuilder->getQuery();
	
	    $paginator = new Paginator($query);
        return count($paginator);
    }
    
    public function getValidComments($content_id){
    	$queryBuilder = $this->createQueryBuilder('c');
		$queryBuilder->where('c.validate = 1');
		$queryBuilder->andWhere('a.id = :content_id')->setParameter('content_id', $content_id);
    	return $repository_comment->getComments(null, null, 'id', 0, $queryBuilder);
    }
}
