<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslatable;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping\OrderBy;

use FAPROD\CoreBundle\Lib\MyConst;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="content")
 * @ORM\Entity(repositoryClass="FAPROD\ContentBundle\Entity\ContentRepository")
 */
class Content extends AbstractTranslatable
{
	
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Prezent\Translations(targetEntity="ContentTranslation")
     */
    protected $translations;
    
    /**
     * @Prezent\CurrentLocale
     */
    private $currentLocale;
    
    /**
     * @ORM\OneToOne(targetEntity="FAPROD\ContentBundle\Entity\Image", cascade={"persist"})
     * @Assert\Valid()
     */
    private $image_principale;
    
    /**
     * @ORM\OneToMany(targetEntity="FAPROD\ContentBundle\Entity\Image", mappedBy="content", cascade={"persist"})
     * @OrderBy({"id" = "DESC"})
     * @Assert\Valid()
     */
    private $images;
    
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\ContentBundle\Entity\TypeContent")
     * @ORM\JoinColumn(nullable=true)
     */
    private $type_content;
    
    /**
     * @ORM\OneToMany(targetEntity="FAPROD\ContentBundle\Entity\ContentComment", mappedBy="content", cascade={"persist"})
     * @OrderBy({"id" = "DESC"})
     */
    private $comments;
    
    /**
     * @ORM\Column(name="is_publish", type="boolean", nullable=true)
     */
    private $is_publish;
    
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\Category")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;
	
	/**
     * @ORM\Column(name="enligne_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $enligne_date;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    
    public $currentTranslation; // Cache current translation. Useful in Doctrine 2.4+
    

    public function __construct()
    {
		$this->translations = new ArrayCollection();
		$this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->image_principale = new \FAPROD\ContentBundle\Entity\Image();
    }
	
	/**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
		
		if ($this->image_principale){
		  $new_tempFilename = $this->image_principale->getUploadRootDir().'/thumb/c'.$this->image_principale->getId().'.'.$this->image_principale->getFilename();
			$new_tempFilename_zoom = $this->image_principale->getUploadRootDir().'/zoom/zoom_c'.$this->image_principale->getId().'.'.$this->image_principale->getFilename();
			
		  // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
		  if (file_exists($new_tempFilename) and $this->image_principale->getFilename() != 'no_image.png') {
			// On supprime le fichier
			unlink($new_tempFilename);
		  }
		  if (file_exists($new_tempFilename_zoom) and $this->image_principale->getFilename() != 'no_image.png') {
			// On supprime le fichier
			unlink($new_tempFilename_zoom);
		  }
	  }
	  
	  foreach ($this->images as $image){
		  $new_tempFilename = $image->getUploadRootDir().'/thumb/c'.$image->getId().'.'.$image->getFilename();
			$new_tempFilename_zoom = $image->getUploadRootDir().'/zoom/zoom_c'.$image->getId().'.'.$image->getFilename();
			
		  // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
		  if (file_exists($new_tempFilename) and $image->getFilename() != 'no_image.png') {
			// On supprime le fichier
			unlink($new_tempFilename);
		  }
		  if (file_exists($new_tempFilename_zoom) and $image->getFilename() != 'no_image.png') {
			// On supprime le fichier
			unlink($new_tempFilename_zoom);
		  }
	  }
    }
    
    public static function getTranslationEntityClass()
    {
        return 'FAPROD\ContentBundle\Entity\ContentTranslation';
    }
    
    public function translate($locale = null)
    {
        if (null === $locale) {
            $locale = $this->currentLocale ? $this->currentLocale : 'fr';
        }

        if (!$locale) {
            throw new \RuntimeException('No locale has been set and currentLocale is empty');
        }

        if ($this->currentTranslation && $this->currentTranslation->getLocale() === $locale) {
            return $this->currentTranslation;
        }

        if (!$translation = $this->translations->get($locale)) {
            $translation = new ContentTranslation();
            $translation->setLocale($locale);
            $this->addTranslation($translation);
        }

        $this->currentTranslation = $translation;
        return $translation;
    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
	
	public function __toString()
    {
		return $this->getTitle();
    }


    // Proxy getters and setters

    public function getTitle($locale = null)
    {
        return $this->translate($locale)->getTitle();
    }

    public function setTitle($title)
    {
        $this->translate()->setTitle($title);
        return $this;
    }
    
    public function getStrippedTitle($locale = null)
    {
        return $this->translate($locale)->getStrippedTitle();
    }

    public function setStrippedTitle($content)
    {
        $this->translate()->setStrippedTitle($content);
        return $this;
    }

    public function getDescription($locale = null)
    {
        return $this->translate($locale)->getDescription();
    }

    public function setDescription($content)
    {
        $this->translate()->setDescription($content);
        return $this;
    }
    
    public function getHtml($locale = null)
    {
        return $this->translate($locale)->getHtml();
    }

    public function setHtml($content)
    {
        $this->translate()->setHtml($content);
        return $this;
    }
    
    public function getMetaTitle($locale = null)
    {
        return $this->translate($locale)->getMetaTitle();
    }

    public function setMetaTitle($content)
    {
        $this->translate()->setMetaTitle($content);
        return $this;
    }
    
    public function getMetaDescription($locale = null)
    {
        return $this->translate($locale)->getMetaDescription();
    }

    public function setMetaDescription($content)
    {
        $this->translate()->setMetaDescription($content);
        return $this;
    }
    
    public function getMetaKeywords($locale = null)
    {
        return $this->translate($locale)->getMetaKeywords();
    }

    public function setMetaKeywords($content)
    {
        $this->translate()->setMetaKeywords($content);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setImagePrincipale(\FAPROD\ContentBundle\Entity\Image $imagePrincipale = null)
    {
        $this->image_principale = $imagePrincipale;
		$imagePrincipale->setFilename('no_image.png');
        return $this;
    }

    public function getImagePrincipale()
    {
        return $this->image_principale;
    }

    public function setEnligneDate($enligneDate)
    {
        $this->enligne_date = $enligneDate;

        return $this;
    }

    public function getEnligneDate($format = '')
    {
        if($this->enligne_date and $this->enligne_date instanceof \DateTime and $format){
		    return $this->enligne_date->format($format);
		} else {
    		return $this->enligne_date;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
        if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
    	if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}      
    }

    public function setIsPublish($isPublish)
    {
        $this->is_publish = $isPublish;

        return $this;
    }

    public function getIsPublish()
    {
        return $this->is_publish;
    }
    
    public function getIsPublishLabel()
    {
        if ($this->getIsPublish())
        {
            return "Oui";
        } else {
            return "";
        }
    }

    public function setTypeContent(\FAPROD\ContentBundle\Entity\TypeContent $typeContent = null)
    {
        $this->type_content = $typeContent;

        return $this;
    }

    public function getTypeContent()
    {
        return $this->type_content;
    }
    
    public function getValidComments(){
		
		$criteria = Criteria::create()
		    ->where(Criteria::expr()->eq("validate", 1))
		    ->orderBy(array("id" => Criteria::DESC))
		;
		
		return $this->comments->matching($criteria);
    }

    public function addComment(\FAPROD\ContentBundle\Entity\ContentComment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    public function removeComment(\FAPROD\ContentBundle\Entity\ContentComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function addImage(\FAPROD\ContentBundle\Entity\Image $image)
    {
        $this->images[] = $image;
        
        $image->setContent($this);

        return $this;
    }

    public function removeImage(\FAPROD\ContentBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setCategory(\FAPROD\UserBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }
}
