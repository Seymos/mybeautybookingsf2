<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="contenttranslation")
 */
class ContentTranslation extends AbstractTranslation
{
	
	/**
     * @Prezent\Translatable(targetEntity="Content")
     */
    protected $translatable;
    
	/**
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=150, nullable=true)
     */
    private $stripped_title;
    
    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\Column(name="html", type="text", nullable=true)
     */
    private $html;
    
    /**
     * @ORM\Column(name="meta_title", type="string", length=100, nullable=true)
     */
    private $meta_title;
    
    /**
     * @ORM\Column(name="meta_description", type="string", length=200, nullable=true)
     */
    private $meta_description;
    
    /**
     * @ORM\Column(name="meta_keywords", type="string", length=200, nullable=true)
     */
    private $meta_keywords;
    
    public static function getTranslationEntityClass()
    {
        return 'ContentTranslation';
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setStrippedTitle($strippedTitle)
    {
        $this->stripped_title = $strippedTitle;

        return $this;
    }

    public function getStrippedTitle()
    {
        return $this->stripped_title;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function setMetaTitle($metaTitle)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    public function setMetaDescription($metaDescription)
    {
        $this->meta_description = $metaDescription;

        return $this;
    }

    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    public function setMetaKeywords($metaKeywords)
    {
        $this->meta_keywords = $metaKeywords;

        return $this;
    }

    public function getMetaKeywords()
    {
        return $this->meta_keywords;
    }
}
