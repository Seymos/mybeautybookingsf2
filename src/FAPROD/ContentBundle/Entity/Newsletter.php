<?php

/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use FAPROD\CoreBundle\Lib\MyConst;

/**
 * @ORM\Entity(repositoryClass="FAPROD\ContentBundle\Entity\NewsletterRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="newsletter")
 */
class Newsletter
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="title", type="string", length=200, nullable=true)
     */
    private $title;
    
    /**
     * @ORM\Column(name="html", type="text", nullable=true)
     */
    private $html;
    
    /**
     * @ORM\Column(name="envoye_date", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    private $envoye_date;
    
    /**
     * @ORM\Column(name="tous", type="boolean", nullable=true)
     */
    private $tous;
	
	/**
     * @ORM\Column(name="cliente", type="boolean", nullable=true)
     */
    private $cliente;
	
	/**
     * @ORM\Column(name="pro", type="boolean", nullable=true)
     */
    private $pro;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {

    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}

    public function getId()
    {
        return $this->id;
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function setEnvoyeDate($envoyeDate)
    {
        $this->envoye_date = $envoyeDate;

        return $this;
    }

    public function getEnvoyeDate($format = '')
    {
        if($this->envoye_date and $this->envoye_date instanceof \DateTime and $format){
		    return $this->envoye_date->format($format);
		} else {
    		return $this->envoye_date;
    	}
    }

    public function setTous($tous)
    {
        $this->tous = $tous;

        return $this;
    }

    public function getTous()
    {
        return $this->tous;
    }
    
    public function getTousLabel()
    {
        if ($this->getTous())
        {
            return "Oui";
        } else {
            return "";
        }
    }

    public function setCliente($cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getCliente()
    {
        return $this->cliente;
    }
	
	public function getClienteLabel()
    {
        if ($this->getCliente())
        {
            return "Oui";
        } else {
            return "";
        }
    }

    public function setPro($pro)
    {
        $this->pro = $pro;

        return $this;
    }

    public function getPro()
    {
        return $this->pro;
    }
	
	public function getProLabel()
    {
        if ($this->getPro())
        {
            return "Oui";
        } else {
            return "";
        }
    }
}
