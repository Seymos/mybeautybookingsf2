<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\ContentBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * TypeContentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TypeContentRepository extends EntityRepository
{
	public function getTypeContents($page = null, $nbPerPage = null, $sort = 'id', $sens = 0, $QueryBuilder = '')
    {
    	if (!$QueryBuilder)$QueryBuilder = $this->createQueryBuilder('tc');
    	
    	$query = $QueryBuilder
		    		->orderBy('tc.'.$sort, $sens ? 'ASC' : 'DESC')
			      	->getQuery();
	
	    if ($page and $nbPerPage){
		    $query
		      ->setFirstResult(($page-1) * $nbPerPage)
		      ->setMaxResults($nbPerPage)
		    ;
		    
		    return new Paginator($query, true);
    	} else {
    		return $query->getResult();
    	}
    }
}
