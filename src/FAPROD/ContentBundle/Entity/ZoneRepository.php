<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\ContentBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * ZoneRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ZoneRepository extends EntityRepository
{
	public function getZones($page = null, $nbPerPage = null, $sort = 'id', $sens = 0, $QueryBuilder = '')
    {
    	if (!$QueryBuilder)$QueryBuilder = $this->createQueryBuilder('z');
    	
    	$QueryBuilder->join('z.translations', 'trans')->addSelect('trans');
			      	
		if ($sort == 'title'){
	    	$QueryBuilder->orderBy('trans.'.$sort, $sens ? 'ASC' : 'DESC');
	    } else {
	    	$QueryBuilder->orderBy('z.'.$sort, $sens ? 'ASC' : 'DESC');
	    }
		    		
		$query = $QueryBuilder->getQuery();
	
	    if ($page and $nbPerPage){
		    $query
		      ->setFirstResult(($page-1) * $nbPerPage)
		      ->setMaxResults($nbPerPage)
		    ;
		    
		    return new Paginator($query, true);
    	} else {
    		return $query->getResult();
    	}
    }
}
