<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Prezent\Doctrine\Translatable\Entity\AbstractTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(name="zonetranslation")
 */
class ZoneTranslation extends AbstractTranslation
{
	
	/**
     * @Prezent\Translatable(targetEntity="Zone")
     */
    protected $translatable;
    
	/**
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     * @Assert\Length(min=5)
     */
    private $title;
    
    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\Column(name="html", type="text", nullable=true)
     */
    private $html;
    
    private $meta_keywords;
    
    public static function getTranslationEntityClass()
    {
        return 'ZoneTranslation';
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    public function getHtml()
    {
        return $this->html;
    }
}
