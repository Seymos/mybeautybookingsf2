<?php

/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="FAPROD\ContentBundle\Entity\TypeContentRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="typecontent")
 */
class TypeContent
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="label", type="string", length=200)
     */
    private $label;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {

    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
	
	public function __toString()
    {
		return $this->getLabel();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
        if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
    	if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}      
    }
}
