<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */
 
namespace FAPROD\ContentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="FAPROD\ContentBundle\Entity\ContentCommentRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="contentcomment")
 */
class ContentComment
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="ip_address", type="string", length=15, nullable=true)
     */
    private $ip_address;
    
	/**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\BookingBundle\Entity\Booking")
     * @ORM\JoinColumn(name="booking_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $booking;
    
    /**
     * @ORM\ManyToOne(targetEntity="FAPROD\UserBundle\Entity\User", inversedBy="comments", cascade={"persist"})
     * @ORM\JoinColumn(name="vendeur_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $vendeur;
    
    /**
     * @ORM\Column(name="note", type="integer", nullable=true)
     */
    private $note;
    
    /**
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;
    
    /**
     * @ORM\Column(name="validate", type="integer", nullable=true)
     */
    private $validate;
    
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updated_at;
    

    public function __construct()
    {

    }
    
    /**
	 *
	 * @ORM\PrePersist
	 * @ORM\PreUpdate
	 */
	public function updatedTimestamps()
	{
	    $this->setUpdatedAt(new \DateTime('now'));
	
	    if ($this->getCreatedAt() == null) {
	        $this->setCreatedAt(new \DateTime('now'));
	    }
	}
  
	public function __toString()
    {
		return $this->getTitle();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    public function getUpdatedAt($format = '')
    {
        if($this->updated_at and $this->updated_at instanceof \DateTime and $format){
		    return $this->updated_at->format($format);
		} else {
    		return $this->updated_at;
    	}
    }

    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    public function getCreatedAt($format = '')
    {
    	if($this->created_at and $this->created_at instanceof \DateTime and $format){
		    return $this->created_at->format($format);
		} else {
    		return $this->created_at;
    	}
    }

    public function setIpAddress($ipAddress)
    {
        $this->ip_address = $ipAddress;

        return $this;
    }

    public function getIpAddress()
    {
        return $this->ip_address;
    }

    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setValidate($validate)
    {
        $this->validate = $validate;

        return $this;
    }

    public function getValidate()
    {
        return $this->validate;
    }
    
    public function getValidateLabel()
    {
        if ($this->getValidate() == 1)
        {
            return "Oui";
        } else if ($this->getValidate() == -1) {
            return "Refusé";
        } else {
        	return "En attente";
        }
    }

    public function setUser(\FAPROD\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setContent(\FAPROD\ContentBundle\Entity\Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setVendeur(\FAPROD\UserBundle\Entity\User $vendeur = null)
    {
        $this->vendeur = $vendeur;

        return $this;
    }

    public function getVendeur()
    {
        return $this->vendeur;
    }

    public function setBooking(\FAPROD\BookingBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    public function getBooking()
    {
        return $this->booking;
    }
}
