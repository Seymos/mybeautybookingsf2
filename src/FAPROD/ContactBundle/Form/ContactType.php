<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sujet', 'choice', array(
            						'choices'   => $options['sujets'],
	            					'required'  => true,
            						)
            )
            ->add('first_name', 'text', array('required' => true))
            ->add('last_name', 'text', array('required' => true))
            ->add('message', 'textarea', array('required' => true))
            ->add('email', 'email', array('required' => true))
            ->add('phone', 'text', array('required' => false, 'attr' => array('maxlength' => 10)))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
        							'sujets' => array(),
        							));
    }
    
    public function getDefaultOptions(array $options)
	{
	    return array(
	    			'sujets' => array(),
	    			);
	}

    /**
     * @return string
     */
    public function getName()
    {
        return 'faprod_contactbundle_contact';
    }
}
