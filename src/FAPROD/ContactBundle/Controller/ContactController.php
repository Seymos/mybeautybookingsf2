<?php
/**
 * FAPROD - Developpement et gestion de sites web
 *
 * CE DOCUMENT EST LA PROPRIETE EXCLUSIVE DE FAPROD,
 * TOUTE REPRODUCTION MEME PARTIELLE EST INTERDITE
 *
 *  @category   FAPROD
 *  @package    Website
 *  @author     Franck Alemany <franck.alemany@faprod.com>
 *  @copyright  2008-2015 FAPROD
 *  @license    http://www.faprod.com/ All rights reserved.
 *  @link       http://www.faprod.com
 */

namespace FAPROD\ContactBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use FAPROD\ContactBundle\Form\ContactType;

class ContactController extends Controller
{
	
	public function preExecute(Request $request)
    {        
        if ($request->query->get('parrain')){
        	$this->container->get('session')->set('parrain', $request->query->get('parrain'));
        }
    }
	
    /**
     * @Route("/", name="contact_index")
     * @Template("ContactBundle:Contact:index.html.twig")
	 */
    public function indexAction(Request $request)
    {
    	$form = $this->createForm(new ContactType());
    	
    	if ($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);
	    	
	    	$contact = $form->getData();
	        
	        $mailBody = $this->renderView('ContactBundle:Mail:contact.html.twig', array('contact' => $contact));
	        $message = \Swift_Message::newInstance()
		        ->setSubject($this->get('translator')->trans('Une demande de contact'))
		        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		        ->setTo($this->get('FAPROD.MyConst')->getSiteEmail())
		        ->setBody($mailBody, 'text/html');
		    //echo $mailBody;exit;
		    $this->get('mailer')->send($message);
		    
		    $contact = $form->getData();
	        
	        $mailBody = $this->renderView('ContactBundle:Mail:confirmation.html.twig', array('contact' => $contact));
	        $message = \Swift_Message::newInstance()
		        ->setSubject($this->get('translator')->trans('Nous avons bien reçu votre message'))
		        ->setFrom(array($this->get('FAPROD.MyConst')->getSiteEmail() => $this->get('FAPROD.MyConst')->getSiteName()))
		        ->setTo($contact['email'])
		        ->setBody($mailBody, 'text/html');
		    //echo $mailBody;exit;
		    $this->get('mailer')->send($message);
	    
	        $request->getSession()->getFlashBag()->add('message', $this->get('translator')->trans('Message envoyé.'));
	    
	        return $this->redirect($this->generateUrl('contact_index'));
	    }
    	
        return array(
				'form'   => $form->createView(),
				);
    }
}
