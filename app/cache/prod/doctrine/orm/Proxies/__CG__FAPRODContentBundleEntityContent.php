<?php

namespace Proxies\__CG__\FAPROD\ContentBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Content extends \FAPROD\ContentBundle\Entity\Content implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', 'id', 'translations', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'currentLocale', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'image_principale', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'images', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'type_content', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'comments', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'is_publish', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'category', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'enligne_date', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'created_at', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'updated_at', 'currentTranslation');
        }

        return array('__isInitialized__', 'id', 'translations', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'currentLocale', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'image_principale', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'images', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'type_content', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'comments', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'is_publish', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'category', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'enligne_date', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'created_at', '' . "\0" . 'FAPROD\\ContentBundle\\Entity\\Content' . "\0" . 'updated_at', 'currentTranslation');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Content $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function preRemoveUpload()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'preRemoveUpload', array());

        return parent::preRemoveUpload();
    }

    /**
     * {@inheritDoc}
     */
    public function translate($locale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'translate', array($locale));

        return parent::translate($locale);
    }

    /**
     * {@inheritDoc}
     */
    public function updatedTimestamps()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'updatedTimestamps', array());

        return parent::updatedTimestamps();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', array());

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle($locale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTitle', array($locale));

        return parent::getTitle($locale);
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle($title)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTitle', array($title));

        return parent::setTitle($title);
    }

    /**
     * {@inheritDoc}
     */
    public function getStrippedTitle($locale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStrippedTitle', array($locale));

        return parent::getStrippedTitle($locale);
    }

    /**
     * {@inheritDoc}
     */
    public function setStrippedTitle($content)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStrippedTitle', array($content));

        return parent::setStrippedTitle($content);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription($locale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescription', array($locale));

        return parent::getDescription($locale);
    }

    /**
     * {@inheritDoc}
     */
    public function setDescription($content)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescription', array($content));

        return parent::setDescription($content);
    }

    /**
     * {@inheritDoc}
     */
    public function getHtml($locale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHtml', array($locale));

        return parent::getHtml($locale);
    }

    /**
     * {@inheritDoc}
     */
    public function setHtml($content)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHtml', array($content));

        return parent::setHtml($content);
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaTitle($locale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMetaTitle', array($locale));

        return parent::getMetaTitle($locale);
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaTitle($content)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMetaTitle', array($content));

        return parent::setMetaTitle($content);
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaDescription($locale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMetaDescription', array($locale));

        return parent::getMetaDescription($locale);
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaDescription($content)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMetaDescription', array($content));

        return parent::setMetaDescription($content);
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaKeywords($locale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMetaKeywords', array($locale));

        return parent::getMetaKeywords($locale);
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaKeywords($content)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMetaKeywords', array($content));

        return parent::setMetaKeywords($content);
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setImagePrincipale(\FAPROD\ContentBundle\Entity\Image $imagePrincipale = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setImagePrincipale', array($imagePrincipale));

        return parent::setImagePrincipale($imagePrincipale);
    }

    /**
     * {@inheritDoc}
     */
    public function getImagePrincipale()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImagePrincipale', array());

        return parent::getImagePrincipale();
    }

    /**
     * {@inheritDoc}
     */
    public function setEnligneDate($enligneDate)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEnligneDate', array($enligneDate));

        return parent::setEnligneDate($enligneDate);
    }

    /**
     * {@inheritDoc}
     */
    public function getEnligneDate($format = '')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEnligneDate', array($format));

        return parent::getEnligneDate($format);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', array($createdAt));

        return parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt($format = '')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', array($format));

        return parent::getCreatedAt($format);
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($updatedAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', array($updatedAt));

        return parent::setUpdatedAt($updatedAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt($format = '')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', array($format));

        return parent::getUpdatedAt($format);
    }

    /**
     * {@inheritDoc}
     */
    public function setIsPublish($isPublish)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIsPublish', array($isPublish));

        return parent::setIsPublish($isPublish);
    }

    /**
     * {@inheritDoc}
     */
    public function getIsPublish()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIsPublish', array());

        return parent::getIsPublish();
    }

    /**
     * {@inheritDoc}
     */
    public function getIsPublishLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getIsPublishLabel', array());

        return parent::getIsPublishLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setTypeContent(\FAPROD\ContentBundle\Entity\TypeContent $typeContent = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTypeContent', array($typeContent));

        return parent::setTypeContent($typeContent);
    }

    /**
     * {@inheritDoc}
     */
    public function getTypeContent()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTypeContent', array());

        return parent::getTypeContent();
    }

    /**
     * {@inheritDoc}
     */
    public function getValidComments()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getValidComments', array());

        return parent::getValidComments();
    }

    /**
     * {@inheritDoc}
     */
    public function addComment(\FAPROD\ContentBundle\Entity\ContentComment $comment)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addComment', array($comment));

        return parent::addComment($comment);
    }

    /**
     * {@inheritDoc}
     */
    public function removeComment(\FAPROD\ContentBundle\Entity\ContentComment $comment)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeComment', array($comment));

        return parent::removeComment($comment);
    }

    /**
     * {@inheritDoc}
     */
    public function getComments()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getComments', array());

        return parent::getComments();
    }

    /**
     * {@inheritDoc}
     */
    public function addImage(\FAPROD\ContentBundle\Entity\Image $image)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addImage', array($image));

        return parent::addImage($image);
    }

    /**
     * {@inheritDoc}
     */
    public function removeImage(\FAPROD\ContentBundle\Entity\Image $image)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeImage', array($image));

        return parent::removeImage($image);
    }

    /**
     * {@inheritDoc}
     */
    public function getImages()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImages', array());

        return parent::getImages();
    }

    /**
     * {@inheritDoc}
     */
    public function setCategory(\FAPROD\UserBundle\Entity\Category $category = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCategory', array($category));

        return parent::setCategory($category);
    }

    /**
     * {@inheritDoc}
     */
    public function getCategory()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCategory', array());

        return parent::getCategory();
    }

    /**
     * {@inheritDoc}
     */
    public function getTranslations()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTranslations', array());

        return parent::getTranslations();
    }

    /**
     * {@inheritDoc}
     */
    public function addTranslation(\Prezent\Doctrine\Translatable\TranslationInterface $translation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addTranslation', array($translation));

        return parent::addTranslation($translation);
    }

    /**
     * {@inheritDoc}
     */
    public function removeTranslation(\Prezent\Doctrine\Translatable\TranslationInterface $translation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeTranslation', array($translation));

        return parent::removeTranslation($translation);
    }

}
