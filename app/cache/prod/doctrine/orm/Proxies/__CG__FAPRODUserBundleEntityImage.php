<?php

namespace Proxies\__CG__\FAPROD\UserBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Image extends \FAPROD\UserBundle\Entity\Image implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'id', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'user', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'offer', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'filename', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'ext', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'alt', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'tempFilename', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'tempFilename_photo', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'tempFilename_zoom', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'created_at', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'updated_at');
        }

        return array('__isInitialized__', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'id', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'user', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'offer', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'filename', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'ext', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'alt', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'tempFilename', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'tempFilename_photo', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'tempFilename_zoom', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'created_at', '' . "\0" . 'FAPROD\\UserBundle\\Entity\\Image' . "\0" . 'updated_at');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Image $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function __toString()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', array());

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function updatedTimestamps()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'updatedTimestamps', array());

        return parent::updatedTimestamps();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($updatedAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', array($updatedAt));

        return parent::setUpdatedAt($updatedAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt($format = '')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', array($format));

        return parent::getUpdatedAt($format);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', array($createdAt));

        return parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt($format = '')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', array($format));

        return parent::getCreatedAt($format);
    }

    /**
     * {@inheritDoc}
     */
    public function rotateImage($angle, $title)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'rotateImage', array($angle, $title));

        return parent::rotateImage($angle, $title);
    }

    /**
     * {@inheritDoc}
     */
    public function upload($file_temp, $title)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'upload', array($file_temp, $title));

        return parent::upload($file_temp, $title);
    }

    /**
     * {@inheritDoc}
     */
    public function preRemoveUpload()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'preRemoveUpload', array());

        return parent::preRemoveUpload();
    }

    /**
     * {@inheritDoc}
     */
    public function getUploadDir()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUploadDir', array());

        return parent::getUploadDir();
    }

    /**
     * {@inheritDoc}
     */
    public function getUploadRootDir()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUploadRootDir', array());

        return parent::getUploadRootDir();
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUrl', array());

        return parent::getUrl();
    }

    /**
     * {@inheritDoc}
     */
    public function getUrlPhoto()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUrlPhoto', array());

        return parent::getUrlPhoto();
    }

    /**
     * {@inheritDoc}
     */
    public function getUrlZoom()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUrlZoom', array());

        return parent::getUrlZoom();
    }

    /**
     * {@inheritDoc}
     */
    public function getWebPath()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWebPath', array());

        return parent::getWebPath();
    }

    /**
     * {@inheritDoc}
     */
    public function getWebPathPhoto()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWebPathPhoto', array());

        return parent::getWebPathPhoto();
    }

    /**
     * {@inheritDoc}
     */
    public function getWebPathZoom()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWebPathZoom', array());

        return parent::getWebPathZoom();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setFilename($filename)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFilename', array($filename));

        return parent::setFilename($filename);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilename()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFilename', array());

        return parent::getFilename();
    }

    /**
     * {@inheritDoc}
     */
    public function setAlt($alt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAlt', array($alt));

        return parent::setAlt($alt);
    }

    /**
     * {@inheritDoc}
     */
    public function getAlt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAlt', array());

        return parent::getAlt();
    }

    /**
     * {@inheritDoc}
     */
    public function setExt($ext)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExt', array($ext));

        return parent::setExt($ext);
    }

    /**
     * {@inheritDoc}
     */
    public function getExt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExt', array());

        return parent::getExt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUser(\FAPROD\UserBundle\Entity\User $user = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUser', array($user));

        return parent::setUser($user);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUser', array());

        return parent::getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function setOffer(\FAPROD\UserBundle\Entity\Offer $offer = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOffer', array($offer));

        return parent::setOffer($offer);
    }

    /**
     * {@inheritDoc}
     */
    public function getOffer()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOffer', array());

        return parent::getOffer();
    }

}
