<?php

namespace Proxies\__CG__\FAPROD\BookingBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Booking extends \FAPROD\BookingBundle\Entity\Booking implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'id', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'user', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'seller', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'offer', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'statut', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'price', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'code', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'code_reduc', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'commission', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'tva', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'price_ttc', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'PreauthorizationId', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'CardId', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'date_rdv', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'heure_rdv', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'minute_rdv', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'created_at', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'updated_at');
        }

        return array('__isInitialized__', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'id', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'user', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'seller', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'offer', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'statut', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'price', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'code', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'code_reduc', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'commission', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'tva', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'price_ttc', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'PreauthorizationId', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'CardId', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'date_rdv', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'heure_rdv', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'minute_rdv', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'created_at', '' . "\0" . 'FAPROD\\BookingBundle\\Entity\\Booking' . "\0" . 'updated_at');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Booking $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function updatedTimestamps()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'updatedTimestamps', array());

        return parent::updatedTimestamps();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($updatedAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', array($updatedAt));

        return parent::setUpdatedAt($updatedAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt($format = '')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', array($format));

        return parent::getUpdatedAt($format);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', array($createdAt));

        return parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt($format = '')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', array($format));

        return parent::getCreatedAt($format);
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setUser(\FAPROD\UserBundle\Entity\User $user)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUser', array($user));

        return parent::setUser($user);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUser', array());

        return parent::getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function setSeller(\FAPROD\UserBundle\Entity\User $seller)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSeller', array($seller));

        return parent::setSeller($seller);
    }

    /**
     * {@inheritDoc}
     */
    public function getSeller()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSeller', array());

        return parent::getSeller();
    }

    /**
     * {@inheritDoc}
     */
    public function setStatut($statut)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStatut', array($statut));

        return parent::setStatut($statut);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatut()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStatut', array());

        return parent::getStatut();
    }

    /**
     * {@inheritDoc}
     */
    public function getStatutLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStatutLabel', array());

        return parent::getStatutLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setPreauthorizationId($PreauthorizationId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPreauthorizationId', array($PreauthorizationId));

        return parent::setPreauthorizationId($PreauthorizationId);
    }

    /**
     * {@inheritDoc}
     */
    public function getPreauthorizationId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPreauthorizationId', array());

        return parent::getPreauthorizationId();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrice($price)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrice', array($price));

        return parent::setPrice($price);
    }

    /**
     * {@inheritDoc}
     */
    public function getPrice()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrice', array());

        return parent::getPrice();
    }

    /**
     * {@inheritDoc}
     */
    public function getPriceLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPriceLabel', array());

        return parent::getPriceLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setOffer(\FAPROD\UserBundle\Entity\Offer $offer = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOffer', array($offer));

        return parent::setOffer($offer);
    }

    /**
     * {@inheritDoc}
     */
    public function getOffer()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOffer', array());

        return parent::getOffer();
    }

    /**
     * {@inheritDoc}
     */
    public function setCommission($commission)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCommission', array($commission));

        return parent::setCommission($commission);
    }

    /**
     * {@inheritDoc}
     */
    public function getCommission()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCommission', array());

        return parent::getCommission();
    }

    /**
     * {@inheritDoc}
     */
    public function getCommissionLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCommissionLabel', array());

        return parent::getCommissionLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setTva($tva)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTva', array($tva));

        return parent::setTva($tva);
    }

    /**
     * {@inheritDoc}
     */
    public function getTva()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTva', array());

        return parent::getTva();
    }

    /**
     * {@inheritDoc}
     */
    public function getTvaLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTvaLabel', array());

        return parent::getTvaLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setPriceTtc($priceTtc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPriceTtc', array($priceTtc));

        return parent::setPriceTtc($priceTtc);
    }

    /**
     * {@inheritDoc}
     */
    public function getPriceTtc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPriceTtc', array());

        return parent::getPriceTtc();
    }

    /**
     * {@inheritDoc}
     */
    public function getPriceTtcLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPriceTtcLabel', array());

        return parent::getPriceTtcLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setDateRdv($dateRdv)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDateRdv', array($dateRdv));

        return parent::setDateRdv($dateRdv);
    }

    /**
     * {@inheritDoc}
     */
    public function getDateRdv($format = '')
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDateRdv', array($format));

        return parent::getDateRdv($format);
    }

    /**
     * {@inheritDoc}
     */
    public function setHeureRdv($heureRdv)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHeureRdv', array($heureRdv));

        return parent::setHeureRdv($heureRdv);
    }

    /**
     * {@inheritDoc}
     */
    public function getHeureRdv()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHeureRdv', array());

        return parent::getHeureRdv();
    }

    /**
     * {@inheritDoc}
     */
    public function getHeureRdvLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHeureRdvLabel', array());

        return parent::getHeureRdvLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setCardId($cardId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCardId', array($cardId));

        return parent::setCardId($cardId);
    }

    /**
     * {@inheritDoc}
     */
    public function getCardId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCardId', array());

        return parent::getCardId();
    }

    /**
     * {@inheritDoc}
     */
    public function setMinuteRdv($minuteRdv)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMinuteRdv', array($minuteRdv));

        return parent::setMinuteRdv($minuteRdv);
    }

    /**
     * {@inheritDoc}
     */
    public function getMinuteRdv()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMinuteRdv', array());

        return parent::getMinuteRdv();
    }

    /**
     * {@inheritDoc}
     */
    public function setCode($code)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCode', array($code));

        return parent::setCode($code);
    }

    /**
     * {@inheritDoc}
     */
    public function getCode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCode', array());

        return parent::getCode();
    }

    /**
     * {@inheritDoc}
     */
    public function setCodeReduc($codeReduc)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCodeReduc', array($codeReduc));

        return parent::setCodeReduc($codeReduc);
    }

    /**
     * {@inheritDoc}
     */
    public function getCodeReduc()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCodeReduc', array());

        return parent::getCodeReduc();
    }

    /**
     * {@inheritDoc}
     */
    public function getCodeReducLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCodeReducLabel', array());

        return parent::getCodeReducLabel();
    }

}
