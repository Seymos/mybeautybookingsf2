<?php

/* UserBundle:Agenda:agenda.html.twig */
class __TwigTemplate_f433fdfb20b44572c636e85e04d7aaee1fa413927efa33cbf6d32fc7b5e0728d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>
function select_date(date){
\$('#'+\$('#date_select').val()).show();
\$('#'+\$('#date_select').val()+'_label').hide();
\$('#date_select').val(date);
\$('#'+date).hide();
\$('#'+date+'_label').show();
}
</script>
<div id=\"agenda_user\">
<a class=\"pull-left btn btn-primary btn-xs\" onclick=\"\$('#agenda_user').empty().append('<center><img style=\\'margin:60px;\\' src=\\'";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
        echo "\\'></center>').load('";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("agenda_getagenda", array("date" => (isset($context["last_date"]) ? $context["last_date"] : null))), "html", null, true);
        echo "');\" href=\"javascript:void(0);\"><<</a>
<a class=\"pull-right btn btn-primary btn-xs\" onclick=\"\$('#agenda_user').empty().append('<center><img style=\\'margin:60px;\\' src=\\'";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
        echo "\\'></center>').load('";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("agenda_getagenda", array("date" => (isset($context["next_date"]) ? $context["next_date"] : null))), "html", null, true);
        echo "');\" href=\"javascript:void(0);\">>></a>
<table class=\"table table-bordered table-striped\">
<tr>
\t<th></th>
\t";
        // line 16
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entete"]) ? $context["entete"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ligne"]) {
            // line 17
            echo "\t<th class=\"agenda_entete\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "jour", array(), "array"), "html", null, true);
            echo "<br/>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "date", array(), "array"), "html", null, true);
            echo "</th>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ligne'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "</tr>
";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["datas"]) ? $context["datas"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 21
            echo "<tr>
\t<td class=\"text-center\">
\t\t";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "heure", array(), "array"), "html", null, true);
            echo "
\t</td>
\t";
            // line 25
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["data"], "jours", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["reservation"]) {
                // line 26
                echo "\t<td class=\"text-center\">
\t\t";
                // line 27
                if ($this->getAttribute($context["reservation"], "booking_id", array(), "array")) {
                    // line 28
                    echo "\t\t\t<a class=\"agenda_booking\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("booking_show", array("booking_id" => $this->getAttribute($context["reservation"], "booking_id", array(), "array"))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["reservation"], "booking_user", array(), "array"), "html", null, true);
                    echo "</a>
\t\t";
                } elseif (($this->getAttribute(                // line 29
$context["reservation"], "ouverture", array(), "array") == 1)) {
                    // line 30
                    echo "\t\t<div id=\"disponibilite_";
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                    echo "\">
\t\t\t<a class=\"agenda_choisir\" id=\"";
                    // line 31
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                    echo "\" href=\"javascript:void(0);\" onclick=\"\$('#disponibilite_";
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                    echo "').empty().append('<center><img style=\\'width:20px;\\' src=\\'";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
                    echo "\\'></center>').load('";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("agenda_disponibilite", array("date" => (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")))), "html", null, true);
                    echo "');\" href=\"javascript:void(0);\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Disponible"), "html", null, true);
                    echo "</a>
\t\t</div>
\t\t";
                } elseif (($this->getAttribute(                // line 33
$context["reservation"], "ouverture", array(), "array") ==  -1)) {
                    // line 34
                    echo "\t\t<div id=\"disponibilite_";
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                    echo "\">
\t\t\t<a class=\"agenda_choisir_ok\" id=\"";
                    // line 35
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                    echo "\" href=\"javascript:void(0);\" onclick=\"\$('#disponibilite_";
                    echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                    echo "').empty().append('<center><img style=\\'width:20px;\\' src=\\'";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
                    echo "\\'></center>').load('";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("agenda_disponibilite", array("date" => (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")))), "html", null, true);
                    echo "');\" href=\"javascript:void(0);\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indisponible"), "html", null, true);
                    echo "</a>
\t\t</div>
\t\t";
                }
                // line 38
                echo "\t</td>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reservation'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "</tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "</table>

</div>";
    }

    public function getTemplateName()
    {
        return "UserBundle:Agenda:agenda.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 42,  141 => 40,  134 => 38,  120 => 35,  115 => 34,  113 => 33,  100 => 31,  95 => 30,  93 => 29,  86 => 28,  84 => 27,  81 => 26,  77 => 25,  72 => 23,  68 => 21,  64 => 20,  61 => 19,  50 => 17,  46 => 16,  37 => 12,  31 => 11,  19 => 1,);
    }
}
