<?php

/* UserBundle:User:favoris.html.twig */
class __TwigTemplate_e291a8748db69ad05fd84ac5fe196f6c4c9ad2c9b30056dbe8917602009d012b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
        echo "</a> > <a href=\"";
        echo $this->env->getExtension('routing')->getPath("user_show");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
        echo "</a> > ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes beauty artists"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"hidden-xs col-sm-3 col-md-3 col-lg-3\">
\t\t\t";
        // line 19
        echo twig_include($this->env, $context, "UserBundle:Partial:menu.html.twig");
        echo "
\t\t</div>
\t\t<div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9\">
\t\t\t
\t\t\t<div class=\"panel panel-default\">
\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t    <h3 class=\"panel-title\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes beauty artists"), "html", null, true);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t
\t\t\t\t\t";
        // line 29
        if (twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "favoris", array()))) {
            // line 30
            echo "\t\t\t\t\t
\t\t\t\t\t\t";
            // line 31
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "favoris", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 32
                echo "\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 vignette_ajax vignette_certifie\">
\t\t\t\t\t\t\t\t<div class=\"vignette\">
\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t<div class=\"vignette_infos_1 col-xs-12 col-sm-3 col-md-3 col-lg-2 text-center\">
\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 36
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_show", array("user_id" => $this->getAttribute($context["user"], "id", array()), "t" => $this->getAttribute($context["user"], "entrepriseStripped", array()))), "html", null, true);
                echo "\"><img class=\"img-thumbnail\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "lastName", array()), "html", null, true);
                echo "\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["user"], "image", array()), "url", array()), "html", null, true);
                echo "\"/></a>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"vignette_infos_2 col-xs-12 col-sm-6 col-md-6 col-lg-7\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"vignette_infos_title\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                // line 40
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_show", array("user_id" => $this->getAttribute($context["user"], "id", array()), "t" => $this->getAttribute($context["user"], "entrepriseStripped", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->truncate($this->getAttribute($context["user"], "entreprise", array()), 70), "html", null, true);
                echo "</a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"vignette_infos_autre\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 43
                echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->truncate($this->getAttribute($context["user"], "city", array()), 30), "html", null, true);
                if ($this->getAttribute($context["user"], "postalCode", array())) {
                    echo " (";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "postalCode", array()), "html", null, true);
                    echo ")";
                }
                // line 44
                echo "\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"vignette_infos_autre_2\">
\t\t\t\t\t\t\t\t\t\t\t";
                // line 46
                if (($this->getAttribute($context["user"], "nbNote", array()) > 0)) {
                    echo "<img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("images/template/b" . $this->getAttribute($context["user"], "note", array())) . ".png")), "html", null, true);
                    echo "\"/>";
                } else {
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aucune note"), "html", null, true);
                }
                // line 47
                echo "\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"vignette_infos_1 col-xs-12 col-sm-3 col-md-3 col-lg-3 text-center\">
\t\t\t\t\t\t\t\t\t\t\t<!-- Single button -->
\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group dropdown pull-right\">
\t\t\t\t\t\t\t\t\t\t\t  <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 53
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans(" Actions"), "html", null, true);
                echo " <span class=\"caret\"></span>
\t\t\t\t\t\t\t\t\t\t\t  </button>
\t\t\t\t\t\t\t\t\t\t\t  <ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                // line 56
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_show", array("user_id" => $this->getAttribute($context["user"], "id", array()), "t" => $this->getAttribute($context["user"], "strippedName", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consulter ce profil"), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                // line 57
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_deletefavoris", array("user_id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Supprimer de ma liste"), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t\t\t\t\t  </ul>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>\t
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t    \t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "\t\t    \t\t
\t\t    \t\t";
        } else {
            // line 67
            echo "\t\t    \t\t
\t\t    \t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t";
            // line 70
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aucun professionnel pour le moment"), "html", null, true);
            echo ".<br/><br/>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t    \t\t
\t\t    \t\t";
        }
        // line 75
        echo "\t\t    \t\t
\t\t    \t</div>
\t\t\t</div>
\t\t    
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "UserBundle:User:favoris.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 75,  181 => 70,  176 => 67,  172 => 65,  156 => 57,  150 => 56,  144 => 53,  136 => 47,  128 => 46,  124 => 44,  117 => 43,  109 => 40,  98 => 36,  92 => 32,  88 => 31,  85 => 30,  83 => 29,  76 => 25,  67 => 19,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
