<?php

/* TwigBundle:Exception:error404.html.twig */
class __TwigTemplate_d75bd233588ef0ebee3816a56a29ae7c4ca85b05d371b1f6856aad25c17de535 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("::error.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::error.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<p><b>Désolé, cette page est introuvable</b></p>
\t<p>
  \t\tVeuillez retourner à la page d'accueil ou nous signaler ce problème par e-mail.
  \t</p>
  \t<p>
  \t\tMerci de votre compréhension.
  \t</p>
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error404.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 4,  36 => 3,  11 => 1,);
    }
}
