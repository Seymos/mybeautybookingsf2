<?php

/* UserBundle:Agenda:disponibilite.html.twig */
class __TwigTemplate_d707934077f90f4178541bbc2d1e0ade81cb76b33930e2f4f39212002fb76f58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["disponible"]) ? $context["disponible"] : null)) {
            // line 2
            echo "<a class=\"agenda_choisir_ok\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["date"]) ? $context["date"] : null), "html", null, true);
            echo "\" href=\"javascript:void(0);\" onclick=\"\$('#disponibilite_";
            echo twig_escape_filter($this->env, (isset($context["date"]) ? $context["date"] : null), "html", null, true);
            echo "').empty().append('<center><img style=\\'width:20px;\\' src=\\'";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
            echo "\\'></center>').load('";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("agenda_disponibilite", array("date" => (isset($context["date"]) ? $context["date"] : null))), "html", null, true);
            echo "');\" href=\"javascript:void(0);\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indisponible"), "html", null, true);
            echo "</a>
";
        } else {
            // line 4
            echo "<a class=\"agenda_choisir\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["date"]) ? $context["date"] : null), "html", null, true);
            echo "\" href=\"javascript:void(0);\" onclick=\"\$('#disponibilite_";
            echo twig_escape_filter($this->env, (isset($context["date"]) ? $context["date"] : null), "html", null, true);
            echo "').empty().append('<center><img style=\\'width:20px;\\' src=\\'";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
            echo "\\'></center>').load('";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("agenda_disponibilite", array("date" => (isset($context["date"]) ? $context["date"] : null))), "html", null, true);
            echo "');\" href=\"javascript:void(0);\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Disponible"), "html", null, true);
            echo "</a>
";
        }
    }

    public function getTemplateName()
    {
        return "UserBundle:Agenda:disponibilite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 4,  21 => 2,  19 => 1,);
    }
}
