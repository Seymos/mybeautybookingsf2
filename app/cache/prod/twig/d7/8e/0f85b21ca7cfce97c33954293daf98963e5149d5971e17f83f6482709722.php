<?php

/* CoreBundle::pagination.html.twig */
class __TwigTemplate_d78e0f85b21ca7cfce97c33954293daf98963e5149d5971e17f83f6482709722 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["nbPages"]) ? $context["nbPages"] : null) > 1)) {
            // line 2
            echo "<ul class=\"pagination\">
  ";
            // line 3
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["nbPages"]) ? $context["nbPages"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 4
                echo "    <li";
                if (($context["p"] == (isset($context["page"]) ? $context["page"] : null))) {
                    echo " class=\"active\"";
                }
                echo ">
      <a href=\"";
                // line 5
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["action"]) ? $context["action"] : null), array("page" => $context["p"], "sort" => (isset($context["sort"]) ? $context["sort"] : null), "sens" => (isset($context["sens"]) ? $context["sens"] : null))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["p"], "html", null, true);
                echo "</a>
    </li>
  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 8
            echo "</ul>
";
        }
    }

    public function getTemplateName()
    {
        return "CoreBundle::pagination.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 8,  35 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
