<?php

/* CoreBundle:Home:welcome.html.twig */
class __TwigTemplate_6138af2f33fd05b1080642edd125a3412e78f143eb65fcb8403078d2ac721833 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-offset-2 col-lg-8\">
\t\t\t<div id=\"temp_contact_home\">
\t\t\t\t
\t\t\t\t<p class=\"text-center\">
\t\t\t\t\t<img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/logo.png"), "html", null, true);
        echo "\">
\t\t\t\t</p>
\t\t\t\t<p class=\"logo_slogan\" style=\"margin-bottom:25px;\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Le home beauty service qui vous ressemble, vraiment"), "html", null, true);
        echo "</p>
\t\t\t\t<p>
\t\t\t\t\t<b>Le premier home beauty service dédié à toutes les beautés arrive bientôt.</b>
\t\t\t\t</p>
\t\t\t\t<p style=\"margin-top:25px;\">
\t\t\t\t\tLes 100 premières inscrites bénéficieront de notre offre exclusive de bienvenue.
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tPour vous inscrire, c’est par ici :
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<form method=\"post\" class=\"form\">
\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t\t<div class=\"form-group\" style=\"margin-bottom:15px;\">
\t\t\t\t\t\t\t\t  <input required=required placeholder=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Votre adresse mail"), "html", null, true);
        echo "\" class=\"form-control\" type=\"email\" id=\"email\" name=\"email\" value=\"\">
\t\t\t\t\t\t\t\t </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-lg-offset-3 col-lg-6\">
\t\t\t\t\t\t\t\t<div class=\"form-group text-center\">
\t\t\t\t\t\t\t\t\t<div class=\"g-recaptcha\" data-sitekey=\"6LetBCwUAAAAAM2ulGdTUS0uyBHm18oVBerlPpUy\"></div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t\t\t\t\t<button class=\"btn btn-primary btn-sadv\">";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Je profite de l'offre"), "html", null, true);
        echo "</button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t</form>
\t\t\t\t</p>
\t\t\t\t<p style=\"margin-top:55px;\">
\t\t\t\t\tVous êtes un/e professionnel/le de la beauté (coiffure, make-up ou esthétique) et souhaitez rejoindre la plateforme ?
\t\t\t\t</p>
\t\t\t\t<p>\t
\t\t\t\t\tContactez-nous à l’adresse : <a href=\"mailto:artists@mybeautybooking.fr\">artists@mybeautybooking.fr</a>
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\tA très vite, l’équipe MyBeauty Booking
\t\t\t\t</p>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

";
        // line 62
        echo twig_include($this->env, $context, "CoreBundle::messages.html.twig");
        echo "

";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Home:welcome.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 62,  88 => 42,  72 => 29,  53 => 13,  48 => 11,  39 => 4,  36 => 3,  11 => 1,);
    }
}
