<?php

/* BookingBundle:Booking:show_booking.html.twig */
class __TwigTemplate_31fc3121b3493c44e936f615701988964c563754343b67487f8d14c642810c19 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row popup_booking\">
\t<div class=\"col-lg-12\" style=\"overflow-y:auto;max-height:349px;\">
\t
\t\t";
        // line 4
        if (twig_length_filter($this->env, (isset($context["liste"]) ? $context["liste"] : null))) {
            // line 5
            echo "
\t\t\t<table class=\"table table-condensed table_popup\">
\t\t\t\t<thead>
\t\t\t\t\t<tr class=\"entete\">
\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t<th>Prestation</th>
\t\t\t\t\t\t";
            // line 11
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1)) {
                // line 12
                echo "\t\t\t\t\t\t<th>Pro</th>
\t\t\t\t\t\t";
            } else {
                // line 14
                echo "\t\t\t\t\t\t<th>Cliente</th>
\t\t\t\t\t\t";
            }
            // line 16
            echo "\t\t\t\t\t\t<th>Date</th>
\t\t\t\t\t\t<th>Heure</th>
\t\t\t\t\t\t<th style=\"width:80px;\">Prix</th>
\t\t\t\t\t\t<th>Statut</th>
\t\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t\t";
            // line 23
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["liste"]) ? $context["liste"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["booking"]) {
                // line 24
                echo "\t\t\t\t\t\t<tr class=\"item";
                if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                    echo "1";
                }
                echo "\">
\t\t\t\t\t\t\t<td><a href=\"";
                // line 25
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("booking_show", array("booking_id" => $this->getAttribute($context["booking"], "id", array()))), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/loupe3.png"), "html", null, true);
                echo "\"></a></td>
\t\t\t\t\t\t\t<td>";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "offer", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t";
                // line 27
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1)) {
                    // line 28
                    echo "\t\t\t\t\t\t\t<td>";
                    if ($this->getAttribute($context["booking"], "seller", array())) {
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["booking"], "seller", array()), "shortUsername", array()), "html", null, true);
                    }
                    echo "</td>
\t\t\t\t\t\t\t";
                } else {
                    // line 30
                    echo "\t\t\t\t\t\t\t<td>";
                    if ($this->getAttribute($context["booking"], "user", array())) {
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["booking"], "user", array()), "shortUsername", array()), "html", null, true);
                    }
                    echo "</td>
\t\t\t\t\t\t\t";
                }
                // line 32
                echo "\t\t\t\t\t\t\t<td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "dateRdv", array(0 => "d/m/Y"), "method"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t<td>";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "heureRdvLabel", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t<td>";
                // line 34
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "priceTtcLabel", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t<td>";
                // line 35
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "statutLabel", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['booking'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "\t\t\t\t</tbody>  
\t\t\t</table>\t
\t\t\t
\t\t\t";
            // line 41
            if ((twig_length_filter($this->env, (isset($context["liste"]) ? $context["liste"] : null)) >= 15)) {
                // line 42
                echo "\t\t\t<p class=\"text-center\">
\t\t\t\t<a class=\"btn btn-primary\" href=\"";
                // line 43
                echo $this->env->getExtension('routing')->getPath("booking_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Voir tout"), "html", null, true);
                echo "</a>
\t\t\t</p>
\t\t\t";
            }
            // line 46
            echo "
\t\t
\t\t";
        } else {
            // line 49
            echo "\t\t
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t\t<br/><br/><br/><br/><br/>
\t\t\t\t\t";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aucun booking pour le moment"), "html", null, true);
            echo ".<br/><br/>
\t\t\t\t</div>
\t\t\t</div>
\t\t
\t\t";
        }
        // line 58
        echo "\t\t
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "BookingBundle:Booking:show_booking.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 58,  164 => 53,  158 => 49,  153 => 46,  145 => 43,  142 => 42,  140 => 41,  135 => 38,  118 => 35,  114 => 34,  110 => 33,  105 => 32,  97 => 30,  89 => 28,  87 => 27,  83 => 26,  77 => 25,  70 => 24,  53 => 23,  44 => 16,  40 => 14,  36 => 12,  34 => 11,  26 => 5,  24 => 4,  19 => 1,);
    }
}
