<?php

/* UserBundle:Partial:user.html.twig */
class __TwigTemplate_dca82fc7cc93cafdb1ace01137bd75aa607716138bb6d1925382c7c9151f357b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12 vignette_ajax vignette_certifie\">
\t<div class=\"vignette\">
\t\t<div class=\"row\">
\t\t\t<div class=\"vignette_infos_1 col-xs-12 col-sm-3 col-md-3 col-lg-2 text-center\">
\t\t\t\t<a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_show", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()), "t" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "entrepriseStripped", array()))), "html", null, true);
        echo "\"><img class=\"img-thumbnail\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lastName", array()), "html", null, true);
        echo "\" src=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "image", array()), "url", array()), "html", null, true);
        echo "\"/></a>
\t\t\t</div>
\t\t\t<div class=\"vignette_infos_2 col-xs-12 col-sm-6 col-md-6 col-lg-7\">
\t\t\t\t<div class=\"vignette_infos_title\">
\t\t\t\t\t<a href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_show", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()), "t" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "entrepriseStripped", array()))), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "shortUserName2", array()), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"vignette_infos_autre\">
\t\t\t\t\t";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->truncate($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "city", array()), 30), "html", null, true);
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "postalCode", array())) {
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "postalCode", array()), "html", null, true);
            echo ")";
        }
        // line 13
        echo "\t\t\t\t</div>
\t\t\t\t<div class=\"vignette_infos_autre_2\">
\t\t\t\t\t";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->truncate($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "description", array()), 150), "html", null, true);
        echo "
\t\t\t\t</div>
\t\t\t\t<div class=\"vignette_infos_autre_2\">
\t\t\t\t";
        // line 18
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nbNote", array()) > 0)) {
            echo "<img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("images/template/b" . $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "note", array())) . ".png")), "html", null, true);
            echo "\"/>";
        } else {
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aucune note"), "html", null, true);
        }
        // line 19
        echo "\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"vignette_infos_1 col-xs-12 col-sm-3 col-md-3 col-lg-3 text-center\">
\t\t\t\t
\t\t\t\t<a href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_show", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()), "t" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "entrepriseStripped", array()))), "html", null, true);
        echo "\" class=\"btn btn-primary btn-lg vignette-btn\"><span class=\"glyphicon glyphicon-user\"></span>&nbsp;&nbsp;";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Voir la fiche"), "html", null, true);
        echo "</a>
\t\t\t\t
\t\t\t</div>
\t\t</div>\t
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "UserBundle:Partial:user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 23,  69 => 19,  61 => 18,  55 => 15,  51 => 13,  44 => 12,  36 => 9,  25 => 5,  19 => 1,);
    }
}
