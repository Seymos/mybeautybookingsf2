<?php

/* UserBundle:User:edit.html.twig */
class __TwigTemplate_bf906a263168c6ad59f33378b185a717b0e6cc8aae803a294255c35eb4383791 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t
\t";
        // line 6
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "type", array()) == 2)) {
            // line 7
            echo "\t<script>
\t
\tfunction rotationImg(div, image_id, rotate, type, title)
    { \t
\t      \$.ajax({
\t\t        url: '";
            // line 12
            echo $this->env->getExtension('routing')->getPath("user_rotateimg");
            echo "',
\t\t        type: 'POST',
\t\t        data: { image_id: image_id, rotate: rotate, type: type, title: title },
\t\t        beforeSend: function(data) {
\t\t            \$(\"#\"+div).empty().append('<center><img style=\\'margin-top:10px;\\' src=\\'";
            // line 16
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
            echo "\\'></center>');
\t\t        },
\t\t        success: function(data) {
\t\t            \$(\"#\"+div).empty().append(data);
\t\t        },
\t\t        error: function () {
\t\t            //alert('La requête n\\'a pas abouti');
\t\t        }
\t      });
    }
    
\t\$(document).ready(function () {
\t\t
\t\tDropzone.autoDiscover = false; // otherwise will be initialized twice
\t\tvar myDropzoneOptions = {
\t\t    url: \"";
            // line 31
            echo $this->env->getExtension('routing')->getUrl("upload_media", array("resize" => "1_250_250_2_800_600"));
            echo "\",
\t\t    acceptedFiles: \"image/jpeg,image/png,image/gif\",
\t        addRemoveLinks: true,
\t        dictRemoveFile : \"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Retirer cette photo"), "html", null, true);
            echo "\",
\t        dictCancelUploadConfirmation : \"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Êtes-vous sûr de vouloir annuler le transfert"), "html", null, true);
            echo "\",
\t        dictCancelUpload : \"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Annuler le transfert"), "html", null, true);
            echo "\",
\t        autoProcessQueue: true,
\t\t    uploadMultiple: false,
\t\t    parallelUploads: 1,
\t\t    maxFiles: 1,
\t\t    maxFilesize: 20, // MB
\t        success: function (file, response) {
\t            var imgName = response['name'];
\t            var input_hidden = Dropzone.createElement(\"<input type='hidden' name='image' value='\"+imgName+\"'>\");
\t            
\t            file.previewElement.classList.add(\"dz-success\");
      \t\t\tfile.previewElement.appendChild(input_hidden);
\t        },
\t        error: function (file, response) {
\t            file.previewElement.classList.add(\"dz-error\");
\t        },
\t        init: function() {
\t\t\t    this.on(\"addedfile\", function() {
\t\t\t      if (this.files[1]!=null){
\t\t\t        this.removeFile(this.files[0]);
\t\t\t      }
\t\t\t    });
\t\t\t}
\t\t};
\t\tvar myDropzone = new Dropzone('#upload_image_div', myDropzoneOptions);

\t});
\t</script>
\t";
        }
        // line 65
        echo "\t\t
";
    }

    // line 68
    public function block_body_content($context, array $blocks = array())
    {
        // line 69
        echo "
<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t";
        // line 74
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "type", array()) == 2)) {
            // line 75
            echo "\t\t\t\t<a href=\"";
            echo $this->env->getExtension('routing')->getPath("home_index");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
            echo "</a> > <a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_index");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
            echo "</a> > ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
            echo "
\t\t\t\t";
        } else {
            // line 77
            echo "\t\t\t\t<a href=\"";
            echo $this->env->getExtension('routing')->getPath("home_index");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
            echo "</a> > <a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_show");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
            echo "</a> > ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes infos"), "html", null, true);
            echo "
\t\t\t\t";
        }
        // line 79
        echo "\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"hidden-xs col-sm-3 col-md-3 col-lg-3\">
\t\t\t";
        // line 88
        echo twig_include($this->env, $context, "UserBundle:Partial:menu.html.twig");
        echo "
\t\t</div>
\t\t<div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9\">
\t\t\t
\t\t\t<div class=\"panel panel-default\">
\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t    <h3 class=\"panel-title\">";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes infos"), "html", null, true);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t
\t\t    \t\t";
        // line 98
        echo twig_include($this->env, $context, "UserBundle:Form:edit.html.twig");
        echo "
\t\t\t\t\t
\t\t\t\t\t";
        // line 100
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "type", array()) == 2)) {
            // line 101
            echo "\t\t\t\t\t<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=";
            echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getGmapApi(), "html", null, true);
            echo "&signed_in=false&libraries=places&callback=initMap\" async defer></script>
\t\t\t\t\t<script>

\t\t\t\t\t\tfunction updatePosition(lat, lng){
\t\t\tvar input_lat = document.getElementById('faprod_userbundle_user_gmap_lat');
\t\t\tvar input_lng = document.getElementById('faprod_userbundle_user_gmap_lng');
\t\t\t
\t\t\tinput_lat.value = lat;
\t\t\tinput_lng.value = lng;
\t\t}

\t\tfunction initMap(lat = '', lng = '') {

\t\t\tif (lat == ''){
\t\t\t\tlat = ";
            // line 115
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "lat", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "lat", array()), "html", null, true);
            } else {
                echo "47.445908";
            }
            echo ";
\t\t\t}
\t\t\t   
\t\t\tif (lng == ''){
\t\t\t\tlng = ";
            // line 119
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "lng", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "lng", array()), "html", null, true);
            } else {
                echo "2.615234";
            }
            echo ";
\t\t\t}


\t\t\tvar map = new google.maps.Map(document.getElementById('map'), {
\t\t\t\tcenter: {lat: Number(lat), lng: Number(lng) },
\t\t\t\tzoom: 13,
\t\t\t});
\t\t\tvar input = document.getElementById('pac-input');
\t\t
\t\t\tvar autocomplete = new google.maps.places.Autocomplete(input);
\t\t\tautocomplete.bindTo('bounds', map);
\t\t
\t\t\tvar center = map.center;
\t\t  
\t\t\tvar marker = new google.maps.Marker({
\t\t\t\tposition: center,
\t\t\t\tdraggable: true,
\t\t\t\tmap: map
\t\t\t});
\t\t
\t\t\tmarker.setIcon(/** @type {google.maps.Icon} */({
\t\t      url: '";
            // line 141
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/map.png"), "html", null, true);
            echo "',
\t\t      size: new google.maps.Size(62, 70),
\t\t      origin: new google.maps.Point(0, 0),
\t\t      anchor: new google.maps.Point(0, 35),
\t\t      scaledSize: new google.maps.Size(31, 35)
\t\t    }));
\t\t    marker.setVisible(true);
\t\t\tmarker.setDraggable(true);
\t\t\t
\t\t\tgoogle.maps.event.addListener(marker, 'dragend', function () {
\t\t\t\tmap.setCenter(this.getPosition());
\t\t\t\tupdatePosition(this.getPosition().lat(), this.getPosition().lng());
\t\t\t});
\t\t  
\t\t
\t\t\tautocomplete.addListener('place_changed', function() {

\t\t    marker.setVisible(false);
\t\t    var place = autocomplete.getPlace();
\t\t    if (!place.geometry) {
\t\t      window.alert(\"";
            // line 161
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Localisation introuvable"), "html", null, true);
            echo "\");
\t\t      return;
\t\t    } else {
\t\t\t\t\$('#faprod_userbundle_user_gmap_lat').val(place.geometry.location.lat());
\t\t\t\t\$('#faprod_userbundle_user_gmap_lng').val(place.geometry.location.lng());
\t\t\t\t\$('#faprod_userbundle_user_gmap_place_id').val(place.place_id);
\t\t\t\t\$('#faprod_userbundle_user_gmap_place_name').val(place.formatted_address);
\t\t    }
\t\t
\t\t    // If the place has a geometry, then present it on a map.
\t\t    if (place.geometry.viewport) {
\t\t      map.fitBounds(place.geometry.viewport);
\t\t    } else {
\t\t      map.setCenter(place.geometry.location);
\t\t      map.setZoom(13);  // Why 17? Because it looks good.
\t\t    }
\t\t    marker.setIcon(/** @type {google.maps.Icon} */({
\t\t      url: '";
            // line 178
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/map.png"), "html", null, true);
            echo "',
\t\t      size: new google.maps.Size(62, 70),
\t\t      origin: new google.maps.Point(0, 0),
\t\t      anchor: new google.maps.Point(0, 35),
\t\t      scaledSize: new google.maps.Size(31, 35)
\t\t    }));
\t\t    marker.setPosition(place.geometry.location);
\t\t    marker.setVisible(true);
\t\t\tmarker.setDraggable(true);
\t\t\t
\t\t\tgoogle.maps.event.addListener(marker, 'dragend', function () {
\t\t\t\tmap.setCenter(this.getPosition());
\t\t\t\tupdatePosition(this.getPosition().lat(), this.getPosition().lng());
\t\t\t});
\t\t
\t\t  });
\t\t}
\t\t
\t\t

\t\t\t\t\t\t
\t\t\t\t\t\tfunction refuserToucheEntree(event){
\t\t\t\t\t\t\t// Compatibilité IE / Firefox
\t\t\t\t\t\t\tif(!event && window.event) {
\t\t\t\t\t\t\t\tevent = window.event;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t// IE
\t\t\t\t\t\t\tif(event.keyCode == 13) {
\t\t\t\t\t\t\t\tevent.returnValue = false;
\t\t\t\t\t\t\t\tevent.cancelBubble = true;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t// DOM
\t\t\t\t\t\t\tif(event.which == 13) {
\t\t\t\t\t\t\t\tevent.preventDefault();
\t\t\t\t\t\t\t\tevent.stopPropagation();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}

\t\t\t\t\t</script>
\t\t\t\t\t";
        }
        // line 218
        echo "    
\t\t    \t</div>
\t\t\t</div>
\t\t    
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "UserBundle:User:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  349 => 218,  306 => 178,  286 => 161,  263 => 141,  234 => 119,  223 => 115,  205 => 101,  203 => 100,  198 => 98,  191 => 94,  182 => 88,  171 => 79,  157 => 77,  143 => 75,  141 => 74,  134 => 69,  131 => 68,  126 => 65,  94 => 36,  90 => 35,  86 => 34,  80 => 31,  62 => 16,  55 => 12,  48 => 7,  46 => 6,  40 => 4,  37 => 3,  11 => 1,);
    }
}
