<?php

/* CoreBundle::layout.html.twig */
class __TwigTemplate_e608c451942b7d7ffc60ede5bfddb3c93ea5ace859c491085d0e31b3dc29f908 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'meta_description' => array($this, 'block_meta_description'),
            'meta_keywords' => array($this, 'block_meta_keywords'),
            'facebook_metas' => array($this, 'block_facebook_metas'),
            'facebook_js' => array($this, 'block_facebook_js'),
            'body' => array($this, 'block_body'),
            'body_content' => array($this, 'block_body_content'),
            'body_bottom' => array($this, 'block_body_bottom'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getMetaTitle(), "html", null, true);
    }

    // line 4
    public function block_meta_description($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getMetaDescription(), "html", null, true);
    }

    // line 5
    public function block_meta_keywords($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getMetaKeywords(), "html", null, true);
    }

    // line 7
    public function block_facebook_metas($context, array $blocks = array())
    {
        // line 8
        echo twig_include($this->env, $context, "RegisterBundle:Partial:facebook_metas.html.twig");
        echo "
";
    }

    // line 11
    public function block_facebook_js($context, array $blocks = array())
    {
        // line 12
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("RegisterBundle:Partial:facebook"));
        echo "
";
    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        // line 16
        echo "<div id=\"page\">
\t
\t";
        // line 18
        echo twig_include($this->env, $context, "CoreBundle:Partial:menu.html.twig");
        echo "

\t<div id=\"header\">
\t\t";
        // line 21
        echo twig_include($this->env, $context, "CoreBundle::header.html.twig");
        echo "
\t</div>
\t
\t<div id=\"content\">
\t\t";
        // line 25
        $this->displayBlock('body_content', $context, $blocks);
        // line 28
        echo "\t\t
\t\t";
        // line 29
        $this->displayBlock('body_bottom', $context, $blocks);
        // line 30
        echo "\t\t
\t\t";
        // line 31
        echo twig_include($this->env, $context, "CoreBundle::popupajax.html.twig");
        echo "

\t\t";
        // line 33
        echo twig_include($this->env, $context, "CoreBundle::messages.html.twig");
        echo "
\t</div>
\t
\t<footer>
\t\t";
        // line 37
        echo twig_include($this->env, $context, "CoreBundle::footer.html.twig");
        echo "
\t</footer>
</div>
";
    }

    // line 25
    public function block_body_content($context, array $blocks = array())
    {
        // line 26
        echo "
\t\t";
    }

    // line 29
    public function block_body_bottom($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "CoreBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 29,  132 => 26,  129 => 25,  121 => 37,  114 => 33,  109 => 31,  106 => 30,  104 => 29,  101 => 28,  99 => 25,  92 => 21,  86 => 18,  82 => 16,  79 => 15,  73 => 12,  70 => 11,  64 => 8,  61 => 7,  55 => 5,  49 => 4,  43 => 3,  11 => 1,);
    }
}
