<?php

/* UserBundle:User:show_infos.html.twig */
class __TwigTemplate_9c7a44a0f0a5ac547f23f793b97c13c6fdb96177fe54c53c4dc3816930da5aac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row popup_mesinfos\">
<div class=\"col-lg-12\">

\t\t<table class=\"table table-condensed table_popup\">
\t\t\t<tbody>
\t\t\t\t<tr>
\t\t\t\t  <th style=\"width:25%;\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nom"), "html", null, true);
        echo "</th>
\t\t\t\t  <td>";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "firstName", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <th>";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prénom"), "html", null, true);
        echo "</th>
\t\t\t\t  <td>";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lastName", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <th>";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Votre adresse"), "html", null, true);
        echo "</th>
\t\t\t\t  <td>";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "address1", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <th>";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Complément adresse"), "html", null, true);
        echo "</th>
\t\t\t\t  <td>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "address2", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <th>";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Code postal"), "html", null, true);
        echo "</th>
\t\t\t\t  <td>";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "postalCode", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <th>";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ville"), "html", null, true);
        echo "</th>
\t\t\t\t  <td>";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "city", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <th>";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Téléphone fixe"), "html", null, true);
        echo "</th>
\t\t\t\t  <td>";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "phone", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <th>";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Téléphone portable"), "html", null, true);
        echo "</th>
\t\t\t\t  <td>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "mobile", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t  <th>";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
\t\t\t\t   <td>";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t</tbody>
\t\t\t<tfoot>
\t\t\t\t<tr>
\t\t\t\t\t<td colspan=\"2\" class=\"text-center\">
\t\t\t\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 46
        echo $this->env->getExtension('routing')->getPath("user_edit");
        echo "\"><span>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Modifier"), "html", null, true);
        echo "</span></a>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t</table>

</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "UserBundle:User:show_infos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 46,  111 => 40,  107 => 39,  101 => 36,  97 => 35,  91 => 32,  87 => 31,  81 => 28,  77 => 27,  71 => 24,  67 => 23,  61 => 20,  57 => 19,  51 => 16,  47 => 15,  41 => 12,  37 => 11,  31 => 8,  27 => 7,  19 => 1,);
    }
}
