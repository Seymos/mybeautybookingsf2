<?php

/* UserBundle:Partial:menu.html.twig */
class __TwigTemplate_c5540d70fbc746167e62d0328896597db16d582a7cfc8babd0166c79f9c47fc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 2)) {
            // line 2
            echo "<div id=\"menu_left\" class=\"menu_profil_pro\">

<a href=\"";
            // line 4
            echo $this->env->getExtension('routing')->getPath("user_index");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 5
            echo $this->env->getExtension('routing')->getPath("user_edit");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkprofilpro", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 6
            echo $this->env->getExtension('routing')->getPath("agenda_index");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkDisponibilites", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon planning"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 7
            echo $this->env->getExtension('routing')->getPath("booking_paiements");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes paiements"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 8
            echo $this->env->getExtension('routing')->getPath("user_bank");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkCompteiban", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon RIB"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 9
            echo $this->env->getExtension('routing')->getPath("document_index");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkDocuments", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes documents"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 10
            echo $this->env->getExtension('routing')->getPath("user_statistique");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tableau de bord"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 11
            echo $this->env->getExtension('routing')->getPath("user_competences");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkcheveux", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes compétences"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 12
            echo $this->env->getExtension('routing')->getPath("offer_index");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkServices", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes prestations"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 13
            echo $this->env->getExtension('routing')->getPath("booking_index");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 14
            echo $this->env->getExtension('routing')->getPath("user_avis");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Notes & Avis"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 15
            echo $this->env->getExtension('routing')->getPath("message_index");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
            echo "</a>

</div>
";
        } else {
            // line 19
            echo "<div id=\"menu_left\" class=\"menu_profil\">

<a href=\"";
            // line 21
            echo $this->env->getExtension('routing')->getPath("user_show");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkprofil", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 22
            echo $this->env->getExtension('routing')->getPath("user_edit");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkmesinfos", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes infos"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 23
            echo $this->env->getExtension('routing')->getPath("user_photos");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkphotos", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes photos"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 24
            echo $this->env->getExtension('routing')->getPath("user_cheveux");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkcheveux", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes cheveux"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 25
            echo $this->env->getExtension('routing')->getPath("user_peau");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkpeau", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ma peau"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 26
            echo $this->env->getExtension('routing')->getPath("user_preferences");
            echo "\" class=\"btn btn-primary ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkpreferences", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes préférences"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 27
            echo $this->env->getExtension('routing')->getPath("user_favoris");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes beauty artists"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 28
            echo $this->env->getExtension('routing')->getPath("user_avis");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes notes & avis"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 29
            echo $this->env->getExtension('routing')->getPath("booking_index");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
            echo "</a>
<a href=\"";
            // line 30
            echo $this->env->getExtension('routing')->getPath("message_index");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
            echo "</a>

</div>
";
        }
    }

    public function getTemplateName()
    {
        return "UserBundle:Partial:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 30,  176 => 29,  170 => 28,  164 => 27,  156 => 26,  148 => 25,  140 => 24,  132 => 23,  124 => 22,  116 => 21,  112 => 19,  103 => 15,  97 => 14,  91 => 13,  83 => 12,  75 => 11,  69 => 10,  61 => 9,  53 => 8,  47 => 7,  39 => 6,  31 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
