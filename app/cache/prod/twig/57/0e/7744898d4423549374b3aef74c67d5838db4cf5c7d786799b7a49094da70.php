<?php

/* ::base.html.twig */
class __TwigTemplate_570e7744898d4423549374b3aef74c67d5838db4cf5c7d786799b7a49094da70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'meta_description' => array($this, 'block_meta_description'),
            'meta_keywords' => array($this, 'block_meta_keywords'),
            'facebook_metas' => array($this, 'block_facebook_metas'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
\t
  <title>";
        // line 5
        $this->displayBlock('meta_title', $context, $blocks);
        echo "</title>
  <meta name=\"description\" content=\"";
        // line 6
        $this->displayBlock('meta_description', $context, $blocks);
        echo "\">
  <meta name=\"keywords\" content=\"";
        // line 7
        $this->displayBlock('meta_keywords', $context, $blocks);
        echo "\">
  
  <meta charset=\"utf-8\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
  <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getSiteUrl(), "html", null, true);
        echo "favicon.ico\" />
  
  ";
        // line 14
        $this->displayBlock('facebook_metas', $context, $blocks);
        // line 15
        echo "
  ";
        // line 16
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 33
        echo "  
  ";
        // line 34
        $this->displayBlock('javascripts', $context, $blocks);
        // line 137
        echo "  
  <!--[if lt IE 9]>
      <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
      <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
  <![endif]-->


   <script src='https://www.google.com/recaptcha/api.js'></script>
   
   
   
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '550444071953644'); 
fbq('track', 'PageView');
</script>
<noscript>
<img height=\"1\" width=\"1\" 
src=\"https://www.facebook.com/tr?id=550444071953644&ev=PageView
&noscript=1\"/>
</noscript>
<!-- End Facebook Pixel Code -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-107296929-2\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107296929-2');
</script>



</head>

<body";
        // line 183
        if (($this->env->getExtension('FAPROD.TwigExtension')->getControllerAction() == "Home/welcome")) {
            echo " class=\"home_temp_bk\"";
        }
        echo ">
<script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = '';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = '';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getFacebookApi(), "html", null, true);
        echo "',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = \"//connect.facebook.net/fr_FR/sdk.js\";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.name);
      div_status = document.getElementById('status');
      if (div_status){
      \tdiv_status.innerHTML = '<br/><a href=\"";
        // line 259
        echo $this->env->getExtension('routing')->getPath("register_facebook");
        echo "\"><b>>> Cliquez ici pour vous connecter maintenant avec Facebook</b></a>';
    \t}
    \tloginbuttonfb = document.getElementById('loginbuttonfb');
      if (loginbuttonfb){
      \tloginbuttonfb.style.display = \"none\" ;
    \t}
\t\t
\t\tdiv_status2 = document.getElementById('status2');
      if (div_status2){
      \tdiv_status2.innerHTML = '<br/><a href=\"";
        // line 268
        echo $this->env->getExtension('routing')->getPath("register_facebook");
        echo "\"><b>>> Cliquez ici pour vous connecter maintenant avec Facebook</b></a>';
    \t}
\t\tloginbuttonfb2 = document.getElementById('loginbuttonfb2');
      if (loginbuttonfb2){
      \tloginbuttonfb2.style.display = \"none\" ;
    \t}
    });
  }
</script>

";
        // line 278
        $this->displayBlock('body', $context, $blocks);
        // line 280
        echo "
</body>
</html>";
    }

    // line 5
    public function block_meta_title($context, array $blocks = array())
    {
    }

    // line 6
    public function block_meta_description($context, array $blocks = array())
    {
    }

    // line 7
    public function block_meta_keywords($context, array $blocks = array())
    {
    }

    // line 14
    public function block_facebook_metas($context, array $blocks = array())
    {
    }

    // line 16
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 17
        echo "    <link href=\"https://fonts.googleapis.com/css?family=Sofia\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\">
    ";
        // line 20
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "6c3769d_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6c3769d_0") : $this->env->getExtension('assets')->getAssetUrl("css/6c3769d_jNotify.jquery_1.css");
            // line 29
            echo "  \t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" />
  \t<link rel=\"stylesheet\" href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\">
\t";
            // asset "6c3769d_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6c3769d_1") : $this->env->getExtension('assets')->getAssetUrl("css/6c3769d_core_2.css");
            echo "  \t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" />
  \t<link rel=\"stylesheet\" href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\">
\t";
            // asset "6c3769d_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6c3769d_2") : $this->env->getExtension('assets')->getAssetUrl("css/6c3769d_advert_3.css");
            echo "  \t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" />
  \t<link rel=\"stylesheet\" href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\">
\t";
            // asset "6c3769d_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6c3769d_3") : $this->env->getExtension('assets')->getAssetUrl("css/6c3769d_content_4.css");
            echo "  \t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" />
  \t<link rel=\"stylesheet\" href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\">
\t";
            // asset "6c3769d_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6c3769d_4") : $this->env->getExtension('assets')->getAssetUrl("css/6c3769d_mmenu_5.css");
            echo "  \t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" />
  \t<link rel=\"stylesheet\" href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\">
\t";
            // asset "6c3769d_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6c3769d_5") : $this->env->getExtension('assets')->getAssetUrl("css/6c3769d_facebox_6.css");
            echo "  \t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" />
  \t<link rel=\"stylesheet\" href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\">
\t";
            // asset "6c3769d_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6c3769d_6") : $this->env->getExtension('assets')->getAssetUrl("css/6c3769d_dropzone_7.css");
            echo "  \t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" />
  \t<link rel=\"stylesheet\" href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\">
\t";
        } else {
            // asset "6c3769d"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_6c3769d") : $this->env->getExtension('assets')->getAssetUrl("css/6c3769d.css");
            echo "  \t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" type=\"text/css\" />
  \t<link rel=\"stylesheet\" href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\">
\t";
        }
        unset($context["asset_url"]);
        // line 32
        echo "  ";
    }

    // line 34
    public function block_javascripts($context, array $blocks = array())
    {
        // line 35
        echo "    <script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
    <script type=\"text/javascript\" src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
    ";
        // line 37
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "4ddf763_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4ddf763_0") : $this->env->getExtension('assets')->getAssetUrl("js/4ddf763_dropzone_1.js");
            // line 44
            echo "  \t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t";
            // asset "4ddf763_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4ddf763_1") : $this->env->getExtension('assets')->getAssetUrl("js/4ddf763_jNotify.jquery.min_2.js");
            echo "  \t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t";
            // asset "4ddf763_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4ddf763_2") : $this->env->getExtension('assets')->getAssetUrl("js/4ddf763_bootstrap-datepicker_3.js");
            echo "  \t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t";
            // asset "4ddf763_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4ddf763_3") : $this->env->getExtension('assets')->getAssetUrl("js/4ddf763_bootstrap-datepicker.fr_4.js");
            echo "  \t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t";
            // asset "4ddf763_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4ddf763_4") : $this->env->getExtension('assets')->getAssetUrl("js/4ddf763_facebox_5.js");
            echo "  \t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t";
        } else {
            // asset "4ddf763"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4ddf763") : $this->env->getExtension('assets')->getAssetUrl("js/4ddf763.js");
            echo "  \t<script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t";
        }
        unset($context["asset_url"]);
        // line 46
        echo "\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.mmenu.js"), "html", null, true);
        echo "\"></script>
\t<script>
\t
\t\tfunction displayDefrisage(input)
\t\t{
\t\t\tvar cases = document.getElementsByClassName('div_defrisage');

\t\t\tif (input.value == 170 || input.value == 171) {
\t\t\t\tif (input.checked) {
\t\t\t\t\t\$('.div_defrisage').show();
\t\t\t\t} else {
\t\t\t\t\t\$('.div_defrisage').hide();
\t\t\t\t}
\t\t\t} else {
\t\t\t\tif (input.checked) {
\t\t\t\t\t\$('.div_defrisage').hide();
\t\t\t\t}
\t\t\t}
\t\t}
\t\t
\t\tfunction nbCoche(class_name)
\t\t{\t
\t\t\tvar cases = document.getElementsByClassName(class_name);
\t\t\tvar nb = 0;
\t\t\t
\t\t\tfor (var i=0;i< cases.length;i++){
\t\t\t\tif(cases[i].type==\"checkbox\" && cases[i].checked)
\t\t\t\t\tnb++;
\t\t\t}
\t\t\t//alert(nb);
\t\t\treturn nb;
\t\t\t
\t\t}
\t\t
\t    function processCoche(class_name, id_name)
\t\t{\t
\t\t\tvar cases = document.getElementsByClassName(class_name);
\t\t\tvar not_in = [];//[\"faprod_userbundle_user_cheveux_247\", \"faprod_userbundle_user_cheveux_248\", \"faprod_userbundle_user_cheveux_249\", \"faprod_userbundle_user_cheveux_250\", \"faprod_userbundle_user_cheveux_251\", \"faprod_userbundle_user_cheveux_252\"];
\t\t\t//alert(not_in.indexOf(id_name));
\t\t\tif (not_in.indexOf(id_name) == -1) {
\t\t\t\tfor (var i=0;i< cases.length;i++){
\t\t\t\t\tif(cases[i].type==\"checkbox\" && cases[i].checked && cases[i].id != id_name && not_in.indexOf(cases[i].id) == -1)
\t\t\t\t\t\tcases[i].checked = false;
\t\t\t\t}
\t\t\t}
\t\t}

\t\tfunction refuserToucheEntree(event){
\t\t\t// Compatibilité IE / Firefox
\t\t\tif(!event && window.event) {
\t\t\t\tevent = window.event;
\t\t\t}
\t\t\t// IE
\t\t\tif(event.keyCode == 13) {
\t\t\t\tevent.returnValue = false;
\t\t\t\tevent.cancelBubble = true;
\t\t\t}
\t\t\t// DOM
\t\t\tif(event.which == 13) {
\t\t\t\tevent.preventDefault();
\t\t\t\tevent.stopPropagation();
\t\t\t}
\t\t}

\t\tjQuery(document).ready(function(\$) {
\t\t
\t\t\t\$(function() {
\t\t\t\t\$('nav#menu').mmenu();
\t\t\t});
\t\t\t
\t\t  \$('#popupajaxModal').on('show.bs.modal', function (event) {
\t\t\t  var button = \$(event.relatedTarget) // Button that triggered the modal
\t\t\t  var url = button.data('whatever') // Extract info from data-* attributes
\t\t\t  var title = button.data('title')
\t\t\t  var modal = \$(this)
\t\t\t  if (modal.find('#urlload').text() != url){
\t\t\t  \tmodal.find('#urlload').text(url)
\t\t\t  \tmodal.find('#poputitle').text(title)
\t\t\t  \tmodal.find('#popupajax').empty().append('<center><img src=\\'";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
        echo "\\'></center>').load(url);
\t\t\t  }
\t\t\t})
\t\t\t\$('#popupajaxModal').on('hide.bs.modal', function (event) {
\t\t\t\tvar modal = \$(this)
\t\t\t\tmodal.find('#urlload').text('')
\t\t\t})
\t\t\t
\t\t  \$('a[rel*=facebox]').facebox();
\t\t  \$('[data-toggle=\"popover\"]').popover({trigger : 'hover',html : true});
\t\t})
\t</script>
  ";
    }

    // line 278
    public function block_body($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  472 => 278,  455 => 124,  373 => 46,  335 => 44,  331 => 37,  327 => 35,  324 => 34,  320 => 32,  262 => 29,  258 => 20,  253 => 17,  250 => 16,  245 => 14,  240 => 7,  235 => 6,  230 => 5,  224 => 280,  222 => 278,  209 => 268,  197 => 259,  152 => 217,  113 => 183,  65 => 137,  63 => 34,  60 => 33,  58 => 16,  55 => 15,  53 => 14,  48 => 12,  40 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
