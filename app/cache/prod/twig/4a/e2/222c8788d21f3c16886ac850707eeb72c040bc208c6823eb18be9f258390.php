<?php

/* RegisterBundle:Register:preferences.html.twig */
class __TwigTemplate_4ae2222c8788d21f3c16886ac850707eeb72c040bc208c6823eb18be9f258390 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid title title_bottom\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t<h1>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Création de mon profil"), "html", null, true);
        echo "</h1>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t\t\t
\t\t\t<div class=\"panel panel-default\">
\t\t\t\t<div class=\"panel-heading text-center\">
\t\t\t\t    <h3 class=\"panel-title\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes préférences"), "html", null, true);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t
\t\t\t\t
\t\t\t\t\t<form action=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("register_preferences");
        echo "\" id=\"myform\" autocomplete=\"off\" method=\"POST\" ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo " class=\"form-horizontal col-lg-12\">
\t\t\t\t\t";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
\t\t\t\t\t
\t\t\t\t\t\t<table class=\"table table_white\">
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["preferences"]) ? $context["preferences"] : null), "children", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["sub_category"]) {
            // line 33
            echo "\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t  <th style=\"width:25%;\">";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["sub_category"], "title", array()), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t\t  <td>
\t\t\t\t\t\t\t\t\t<ul class=\"list_checks_profil\">
\t\t\t\t\t\t\t\t\t";
            // line 37
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["sub_category"], "children", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["competence"]) {
                // line 38
                echo "\t\t\t\t\t\t\t\t\t\t";
                if ($this->getAttribute($context["competence"], "display", array(0 => (isset($context["is_crepu"]) ? $context["is_crepu"] : null), 1 => (isset($context["is_lisse"]) ? $context["is_lisse"] : null), 2 => (isset($context["is_boucle"]) ? $context["is_boucle"] : null)), "method")) {
                    // line 39
                    echo "\t\t\t\t\t\t\t\t\t\t<li> <input class=\"nbCoche\" ";
                    if (twig_in_filter($context["competence"], $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "preferences", array()))) {
                        echo "checked=checked";
                    }
                    echo " type=\"checkbox\" name=\"faprod_userbundle_user[preferences][]\" id=\"faprod_userbundle_user_preferences_";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                    echo "\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "title", array()), "html", null, true);
                    echo "</li>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 41
                echo "\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competence'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t  </td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t<tfoot>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("register_peau");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("< Retour"), "html", null, true);
        echo "</a>
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t<a class=\"btn btn-primary btn-lg\" href=\"javascript:void(0);\" onclick=\"if (nbCoche('nbCoche') >= 3) {\$('#myform').submit();} else {alert('Vous devez compléter tous les critéres');}\"><span class=\"glyphicon glyphicon-ok-sign\"></span> ";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Continuer >"), "html", null, true);
        echo "</a>
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t</table>

\t\t\t\t\t";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token", array()), 'widget');
        echo "
\t\t\t\t\t</form>
\t\t\t\t\t
\t\t\t\t\t
\t\t    \t</div>
\t\t\t</div>
\t\t    
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "RegisterBundle:Register:preferences.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 59,  146 => 53,  138 => 50,  132 => 46,  123 => 42,  117 => 41,  103 => 39,  100 => 38,  96 => 37,  90 => 34,  87 => 33,  83 => 32,  76 => 28,  70 => 27,  62 => 22,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
