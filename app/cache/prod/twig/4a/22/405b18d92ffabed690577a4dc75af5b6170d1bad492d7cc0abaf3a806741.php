<?php

/* CoreBundle:Partial:search.html.twig */
class __TwigTemplate_4a22405b18d92ffabed690577a4dc75af5b6170d1bad492d7cc0abaf3a806741 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>

function goprestation(metier_id){

    \$.ajax({
        url: '";
        // line 6
        echo $this->env->getExtension('routing')->getPath("home_prestation");
        echo "',
        type: 'GET',
        data: 'metier_id='+metier_id,
        beforeSend: function(data) {
            \$('#div_modele').empty().append('<center><img style=\\'width:25px;\\' src=\\'";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
        echo "\\'></center>');
        },
        success: function(data) {
            \$('#div_modele').empty().append(data);
        },
        error: function () {
            alert('La requête n\\'a pas abouti');
        }
    });
\t
};
</script>

<div class=\"row home_search_large\">
\t<div class=\"col-lg-12 search_box_xs\">
\t\t<form action=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("home_gosearch");
        echo "\" autocomplete=\"off\" method=\"post\" class=\"form\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\"> 
\t\t\t\t<div class=\"col-sm-6 col-md-3 col-lg-offset-2 col-lg-3\">
\t\t\t\t\t<select required=required onchange=\"goprestation(this.value)\" class=\"form-control\" id=\"search_category\" name=\"search_category\">
\t\t\t\t\t\t<option value=\"\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prestation"), "html", null, true);
        echo "</option>
\t\t\t\t\t\t";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["metiers"]) ? $context["metiers"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["metier"]) {
            // line 32
            echo "\t\t\t\t\t\t\t<option ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "search_category"), "method") == $this->getAttribute($context["metier"], "id", array()))) {
                echo "selected=selected";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["metier"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["metier"], "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['metier'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "\t\t\t\t\t</select>
\t\t\t\t\t<div id=\"div_modele\" style=\"margin-top:10px;\">
\t\t\t\t\t\t";
        // line 36
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "search_category"), "method")) {
            // line 37
            echo "\t\t\t\t\t\t<select class=\"form-control\" id=\"search_prestation\" name=\"search_prestation\">
\t\t\t\t\t\t\t<option value=\"\">";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Type de prestation"), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t";
            // line 39
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["prestations"]) ? $context["prestations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["prestation"]) {
                // line 40
                echo "\t\t\t\t\t\t\t<option ";
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "search_prestation"), "method") == $this->getAttribute($context["prestation"], "id", array()))) {
                    echo "selected=selected";
                }
                echo " value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["prestation"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["prestation"], "title", array()), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t";
                // line 41
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["prestation"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_prestation"]) {
                    // line 42
                    echo "\t\t\t\t\t\t\t<option ";
                    if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "search_prestation"), "method") == $this->getAttribute($context["sub_prestation"], "id", array()))) {
                        echo "selected=selected";
                    }
                    echo " value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_prestation"], "id", array()), "html", null, true);
                    echo "\">&nbsp;&nbsp;&nbsp;&nbsp;";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_prestation"], "title", array()), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_prestation'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 44
                echo "\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prestation'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "\t\t\t\t\t\t</select>
\t\t\t\t\t\t";
        }
        // line 47
        echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-6 col-md-3 col-lg-3\">
\t\t\t\t\t  <input type=\"hidden\" id=\"search_ou_location\" name=\"search_ou_location\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "search_ou_location"), "method"), "html", null, true);
        echo "\">
\t\t\t\t\t  <input onkeypress=\"refuserToucheEntree(event)\" placeholder=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Où ?"), "html", null, true);
        echo "\" class=\"form-control\" type=\"text\" id=\"search_ou\" name=\"search_ou\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "get", array(0 => "search_ou"), "method"), "html", null, true);
        echo "\">
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-md-3 col-lg-2\">
\t\t\t\t\t<button class=\"btn btn-primary btn-sadv\"><span class=\"glyphicon glyphicon-search\"></span> ";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Recherche"), "html", null, true);
        echo "</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t</form>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Partial:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 54,  148 => 51,  144 => 50,  139 => 47,  135 => 45,  129 => 44,  114 => 42,  110 => 41,  99 => 40,  95 => 39,  91 => 38,  88 => 37,  86 => 36,  82 => 34,  67 => 32,  63 => 31,  59 => 30,  51 => 25,  33 => 10,  26 => 6,  19 => 1,);
    }
}
