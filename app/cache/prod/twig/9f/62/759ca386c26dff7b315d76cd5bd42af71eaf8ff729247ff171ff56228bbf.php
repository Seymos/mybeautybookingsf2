<?php

/* UserBundle:Form:fields.html.twig */
class __TwigTemplate_9f62759ca386c26dff7b315d76cd5bd42af71eaf8ff729247ff171ff56228bbf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("form_div_layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'file_widget' => array($this, 'block_file_widget'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_file_widget($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "\t\t";
        if ( !(null === (isset($context["image_url"]) ? $context["image_url"] : null))) {
            // line 6
            echo "\t\t\t<img class=\"img-thumbnail\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((isset($context["image_url"]) ? $context["image_url"] : null)), "html", null, true);
            echo "\"/>
\t\t\t";
            // line 7
            $this->displayBlock("form_widget", $context, $blocks);
            echo "
\t\t";
        } else {
            // line 9
            echo "\t\t\t";
            $this->displayBlock("form_widget", $context, $blocks);
            echo "
\t\t";
        }
        // line 11
        echo "    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 14
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        // line 15
        ob_start();
        // line 16
        echo "    <ul ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
    ";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 18
            echo "        <li>
            ";
            // line 19
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo "&nbsp;&nbsp;";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "    </ul>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "UserBundle:Form:fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 22,  84 => 19,  81 => 18,  77 => 17,  72 => 16,  70 => 15,  67 => 14,  62 => 11,  56 => 9,  51 => 7,  46 => 6,  43 => 5,  40 => 4,  37 => 3,  11 => 1,);
    }
}
