<?php

/* RegisterBundle:Form:cliente.html.twig */
class __TwigTemplate_3090de1f09eb2dbcf0127b2bf29b5f52ba82553fb45f569399a319f3750ff04e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : null), array(0 => "CoreBundle:Form:fields_errors.html.twig"));
        // line 2
        echo "
<form method=\"POST\" ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo " autocomplete=\"off\" class=\"form-horizontal col-lg-12\">
";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "validate", array()), 'widget');
        echo "   
    ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
\t<div class=\"row\">
\t  <div class=\"form-group\">
\t\t<div class=\"col-lg-4\">
\t\t\t<label>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Inscription via Facebook"), "html", null, true);
        echo "</label>
\t\t</div>
\t\t<div class=\"col-lg-7\">
\t\t\t<div id=\"loginbuttonfb2\">
\t\t\t\t<fb:login-button size=\"large\" scope=\"public_profile,email\" onlogin=\"checkLoginState();\">";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Connexion Facebook"), "html", null, true);
        echo "</fb:login-button>
\t\t\t</div>
\t\t\t
\t\t\t<div id=\"status2\">
\t\t\t</div>
\t\t</div>
\t  </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-4\">
\t\t\t<label>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nom"), "html", null, true);
        echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-7\">
\t        ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'errors');
        echo "
\t        ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-4\">
\t\t\t<label>";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prénom"), "html", null, true);
        echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-7\">
\t        ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'errors');
        echo "
\t        ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t      </div>
\t    </div>
\t</div>
    <div class=\"row\">
\t    <div class=\"form-group\">
\t\t   <div class=\"col-lg-4\">
\t\t\t<label>";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-7\">
\t        ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
        echo "
\t        ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t\t<div class=\"col-lg-4\">
\t\t\t<label>";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mot de passe"), "html", null, true);
        echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t    </div>
\t    \t<div class=\"col-lg-7\">
\t      \t\t";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), "first", array()), 'errors');
        echo "
\t        \t";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t        </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t     <div class=\"col-lg-4\">
\t\t\t    <label>";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Confirmez le mot de passe"), "html", null, true);
        echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t    </div>
\t    \t<div class=\"col-lg-7\">
\t      \t\t";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), "second", array()), 'errors');
        echo "
\t        \t";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t        </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t  <div class=\"form-group\">
\t\t<div class=\"col-lg-4\">
\t\t\t<label>";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CGU"), "html", null, true);
        echo "</label>
\t\t</div>
\t\t<div class=\"col-lg-7\">
\t\t\t";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cgu", array()), 'widget');
        echo " <a target=\"_blank\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("content_show", array("content_id" => 1, "t" => "conditions-generales-d-utilisation")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("J'accepte les conditions générales d'utilisation"), "html", null, true);
        echo "</a><br/>
\t\t</div>
\t  </div>
\t</div>
\t<div class=\"row\">
\t  <div class=\"form-group\">
\t\t<div class=\"col-lg-4\">
\t\t\t<label>";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Newsletter"), "html", null, true);
        echo "</label>
\t\t</div>
\t\t<div class=\"col-lg-7\">
\t\t\t";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newsletter", array()), 'widget');
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Je souhaite recevoir la newsletter"), "html", null, true);
        echo "<br/>
\t\t</div>
\t  </div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t<button class=\"btn btn-primary btn-lg\"><span class=\"glyphicon glyphicon-ok-sign\"></span> ";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Validation"), "html", null, true);
        echo "</button>
\t\t   </div>
\t\t</div>
\t</div>
";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token", array()), 'widget');
        echo "
</form>

";
    }

    public function getTemplateName()
    {
        return "RegisterBundle:Form:cliente.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 103,  198 => 99,  186 => 92,  180 => 89,  166 => 82,  160 => 79,  150 => 72,  146 => 71,  140 => 68,  130 => 61,  126 => 60,  120 => 57,  110 => 50,  106 => 49,  100 => 46,  90 => 39,  86 => 38,  80 => 35,  70 => 28,  66 => 27,  60 => 24,  46 => 13,  39 => 9,  32 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
