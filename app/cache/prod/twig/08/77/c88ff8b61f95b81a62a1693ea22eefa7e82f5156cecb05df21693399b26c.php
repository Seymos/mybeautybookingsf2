<?php

/* CoreBundle::messages.html.twig */
class __TwigTemplate_0877c88ff8b61f95b81a62a1693ea22eefa7e82f5156cecb05df21693399b26c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "has", array(0 => "message"), "method") || $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashbag", array()), "has", array(0 => "error"), "method"))) {
            // line 2
            echo "     ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashBag", array()), "all", array()));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 3
                echo "\t    ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 4
                    echo "\t    
\t        ";
                    // line 5
                    if (($context["type"] == "message")) {
                        echo "    
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t  \$(document).ready(function(){\t\t\t\t\t  
\t\t\t\t\t  jSuccess(
\t\t\t\t\t\t'";
                        // line 9
                        echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                        echo "',
\t\t\t\t\t\t{
\t\t\t\t\t\t  autoHide : true, // added in v2.0
\t\t\t\t\t\t  clickOverlay : true, // added in v2.0
\t\t\t\t\t\t  MinWidth : 250,
\t\t\t\t\t\t  TimeShown : 2000,
\t\t\t\t\t\t  ShowTimeEffect : 200,
\t\t\t\t\t\t  HideTimeEffect : 200,
\t\t\t\t\t\t  LongTrip :20,
\t\t\t\t\t\t  HorizontalPosition : 'center',
\t\t\t\t\t\t  VerticalPosition : 'top',
\t\t\t\t\t\t  ShowOverlay : true,
\t\t\t\t   \t\t  ColorOverlay : '#000',
\t\t\t\t\t\t  OpacityOverlay : 0.3,
\t\t\t\t\t\t  onClosed : function(){ // added in v2.0
\t\t\t\t\t\t   
\t\t\t\t\t\t  },
\t\t\t\t\t\t  onCompleted : function(){ // added in v2.0
\t\t\t\t\t\t   
\t\t\t\t\t\t  }
\t\t\t\t\t\t});
\t\t\t\t\t  });
\t\t\t\t\t  
\t\t\t\t\t  
\t\t\t\t</script>
\t\t\t ";
                    }
                    // line 35
                    echo "\t        
\t         ";
                    // line 36
                    if (($context["type"] == "error")) {
                        // line 37
                        echo "\t            
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t  \$(document).ready(function(){\t\t\t\t\t  
\t\t\t\t\t  jError(
\t\t\t\t\t\t'";
                        // line 41
                        echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                        echo "',
\t\t\t\t\t\t{
\t\t\t\t\t\t  autoHide : true, // added in v2.0
\t\t\t\t\t\t  clickOverlay : true, // added in v2.0
\t\t\t\t\t\t  MinWidth : 250,
\t\t\t\t\t\t  TimeShown : 2000,
\t\t\t\t\t\t  ShowTimeEffect : 200,
\t\t\t\t\t\t  HideTimeEffect : 200,
\t\t\t\t\t\t  LongTrip :20,
\t\t\t\t\t\t  HorizontalPosition : 'center',
\t\t\t\t\t\t  VerticalPosition : 'top',
\t\t\t\t\t\t  ShowOverlay : true,
\t\t\t\t   \t\t  ColorOverlay : '#000',
\t\t\t\t\t\t  OpacityOverlay : 0.3,
\t\t\t\t\t\t  onClosed : function(){ // added in v2.0
\t\t\t\t\t\t   
\t\t\t\t\t\t  },
\t\t\t\t\t\t  onCompleted : function(){ // added in v2.0
\t\t\t\t\t\t   
\t\t\t\t\t\t  }
\t\t\t\t\t\t});
\t\t\t\t\t  });
\t\t\t\t\t  
\t\t\t\t\t  
\t\t\t\t</script>
\t        ";
                    }
                    // line 67
                    echo "\t    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 68
                echo "\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
    }

    public function getTemplateName()
    {
        return "CoreBundle::messages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 68,  110 => 67,  81 => 41,  75 => 37,  73 => 36,  70 => 35,  41 => 9,  34 => 5,  31 => 4,  26 => 3,  21 => 2,  19 => 1,);
    }
}
