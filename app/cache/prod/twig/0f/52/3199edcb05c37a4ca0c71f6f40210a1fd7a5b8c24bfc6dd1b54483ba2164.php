<?php

/* AdminBundle::admin.html.twig */
class __TwigTemplate_0f523199edcb05c37a4ca0c71f6f40210a1fd7a5b8c24bfc6dd1b54483ba2164 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
  <meta charset=\"utf-8\">
  <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

  ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 13
        echo "  
  ";
        // line 14
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "\t
</head>

<body>
    <div id=\"header\">
\t    <div id=\"logo\">
\t        <img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.jpg"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getSiteName(), "html", null, true);
        echo "\">
\t    </div>
\t</div>
\t<div id=\"linklogout\">
\t\t";
        // line 30
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 31
            echo "\t\t<table>
\t\t\t<tr>
\t\t\t\t<td>
\t\t\t\t\t";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo " - <a href=\"";
            echo $this->env->getExtension('routing')->getPath("admin_login_logout");
            echo "\">Déconnexion</a><br/>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getSiteUrl(), "html", null, true);
            echo "\" target=\"_blank\">Visiter le site</a>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t";
            // line 40
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("AdminBundle:Home:infosheader"));
            echo "
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
\t\t";
        }
        // line 45
        echo "\t</div>

\t";
        // line 47
        echo twig_include($this->env, $context, "AdminBundle::menu.html.twig");
        echo "

\t<div id=\"content\">
        ";
        // line 50
        $this->displayBlock('body', $context, $blocks);
        // line 52
        echo "\t</div>

\t<div id=\"footer\">
\t
\t</div>
\t
\t";
        // line 58
        echo twig_include($this->env, $context, "AdminBundle::messages.html.twig");
        echo "
</body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "ADMIN - ";
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getSiteName(), "html", null, true);
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bo.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jqueryslidemenu.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jNotify.jquery.css"), "html", null, true);
        echo "\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/dropzone/dropzone.css"), "html", null, true);
        echo "\"/>
  ";
    }

    // line 14
    public function block_javascripts($context, array $blocks = array())
    {
        // line 15
        echo "\t<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
\t<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jqueryslidemenu.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jNotify.jquery.min.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/dropzone/dropzone.js"), "html", null, true);
        echo "\"></script>
  ";
    }

    // line 50
    public function block_body($context, array $blocks = array())
    {
        // line 51
        echo "        ";
    }

    public function getTemplateName()
    {
        return "AdminBundle::admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 51,  163 => 50,  157 => 18,  153 => 17,  149 => 16,  146 => 15,  143 => 14,  137 => 11,  133 => 10,  129 => 9,  124 => 8,  121 => 7,  114 => 5,  107 => 58,  99 => 52,  97 => 50,  91 => 47,  87 => 45,  79 => 40,  73 => 37,  65 => 34,  60 => 31,  58 => 30,  49 => 26,  41 => 20,  39 => 14,  36 => 13,  34 => 7,  29 => 5,  23 => 1,);
    }
}
