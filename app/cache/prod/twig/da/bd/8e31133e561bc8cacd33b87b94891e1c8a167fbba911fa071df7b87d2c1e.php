<?php

/* ContactBundle:Contact:index.html.twig */
class __TwigTemplate_dabd8e31133e561bc8cacd33b87b94891e1c8a167fbba911fa071df7b87d2c1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container-fluid title title_bottom\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<h1>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contactez-nous"), "html", null, true);
        echo "</h1>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t\t\t";
        // line 17
        echo $this->env->getExtension('http_kernel')->renderFragmentStrategy("esi", $this->env->getExtension('http_kernel')->controller("ContentBundle:Partial:zone", array("id" => 21)));
        echo "
\t\t</div>
\t</div>

\t<div class=\"row\">
\t\t<div class=\"col-lg-offset-1 col-lg-10\">\t\t
\t\t\t<div class=\"jumbotron\">

\t\t\t    <form method=\"post\" class=\"form-horizontal\">
\t\t\t\t    <div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 28
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Email *")) ? array() : array("label" => $_label_)));
        echo "
\t\t\t\t\t      <div class=\"col-lg-9\">
\t\t\t\t\t        ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
        echo "
\t\t\t\t\t        ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t  \t<div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Nom *")) ? array() : array("label" => $_label_)));
        echo "
\t\t\t\t\t      <div class=\"col-lg-9\">
\t\t\t\t\t        ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'errors');
        echo "
\t\t\t\t\t        ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Prénom *")) ? array() : array("label" => $_label_)));
        echo "
\t\t\t\t\t      <div class=\"col-lg-9\">
\t\t\t\t\t        ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'errors');
        echo "
\t\t\t\t\t        ";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Téléphone")) ? array() : array("label" => $_label_)));
        echo "
\t\t\t\t\t      <div class=\"col-lg-9\">
\t\t\t\t\t        ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'errors');
        echo "
\t\t\t\t\t        ";
        // line 58
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t      ";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "message", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Votre message *")) ? array() : array("label" => $_label_)));
        echo "
\t\t\t\t\t      <div class=\"col-lg-9\">
\t\t\t\t\t        ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "message", array()), 'errors');
        echo "
\t\t\t\t\t        ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "message", array()), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t\t\t\t      </div>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t    <div class=\"form-group\">
\t\t\t\t       <button class=\"pull-right btn btn-primary\"><span class=\"glyphicon glyphicon-send\"></span>&nbsp;&nbsp;";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Envoyer"), "html", null, true);
        echo "</button>
\t\t\t\t    </div>
\t\t\t\t    <div class=\"form-group\">
\t\t\t\t       <i>";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("* champs obligatoires"), "html", null, true);
        echo "</i>
\t\t\t\t    </div>
\t\t\t    </form>
  \t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "ContactBundle:Contact:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 75,  160 => 72,  152 => 67,  148 => 66,  143 => 64,  134 => 58,  130 => 57,  125 => 55,  116 => 49,  112 => 48,  107 => 46,  98 => 40,  94 => 39,  89 => 37,  80 => 31,  76 => 30,  71 => 28,  57 => 17,  45 => 8,  39 => 4,  36 => 3,  11 => 1,);
    }
}
