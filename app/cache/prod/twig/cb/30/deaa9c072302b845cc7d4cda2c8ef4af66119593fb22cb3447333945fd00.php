<?php

/* RegisterBundle:Register:cliente.html.twig */
class __TwigTemplate_cb30deaa9c072302b845cc7d4cda2c8ef4af66119593fb22cb3447333945fd00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid title title_bottom\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t<h1>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Inscription gratuite"), "html", null, true);
        echo "</h1>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"col-lg-9\">\t
\t\t\t";
        // line 19
        echo twig_include($this->env, $context, "RegisterBundle:Form:cliente.html.twig");
        echo "
\t\t</div>
\t\t<div class=\"col-lg-3\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t\t<p>
\t\t\t\t\t\t<br/><br/><br/><br/><br/><a href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("content_show", array("content_id" => 13, "t" => "rejoindre-notre-reseau-professionnel")), "html", null, true);
        echo "\" class=\"btn btn-primary btn-lg\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("S’inscrire comme professionnel"), "html", null, true);
        echo "</a>
\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

</div>

";
    }

    public function getTemplateName()
    {
        return "RegisterBundle:Register:cliente.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 25,  59 => 19,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
