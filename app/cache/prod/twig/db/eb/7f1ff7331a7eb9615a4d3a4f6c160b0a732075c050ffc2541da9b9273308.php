<?php

/* CoreBundle:Partial:prestation.html.twig */
class __TwigTemplate_dbeb7f1ff7331a7eb9615a4d3a4f6c160b0a732075c050ffc2541da9b9273308 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<select class=\"form-control\" id=\"search_prestation\" name=\"search_prestation\">
<option value=\"\">";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Type de prestation"), "html", null, true);
        echo "</option>
";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prestations"]) ? $context["prestations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["prestation"]) {
            // line 4
            echo "<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["prestation"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["prestation"], "title", array()), "html", null, true);
            echo "</option>
";
            // line 5
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["prestation"], "children", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_prestation"]) {
                // line 6
                echo "<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sub_prestation"], "id", array()), "html", null, true);
                echo "\">&nbsp;&nbsp;&nbsp;&nbsp;";
                echo twig_escape_filter($this->env, $this->getAttribute($context["sub_prestation"], "title", array()), "html", null, true);
                echo "</option>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_prestation'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prestation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "</select>";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Partial:prestation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 9,  41 => 6,  37 => 5,  30 => 4,  26 => 3,  22 => 2,  19 => 1,);
    }
}
