<?php

/* RegisterBundle:Register:password.html.twig */
class __TwigTemplate_f1d81bafd6cba4ecc88654c2ccefd78fe7d43a091e4851f3b5db5233dea4bf68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid title title_bottom\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<h1>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Réinitialisation du mot de passe"), "html", null, true);
        echo "</h1>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<form method=\"post\" action=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("register_sendpassword");
        echo "\" class=\"form col-lg-10\">
       <div class=\"form-group\">
\t      <legend>";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indiquez votre email"), "html", null, true);
        echo "</legend>
\t    </div>
\t  \t<div class=\"row\">
\t\t    <div class=\"form-group\">
\t\t      <label for=\"email\" class=\"col-lg-2\">";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
\t\t      <div class=\"col-lg-6\">
\t\t        <input required=\"required\" class=\"form-control\" type=\"email\" id=\"email\" name=\"email\" value=\"\" />
\t\t      </div>
\t\t      <div class=\"col-lg-2\">
\t\t       <button class=\"btn btn-primary\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Valider"), "html", null, true);
        echo "</button>
\t\t    </div>
\t\t    </div>
\t\t</div>
   </form>
\t<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
";
    }

    public function getTemplateName()
    {
        return "RegisterBundle:Register:password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 28,  69 => 23,  62 => 19,  57 => 17,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
