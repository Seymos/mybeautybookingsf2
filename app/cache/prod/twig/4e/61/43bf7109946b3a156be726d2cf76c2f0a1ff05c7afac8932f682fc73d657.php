<?php

/* CoreBundle::header.html.twig */
class __TwigTemplate_4e6143bf7109946b3a156be726d2cf76c2f0a1ff05c7afac8932f682fc73d657 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"visible-xs navbar navbar-default navbar-custom navbar-fixed-top\" role=\"navigation\">
  <div class=\"container-fluid\">
    <div class=\"navbar-header\">
\t\t<div class=\"col-xs-4\">
      \t\t<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
\t\t        <span class=\"sr-only\">Navigation</span>
\t\t        <span class=\"icon-bar\"></span>
\t\t        <span class=\"icon-bar\"></span>
\t\t        <span class=\"icon-bar\"></span>
\t\t      </button>
      \t</div>
    \t<div class=\"col-xs-8 text-right\">
      \t\t<a id=\"logo\" href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/logo.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getSiteName(), "html", null, true);
        echo "\"></a>
      \t</div>
      \t
    </div>
    <div id=\"navbar\" class=\"navbar-collapse collapse\">
      <ul class=\"nav navbar-nav\">
      \t";
        // line 19
        if ( !$this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 20
            echo "\t\t\t<li class=\"nav-item-menu\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("home_gosearch", array("search_update" => 1));
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prendre un rendez-vous"), "html", null, true);
            echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
            // line 21
            echo $this->env->getExtension('routing')->getPath("register_cliente");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Inscription"), "html", null, true);
            echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
            // line 22
            echo $this->env->getExtension('routing')->getPath("register_connexion");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Connexion"), "html", null, true);
            echo "</a></li>
\t\t";
        } else {
            // line 24
            echo "\t\t\t";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 2)) {
                // line 25
                echo "\t\t\t<li class=\"nav-item-menu\"><a href=\"";
                echo $this->env->getExtension('routing')->getPath("user_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
                echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
                // line 26
                echo $this->env->getExtension('routing')->getPath("booking_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
                echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
                // line 27
                echo $this->env->getExtension('routing')->getPath("message_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
                echo "</a></li>
\t\t\t";
            } else {
                // line 29
                echo "\t\t\t<li class=\"nav-item-menu\"><a href=\"";
                echo $this->env->getExtension('routing')->getPath("home_gosearch", array("search_update" => 1));
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prendre un rendez-vous"), "html", null, true);
                echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
                // line 30
                echo $this->env->getExtension('routing')->getPath("user_show");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
                echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
                // line 31
                echo $this->env->getExtension('routing')->getPath("booking_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
                echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
                // line 32
                echo $this->env->getExtension('routing')->getPath("message_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
                echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
                // line 33
                echo $this->env->getExtension('routing')->getPath("user_favoris");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes beauty artists"), "html", null, true);
                echo "</a></li>
\t\t\t";
            }
            // line 35
            echo "\t\t\t<li class=\"nav-item-menu\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_editpassword");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Modifier mon mot de passe"), "html", null, true);
            echo "</a></li>
\t\t\t<li class=\"nav-item-menu\"><a href=\"";
            // line 36
            echo $this->env->getExtension('routing')->getPath("register_logout");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Se déconnecter"), "html", null, true);
            echo "</a></li>
\t\t";
        }
        // line 38
        echo "      </ul>
    </div>
  </div>
</nav>

<audio id=\"audiotag1\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getSiteUrl(), "html", null, true);
        echo "mp3/menu.mp3\" preload=\"auto\"></audio>

<script type=\"text/javascript\">
\tfunction play_single_sound() {
\t\tdocument.getElementById('audiotag1').play();
\t}
</script>

<div class=\"hidden-xs container\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"header\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-2 col-md-2 col-lg-3\">
\t\t\t\t\t\t<a onclick=\"play_single_sound();\" id=\"menu_a\" class=\"btn btn-primary\" href=\"#menu\"><span class=\"glyphicon glyphicon-menu-hamburger\"></span> &nbsp;&nbsp;";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Découvrir"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-5 col-md-6 col-lg-6 text-center\">
\t\t\t\t\t\t<a id=\"logo\" href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/logo.png"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getSiteName(), "html", null, true);
        echo "\"></a>
\t\t\t\t\t\t<div class=\"logo_slogan\">";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Le home beauty service qui vous ressemble, vraiment"), "html", null, true);
        echo "</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-5 col-md-4 col-lg-3\" style=\"padding-left:0px;padding-right:0px;padding-top:15px;\">
\t\t\t\t\t\t";
        // line 64
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 65
            echo "\t\t\t\t\t\t<div class=\"col-zonemembre\">
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-sm-2 col-md-2 col-lg-3\">
\t\t\t\t\t\t\t\t\t<!-- Single button -->
\t\t\t\t\t\t\t\t\t<div class=\"btn-group dropdown\">
\t\t\t\t\t\t\t\t\t  <button id=\"zonemembre-dropdown\" type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down fa-2\"></i>
\t\t\t\t\t\t\t\t\t  </button>
\t\t\t\t\t\t\t\t\t  <ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t\t\t\t";
            // line 74
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 2)) {
                // line 75
                echo "\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("user_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                // line 76
                echo $this->env->getExtension('routing')->getPath("booking_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                // line 77
                echo $this->env->getExtension('routing')->getPath("message_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 79
                echo "\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("user_show");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                // line 80
                echo $this->env->getExtension('routing')->getPath("message_index");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
                echo "</a></li>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 82
            echo "\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("register_logout");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Se déconnecter"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t\t\t\t\t  </ul>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"col-sm-10 col-md-10 col-lg-9\">
\t\t\t\t\t\t\t\t\t<p class=\"zonemembre-name\">
\t\t\t\t\t\t\t\t\t\t<b class=\"zonemembre-name-1\">";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "lastName", array()), "html", null, true);
            echo "</b> ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "score", array()) < 100)) {
                echo "<b class=\"pull-right\" style=\"font-size:13px;\"><span class=\"glyphicon glyphicon-user\"></span>&nbsp;";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "score", array()), "html", null, true);
                echo "</b>";
            }
            // line 89
            echo "\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"zonemembre-infos\">
\t\t\t\t\t\t\t\t\t\t";
            // line 91
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("MessageBundle:Partial:usermessages"));
            echo "
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t<p class=\"zonemembre-infos\">
\t\t\t\t\t\t\t\t\t\t";
            // line 94
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1)) {
                // line 95
                echo "\t\t\t\t\t\t\t\t\t\t";
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BookingBundle:Partial:bookingcliente"));
                echo "
\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 97
                echo "\t\t\t\t\t\t\t\t\t\t";
                echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("BookingBundle:Partial:bookingpro"));
                echo "
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 99
            echo "\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        } else {
            // line 104
            echo "\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-lg-12\" style=\"padding-left:0px;padding-right:0px;\">
\t\t\t\t\t\t\t\t<ul class=\"menu-connexion pull-right\">
\t\t\t\t\t\t\t\t\t<li style=\"margin-left:10px;\"><a class=\"btn btn-primary\" href=\"";
            // line 107
            echo $this->env->getExtension('routing')->getPath("register_cliente");
            echo "\"><span class=\"glyphicon glyphicon-log-in\"></span> &nbsp;&nbsp;";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("S'inscrire"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t\t\t\t\t<li style=\"margin-left:10px;\"><a class=\"btn btn-primary\" style=\"cursor:pointer;\" data-toggle=\"modal\" data-target=\"#modal_login\"><span class=\"glyphicon glyphicon-user\"></span> &nbsp;&nbsp;";
            // line 108
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Se connecter"), "html", null, true);
            echo "</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        }
        // line 113
        echo "\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<!-- Modal -->
<div class=\"modal fade clearfix\" id=\"modal_login\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modal_login_modalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>
        <h4 class=\"modal-title\" id=\"modal_login_modalLabel\">";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Connexion à mon compte"), "html", null, true);
        echo "</h4>
      </div>
      <form action=\"";
        // line 129
        echo $this->env->getExtension('routing')->getPath("register_login");
        echo "\" method=\"post\" class=\"form-horizontal col-xs-offset-1 col-xs-10 col-lg-10\">
      <div class=\"modal-body\">
\t  
\t\t\t<div class=\"row text-center\" style=\"margin-top:30px;\">
\t\t    \t
\t\t    \t<div id=\"loginbuttonfb\">
\t\t\t    \t<fb:login-button scope=\"public_profile,email\" onlogin=\"checkLoginState();\"></fb:login-button>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"status\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<hr/>

\t\t\t<div class=\"row\" style=\"margin-top:50px;\">
\t\t    \t<div class=\"col-lg-offset-1 col-lg-9\">
\t\t\t\t    <div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group input-group-md\">
\t\t\t\t\t\t\t  <span class=\"input-group-addon\">
\t\t\t\t\t\t\t    <i class=\"fa fa-envelope\"></i>
\t\t\t\t\t\t\t  </span>
\t\t\t\t\t\t\t  <input required=\"required\" class=\"form-control\" type=\"email\" id=\"email\" name=\"email\" value=\"\" placeholder=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Votre Email"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group input-group-md\">
\t\t\t\t\t\t\t  <span class=\"input-group-addon\">
\t\t\t\t\t\t\t    <i class=\"fa fa-lock\"></i>
\t\t\t\t\t\t\t  </span>
\t\t\t\t\t\t\t  <input required=\"required\" class=\"form-control\" type=\"password\" id=\"password\" name=\"password\" placeholder=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mot de passe"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div> 
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group input-group-md\">
\t\t\t\t\t    \t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" checked=checked/> ";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Se souvenir de moi"), "html", null, true);
        echo "
\t\t\t\t\t    \t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t    <div class=\"form-group\">
\t\t\t\t       <button class=\"pull-right btn btn-primary\">";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Connexion"), "html", null, true);
        echo "</button>
\t\t\t\t    </div>
\t\t\t\t</div>
\t\t\t</div>
\t\t
\t    \t<hr/>
\t    </div>
\t    </form>
\t    <div class=\"modal-footer\">
\t    \t<div class=\"row\">
\t    \t\t<div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6 text-left\">
\t    \t\t\t<a href=\"";
        // line 185
        echo $this->env->getExtension('routing')->getPath("register_cliente");
        echo "\"><b>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pas encore inscrit ?"), "html", null, true);
        echo "</b></a>
\t    \t\t</div>
\t    \t\t<div class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\">
\t    \t\t\t<a href=\"";
        // line 188
        echo $this->env->getExtension('routing')->getPath("register_password");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mot de passe oublié ?"), "html", null, true);
        echo "</a>
\t    \t\t</div>
\t    \t</div>
\t    </div>
\t </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "CoreBundle::header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  400 => 188,  392 => 185,  378 => 174,  370 => 169,  360 => 162,  347 => 152,  321 => 129,  316 => 127,  300 => 113,  292 => 108,  286 => 107,  281 => 104,  274 => 99,  268 => 97,  262 => 95,  260 => 94,  254 => 91,  250 => 89,  242 => 88,  230 => 82,  223 => 80,  216 => 79,  209 => 77,  203 => 76,  196 => 75,  194 => 74,  183 => 65,  181 => 64,  175 => 61,  167 => 60,  161 => 57,  144 => 43,  137 => 38,  130 => 36,  123 => 35,  116 => 33,  110 => 32,  104 => 31,  98 => 30,  91 => 29,  84 => 27,  78 => 26,  71 => 25,  68 => 24,  61 => 22,  55 => 21,  48 => 20,  46 => 19,  33 => 13,  19 => 1,);
    }
}
