<?php

/* BookingBundle:Booking:pay.html.twig */
class __TwigTemplate_18270bac410bc582890b4ef86ceab2ba47a13b1e37b5a881bf4fe9a6a9b4bcdb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"POST\" action=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : null), "CardRegistrationURL", array()), "html", null, true);
        echo "\">
\t<input type=\"hidden\" name=\"ID\" value=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : null), "Id", array()), "html", null, true);
        echo "\" />
\t<input type=\"hidden\" name=\"data\" value=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : null), "PreregistrationData", array()), "html", null, true);
        echo "\" />
\t<input type=\"hidden\" name=\"accessKeyRef\" value=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["result"]) ? $context["result"] : null), "AccessKey", array()), "html", null, true);
        echo "\" />
\t<input type=\"hidden\" name=\"returnURL\" value=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["returnURL"]) ? $context["returnURL"] : null), "html", null, true);
        echo "\" />
\t<input type=\"hidden\" name=\"price\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["price_ttc"]) ? $context["price_ttc"] : null), "html", null, true);
        echo "\" />
\t<input type=\"hidden\" name=\"currency\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["currency"]) ? $context["currency"] : null), "html", null, true);
        echo "\" />
\t<input type=\"hidden\" name=\"cardExpirationDate\" id=\"cardExpirationDate\" value=\"\" />
\t
\t<div class=\"message_discussion_bloc\">
\t\t<table class=\"table table-condensed\">
\t\t<tbody>
\t\t\t<tr>
\t\t\t\t<td colspan=\"2\" class=\"text-center\">
\t\t\t\t\t<h4>";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Votre paiement de : "), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["price_ttc"]) ? $context["price_ttc"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["currency"]) ? $context["currency"] : null), "html", null, true);
        echo "</h4>
\t\t\t\t</td>
\t\t\t</tr>
\t\t\t<tr>
\t\t\t\t<th>";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Numéro de carte"), "html", null, true);
        echo "</th>
\t\t\t\t<td><input class=\"form-control\" type=\"text\" name=\"cardNumber\" value=\"\" /></td>
\t\t\t</tr>
\t\t\t<tr>
\t\t\t\t<th>";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date d'expiration"), "html", null, true);
        echo "</th>
\t\t\t\t<td>
\t\t\t\t\t<select style=\"width:100px;display:inline;\" class=\"form-control\" id=\"expir_month\" onchange=\"expire()\">
\t\t\t\t\t\t<option value=\"01\">01</option>
\t\t\t\t\t\t<option value=\"02\">02</option>
\t\t\t\t\t\t<option value=\"03\">03</option>
\t\t\t\t\t\t<option value=\"04\">04</option>
\t\t\t\t\t\t<option value=\"05\">05</option>
\t\t\t\t\t\t<option value=\"06\">06</option>
\t\t\t\t\t\t<option value=\"07\">07</option>
\t\t\t\t\t\t<option value=\"08\">08</option>
\t\t\t\t\t\t<option value=\"09\">09</option>
\t\t\t\t\t\t<option value=\"10\">10</option>
\t\t\t\t\t\t<option value=\"11\">11</option>
\t\t\t\t\t\t<option value=\"12\">12</option>
\t\t\t\t\t</select>
\t\t\t\t\t<span class=\"display:inline;\">/</span>
\t\t\t\t\t<select style=\"width:100px;display:inline;\" class=\"form-control\" id=\"expir_year\" onchange=\"expire()\">
\t\t\t\t\t\t<option value=";
        // line 41
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "y"), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "y"), "html", null, true);
        echo "</option>
\t\t\t\t\t\t<option value=";
        // line 42
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 1), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 1), "html", null, true);
        echo "</option>
\t\t\t\t\t\t<option value=";
        // line 43
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 2), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 2), "html", null, true);
        echo "</option>
\t\t\t\t\t\t<option value=";
        // line 44
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 3), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 3), "html", null, true);
        echo "</option>
\t\t\t\t\t\t<option value=";
        // line 45
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 4), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 4), "html", null, true);
        echo "</option>
\t\t\t\t\t\t<option value=";
        // line 46
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 5), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 5), "html", null, true);
        echo "</option>
\t\t\t\t\t\t<option value=";
        // line 47
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 6), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 6), "html", null, true);
        echo "</option>
\t\t\t\t\t\t<option value=";
        // line 48
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 7), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 7), "html", null, true);
        echo "</option>
\t\t\t\t\t\t<option value=";
        // line 49
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 8), "html", null, true);
        echo ">";
        echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, "now", "y") + 8), "html", null, true);
        echo "</option>
\t\t\t\t\t</select>
\t\t\t\t</td>
\t\t\t</tr>
\t\t\t<tr>
\t\t\t\t<th>";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pictogramme visuel"), "html", null, true);
        echo "</th>
\t\t\t\t<td><input class=\"form-control\" type=\"text\" name=\"cardCvx\" value=\"\" /></td>
\t\t\t</tr>
\t\t\t<tr>
\t\t\t\t<th style=\"width:200px;\"></th>
\t\t\t\t<td>
\t\t\t\t\t<input id=\"btnsubmit\" class=\"btn btn-danger\" type=\"submit\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Valider mon paiment"), "html", null, true);
        echo "\" onclick=\"wait()\"/><img id=\"loading\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
        echo "\" style=\"display:none;\">
\t\t\t\t</td>
\t\t\t</tr>
\t\t</tbody>
\t</table>
\t
\t</div>
</form>
\t\t\t

<script>
function expire() {
\tvar month = document.getElementById(\"expir_month\").value;
\tvar year = document.getElementById(\"expir_year\").value;
\tvar expire_date = month + year;
\tdocument.getElementById(\"cardExpirationDate\").value = expire_date;
}
function wait() {
\tvar btnsubmit = document.getElementById(\"btnsubmit\").style.display = \"none\";
\tvar wait = document.getElementById(\"loading\").style.display = \"inline\";
}
</script>
";
    }

    public function getTemplateName()
    {
        return "BookingBundle:Booking:pay.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 60,  151 => 54,  141 => 49,  135 => 48,  129 => 47,  123 => 46,  117 => 45,  111 => 44,  105 => 43,  99 => 42,  93 => 41,  72 => 23,  65 => 19,  55 => 15,  44 => 7,  40 => 6,  36 => 5,  32 => 4,  28 => 3,  24 => 2,  19 => 1,);
    }
}
