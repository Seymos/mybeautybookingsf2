<?php

/* BookingBundle:Booking:show.html.twig */
class __TwigTemplate_b677d223f4f907fb9a922b200964e70624aca96c55c01698d577521273189c3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t";
        // line 9
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 2)) {
            // line 10
            echo "\t\t\t\t<a href=\"";
            echo $this->env->getExtension('routing')->getPath("home_index");
            echo "\">";
            echo "Accueil";
            echo "</a> > <a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_index");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
            echo "</a> > ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
            echo "
\t\t\t\t";
        } else {
            // line 12
            echo "\t\t\t\t<a href=\"";
            echo $this->env->getExtension('routing')->getPath("home_index");
            echo "\">";
            echo "Accueil";
            echo "</a> > <a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_show");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
            echo "</a> > ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
            echo "
\t\t\t\t";
        }
        // line 14
        echo "\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"hidden-xs col-sm-3 col-md-3 col-lg-3\">
\t\t
\t\t\t";
        // line 24
        echo twig_include($this->env, $context, "UserBundle:Partial:menu.html.twig");
        echo "
\t\t\t
\t\t</div>
\t\t<div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9\">
\t\t\t
\t\t\t<div class=\"panel panel-default\">
\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t<h3 class=\"panel-title\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon booking"), "html", null, true);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t
\t\t\t\t\t<h3 style=\"margin-top:0px;\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statutLabel", array()), "html", null, true);
        echo "</h3>
\t\t\t\t\t
\t\t\t\t\t";
        // line 37
        if (($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) == 1)) {
            // line 38
            echo "\t\t\t\t\t\t";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1)) {
                // line 39
                echo "\t\t\t\t\t\t\t<p><b>Vous devez effectuer le paiement afin de confirmer la réservation</b></p>
\t\t\t\t\t\t";
            } else {
                // line 41
                echo "\t\t\t\t\t\t\t<p><b>La cliente doit effectuer le paiement afin de confirmer la réservation</b></p>
\t\t\t\t\t\t";
            }
            // line 43
            echo "\t\t\t\t\t";
        }
        // line 44
        echo "\t\t\t\t\t";
        if (($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) == 2)) {
            // line 45
            echo "\t\t\t\t\t\t";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1)) {
                // line 46
                echo "\t\t\t\t\t\t\t<p><b>Le paiement est effectué, la réservation doit maintenant être validée par votre Beauty Artist</b></p>
\t\t\t\t\t\t";
            } else {
                // line 48
                echo "\t\t\t\t\t\t\t<p><b>Le paiement est effectué, vous devez maintenant valider la réservation</b></p>
\t\t\t\t\t\t";
            }
            // line 50
            echo "\t\t\t\t\t";
        }
        // line 51
        echo "\t\t\t\t\t";
        if (($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) == 3)) {
            // line 52
            echo "\t\t\t\t\t\t<p><b>La prestation sera effectuée le ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "dateRdv", array(0 => "d/m/Y"), "method"), "html", null, true);
            echo " à ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "heureRdvLabel", array()), "html", null, true);
            echo "</b></p>
\t\t\t\t\t";
        }
        // line 54
        echo "\t\t\t\t\t";
        if (($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) == 4)) {
            // line 55
            echo "\t\t\t\t\t\t";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1)) {
                // line 56
                echo "\t\t\t\t\t\t\t<p><b>La prestation est effectuée, vous devez laisser une note et avis</b></p>
\t\t\t\t\t\t";
            } else {
                // line 58
                echo "\t\t\t\t\t\t\t<p><b>La prestation est effectuée, la cliente doit laisser une note et avis</b></p>
\t\t\t\t\t\t";
            }
            // line 60
            echo "\t\t\t\t\t";
        }
        // line 61
        echo "\t\t\t\t\t\t\t
\t\t\t\t\t<table class=\"table table-condensed table_white\">
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th style=\"width:20%;\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prestation"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "offer", array()), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "dateRdv", array(0 => "d/m/Y"), "method"), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heure"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "heureRdvLabel", array()), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Description"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 78
        echo $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "offer", array()), "description", array());
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Durée"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "offer", array()), "dureeLabel", array()), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Photos"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<ul class=\"liste-photo\">
\t\t\t\t\t\t\t\t\t";
        // line 88
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "offer", array()), "images", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
            // line 89
            echo "\t\t\t\t\t\t\t\t\t\t<li><a rel=\"facebox\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "urlzoom", array()), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "url", array()), "html", null, true);
            echo "\"/></a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 91
        echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prix"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "priceLabel", array()), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
        // line 98
        if ($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "code", array())) {
            // line 99
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Code de réduction"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t\t<td>";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "code", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
            // line 104
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Réduction"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t\t<td>- ";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "codeReducLabel", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
        }
        // line 108
        echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Frais de service"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "commissionLabel", array()), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TVA"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "tvaLabel", array()), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total"), "html", null, true);
        echo "</th>
\t\t\t\t\t\t\t\t<td>";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "priceTtcLabel", array()), "html", null, true);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td colspan=\"2\" class=\"text-center\">
\t\t\t\t\t\t\t\t\t";
        // line 124
        if ((($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) == 1) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1))) {
            // line 125
            echo "\t\t\t\t\t\t\t\t\t\t<a  class=\"btn btn-warning btn-lg\" href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Payer cette réservation"), "html", null, true);
            echo "\" data-title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Payer cette réservation"), "html", null, true);
            echo "\" data-whatever=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("booking_payer", array("booking_id" => $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "id", array()))), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-hand-up\"></span>&nbsp;&nbsp;";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Payer cette réservation"), "html", null, true);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 127
        echo "\t\t\t\t\t\t\t\t\t";
        if ((($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) == 2) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 2))) {
            // line 128
            echo "\t\t\t\t\t\t\t\t\t\t<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("booking_accept", array("booking_id" => $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning btn-lg\"><span class=\"glyphicon glyphicon-thumbs-up\"></span>&nbsp;&nbsp;";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Confirmer cette réservation"), "html", null, true);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 130
        echo "\t\t\t\t\t\t\t\t\t";
        if ((($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) == 2) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 2))) {
            // line 131
            echo "\t\t\t\t\t\t\t\t\t\t&nbsp;&nbsp;<a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("booking_refuse", array("booking_id" => $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-warning btn-lg\"><span class=\"glyphicon glyphicon-thumbs-down\"></span>&nbsp;&nbsp;";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Annuler cette réservation"), "html", null, true);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 133
        echo "\t\t\t\t\t\t\t\t\t";
        if ((($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) == 4) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1))) {
            // line 134
            echo "\t\t\t\t\t\t\t\t\t\t<a  class=\"btn btn-warning btn-lg\" href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ajouter une évaluation"), "html", null, true);
            echo "\" data-title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ajouter une évaluation"), "html", null, true);
            echo "\" data-whatever=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("booking_note", array("booking_id" => $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "id", array()))), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-thumbs-up\"></span>&nbsp;&nbsp;";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ajouter une évaluation"), "html", null, true);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 136
        echo "\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t\t";
        // line 141
        if ((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1) && $this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "seller", array()))) {
            // line 142
            echo "\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t<h3 class=\"panel-title\">";
            // line 143
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Beauty Artist"), "html", null, true);
            echo "</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">

\t\t\t\t\t<table class=\"table table-condensed table_white\">
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t  <th>";
            // line 150
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Photo"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t  <td colspan=\"3\">
\t\t\t\t\t\t\t\t";
            // line 152
            if ( !(null === $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "seller", array()), "image", array()))) {
                // line 153
                echo "\t\t\t\t\t\t\t\t\t<a rel=\"facebox\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "seller", array()), "image", array()), "urlZoom", array()), "html", null, true);
                echo "\"><img class=\"img-thumbnail\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "seller", array()), "image", array()), "url", array()), "html", null, true);
                echo "\"/></a>
\t\t\t\t\t\t\t\t";
            }
            // line 155
            echo "\t\t\t\t\t\t\t  </td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th style=\"width:20%;\">";
            // line 158
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Beauty Artist"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t";
            // line 160
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "seller", array()), "shortUsername", array()), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
            // line 163
            if (($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) >= 2)) {
                // line 164
                echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th style=\"width:20%;\">";
                // line 165
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Portable"), "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t\t<td>";
                // line 166
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "seller", array()), "phoneLabel", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
            }
            // line 169
            echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t<tfoot>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th colspan=\"2\" class=\"text-center\">
\t\t\t\t\t\t\t\t\t";
            // line 173
            if (($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) > 1)) {
                // line 174
                echo "\t\t\t\t\t\t\t\t\t\t <a class=\"btn btn-primary\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("message_newdiscussion", array("destinataire_id" => $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "seller", array()), "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-send\"></span> &nbsp;&nbsp;";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Envoyer un message à"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "seller", array()), "shortUsername", array()), "html", null, true);
                echo "</a>
\t\t\t\t\t\t\t\t\t";
            }
            // line 176
            echo "\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</tfoot>
\t\t\t\t\t</table>
\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
        } elseif ($this->getAttribute(        // line 183
(isset($context["booking"]) ? $context["booking"] : null), "user", array())) {
            // line 184
            echo "\t\t\t\t
\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t<h3 class=\"panel-title\">";
            // line 186
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("La cliente"), "html", null, true);
            echo "</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t
\t\t\t\t\t<table class=\"table table-condensed table_white\">
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t  <th>";
            // line 193
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Photo"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t  <td colspan=\"3\">
\t\t\t\t\t\t\t\t";
            // line 195
            if ( !(null === $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "image", array()))) {
                // line 196
                echo "\t\t\t\t\t\t\t\t\t<a rel=\"facebox\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "image", array()), "urlZoom", array()), "html", null, true);
                echo "\"><img class=\"img-thumbnail\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "image", array()), "url", array()), "html", null, true);
                echo "\"/></a>
\t\t\t\t\t\t\t\t";
            }
            // line 198
            echo "\t\t\t\t\t\t\t  </td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th style=\"width:20%;\">";
            // line 201
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prénom"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t\t<td colspan=\"3\">
\t\t\t\t\t\t\t\t\t";
            // line 203
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "shortUsername", array()), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
            // line 206
            if (($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) >= 2)) {
                // line 207
                echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t  <th>";
                // line 208
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Adresse"), "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t  <td>";
                // line 209
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "address1", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t  <th style=\"width:20%;\">";
                // line 210
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Complément"), "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t  <td>";
                // line 211
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "address2", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t  <th>";
                // line 214
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Code postal"), "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t  <td>";
                // line 215
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "postalCode", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t  <th>";
                // line 216
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ville"), "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t  <td>";
                // line 217
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "city", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t  <th>";
                // line 220
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fixe"), "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t  <td>";
                // line 221
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "phone", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t  <th>";
                // line 222
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Portable"), "html", null, true);
                echo "</th>
\t\t\t\t\t\t\t  <td>";
                // line 223
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "mobile", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
            }
            // line 226
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t  <th>";
            // line 227
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cheveux"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t   <td colspan=\"3\">";
            // line 228
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "cheveuxLabel", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t  <th>";
            // line 231
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Peau"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t   <td colspan=\"3\">
\t\t\t\t\t\t\t\t    <ul>
\t\t\t\t\t\t\t\t\t";
            // line 234
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "peau", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["competence"]) {
                // line 235
                echo "\t\t\t\t\t\t\t\t\t\t<li style=\"margin-right:10px;display:inline;\">
\t\t\t\t\t\t\t\t\t\t";
                // line 236
                if ($this->getAttribute($context["competence"], "couleurPeau", array())) {
                    // line 237
                    echo "\t\t\t\t\t\t\t\t\t\t<div class=\"img-circle img-thumbnail\" style=\"width:50px;height:50px;background-color:#";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "couleurPeau", array()), "html", null, true);
                    echo "\"></div>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 239
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "title", array()), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 241
                echo "\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competence'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 243
            echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t   </td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t  <th>";
            // line 247
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Preferences"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t   <td colspan=\"3\">";
            // line 248
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "preferencesLabel", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th>";
            // line 251
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Autres photos"), "html", null, true);
            echo "</th>
\t\t\t\t\t\t\t\t<td colspan=\"3\">
\t\t\t\t\t\t\t\t\t<ul class=\"liste-photo\">
\t\t\t\t\t\t\t\t\t";
            // line 254
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 255
                echo "\t\t\t\t\t\t\t\t\t\t<li><a rel=\"facebox\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "urlZoom", array()), "html", null, true);
                echo "\"><img class=\"img-thumbnail\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "url", array()), "html", null, true);
                echo "\"/></a></li>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 257
            echo "\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t<tfoot>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th colspan=\"4\" class=\"text-center\">
\t\t\t\t\t\t\t\t\t";
            // line 264
            if (($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "statut", array()) > 1)) {
                // line 265
                echo "\t\t\t\t\t\t\t\t\t\t <a class=\"btn btn-primary\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("message_newdiscussion", array("destinataire_id" => $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "id", array()))), "html", null, true);
                echo "\"><span class=\"glyphicon glyphicon-send\"></span> &nbsp;&nbsp;";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Envoyer un message à"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["booking"]) ? $context["booking"] : null), "user", array()), "shortUsername", array()), "html", null, true);
                echo "</a>
\t\t\t\t\t\t\t\t\t";
            }
            // line 267
            echo "\t\t\t\t\t\t\t\t</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</tfoot>
\t\t\t\t\t</table>
\t\t\t\t
\t\t\t\t</div>
\t\t\t\t";
        }
        // line 274
        echo "
\t\t\t\t\t
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t<a style=\"margin:30px;\" class=\"btn btn-primary\" href=\"";
        // line 278
        echo $this->env->getExtension('routing')->getPath("booking_index");
        echo "\"><span>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Retour"), "html", null, true);
        echo "</span></a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t

\t\t\t</div>
\t\t    
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "BookingBundle:Booking:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  696 => 278,  690 => 274,  681 => 267,  671 => 265,  669 => 264,  660 => 257,  649 => 255,  645 => 254,  639 => 251,  633 => 248,  629 => 247,  623 => 243,  616 => 241,  610 => 239,  604 => 237,  602 => 236,  599 => 235,  595 => 234,  589 => 231,  583 => 228,  579 => 227,  576 => 226,  570 => 223,  566 => 222,  562 => 221,  558 => 220,  552 => 217,  548 => 216,  544 => 215,  540 => 214,  534 => 211,  530 => 210,  526 => 209,  522 => 208,  519 => 207,  517 => 206,  511 => 203,  506 => 201,  501 => 198,  493 => 196,  491 => 195,  486 => 193,  476 => 186,  472 => 184,  470 => 183,  461 => 176,  451 => 174,  449 => 173,  443 => 169,  437 => 166,  433 => 165,  430 => 164,  428 => 163,  422 => 160,  417 => 158,  412 => 155,  404 => 153,  402 => 152,  397 => 150,  387 => 143,  384 => 142,  382 => 141,  375 => 136,  363 => 134,  360 => 133,  352 => 131,  349 => 130,  341 => 128,  338 => 127,  326 => 125,  324 => 124,  315 => 118,  311 => 117,  305 => 114,  301 => 113,  295 => 110,  291 => 109,  288 => 108,  282 => 105,  278 => 104,  272 => 101,  268 => 100,  265 => 99,  263 => 98,  258 => 96,  254 => 95,  248 => 91,  237 => 89,  233 => 88,  227 => 85,  221 => 82,  217 => 81,  211 => 78,  207 => 77,  201 => 74,  197 => 73,  191 => 70,  187 => 69,  181 => 66,  177 => 65,  171 => 61,  168 => 60,  164 => 58,  160 => 56,  157 => 55,  154 => 54,  146 => 52,  143 => 51,  140 => 50,  136 => 48,  132 => 46,  129 => 45,  126 => 44,  123 => 43,  119 => 41,  115 => 39,  112 => 38,  110 => 37,  105 => 35,  98 => 31,  88 => 24,  76 => 14,  62 => 12,  48 => 10,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
