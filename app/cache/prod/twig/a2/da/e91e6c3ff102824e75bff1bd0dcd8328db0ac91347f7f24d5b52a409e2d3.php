<?php

/* RegisterBundle:Register:sendpassword.html.twig */
class __TwigTemplate_a2dae91e6c3ff102824e75bff1bd0dcd8328db0ac91347f7f24d5b52a409e2d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid title title_bottom\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<h1>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Réinitialisation du mot de passe"), "html", null, true);
        echo "</h1>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t";
        // line 17
        if ((isset($context["result"]) ? $context["result"] : null)) {
            // line 18
            echo "    
    <p>
    \t";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Vous allez recevoir un email afin de renouveler votre mot de passe"), "html", null, true);
            echo ".
    </p>
    
    ";
        } else {
            // line 24
            echo "    
    <p>
    \t";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cette adresse email est invalide ou n'existe pas dans notre base de données"), "html", null, true);
            echo ".
    </p>
    <p>
    \t<a href=\"";
            // line 29
            echo $this->env->getExtension('routing')->getPath("register_password");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Recommencer"), "html", null, true);
            echo "</a>
    </p>
    
    ";
        }
        // line 33
        echo "\t<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
</div>
";
    }

    public function getTemplateName()
    {
        return "RegisterBundle:Register:sendpassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 33,  80 => 29,  74 => 26,  70 => 24,  63 => 20,  59 => 18,  57 => 17,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
