<?php

/* RegisterBundle:Register:connexion.html.twig */
class __TwigTemplate_168ec006b802372151ee560ff1799f67e6c32d2a14fd3135862ec1cf2b71bee9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid title title_bottom\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<h1>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Connexion à votre compte"), "html", null, true);
        echo "</h1>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t
\t\t<div class=\"col-lg-6\">
\t\t\t
\t\t  <form action=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("register_login");
        echo "\" method=\"post\" class=\"form-horizontal col-xs-offset-1 col-xs-10 col-lg-10\">
\t\t\t
\t\t\t<div class=\"row\">
\t\t    \t<div class=\"col-lg-offset-1 col-lg-9\">
\t\t\t\t    <div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group input-group-md\">
\t\t\t\t\t\t\t  <span class=\"input-group-addon\">
\t\t\t\t\t\t\t    <i class=\"fa fa-envelope\"></i>
\t\t\t\t\t\t\t  </span>
\t\t\t\t\t\t\t  <input required=\"required\" class=\"form-control\" type=\"email\" id=\"email\" name=\"email\" value=\"\" placeholder=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group input-group-md\">
\t\t\t\t\t\t\t  <span class=\"input-group-addon\">
\t\t\t\t\t\t\t    <i class=\"fa fa-lock\"></i>
\t\t\t\t\t\t\t  </span>
\t\t\t\t\t\t\t  <input required=\"required\" class=\"form-control\" type=\"password\" id=\"password\" name=\"password\" placeholder=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mot de passe"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t</div> 
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t    <div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group input-group-md\">
\t\t\t\t\t    \t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" checked=checked/> ";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("se souvenir de moi"), "html", null, true);
        echo "
\t\t\t\t\t    \t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t    <div class=\"form-group\">
\t\t\t\t       <button class=\"pull-right btn btn-primary\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Connexion"), "html", null, true);
        echo "</button>
\t\t\t\t       
\t\t\t\t       <a href=\"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("register_password");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mot de passe oublié ?"), "html", null, true);
        echo "</a>
\t\t\t\t    </div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t  </form>
\t
\t\t  
\t\t</div>
\t\t
\t</div>

</div>
";
    }

    // line 70
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 71
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link href=\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">
";
    }

    public function getTemplateName()
    {
        return "RegisterBundle:Register:connexion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 71,  131 => 70,  111 => 55,  106 => 53,  98 => 48,  88 => 41,  75 => 31,  62 => 21,  47 => 9,  40 => 4,  37 => 3,  11 => 1,);
    }
}
