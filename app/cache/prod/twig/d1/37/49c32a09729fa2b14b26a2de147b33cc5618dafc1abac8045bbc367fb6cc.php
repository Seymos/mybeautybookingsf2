<?php

/* UserBundle:User:show.html.twig */
class __TwigTemplate_d13749c32a09729fa2b14b26a2de147b33cc5618dafc1abac8045bbc367fb6cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body_content($context, array $blocks = array())
    {
        // line 5
        echo "
<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
        echo "</a> > ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"visible-xs visible-sm\">
\t\t\t\t";
        // line 21
        echo twig_include($this->env, $context, "UserBundle:Partial:menu.html.twig");
        echo "
\t\t\t</div>
\t\t\t<div id=\"profil_cliente_bk\" class=\"visible-md visible-lg\">
\t\t\t
\t\t\t\t<div id=\"profil_cliente_photo\">
\t\t\t\t\t<img class=\"img-circle\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "image", array()), "url", array()), "html", null, true);
        echo "\"/>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_mesphotos\">
\t\t\t\t\t<div id=\"menu_left\">
\t\t\t\t\t<a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("user_photos");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkphotos", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes photos"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_prenom\">
\t\t\t\t\t";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lastName", array()), "html", null, true);
        echo "
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_mesinfos\">
\t\t\t\t\t<div id=\"menu_left\">
\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes infos"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showinfos");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkmesinfos", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes infos"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_mescheveux\">
\t\t\t\t\t<div id=\"menu_left\">
\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes cheveux"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showcheveux");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkcheveux", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes cheveux"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_mapeau\">
\t\t\t\t\t<div id=\"menu_left\">
\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ma peau"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showpeau");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkpeau", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ma peau"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_mespreferences\">
\t\t\t\t\t<div id=\"menu_left\">
\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes préférences"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showpreferences");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkpreferences", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes préférences"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_favoris\">
\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes beauty artists"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showfavoris");
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes beauty artists"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_notes\">
\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes notes & avis"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showavis");
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes notes & avis"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_reservations\">
\t\t\t\t    <a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("booking_popup");
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_cliente_messages\">
\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("message_popup");
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "UserBundle:User:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 76,  181 => 72,  170 => 68,  159 => 64,  145 => 59,  130 => 53,  115 => 47,  100 => 41,  92 => 36,  80 => 31,  72 => 26,  64 => 21,  46 => 10,  39 => 5,  36 => 4,  11 => 1,);
    }
}
