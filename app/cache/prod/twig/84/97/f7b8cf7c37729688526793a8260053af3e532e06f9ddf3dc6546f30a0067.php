<?php

/* RegisterBundle:Mail:cliente.html.twig */
class __TwigTemplate_8497f7b8cf7c37729688526793a8260053af3e532e06f9ddf3dc6546f30a0067 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />

</head>

<body topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" bottommargin=\"0\" marginwidth=\"0\" marginheight=\"0\">
    
<div id=\"main\">
    
    <div id=\"texte\">
    <p>
    \t<img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/logo.jpg", null, true), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getSiteName(), "html", null, true);
        echo "\">
    </p>
    <p>
    \t";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bonjour"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lastName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "firstName", array()), "html", null, true);
        echo ",
    </p>
    <p>
    \t";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bienvenue dans notre communauté"), "html", null, true);
        echo " !
    </p>
\t<p>
    \t";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Afin de recevoir les prochains emails sans difficulté pensez à nous ajouter dans vos contacts, ainsi les messages n'arriveront pas dans les spams"), "html", null, true);
        echo ".
    </p>
\t<p>
\t\t";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Vous pouvez dès à présent commencer à rechercher un professionnel pour prendre rendez-vous"), "html", null, true);
        echo ".
\t</p>
\t<p>
    \t<a href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("register_validate", array("id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()), "e" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()))), "html", null, true);
        echo "\">
    \t\t";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cliquez ici pour valider votre adresse email"), "html", null, true);
        echo "
\t\t</a>
    </p>
\t<p>
\t\t";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nous restons à votre disposition"), "html", null, true);
        echo ".
\t</p>
\t<p>
\t\t";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cordialement"), "html", null, true);
        echo "
\t</p>
 </div>

</div>

</body>
</html>


";
    }

    public function getTemplateName()
    {
        return "RegisterBundle:Mail:cliente.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 37,  81 => 34,  74 => 30,  70 => 29,  64 => 26,  58 => 23,  52 => 20,  42 => 17,  34 => 14,  19 => 1,);
    }
}
