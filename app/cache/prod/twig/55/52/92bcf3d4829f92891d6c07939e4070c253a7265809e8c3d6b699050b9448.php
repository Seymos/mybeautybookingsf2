<?php

/* CoreBundle:Home:search.html.twig */
class __TwigTemplate_555292bcf3d4829f92891d6c07939e4070c253a7265809e8c3d6b699050b9448 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid container-rose\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t";
        // line 9
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("CoreBundle:Partial:search"));
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
        echo "</a> > ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Recherche"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>


<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t\t
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t";
        // line 33
        if (((isset($context["nb_result"]) ? $context["nb_result"] : null) == 1)) {
            // line 34
            echo "\t\t\t\t\t\t<h1 class=\"search_title_2\"><b>1 ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("résultat"), "html", null, true);
            echo "</b> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("correspond à votre recherche"), "html", null, true);
            echo "</h1>
\t\t\t\t\t";
        } elseif ((        // line 35
(isset($context["nb_result"]) ? $context["nb_result"] : null) > 1)) {
            // line 36
            echo "\t\t\t\t\t\t<h1 class=\"search_title_2\"><b>";
            echo twig_escape_filter($this->env, (isset($context["nb_result"]) ? $context["nb_result"] : null), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("résultats"), "html", null, true);
            echo "</b> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("correspondent à votre recherche"), "html", null, true);
            echo "</h1>
\t\t\t\t\t";
        } else {
            // line 38
            echo "\t\t\t\t\t\t<h1 class=\"search_title_2\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aucun résultat correspond à votre recherche"), "html", null, true);
            echo "</h1>
\t\t\t\t\t";
        }
        // line 40
        echo "\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 44
        $this->env->loadTemplate("CoreBundle:Partial:result.html.twig")->display(array_merge($context, array("liste" => (isset($context["liste"]) ? $context["liste"] : null))));
        // line 45
        echo "\t\t\t</div>
\t\t\t
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 48
        $this->env->loadTemplate("CoreBundle::pagination.html.twig")->display(array_merge($context, array("nbPages" => (isset($context["nbPages"]) ? $context["nbPages"] : null), "page" => (isset($context["page"]) ? $context["page"] : null), "sort" => (isset($context["sort"]) ? $context["sort"] : null), "sens" => (isset($context["sens"]) ? $context["sens"] : null), "action" => (isset($context["action"]) ? $context["action"] : null))));
        // line 49
        echo "\t\t\t</div>

\t\t</div>
\t</div>
\t\t
</div>

";
    }

    // line 58
    public function block_javascripts($context, array $blocks = array())
    {
        // line 59
        echo "<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=";
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getGmapApi(), "html", null, true);
        echo "&signed_in=false&libraries=places\"></script>
";
        // line 60
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script>

\$(document).ready(function(){
\t
\tinitMap();
\t
\tfunction extractFromAdress(components, type){
\t\tfor (var i=0; i<components.length; i++)
\t\t\tfor (var j=0; j<components[i].types.length; j++)
\t\t\t\tif (components[i].types[j]==type) return components[i].long_name;
\t\treturn \"\";
\t}
\t
\tfunction initMap() {
\t\tvar input = document.getElementById('search_ou');
\t    var options = {componentRestrictions: {country: \"fr\"}};
\t    var autocomplete = new google.maps.places.Autocomplete(input,options);
\t    google.maps.event.addListener(
\t\t\tautocomplete, 'place_changed', function() {
\t\t\t\tvar place = autocomplete.getPlace();
\t\t\t\tif (place.geometry){
\t\t\t\t\t\$('#search_ou_location').val(place.geometry.location);
\t\t\t\t\t\$('#search_ou_city').val(extractFromAdress(place.address_components, \"locality\"));
\t\t\t\t}
\t\t\t}
\t\t);
\t}
\t

  
});
</script>
";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Home:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 60,  137 => 59,  134 => 58,  123 => 49,  121 => 48,  116 => 45,  114 => 44,  108 => 40,  102 => 38,  92 => 36,  90 => 35,  83 => 34,  81 => 33,  60 => 19,  47 => 9,  40 => 4,  37 => 3,  11 => 1,);
    }
}
