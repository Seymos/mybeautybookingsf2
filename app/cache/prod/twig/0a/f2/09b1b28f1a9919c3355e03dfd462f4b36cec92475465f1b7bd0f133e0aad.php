<?php

/* AdminBundle:User:form.html.twig */
class __TwigTemplate_0af209b1b28f1a9919c3355e03dfd462f4b36cec92475465f1b7bd0f133e0aad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : null), array(0 => "CoreBundle:Form:fields_errors.html.twig"));
        // line 2
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : null), array(0 => "UserBundle:Form:fields.html.twig"));
        // line 3
        echo "
<form method=\"POST\" autocomplete=\"off\" ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo ">
<div class=\"form_admin\">
 <table>
    <thead>
      <tr>
        <td colspan=\"2\">
 
        <ul id=\"bout-action\">
          <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("admin_user_index");
        echo "\"><span>Annuler</span></a>
\t\t\t</div>
          </li>
          <li class=\"button-valider\">
          \t<input type=\"submit\" value=\"Valider\"/>
          </li>
        </ul> 
        
        </td>
      </tr>
    </thead>
\t<tfoot>
      <tr>
        <td colspan=\"2\">
 
        <ul id=\"bout-action\">
          <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("admin_user_index");
        echo "\"><span>Annuler</span></a>
\t\t\t</div>
          </li>
          <li class=\"button-valider\">
          \t<input type=\"submit\" value=\"Valider\"/>
          </li>
        </ul> 
        
        </td>
      </tr>
    </tfoot>
    <tbody>
     ";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
     <tr>
     \t<th>";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "image", array()), 'label', array("label" => "Photo principale"));
        echo "</th>
     \t<td>
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t";
        // line 51
        if ( !(null === $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()))) {
            // line 52
            echo "\t\t\t\t\t\t <img class=\"img-circle img-thumbnail\" id=\"image_old_img\" src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()), "url", array()), "html", null, true);
            echo "\"/>
\t\t\t\t\t\t <input type=\"hidden\" id=\"image_old\" name=\"image_old\" value=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()), "filename", array()), "html", null, true);
            echo "\">
\t\t\t\t\t\t ";
            // line 54
            if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()), "filename", array()) != "no_image.png")) {
                // line 55
                echo "\t\t\t\t\t\t\t<a href=\"javascript:void(0);\" onclick=\"\$('#image_old').val('');\$('#image_old_img').hide();\$(this).hide();\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Supprimer"), "html", null, true);
                echo "</a>
\t\t\t\t\t\t ";
            }
            // line 57
            echo "\t\t\t\t\t\t";
        }
        // line 58
        echo "\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<div id=\"upload_image_div\" class=\"dropzone text-center\">
\t\t\t\t\t\t\t<div class=\"dz-default dz-message\">";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cliquez ici pour ajouter votre photo"), "html", null, true);
        echo "</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t</table>
     \t</td>
     </tr>
\t ";
        // line 68
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "type", array()) == 1)) {
            // line 69
            echo "\t <tr>
        <th>";
            // line 70
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'label', array("label" => "Nom"));
            echo "</th>
        <td>
          ";
            // line 72
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'errors');
            echo "
          ";
            // line 73
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'widget');
            echo "
        </td>
      </tr>
      <tr>
        <th>";
            // line 77
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'label', array("label" => "Prénom"));
            echo "</th>
        <td>
          ";
            // line 79
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'errors');
            echo "
          ";
            // line 80
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 84
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cheveux", array()), 'label', array("label" => "Cheveux"));
            echo "</th>
        <td class=\"checkbox-register\">
\t\t\t
\t\t\t";
            // line 87
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["cheveux"]) ? $context["cheveux"] : null), "children", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_category"]) {
                // line 88
                echo "\t\t\t
\t\t\t<h4 class=\"liste-competences-title\">";
                // line 89
                echo twig_escape_filter($this->env, $this->getAttribute($context["sub_category"], "title", array()), "html", null, true);
                echo "</h4>
\t\t\t
          <ul>
\t\t\t";
                // line 92
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["sub_category"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["competence"]) {
                    // line 93
                    echo "\t\t\t\t<li style=\"margin-right:10px;display:inline;\"> <input ";
                    if (twig_in_filter($context["competence"], $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "cheveux", array()))) {
                        echo "checked=checked";
                    }
                    echo " type=\"checkbox\" name=\"faprod_userbundle_user[cheveux][]\" id=\"faprod_userbundle_user_cheveux_";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                    echo "\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "title", array()), "html", null, true);
                    echo "</li>
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competence'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 95
                echo "\t\t\t</ul>
\t\t\t
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 98
            echo "        </td>
      </tr>
\t  <tr>
        <th>";
            // line 101
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "peau", array()), 'label', array("label" => "Peau"));
            echo "</th>
        <td class=\"checkbox-register\">

\t\t\t";
            // line 104
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["peau"]) ? $context["peau"] : null), "children", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_category"]) {
                // line 105
                echo "\t\t\t
\t\t\t<h4 class=\"liste-competences-title\">";
                // line 106
                echo twig_escape_filter($this->env, $this->getAttribute($context["sub_category"], "title", array()), "html", null, true);
                echo "</h4>
\t\t\t
          <ul>
\t\t\t";
                // line 109
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["sub_category"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["competence"]) {
                    // line 110
                    echo "\t\t\t\t<li style=\"margin-right:10px;display:inline;\"> <input ";
                    if (twig_in_filter($context["competence"], $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "peau", array()))) {
                        echo "checked=checked";
                    }
                    echo " type=\"checkbox\" name=\"faprod_userbundle_user[peau][]\" id=\"faprod_userbundle_user_peau_";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                    echo "\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "title", array()), "html", null, true);
                    echo "</li>
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competence'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 112
                echo "\t\t\t</ul>
\t\t\t
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 115
            echo "        </td>
      </tr>
\t  <tr>
        <th>";
            // line 118
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "preferences", array()), 'label', array("label" => "Preferences"));
            echo "</th>
        <td class=\"checkbox-register\">
\t\t\t
\t\t\t";
            // line 121
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["preferences"]) ? $context["preferences"] : null), "children", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_category"]) {
                // line 122
                echo "\t\t\t
\t\t\t<h4 class=\"liste-competences-title\">";
                // line 123
                echo twig_escape_filter($this->env, $this->getAttribute($context["sub_category"], "title", array()), "html", null, true);
                echo "</h4>
\t\t\t
          <ul>
\t\t\t";
                // line 126
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["sub_category"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["competence"]) {
                    // line 127
                    echo "\t\t\t\t<li style=\"margin-right:10px;display:inline;\"> <input ";
                    if (twig_in_filter($context["competence"], $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "preferences", array()))) {
                        echo "checked=checked";
                    }
                    echo " type=\"checkbox\" name=\"faprod_userbundle_user[preferences][]\" id=\"faprod_userbundle_user_preferences_";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                    echo "\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                    echo "\"> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "title", array()), "html", null, true);
                    echo "</li>
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competence'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 129
                echo "\t\t\t</ul>
\t\t\t
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 132
            echo "        </td>
      </tr>
\t  <tr>
        <th>";
            // line 135
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'label', array("label" => "Adresse"));
            echo "</th>
        <td>
          ";
            // line 137
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_1", array()), 'errors');
            echo "
\t\t  ";
            // line 138
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_1", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 142
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'label', array("label" => "Complément adresse"));
            echo "</th>
        <td>
          ";
            // line 144
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_2", array()), 'errors');
            echo "
\t\t  ";
            // line 145
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_2", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 149
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'label', array("label" => "Code postal"));
            echo "</th>
        <td>
          ";
            // line 151
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'errors');
            echo "
          ";
            // line 152
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 156
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label', array("label" => "Ville"));
            echo "</th>
        <td>
          ";
            // line 158
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'errors');
            echo "
          ";
            // line 159
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget');
            echo "
        </td>
      </tr>
\t   <tr>
        <th>";
            // line 163
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label', array("label" => "Email"));
            echo "</th>
        <td>
          ";
            // line 165
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
            echo "
          ";
            // line 166
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
            echo "
        </td>
      </tr>
      <tr>
        <th>";
            // line 170
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'label', array("label" => "Mot de passe"));
            echo "</th>
        <td>
          ";
            // line 172
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'errors');
            echo "
          ";
            // line 173
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 177
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'label', array("label" => "Téléphone fixe"));
            echo "</th>
        <td>
          ";
            // line 179
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'errors');
            echo "
          ";
            // line 180
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 184
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'label', array("label" => "Téléphone portable"));
            echo "</th>
        <td>
          ";
            // line 186
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'errors');
            echo "
          ";
            // line 187
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget');
            echo "
        </td>
      </tr>
\t ";
        } else {
            // line 191
            echo "\t <tr>
        <th>";
            // line 192
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "genre", array()), 'label', array("label" => "Je suis"));
            echo "</th>
        <td class=\"checkbox-register\">
          ";
            // line 194
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "genre", array()), 'errors');
            echo "
          ";
            // line 195
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "genre", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 199
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "metiers", array()), 'label', array("label" => "Métiers"));
            echo "</th>
        <td class=\"checkbox-register\">
          ";
            // line 201
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "metiers", array()), 'errors');
            echo "
          ";
            // line 202
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "metiers", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 206
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cheveux", array()), 'label', array("label" => "Compétences Cheveux"));
            echo "</th>
        <td class=\"checkbox-register\">
\t\t\t
\t\t\t";
            // line 209
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["cheveux"]) ? $context["cheveux"] : null), "children", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_category"]) {
                // line 210
                echo "\t\t\t";
                if ($this->getAttribute($context["sub_category"], "matching", array())) {
                    // line 211
                    echo "\t\t\t<h4 class=\"liste-competences-title\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_category"], "title", array()), "html", null, true);
                    echo "</h4>
\t\t\t
          <ul>
\t\t\t";
                    // line 214
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["sub_category"], "children", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["competence"]) {
                        // line 215
                        echo "\t\t\t\t<li style=\"margin-right:10px;display:inline;\"> <input ";
                        if (twig_in_filter($context["competence"], $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "cheveux", array()))) {
                            echo "checked=checked";
                        }
                        echo " type=\"checkbox\" name=\"faprod_userbundle_user[cheveux][]\" id=\"faprod_userbundle_user_cheveux_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                        echo "\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                        echo "\"> ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "title", array()), "html", null, true);
                        echo "</li>
\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competence'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 217
                    echo "\t\t\t</ul>
\t\t\t";
                }
                // line 219
                echo "\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 220
            echo "        </td>
      </tr>
\t  <tr>
        <th>";
            // line 223
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "peau", array()), 'label', array("label" => "Compétences Peau"));
            echo "</th>
        <td class=\"checkbox-register\">

\t\t\t";
            // line 226
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["peau"]) ? $context["peau"] : null), "children", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_category"]) {
                // line 227
                echo "\t\t\t";
                if ($this->getAttribute($context["sub_category"], "matching", array())) {
                    // line 228
                    echo "\t\t\t<h4 class=\"liste-competences-title\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["sub_category"], "title", array()), "html", null, true);
                    echo "</h4>
\t\t\t
          <ul>
\t\t\t";
                    // line 231
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["sub_category"], "children", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["competence"]) {
                        // line 232
                        echo "\t\t\t\t<li style=\"margin-right:10px;display:inline;\"> <input ";
                        if (twig_in_filter($context["competence"], $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "peau", array()))) {
                            echo "checked=checked";
                        }
                        echo " type=\"checkbox\" name=\"faprod_userbundle_user[peau][]\" id=\"faprod_userbundle_user_peau_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                        echo "\" value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "id", array()), "html", null, true);
                        echo "\"> ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["competence"], "title", array()), "html", null, true);
                        echo "</li>
\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['competence'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 234
                    echo "\t\t\t</ul>
\t\t\t";
                }
                // line 236
                echo "\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 237
            echo "        </td>
      </tr>
\t  <tr>
        <th>";
            // line 240
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "statut", array()), 'label', array("label" => "Statut"));
            echo "</th>
        <td class=\"checkbox-register\">
          ";
            // line 242
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "statut", array()), 'errors');
            echo "
          ";
            // line 243
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "statut", array()), 'widget');
            echo "
        </td>
      </tr>
\t <tr>
        <th>";
            // line 247
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'label', array("label" => "Nom"));
            echo "</th>
        <td>
          ";
            // line 249
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'errors');
            echo "
          ";
            // line 250
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'widget');
            echo "
        </td>
      </tr>
      <tr>
        <th>";
            // line 254
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'label', array("label" => "Prénom"));
            echo "</th>
        <td>
          ";
            // line 256
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'errors');
            echo "
          ";
            // line 257
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 261
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'label', array("label" => "Adresse"));
            echo "</th>
        <td>
          ";
            // line 263
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_1", array()), 'errors');
            echo "
\t\t  ";
            // line 264
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_1", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 268
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'label', array("label" => "Complément adresse"));
            echo "</th>
        <td>
          ";
            // line 270
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_2", array()), 'errors');
            echo "
\t\t  ";
            // line 271
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_2", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 275
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'label', array("label" => "Code postal"));
            echo "</th>
        <td>
          ";
            // line 277
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'errors');
            echo "
          ";
            // line 278
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 282
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'label', array("label" => "Ville"));
            echo "</th>
        <td>
          ";
            // line 284
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'errors');
            echo "
          ";
            // line 285
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th><label>";
            // line 289
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Géolocalisation"), "html", null, true);
            echo "</label></th>
        <td>
      \t  ";
            // line 291
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "gmap_lat", array()), 'widget');
            echo "
      \t  ";
            // line 292
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "gmap_lng", array()), 'widget');
            echo "
              
\t\t  <input id=\"pac-input\" onkeypress=\"refuserToucheEntree(event)\" class=\"form-control\" type=\"text\" placeholder=\"";
            // line 294
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Saisissez votre adresse"), "html", null, true);
            echo "\" value=\"\">
\t\t  
\t\t  <div id=\"map\" style=\"height:300px;\"></div>
\t\t  
\t\t  <script type=\"text/javascript\">
\t\t\tinitMap();
\t\t  </script>
\t\t  
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 305
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "distance", array()), 'label', array("label" => "Distance"));
            echo "</th>
        <td>
          ";
            // line 307
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "distance", array()), 'errors');
            echo "
          ";
            // line 308
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "distance", array()), 'widget');
            echo "
        </td>
      </tr>
\t   <tr>
        <th>";
            // line 312
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'label', array("label" => "Email"));
            echo "</th>
        <td>
          ";
            // line 314
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
            echo "
          ";
            // line 315
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
            echo "
        </td>
      </tr>
      <tr>
        <th>";
            // line 319
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'label', array("label" => "Mot de passe"));
            echo "</th>
        <td>
          ";
            // line 321
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'errors');
            echo "
          ";
            // line 322
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "password", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 326
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'label', array("label" => "Téléphone fixe"));
            echo "</th>
        <td>
          ";
            // line 328
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'errors');
            echo "
          ";
            // line 329
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 333
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'label', array("label" => "Téléphone portable"));
            echo "</th>
        <td>
          ";
            // line 335
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'errors');
            echo "
          ";
            // line 336
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget');
            echo "
        </td>
      </tr>
      <tr>
        <th>";
            // line 340
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Autres Photos"), "html", null, true);
            echo "</th>
        <td>
            ";
            // line 342
            if ( !(null === $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "images", array()))) {
                // line 343
                echo "            <ul>
\t\t         ";
                // line 344
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "images", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 345
                    echo "\t\t\t\t <li style=\"float:left;width:260px;\">
\t\t\t\t\t\t<p>
\t\t\t\t\t \t\t<img style=\"width:100px;height:100px;\" id=\"images_old_img_";
                    // line 347
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                    echo "\" src=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "url", array()), "html", null, true);
                    echo "\"/>
\t\t\t\t\t \t\t<input type=\"hidden\" id=\"images_old_";
                    // line 348
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                    echo "\" name=\"images_old[]\" value=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                    echo "\">
\t\t\t\t\t \t</p>
\t\t\t\t\t \t";
                    // line 350
                    if (($this->getAttribute($context["image"], "filename", array()) != "no_image.png")) {
                        // line 351
                        echo "\t\t\t\t\t \t<p>
\t\t\t\t\t \t\t<a href=\"javascript:void(0);\" onclick=\"\$('#images_old_";
                        // line 352
                        echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                        echo "').val('');\$('#images_old_img_";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "id", array()), "html", null, true);
                        echo "').hide();\$(this).hide();\">";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Supprimer"), "html", null, true);
                        echo "</a>
\t\t\t\t\t \t</p>
\t\t\t\t\t \t";
                    }
                    // line 355
                    echo "\t\t\t\t </li>
\t\t\t\t ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 357
                echo "\t\t\t</ul>
\t\t\t";
            }
            // line 359
            echo "        </td>
      </tr>
      <tr>
    \t<th>";
            // line 362
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ajouter Photos"), "html", null, true);
            echo "</th>
    \t<td>
    \t\t<div id=\"upload_images_div\" class=\"dropzone\">
\t\t\t    <div class=\"dz-default dz-message\">";
            // line 365
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cliquez ici pour ajouter vos photos"), "html", null, true);
            echo "</div>
\t\t\t</div>
    \t</td>
      </tr>
\t  <tr>
        <th>";
            // line 370
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entreprise", array()), 'label', array("label" => "Nom de l'entreprise"));
            echo "</th>
        <td>
          ";
            // line 372
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entreprise", array()), 'errors');
            echo "
          ";
            // line 373
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entreprise", array()), 'widget');
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 377
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "siren", array()), 'label', array("label" => "SIRET"));
            echo "</th>
        <td>
          ";
            // line 379
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "siren", array()), 'errors');
            echo "
          ";
            // line 380
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "siren", array()), 'widget');
            echo "
        </td>
      </tr>
      <tr>
        <th>";
            // line 384
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'label', array("label" => "Qui êtes vous ?"));
            echo "</th>
        <td>
          ";
            // line 386
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'errors');
            echo "
          ";
            // line 387
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget', array("attr" => array("rows" => "15")));
            echo "
        </td>
      </tr>
\t  <tr>
        <th>";
            // line 391
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "charte", array()), 'label', array("label" => "Charte"));
            echo "</th>
        <td class=\"checkbox-register\">
          ";
            // line 393
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "charte", array()), 'errors');
            echo "
          ";
            // line 394
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "charte", array()), 'widget');
            echo "
        </td>
      </tr>
\t  ";
        }
        // line 398
        echo "\t  <tr>
        <th>";
        // line 399
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cgu", array()), 'label', array("label" => "CGU"));
        echo "</th>
        <td class=\"checkbox-register\">
          ";
        // line 401
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cgu", array()), 'errors');
        echo "
          ";
        // line 402
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "cgu", array()), 'widget');
        echo "
        </td>
      </tr>
\t  <tr>
        <th>";
        // line 406
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newsletter", array()), 'label', array("label" => "Newsletter"));
        echo "</th>
        <td class=\"checkbox-register\">
          ";
        // line 408
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newsletter", array()), 'errors');
        echo "
          ";
        // line 409
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newsletter", array()), 'widget');
        echo "
        </td>
      </tr>
      <tr>
        <th>";
        // line 413
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "validate", array()), 'label', array("label" => "Valide"));
        echo "</th>
        <td>
          ";
        // line 415
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "validate", array()), 'errors');
        echo "
          <select name=\"faprod_userbundle_user[validate]\" id=\"faprod_userbundle_user_validate\">
          \t<option ";
        // line 417
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "validate", array()) == 0)) {
            echo "selected=selected";
        }
        echo " value=\"0\">Non activé</option>
          \t<option ";
        // line 418
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "validate", array()) == 1)) {
            echo "selected=selected";
        }
        echo " value=\"1\">Activé</option>
          \t<option ";
        // line 419
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "validate", array()) ==  -1)) {
            echo "selected=selected";
        }
        echo " value=\"-1\">Blacklisté</option>
          </select>
        </td>
      </tr>
    </tbody>
  </table>
  </div>
";
        // line 426
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token", array()), 'widget');
        echo "
</form>

  \t       
<script language=\"JavaScript\" type=\"text/javascript\">

function clearPwBox()
{
if (document.getElementById)
{
var pw = document.getElementById('user_password');
if (pw != null)
{
pw.value = '';
}

}
}
window.setTimeout(\"clearPwBox()\", 100);
</script>";
    }

    public function getTemplateName()
    {
        return "AdminBundle:User:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1025 => 426,  1013 => 419,  1007 => 418,  1001 => 417,  996 => 415,  991 => 413,  984 => 409,  980 => 408,  975 => 406,  968 => 402,  964 => 401,  959 => 399,  956 => 398,  949 => 394,  945 => 393,  940 => 391,  933 => 387,  929 => 386,  924 => 384,  917 => 380,  913 => 379,  908 => 377,  901 => 373,  897 => 372,  892 => 370,  884 => 365,  878 => 362,  873 => 359,  869 => 357,  862 => 355,  852 => 352,  849 => 351,  847 => 350,  840 => 348,  834 => 347,  830 => 345,  826 => 344,  823 => 343,  821 => 342,  816 => 340,  809 => 336,  805 => 335,  800 => 333,  793 => 329,  789 => 328,  784 => 326,  777 => 322,  773 => 321,  768 => 319,  761 => 315,  757 => 314,  752 => 312,  745 => 308,  741 => 307,  736 => 305,  722 => 294,  717 => 292,  713 => 291,  708 => 289,  701 => 285,  697 => 284,  692 => 282,  685 => 278,  681 => 277,  676 => 275,  669 => 271,  665 => 270,  660 => 268,  653 => 264,  649 => 263,  644 => 261,  637 => 257,  633 => 256,  628 => 254,  621 => 250,  617 => 249,  612 => 247,  605 => 243,  601 => 242,  596 => 240,  591 => 237,  585 => 236,  581 => 234,  564 => 232,  560 => 231,  553 => 228,  550 => 227,  546 => 226,  540 => 223,  535 => 220,  529 => 219,  525 => 217,  508 => 215,  504 => 214,  497 => 211,  494 => 210,  490 => 209,  484 => 206,  477 => 202,  473 => 201,  468 => 199,  461 => 195,  457 => 194,  452 => 192,  449 => 191,  442 => 187,  438 => 186,  433 => 184,  426 => 180,  422 => 179,  417 => 177,  410 => 173,  406 => 172,  401 => 170,  394 => 166,  390 => 165,  385 => 163,  378 => 159,  374 => 158,  369 => 156,  362 => 152,  358 => 151,  353 => 149,  346 => 145,  342 => 144,  337 => 142,  330 => 138,  326 => 137,  321 => 135,  316 => 132,  308 => 129,  291 => 127,  287 => 126,  281 => 123,  278 => 122,  274 => 121,  268 => 118,  263 => 115,  255 => 112,  238 => 110,  234 => 109,  228 => 106,  225 => 105,  221 => 104,  215 => 101,  210 => 98,  202 => 95,  185 => 93,  181 => 92,  175 => 89,  172 => 88,  168 => 87,  162 => 84,  155 => 80,  151 => 79,  146 => 77,  139 => 73,  135 => 72,  130 => 70,  127 => 69,  125 => 68,  115 => 61,  110 => 58,  107 => 57,  101 => 55,  99 => 54,  95 => 53,  90 => 52,  88 => 51,  80 => 46,  75 => 44,  60 => 32,  39 => 14,  26 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }
}
