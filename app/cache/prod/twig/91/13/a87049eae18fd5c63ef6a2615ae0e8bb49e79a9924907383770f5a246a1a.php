<?php

/* AdminBundle:Login:index.html.twig */
class __TwigTemplate_9113a87049eae18fd5c63ef6a2615ae0e8bb49e79a9924907383770f5a246a1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AdminBundle::admin.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
  <form action=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("admin_login_login");
        echo "\" method=\"post\">
  \t<table>
  \t\t<tr>
  \t\t\t<th style=\"text-align:right;\">
  \t\t\t\t<label for=\"username\">Login :</label>
  \t\t\t</th>
  \t\t\t<td>
    \t\t\t<input type=\"text\" id=\"username\" name=\"username\" value=\"\" />
    \t\t</td>
    \t</tr>
    \t<tr>
    \t\t<th style=\"text-align:right;\">
    \t\t\t<label for=\"password\">Mot de passe :</label>
    \t\t</th>
    \t\t<td>
    \t\t\t<input type=\"password\" id=\"password\" name=\"password\" />
    \t\t</td>
    \t</tr>
    \t<tr>
    \t\t<td></td>
    \t\t<td>
    \t\t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" checked=checked/> se souvenir de moi
    \t\t</td>
    \t</tr>
    \t<tr>
    \t\t<td></td>
    \t\t<td><input type=\"submit\" value=\"Connexion\" /></td>
    \t</tr>
    </table>
  </form>

";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Login:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 5,  39 => 4,  36 => 3,  11 => 1,);
    }
}
