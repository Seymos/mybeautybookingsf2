<?php

/* AdminBundle:User:index.html.twig */
class __TwigTemplate_58800d52325462b6e5dd87f446f27a935fcb8245bb50affafba5e0e98ecb663e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AdminBundle::admin.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
  <h1>";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["nb_result"]) ? $context["nb_result"] : null), "html", null, true);
        echo " Membres</h1>
  
  <div class=\"table_admin\">
\t<table>
\t  <thead>
\t    <tr style=\"background-color:white;\">
\t        <th colspan=\"16\">
\t\t        <ul id=\"bout-action\">
\t\t          <li>
\t\t          \t<div class=\"buttonwrapper\">
\t\t\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("admin_user_add");
        echo "\"><span>Ajouter</span></a>
\t\t\t\t\t</div>
\t\t          </li>
\t\t          <li>
\t\t          \t<div class=\"buttonwrapper\">
\t\t\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("admin_user_export");
        echo "\"><span>Exporter</span></a>
\t\t\t\t\t</div>
\t\t          </li>
\t\t          <li>
\t\t            \t<form action=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("admin_user_gosearch");
        echo "\" method=\"post\">
\t\t\t\t\t\t    Recherche: <input type='text' id=\"search_user\" name=\"search_user\">
\t\t\t\t\t\t    <input type=\"submit\" id=\"boutton-recherche\" class=\"button-valider\" value=\"Chercher\"/>
\t\t\t\t\t\t</form>
\t\t      \t\t</li>
\t\t        </ul>
\t        </th>
\t    </tr>  
\t    <tr class=\"entete\">
\t      <th></th>
\t      <th></th>
\t      <th></th>
\t      <th><a href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "id", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">ID</a></th>
\t      <th><a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "first_name", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">Nom</a></th>
\t      <th><a href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "last_name", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">Prénom</a></th>
\t      <th><a href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "entreprise", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">Entreprise</a></th>
\t\t  <th><a href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "city", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">Ville</a></th>
\t\t  <th><a href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "type", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">Type</a></th>
\t\t  <th><a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "metiers", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">Métiers</a></th>
\t      <th><a href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "validate", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">Compte actif</a></th>
\t\t  <th><a href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_index", array("sort" => "created_at", "sens" => (isset($context["sens_inv"]) ? $context["sens_inv"] : null))), "html", null, true);
        echo "\">Date</a></th>
\t    </tr>
\t  </thead>
\t  <tbody>
\t    ";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["liste"]) ? $context["liste"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 49
            echo "\t    <tr class=\"item";
            if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                echo "1";
            }
            echo "\">
\t      <td><a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_show", array("user_id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/loupe.png"), "html", null, true);
            echo "\"></a></td>
\t      <td><a target=\"_blank\" href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("register_autologin", array("p1" => $this->env->getExtension('FAPROD.TwigExtension')->base64Encode($this->getAttribute($context["user"], "id", array())), "p2" => $this->env->getExtension('FAPROD.TwigExtension')->base64Encode($this->getAttribute($context["user"], "email", array())), "p3" => $this->env->getExtension('FAPROD.TwigExtension')->base64Encode($this->getAttribute($context["user"], "password", array())))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/player_play.png"), "html", null, true);
            echo "\"></a></td>
\t      <td>
\t      \t";
            // line 53
            if ( !(null === $this->getAttribute($context["user"], "image", array()))) {
                // line 54
                echo "      \t\t\t<img style=\"width:45px;\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["user"], "image", array()), "url", array()), "html", null, true);
                echo "\"/>
      \t\t";
            }
            // line 56
            echo "  \t\t  </td>
\t      <td>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</td>
\t      <td>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "firstname", array()), "html", null, true);
            echo "</td>
\t      <td>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "lastname", array()), "html", null, true);
            echo "</td>
\t\t  <td>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "entreprise", array()), "html", null, true);
            echo "</td>
\t      <td>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "city", array()), "html", null, true);
            echo "</td>
\t\t  <td>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "typeLabel", array()), "html", null, true);
            echo "</td>
\t\t  <td>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "metiersLabel", array()), "html", null, true);
            echo "</td>
\t      <td><center>";
            // line 64
            if (($this->getAttribute($context["user"], "validate", array()) && $this->getAttribute($context["user"], "mangoid", array()))) {
                echo "<img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/success.png"), "html", null, true);
                echo "\">";
            } else {
                echo "<img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/sup.gif"), "html", null, true);
                echo "\">";
            }
            echo "</center></td>
\t      <td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "createdat", array(0 => "d/m/Y"), "method"), "html", null, true);
            echo "</td>
\t    </tr>
\t    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "\t  </tbody>
\t  
\t</table>
\t
\t";
        // line 72
        $this->env->loadTemplate("AdminBundle::pagination.html.twig")->display(array_merge($context, array("nbPages" => (isset($context["nbPages"]) ? $context["nbPages"] : null), "page" => (isset($context["page"]) ? $context["page"] : null), "sort" => (isset($context["sort"]) ? $context["sort"] : null), "sens" => (isset($context["sens"]) ? $context["sens"] : null), "action" => (isset($context["action"]) ? $context["action"] : null))));
        // line 73
        echo "\t
  </div>

";
    }

    public function getTemplateName()
    {
        return "AdminBundle:User:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 73,  235 => 72,  229 => 68,  212 => 65,  200 => 64,  196 => 63,  192 => 62,  188 => 61,  184 => 60,  180 => 59,  176 => 58,  172 => 57,  169 => 56,  163 => 54,  161 => 53,  154 => 51,  148 => 50,  141 => 49,  124 => 48,  117 => 44,  113 => 43,  109 => 42,  105 => 41,  101 => 40,  97 => 39,  93 => 38,  89 => 37,  85 => 36,  70 => 24,  63 => 20,  55 => 15,  42 => 5,  39 => 4,  36 => 3,  11 => 1,);
    }
}
