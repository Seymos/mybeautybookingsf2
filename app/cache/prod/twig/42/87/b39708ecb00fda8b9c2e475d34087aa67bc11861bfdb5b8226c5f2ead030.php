<?php

/* BookingBundle:Booking:nbcliente.html.twig */
class __TwigTemplate_4287b39708ecb00fda8b9c2e475d34087aa67bc11861bfdb5b8226c5f2ead030 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["result"]) ? $context["result"] : null) > 0)) {
            echo "<a style=\"text-decoration:none;\" href=\"";
            echo $this->env->getExtension('routing')->getPath("booking_index");
            echo "\"><b>";
            echo twig_escape_filter($this->env, (isset($context["result"]) ? $context["result"] : null), "html", null, true);
            echo "</b> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("réservation(s) confirmée(s)"), "html", null, true);
            echo "</a>";
        }
    }

    public function getTemplateName()
    {
        return "BookingBundle:Booking:nbcliente.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
