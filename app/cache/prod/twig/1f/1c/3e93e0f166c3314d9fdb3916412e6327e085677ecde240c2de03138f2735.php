<?php

/* RegisterBundle:Partial:facebook_metas.html.twig */
class __TwigTemplate_1f1c3e93e0f166c3314d9fdb3916412e6327e085677ecde240c2de03138f2735 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<meta property=\"og:type\" content=\"website\"/>
<meta property=\"og:title\" content=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getMetaTitle(), "html", null, true);
        echo "\" />
<meta property=\"og:description\" content=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getMetaDescription(), "html", null, true);
        echo "\" />";
    }

    public function getTemplateName()
    {
        return "RegisterBundle:Partial:facebook_metas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }
}
