<?php

/* CoreBundle:Partial:home.html.twig */
class __TwigTemplate_2d2c52b60009c9b3e83c0df0abbc868f489939c0bce6bbab10880a7733bc8136 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container container-blanc\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12 container-avis-title\">
\t\t\t";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Avis"), "html", null, true);
        echo "
\t\t</div>
\t</div>
\t<div class=\"row home-categories-a\">
\t\t<div class=\"col-lg-offset-1 col-lg-10\">

\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t<!-- Carousel
\t\t\t\t\t================================================== -->
\t\t\t\t\t<div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\" data-interval=\"false\" data-pause=\"false\">
\t\t\t\t\t  <div class=\"carousel-inner\" role=\"listbox\">

\t\t\t\t\t\t<div class=\"row item active\">

\t\t\t\t\t\t\t<div class=\"col-sm-4 col-md-4 col-lg-4 text-center\">
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<b>Nadia</b> <img src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/b4.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\tCéline est super très pro. <br/>
\t\t\t\t\t\t\t\t\tMon tie and dye est réussi à la perfection. <br/>
\t\t\t\t\t\t\t\t\tJe recommande.<br/>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"col-sm-4 col-md-4 col-lg-4 text-center\">
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<b>Stéphanie</b> <img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/b5.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\tBooking la veille pour le lendemain avec Aurélie pour l'épilation intégrale. RAS
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"col-sm-4 col-md-4 col-lg-4 text-center\">
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<b>Khadi</b> <img src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/b5.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\tMake up au top. Rien à dire.<br/>
\t\t\t\t\t\t\t\t\tJ'ai trouvé ma nouvelle perle.<br/>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>

\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"row item\">

\t\t\t\t\t\t\t<div class=\"col-sm-4 col-md-4 col-lg-4 text-center\">
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<b>Marion</b> <img src=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/b3.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\tLes doigts de fée de Linda...<br/>
\t\t\t\t\t\t\t\t\tSes massages sont un peu brusques mais très efficaces. <br/>
\t\t\t\t\t\t\t\t\tJe recommande!<br/>
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"col-sm-4 col-md-4 col-lg-4 text-center\">
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<b>Sarah</b> <img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/b4.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\tBrushing impeccable. Cathy sait très bien y faire avec ma tignasse! lol
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"col-sm-4 col-md-4 col-lg-4 text-center\">
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t<b>Vanessa</b> <img src=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/b5.png"), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\tTrés contente du massage, je recommande !
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>


\t\t\t\t\t  </div>
\t\t\t\t\t  
\t\t\t\t\t  <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">
\t\t\t\t\t\t<span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t<span class=\"sr-only\">Previous</span>
\t\t\t\t\t  </a>
\t\t\t\t\t  <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">
\t\t\t\t\t\t<span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
\t\t\t\t\t\t<span class=\"sr-only\">Next</span>
\t\t\t\t\t  </a>
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t  
\t\t\t\t\t</div><!-- /.carousel -->
\t\t\t\t</div>
\t\t\t</div>


\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Partial:home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 76,  102 => 67,  88 => 56,  70 => 41,  58 => 32,  44 => 21,  24 => 4,  19 => 1,);
    }
}
