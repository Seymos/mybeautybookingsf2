<?php

/* ::error.html.twig */
class __TwigTemplate_1b06173016155d854d28da04263614b40e924a5289aeb95f37e06492da062114 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" dir=\"ltr\" lang=\"fr-FR\">
<title>Erreur</title>
<meta charset=\"utf-8\"/>
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=8\">
<link href='http://fonts.googleapis.com/css?family=Istok+Web|Chivo' rel='stylesheet' type='text/css'>

<style>

html, body, div, span, applet, object, iframe, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, font, ins, kbd, menu, q, s, samp,
small, strike, strong, sub, sup, tt, var,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td {
\tborder: 0;
\tfont-family: inherit;
\tfont-size: 100%;
\tfont-style: inherit;
\tfont-weight: inherit;
\tmargin: 0;
\toutline: 0;
\tpadding: 0;
\tvertical-align: baseline;
}
:focus {
        outline: 0;
}
body {
\tbackground: #fff;
\tline-height: 1;
}


/******************************************************************************/

body, html {
  width:100%;
  margin:0 auto;
  padding:0;
}

body {
  background: #f2f2f2 url(\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/core/images/error/background.png"), "html", null, true);
        echo "\") top left;
}

/* page wrappers **************************************************************/


/* page Wrapper */
.wrapper { 
  \twidth:100%;
  \tmargin:0 auto 0;
  \tdisplay:table;
\tposition:absolute;
\ttop:130px;
\tbackground: url(\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/core/images/error/container_background.png"), "html", null, true);
        echo "\") repeat-x scroll left top transparent;
}

/* content wrapper */
.mainWrapper {
    margin: 0 auto;
    position: relative;
    width: 830px;\t
}

/* main holders */
/* left holder - Logo, 404 Error */
.leftHolder {
    border-right: 1px solid #A99159;
    display: block;
    float: left;
    height: 269px;
    margin: 41px 0 0;
    position: relative;
    padding-right:25px;
    margin-right:25px;
}

/* right holder - Message, Robot, Try to, Search Form */
.rightHolder {
    display: block;
    float: right;
    padding-top:30px;
    font-family:verdana;
    font-size:21px;
    color:#897547;
    height: 321px;
    margin: 0 0 31px;
}
.rightHolder a{
\tcolor:#897547;
}

/* your logo */
.logo {
    display: block;
}

/* error 404 */
.errorNumber {
    color: #FFFFFF;
    display: block;
    float: left;
    left: 0;
    margin: 0;
    position: absolute;
    text-align: center;
}

.error_logo{
margin-top:50px;
margin-left:90px;
}

</style>

<!--[if IE]>
<script type=\"text/javascript\" src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script>
<![endif]-->

</head>

<body>


<div class=\"wrapper\">

\t<div class=\"mainWrapper\">
\t\t<table border=\"0\" width=\"100%\" cellpadding=\"0\" style=\"border-collapse: collapse\">
\t\t\t<tr>
\t\t\t\t<td style=\"vertical-align:top;\">
\t\t\t\t\t<div class=\"leftHolder\">
\t\t\t        \t<a class=\"logo\" href=\"";
        // line 135
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\">
\t\t\t\t        \t<img src=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/logo.png"), "html", null, true);
        echo "\"/>
\t\t\t\t        </a>
\t\t\t            <img class=\"error_logo\" src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/core/images/error/error.png"), "html", null, true);
        echo "\"/>
\t\t\t        </div>
\t\t\t    </td>
\t\t\t\t<td style=\"vertical-align:top;\">
\t\t\t\t\t<div class=\"rightHolder\">
\t\t\t            <div class=\"tryToMessage\">
\t\t\t               ";
        // line 144
        $this->displayBlock('body_content', $context, $blocks);
        // line 145
        echo "\t\t\t               
\t\t\t               <p>
\t\t\t\t               <a class=\"logo\" href=\"";
        // line 147
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\">
\t\t\t\t               \t\tRetour page d'accueil
\t\t\t\t               </a>
\t\t\t               </p>
\t\t\t            </div>
\t\t\t        </div>
\t\t\t\t</td>
\t\t\t</tr>
\t\t</table>
    
\t</div>

</div>
<!-- end .wrapper -->


</body>
</html>";
    }

    // line 144
    public function block_body_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 144,  186 => 147,  182 => 145,  180 => 144,  171 => 138,  166 => 136,  162 => 135,  82 => 58,  66 => 45,  20 => 1,);
    }
}
