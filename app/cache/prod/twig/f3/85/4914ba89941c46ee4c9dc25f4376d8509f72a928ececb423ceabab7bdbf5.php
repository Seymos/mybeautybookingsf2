<?php

/* CoreBundle:Home:show.html.twig */
class __TwigTemplate_f3854914ba89941c46ee4c9dc25f4376d8509f72a928ececb423ceabab7bdbf5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'meta_description' => array($this, 'block_meta_description'),
            'facebook_metas' => array($this, 'block_facebook_metas'),
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "entreprise", array()), "html", null, true);
    }

    // line 4
    public function block_meta_description($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->truncate($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "description", array()), 200), "html", null, true);
    }

    // line 6
    public function block_facebook_metas($context, array $blocks = array())
    {
        // line 7
        echo "<meta property=\"og:type\" content=\"website\"/>
<meta property=\"og:title\" content=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "entreprise", array()), "html", null, true);
        echo "\" />
<meta property=\"og:description\" content=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->truncate($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "description", array()), 200), "html", null, true);
        echo "\" />
";
        // line 10
        if ( !(null === $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "image", array()))) {
            // line 11
            echo "<meta property=\"og:image\" content=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "image", array()), "url", array()), "html", null, true);
            echo "\"/>
";
        }
    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        // line 16
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fotorama/fotorama.js"), "html", null, true);
        echo "\"></script>\t
";
        // line 18
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lat", array())) {
            echo "\t
<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getGmapApi(), "html", null, true);
            echo "&signed_in=false&sensor=false&callback=initMap\" async defer></script>
<script type=\"text/javascript\">
function initMap() {
  var myLatLng = {lat: ";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lat", array()), "html", null, true);
            echo ", lng: ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lng", array()), "html", null, true);
            echo "};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 14,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: '";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "entreprise", array()), "html", null, true);
            echo "'
  });
}
</script>
";
        }
    }

    // line 39
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 40
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fotorama/fotorama.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
    }

    // line 44
    public function block_body_content($context, array $blocks = array())
    {
        // line 45
        echo "
<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<a style=\"font-weight:bold;font-size:18px;\" href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("home_search");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Retour à la recherche"), "html", null, true);
        echo "</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"col-md-8 col-lg-8\">
\t\t\t
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12\">
\t\t\t
\t\t\t\t\t<h2 class=\"title2 title_bottom title_top\">";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Qui suis-je ?"), "html", null, true);
        echo "</h2>
\t\t\t\t\t
\t\t\t\t\t";
        // line 66
        echo nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "description", array()), "html", null, true));
        echo "
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12\">
\t\t\t
\t\t\t\t\t<h2 class=\"title2 title_bottom title_top\">";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes prestations / tarifs"), "html", null, true);
        echo "</h2>
\t\t\t\t\t
\t\t\t\t\t";
        // line 76
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "offers", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["offer"]) {
            // line 77
            echo "\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-8 offer-title\">
\t\t\t\t\t\t\t";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "title", array()), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-lg-2 offer-duree\">
\t\t\t\t\t\t\t";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "dureeLabel", array()), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-lg-2 offer-price\">
\t\t\t\t\t\t\t";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "priceLabel", array()), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t\t\t";
            // line 90
            echo $this->getAttribute($context["offer"], "description", array());
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-6\">
\t\t\t\t\t\t\t<ul class=\"liste-photo\">
\t\t\t\t\t\t\t";
            // line 96
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["offer"], "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 97
                echo "\t\t\t\t\t\t\t\t<li><a rel=\"facebox\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "urlzoom", array()), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["image"], "url", array()), "html", null, true);
                echo "\"/></a></li>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 99
            echo "\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<hr/>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offer'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t</div>
\t\t<div class=\"col-md-4 col-lg-4\">
\t\t
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12 user-detail-title text-center\">
\t\t\t\t\t<h1>";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "shortUserName2", array()), "html", null, true);
        echo "</h1>
\t\t\t\t\t<p>
\t\t\t\t\t\t";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "metiersLabel", array()), "html", null, true);
        echo "
\t\t\t\t\t</p>
\t\t\t\t\t<p>
\t\t\t\t\t\t<img class=\"img-thumbnail\" alt=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lastName", array()), "html", null, true);
        echo "\" src=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "image", array()), "url", array()), "html", null, true);
        echo "\"/>
\t\t\t\t\t</p>\t\t\t\t\t
\t\t\t\t\t<p>
\t\t\t\t\t\t";
        // line 121
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "nbNote", array()) > 0)) {
            echo "<img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("images/template/b" . $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "note", array())) . ".png")), "html", null, true);
            echo "\"/> <a href=\"#a_comments\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lire les avis"), "html", null, true);
            echo "</a>";
        } else {
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aucune note"), "html", null, true);
        }
        // line 122
        echo "\t\t\t\t\t</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t
\t\t\t";
        // line 126
        if (($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED") && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1))) {
            // line 127
            echo "\t\t\t
\t\t\t";
            // line 128
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()) != $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "id", array()))) {
                // line 129
                echo "\t\t\t
\t\t\t<hr/>
\t\t\t
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t
\t\t\t\t\t";
                // line 135
                if (!twig_in_filter((isset($context["user"]) ? $context["user"] : null), $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "favoris", array()))) {
                    // line 136
                    echo "\t\t\t\t\t\t<a class=\"btn btn-info btn-partage-t\" style=\"border-color:white !important;margin-right:20px;background-color:#EA988B !important;color:white !important;\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_addfavoris", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
                    echo "\"><span class=\"glyphicon glyphicon-thumbs-up\"></span>&nbsp;&nbsp;";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ajouter dans mes favoris"), "html", null, true);
                    echo "</a>
\t\t\t\t\t";
                } else {
                    // line 138
                    echo "\t\t\t\t\t\t<a class=\"btn btn-info btn-partage-t\" style=\"border-color:white !important;margin-right:20px;background-color:#EA988B !important;color:white !important;\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("user_deletefavoris", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Supprimer de vos favoris"), "html", null, true);
                    echo "</a>
\t\t\t\t\t";
                }
                // line 140
                echo "\t\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>\t
\t\t\t";
            }
            // line 144
            echo "\t\t\t
\t\t\t";
        }
        // line 145
        echo "\t
\t\t\t
\t\t</div>
\t</div>
\t
\t";
        // line 150
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "offers", array()))) {
            // line 151
            echo "\t<h2 class=\"title title_top\" style=\"background-color:#F9DCD3;color:#BC6E5D;border-bottom-left-radius:0px;border-bottom-right-radius:0px;font-size:24px;\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prendre un rendez-vous"), "html", null, true);
            echo "</h2>
\t
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t
\t\t\t<div class=\"zone_message_advert\" style=\"border-bottom-left-radius:10px;border-bottom-right-radius:10px;\">
\t\t
\t\t\t\t<form class=\"zone_message_advert\" type=\"POST\" onsubmit=\"\$(this).find(':button').attr('disabled', 'disabled');this.submit();return true;\" action=\"";
            // line 158
            echo $this->env->getExtension('routing')->getPath("booking_newbooking");
            echo "\" method=\"post\" class=\"form-horizontal col-lg-12\">
\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"col-lg-offset-1 col-lg-10\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<div class=\"zone_message_advert_etapes\">Je choisi ma formule</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\tfunction initresa(){

\t\t\t\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\t\t\t\turl: '";
            // line 173
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_agenda", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
            echo "',
\t\t\t\t\t\t\t\t\t\t\ttype: 'GET',
\t\t\t\t\t\t\t\t\t\t\tbeforeSend: function(data) {
\t\t\t\t\t\t\t\t\t\t\t\t\$('#agenda_user').empty().append('<center><img src=\\'";
            // line 176
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
            echo "\\'></center>');
\t\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\t\tsuccess: function(data) {
\t\t\t\t\t\t\t\t\t\t\t\t\$('#agenda_user').empty().append(data);
\t\t\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\t\t\terror: function () {
\t\t\t\t\t\t\t\t\t\t\t\talert('La requête n\\'a pas abouti');
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
            // line 189
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "offers", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["offer"]) {
                // line 190
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-8 offer-title\">
\t\t\t\t\t\t\t\t\t\t\t\t<input onchange=\"initresa()\" checked=checked type=\"radio\" id=\"offer_";
                // line 192
                echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "id", array()), "html", null, true);
                echo "\" name=\"offer\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "id", array()), "html", null, true);
                echo "\"> <label for=\"offer_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["offer"], "html", null, true);
                echo "</label>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-2 offer-duree\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 195
                echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "dureeLabel", array()), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-lg-2 offer-price\">
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 198
                echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "priceLabel", array()), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 202
            echo "\t\t\t\t\t\t\t\t  </div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t\t\t<div class=\"zone_message_advert_etapes\">Je choisi une date et heure de RDV</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
            // line 212
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("CoreBundle:Partial:agenda", array("user" => (isset($context["user"]) ? $context["user"] : null))));
            echo "\t
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</form>

\t\t\t</div>
\t\t\t
\t\t</div>
\t</div>
\t";
        }
        // line 222
        echo "\t
\t<h2 class=\"title title_top\">";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Avis"), "html", null, true);
        echo "</h2>
\t
\t<a name=\"a_comments\"></a>
\t<div class=\"row\">
\t\t<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">
\t\t\t";
        // line 228
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "validComments", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 229
            echo "\t\t\t<article class=\"comment\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t<span class=\"comment_author\">
\t\t\t\t\t\t\t";
            // line 233
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["comment"], "user", array()), "shortUsername", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["comment"], "createdAt", array(0 => "d/m/Y"), "method"), "html", null, true);
            echo "<br/>
\t\t\t\t\t\t\t<img style=\"margin-left:45px;\" src=\"";
            // line 234
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl((("images/template/b" . $this->getAttribute($context["comment"], "note", array())) . ".png")), "html", null, true);
            echo "\"/>
\t\t\t\t\t\t</span>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-12 comment_message\" style=\"padding-top:8px;\">
\t\t\t\t\t\t";
            // line 240
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute($context["comment"], "message", array()), "html", null, true));
            echo "
\t\t\t\t   </div>
\t\t\t\t</div>
\t\t\t</article>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 245
        echo "\t\t</div>
\t</div>
\t
\t
</div>



";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Home:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  505 => 245,  494 => 240,  485 => 234,  479 => 233,  473 => 229,  469 => 228,  461 => 223,  458 => 222,  445 => 212,  433 => 202,  423 => 198,  417 => 195,  405 => 192,  401 => 190,  397 => 189,  381 => 176,  375 => 173,  357 => 158,  346 => 151,  344 => 150,  337 => 145,  333 => 144,  327 => 140,  319 => 138,  311 => 136,  309 => 135,  301 => 129,  299 => 128,  296 => 127,  294 => 126,  288 => 122,  278 => 121,  270 => 118,  264 => 115,  259 => 113,  248 => 104,  238 => 99,  227 => 97,  223 => 96,  214 => 90,  206 => 85,  200 => 82,  194 => 79,  190 => 77,  186 => 76,  181 => 74,  170 => 66,  165 => 64,  146 => 50,  139 => 45,  136 => 44,  130 => 41,  126 => 40,  123 => 39,  113 => 32,  98 => 22,  92 => 19,  88 => 18,  84 => 17,  80 => 16,  77 => 15,  69 => 11,  67 => 10,  63 => 9,  59 => 8,  56 => 7,  53 => 6,  47 => 4,  41 => 3,  11 => 1,);
    }
}
