<?php

/* ContentBundle:Partial:zone.html.twig */
class __TwigTemplate_dd0f354ccadbda87c7b02dfd504aeccff9b04670445d08254191969a60761c98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["zone"]) ? $context["zone"] : null), "isPublish", array())) {
            echo $this->getAttribute((isset($context["zone"]) ? $context["zone"] : null), "html", array());
        }
    }

    public function getTemplateName()
    {
        return "ContentBundle:Partial:zone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
