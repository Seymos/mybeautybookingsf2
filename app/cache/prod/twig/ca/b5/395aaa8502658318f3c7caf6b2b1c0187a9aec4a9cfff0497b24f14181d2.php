<?php

/* CoreBundle::footer.html.twig */
class __TwigTemplate_cab5395aaa8502658318f3c7caf6b2b1c0187a9aec4a9cfff0497b24f14181d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"footer\"";
        // line 4
        if ($this->env->getExtension('FAPROD.TwigExtension')->isHomeIndex()) {
            echo "style=\"margin-top:0px;\"";
        }
        echo ">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t";
        // line 7
        echo $this->env->getExtension('http_kernel')->renderFragmentStrategy("esi", $this->env->getExtension('http_kernel')->controller("ContentBundle:Partial:zone", array("id" => 1)));
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "CoreBundle::footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 7,  24 => 4,  19 => 1,);
    }
}
