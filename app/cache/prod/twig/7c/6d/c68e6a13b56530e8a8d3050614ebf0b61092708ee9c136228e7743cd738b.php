<?php

/* CoreBundle:Partial:agenda.html.twig */
class __TwigTemplate_7c6dc68e6a13b56530e8a8d3050614ebf0b61092708ee9c136228e7743cd738b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<script>

function goresa(date, date_select){

    \$.ajax({
        url: '";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_agenda", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
        echo "',
        type: 'GET',
        data: 'offer_id='+\$('input[name=offer]:checked').val()+'&date='+date+'&date_select='+date_select,
        beforeSend: function(data) {
            \$('#agenda_user').empty().append('<center><img src=\\'";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
        echo "\\'></center>');
        },
        success: function(data) {
            \$('#agenda_user').empty().append(data);
        },
        error: function () {
            alert('La requête n\\'a pas abouti');
        }
    });
\t
};
</script>

<div id=\"agenda_user\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\" style=\"overflow-x:auto;\">\t
\t\t\t
\t\t\t\t";
        // line 28
        if (((isset($context["selection_valide"]) ? $context["selection_valide"] : null) ==  -1)) {
            // line 29
            echo "\t\t\t\t<div id=\"agenda_user_erreur\">
\t\t\t\t\t";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Le créneau horaire choisi ne permet pas d'effectuer cette prestation"), "html", null, true);
            echo "
\t\t\t\t</div>
\t\t\t\t";
        }
        // line 33
        echo "\t\t\t\t<a class=\"pull-left btn btn-primary btn-xs\" onclick=\"\$('#agenda_user').empty().append('<center><img style=\\'margin:60px;\\' src=\\'";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
        echo "\\'></center>').load('";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_agenda", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()), "date" => (isset($context["last_date"]) ? $context["last_date"] : null))), "html", null, true);
        echo "');\" href=\"javascript:void(0);\"><<</a>
\t\t\t\t<a class=\"pull-right btn btn-primary btn-xs\" onclick=\"\$('#agenda_user').empty().append('<center><img style=\\'margin:60px;\\' src=\\'";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/loader.gif"), "html", null, true);
        echo "\\'></center>').load('";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_agenda", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()), "date" => (isset($context["next_date"]) ? $context["next_date"] : null))), "html", null, true);
        echo "');\" href=\"javascript:void(0);\">>></a>
\t\t\t\t<table class=\"table table-bordered\" style=\"background-color:white;\">
\t\t\t\t";
        // line 36
        if (((isset($context["selection_valide"]) ? $context["selection_valide"] : null) == 1)) {
            // line 37
            echo "\t\t\t\t<input type=\"hidden\" id=\"date_select\" name=\"date_select\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["date_select"]) ? $context["date_select"] : null), "html", null, true);
            echo "\">
\t\t\t\t";
        }
        // line 39
        echo "\t\t\t\t<tr>
\t\t\t\t\t<th></th>
\t\t\t\t\t";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entete"]) ? $context["entete"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["ligne"]) {
            // line 42
            echo "\t\t\t\t\t<th class=\"agenda_entete\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "jour", array(), "array"), "html", null, true);
            echo "<br/>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["ligne"], "date", array(), "array"), "html", null, true);
            echo "  ";
            echo twig_escape_filter($this->env, (isset($context["duree"]) ? $context["duree"] : null), "html", null, true);
            echo "</th>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ligne'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "\t\t\t\t</tr>
\t\t\t\t";
        // line 45
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["datas"]) ? $context["datas"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 46
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td class=\"text-center\" style=\"font-size:13px;padding-top:8px !important;\">
\t\t\t\t\t\t";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["data"], "heure", array(), "array"), "html", null, true);
            echo "
\t\t\t\t\t</td>
\t\t\t\t\t";
            // line 50
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["data"], "jours", array(), "array"));
            foreach ($context['_seq'] as $context["_key"] => $context["reservation"]) {
                // line 51
                echo "\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t";
                // line 52
                if (($this->getAttribute($context["reservation"], "ouverture", array(), "array") == 1)) {
                    // line 53
                    echo "\t\t\t\t\t\t\t";
                    if ((($this->getAttribute($context["reservation"], "case_select", array(), "array") == 1) && ((isset($context["selection_valide"]) ? $context["selection_valide"] : null) == 1))) {
                        // line 54
                        echo "\t\t\t\t\t\t\t\t<label class=\"agenda_select_date\" id=\"";
                        echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                        echo "_label\">";
                        echo "booké";
                        echo "</label>
\t\t\t\t\t\t\t";
                    } else {
                        // line 56
                        echo "\t\t\t\t\t\t\t\t<a class=\"agenda_choisir\" id=\"";
                        echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                        echo "\" onclick=\"goresa('";
                        echo twig_escape_filter($this->env, (isset($context["date_agenda"]) ? $context["date_agenda"] : null), "html", null, true);
                        echo "', '";
                        echo twig_escape_filter($this->env, (($this->getAttribute($context["reservation"], "date", array(), "array") . "_") . $this->getAttribute($context["data"], "heure", array(), "array")), "html", null, true);
                        echo "');\" href=\"javascript:void(0);\">";
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Je booke"), "html", null, true);
                        echo "</a>
\t\t\t\t\t\t\t";
                    }
                    // line 58
                    echo "\t\t\t\t\t\t";
                } elseif (($this->getAttribute($context["reservation"], "ouverture", array(), "array") ==  -1)) {
                    // line 59
                    echo "\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans(""), "html", null, true);
                    echo "
\t\t\t\t\t\t";
                } else {
                    // line 61
                    echo "\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans(""), "html", null, true);
                    echo "
\t\t\t\t\t\t";
                }
                // line 63
                echo "\t\t\t\t\t</td>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['reservation'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "\t\t\t\t</table>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
\t";
        // line 72
        if (((isset($context["selection_valide"]) ? $context["selection_valide"] : null) == 1)) {
            // line 73
            echo "\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t\t<div class=\"col-lg-12 text-center\" style=\"font-size:14px;margin-bottom:7px;\">
\t\t\t\t";
            // line 76
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si vous disposez d'un code de réduction vous pouvez le saisir ici"), "html", null, true);
            echo " : <input style=\"padding:5px;\" name=\"code_reduction\" id=\"code_reduction\" placeholder=\"Mon code de réduction\">
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t<button class=\"btn btn-primary btn-lg\"><span class=\"glyphicon glyphicon-hand-up\"></span>&nbsp;&nbsp;";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Valider ma réservation"), "html", null, true);
            echo "</button>
\t\t\t</div>
\t\t</div>
\t</div>
\t";
        }
        // line 88
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Partial:agenda.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 88,  209 => 83,  199 => 76,  194 => 73,  192 => 72,  185 => 67,  178 => 65,  171 => 63,  165 => 61,  159 => 59,  156 => 58,  144 => 56,  136 => 54,  133 => 53,  131 => 52,  128 => 51,  124 => 50,  119 => 48,  115 => 46,  111 => 45,  108 => 44,  95 => 42,  91 => 41,  87 => 39,  81 => 37,  79 => 36,  72 => 34,  65 => 33,  59 => 30,  56 => 29,  54 => 28,  34 => 11,  27 => 7,  19 => 1,);
    }
}
