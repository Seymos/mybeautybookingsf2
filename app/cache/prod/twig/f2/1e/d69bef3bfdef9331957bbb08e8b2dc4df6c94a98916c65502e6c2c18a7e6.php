<?php

/* A2lixTranslationFormBundle::default.html.twig */
class __TwigTemplate_f21ed69bef3bfdef9331957bbb08e8b2dc4df6c94a98916c65502e6c2c18a7e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'a2lix_translations_widget' => array($this, 'block_a2lix_translations_widget'),
            'a2lix_translationsForms_widget' => array($this, 'block_a2lix_translationsForms_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('a2lix_translations_widget', $context, $blocks);
        // line 30
        echo "
";
        // line 31
        $this->displayBlock('a2lix_translationsForms_widget', $context, $blocks);
    }

    // line 1
    public function block_a2lix_translations_widget($context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
    <div class=\"a2lix_translations tabbable\">
        <ul class=\"a2lix_translationsLocales nav nav-tabs\">
        ";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
            // line 6
            echo "            ";
            $context["locale"] = $this->getAttribute($this->getAttribute($context["translationsFields"], "vars", array()), "name", array());
            // line 7
            echo "
            <li ";
            // line 8
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "locale", array()) == (isset($context["locale"]) ? $context["locale"] : null))) {
                echo "class=\"active\"";
            }
            echo ">
                <a href=\"#\" data-toggle=\"tab\" data-target=\".a2lix_translationsFields-";
            // line 9
            echo twig_escape_filter($this->env, (isset($context["locale"]) ? $context["locale"] : null), "html", null, true);
            echo "\">
                    ";
            // line 10
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, (isset($context["locale"]) ? $context["locale"] : null)), "html", null, true);
            echo "
                    ";
            // line 11
            if (($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "default_locale", array()) == (isset($context["locale"]) ? $context["locale"] : null))) {
                echo "[Default]";
            }
            // line 12
            echo "                    ";
            if ($this->getAttribute($this->getAttribute($context["translationsFields"], "vars", array()), "required", array())) {
                echo "*";
            }
            // line 13
            echo "                </a>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "        </ul>

        <div class=\"a2lix_translationsFields tab-content\">
        ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
            // line 20
            echo "            ";
            $context["locale"] = $this->getAttribute($this->getAttribute($context["translationsFields"], "vars", array()), "name", array());
            // line 21
            echo "
            <div class=\"a2lix_translationsFields-";
            // line 22
            echo twig_escape_filter($this->env, (isset($context["locale"]) ? $context["locale"] : null), "html", null, true);
            echo " tab-pane ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "locale", array()) == (isset($context["locale"]) ? $context["locale"] : null))) {
                echo "active";
            }
            echo "\">
                ";
            // line 23
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["translationsFields"], 'errors');
            echo "
                ";
            // line 24
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["translationsFields"], 'widget');
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "        </div>
    </div>
";
    }

    // line 31
    public function block_a2lix_translationsForms_widget($context, array $blocks = array())
    {
        // line 32
        echo "    ";
        $this->displayBlock("a2lix_translations_widget", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "A2lixTranslationFormBundle::default.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  126 => 32,  123 => 31,  117 => 27,  108 => 24,  104 => 23,  96 => 22,  93 => 21,  90 => 20,  86 => 19,  81 => 16,  73 => 13,  68 => 12,  64 => 11,  60 => 10,  56 => 9,  50 => 8,  47 => 7,  44 => 6,  40 => 5,  33 => 2,  30 => 1,  26 => 31,  23 => 30,  21 => 1,);
    }
}
