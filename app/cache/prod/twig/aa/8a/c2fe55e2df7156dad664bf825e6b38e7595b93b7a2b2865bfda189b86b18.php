<?php

/* CoreBundle::popupajax.html.twig */
class __TwigTemplate_aa8ac2fe55e2df7156dad664bf825e6b38e7595b93b7a2b2865bfda189b86b18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"popupajaxModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"popupajaxModalLabel\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
        <h4 class=\"modal-title\" id=\"poputitle\"></h4>
        <div id=\"urlload\" style=\"display:none;\"></div>
      </div>
      <div class=\"modal-body\" id=\"popupajax\">


      </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "CoreBundle::popupajax.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
