<?php

/* UserBundle:User:index.html.twig */
class __TwigTemplate_34e8b98d54a6a8d71f8cb2e2c97dfc1f10325782b09510f11e4db81b4359b4f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
        echo "</a> > ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t\t\t<div class=\"visible-xs visible-sm\">
\t\t\t\t";
        // line 20
        echo twig_include($this->env, $context, "UserBundle:Partial:menu.html.twig");
        echo "
\t\t\t</div>
\t\t\t<div id=\"profil_pro_bk\" class=\"visible-md visible-lg\">
\t\t\t
\t\t\t\t<div id=\"profil_pro_photo\">
\t\t\t\t\t<img class=\"img-rounded\" src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "image", array()), "url", array()), "html", null, true);
        echo "\"/>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_pro_espacepro_titre\">
\t\t\t\t\t<h3>";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_pro_espacepro\">
\t\t\t\t\t<div id=\"menu_left\">
\t\t\t\t\t\t<a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("user_edit");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkprofilpro", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
        echo "</a><br/>
\t\t\t\t\t\t<a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("agenda_index");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkDisponibilites", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon planning"), "html", null, true);
        echo "</a><br/>
\t\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes paiements"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("booking_paiementspopup");
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes paiements"), "html", null, true);
        echo "</a><br/>
\t\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes documents"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showdocuments");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkDocuments", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes documents"), "html", null, true);
        echo "</a><br/>
\t\t\t\t\t\t<a href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("user_statistique");
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon tableau de bord"), "html", null, true);
        echo "</a><br/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_pro_espacepro_titre2\">
\t\t\t\t\t<h3>";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Artist profil"), "html", null, true);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_pro_monprofil\">
\t\t\t\t\t<div id=\"menu_left\">
\t\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes compétences"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showcompetences");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkcompetences", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes compétences"), "html", null, true);
        echo "</a>
\t\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes prestations"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showprestations");
        echo "\" class=\"btn btn-primary ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "checkServices", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes prestations"), "html", null, true);
        echo "</a><br/>
\t\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("booking_popup");
        echo "\"  class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes booking"), "html", null, true);
        echo "</a>
\t\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes notes & avis"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("user_showavis");
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Notes & Avis"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div id=\"profil_pro_messagerie\">
\t\t\t\t\t<div id=\"menu_left\">
\t\t\t\t\t<a href=\"javascript:void(0);\" data-toggle=\"modal\" data-target=\"#popupajaxModal\" data-title=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
        echo "\" data-whatever=\"";
        echo $this->env->getExtension('routing')->getPath("message_popup");
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Messagerie"), "html", null, true);
        echo "</a>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "UserBundle:User:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 57,  167 => 51,  159 => 50,  149 => 49,  139 => 48,  131 => 43,  121 => 38,  111 => 37,  103 => 36,  95 => 35,  87 => 34,  79 => 29,  72 => 25,  64 => 20,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
