<?php

/* CoreBundle:Partial:menu.html.twig */
class __TwigTemplate_38f5afc92ba21a2c230777bd0d0d66f5d71d08ecab11e042eed4007068be4a97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav id=\"menu\" class=\"mmenu mmenu-horizontal\">
\t<ul>
\t\t<li>
\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("content_show", array("content_id" => 11, "t" => "qui-sommes-nous")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Qui sommes-nous ?"), "html", null, true);
        echo "</a>
\t\t</li>
\t\t<li class=\"text-center\" style=\"color:white;font-size:18px;\">
\t\t\t<hr>
\t\t\t";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos clientes"), "html", null, true);
        echo "
\t\t</li>
\t\t<li>
\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("content_show", array("content_id" => 15, "t" => "nos-prestations")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Liste des prestations"), "html", null, true);
        echo "</a>
\t\t</li>
\t\t<li>
\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accès réservations"), "html", null, true);
        echo "</a>
\t\t</li>
\t\t<li class=\"text-center\" style=\"color:white;font-size:18px;\">
\t\t\t<hr>
\t\t\t";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos beauty artists "), "html", null, true);
        echo "
\t\t</li>
\t\t<li>
\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("content_show", array("content_id" => 12, "t" => "pourquoi-choisir-my-beauty-booking")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pourquoi rejoindre MBB ?"), "html", null, true);
        echo "</a>
\t\t</li>
\t\t<li>
\t\t\t<a class=\"btn btn-primary\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("content_show", array("content_id" => 13, "t" => "rejoindre-notre-reseau-professionnel")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Adhérer"), "html", null, true);
        echo "</a>
\t\t</li>
\t</ul>
</nav>";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Partial:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 24,  62 => 21,  56 => 18,  47 => 14,  39 => 11,  33 => 8,  24 => 4,  19 => 1,);
    }
}
