<?php

/* ContentBundle:Content:show.html.twig */
class __TwigTemplate_cef1fb1ec78dc3dec78c50fe789e2a8948d075a7cfdd692473b2f6b4253da875 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'meta_title' => array($this, 'block_meta_title'),
            'meta_description' => array($this, 'block_meta_description'),
            'meta_keywords' => array($this, 'block_meta_keywords'),
            'facebook_metas' => array($this, 'block_facebook_metas'),
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_meta_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "metaTitle", array()), "html", null, true);
    }

    // line 4
    public function block_meta_description($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "metaDescription", array()), "html", null, true);
    }

    // line 5
    public function block_meta_keywords($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "metaKeywords", array()), "html", null, true);
    }

    // line 7
    public function block_facebook_metas($context, array $blocks = array())
    {
        // line 8
        echo "<meta property=\"og:type\" content=\"website\"/>
<meta property=\"og:title\" content=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "title", array()), "html", null, true);
        echo "\" />
<meta property=\"og:description\" content=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->truncate($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "description", array()), 200), "html", null, true);
        echo "\" />
";
        // line 11
        if ( !(null === $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "imagePrincipale", array()))) {
            // line 12
            echo "<meta property=\"og:image\" content=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "imagePrincipale", array()), "url", array()), "html", null, true);
            echo "\"/>
";
        }
    }

    // line 16
    public function block_javascripts($context, array $blocks = array())
    {
        // line 17
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fotorama/fotorama.js"), "html", null, true);
        echo "\"></script>\t\t\t
<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getGmapApi(), "html", null, true);
        echo "&signed_in=false&sensor=false&callback=initMap\" async defer></script>
";
    }

    // line 22
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 23
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/fotorama/fotorama.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
    }

    // line 27
    public function block_body_content($context, array $blocks = array())
    {
        // line 28
        echo "
<div class=\"container-fluid title title_bottom\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<h1>";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "title", array()), "html", null, true);
        echo "</h1>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">

\t<div class=\"row\">
\t\t<div class=\"col-lg-12\">
\t
\t\t\t";
        // line 44
        echo $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "html", array());
        echo "
\t\t\t
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "ContentBundle:Content:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 44,  124 => 33,  117 => 28,  114 => 27,  108 => 24,  104 => 23,  101 => 22,  95 => 19,  91 => 18,  87 => 17,  84 => 16,  76 => 12,  74 => 11,  70 => 10,  66 => 9,  63 => 8,  60 => 7,  54 => 5,  48 => 4,  42 => 3,  11 => 1,);
    }
}
