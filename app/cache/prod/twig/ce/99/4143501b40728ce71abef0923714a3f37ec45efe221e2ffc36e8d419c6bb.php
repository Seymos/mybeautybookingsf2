<?php

/* CoreBundle:Home:index.html.twig */
class __TwigTemplate_ce994143501b40728ce71abef0923714a3f37ec45efe221e2ffc36e8d419c6bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "

<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-lg-12 text-center search_promesse\">
\t\t\t";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Des prestations beauté de qualité et sur mesure, à domicile pour TOUTES les femmes"), "html", null, true);
        echo ".<br/> 
\t\t\t<a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("register_cliente");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Curieuse ? Inscrivez-vous pour tester le service"), "html", null, true);
        echo "</a> 7jours/7 de 6h à 22h
\t\t</div>
\t</div>
</div>
\t\t\t
<div class=\"container-fluid container-rose home_temp_bk\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t";
        // line 19
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("CoreBundle:Partial:search"));
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container container-blanc\">
\t<div class=\"row home-categories-a\">
\t\t<div class=\"col-md-offset-1 col-md-11 col-lg-offset-2 col-lg-10\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-sm-2 col-md-2 col-lg-2 text-center\">
\t\t\t\t\t<a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 1)), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/home/picto1.jpg"), "html", null, true);
        echo "\"></a><br/>
\t\t\t\t\t<a href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 1)), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Coiffure"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-2 col-md-2 col-lg-2 text-center\">
\t\t\t\t\t<a href=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 20)), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/home/picto2.jpg"), "html", null, true);
        echo "\"></a><br/>
\t\t\t\t\t<a href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 20)), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Maquillage et regard"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-2 col-md-2 col-lg-2 text-center\">
\t\t\t\t\t<a href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 30)), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/home/picto3.jpg"), "html", null, true);
        echo "\"></a><br/>
\t\t\t\t\t<a href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 30)), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Epilation"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-2 col-md-2 col-lg-2 text-center\">
\t\t\t\t\t<a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 46)), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/home/picto4.jpg"), "html", null, true);
        echo "\"></a><br/>
\t\t\t\t\t<a href=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 46)), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ongles"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-2 col-md-2 col-lg-2 text-center\">
\t\t\t\t\t<a href=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 47)), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/home/picto5.jpg"), "html", null, true);
        echo "\"></a><br/>
\t\t\t\t\t<a href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("home_gosearch", array("update" => 1, "search_category" => 47)), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Soins"), "html", null, true);
        echo "</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container-fluid container-rose\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12 container-vision-title\">
\t\t\t\t";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Le principe"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12 container-vision-description\">
\t\t\t\t";
        // line 63
        echo $this->env->getExtension('http_kernel')->renderFragmentStrategy("esi", $this->env->getExtension('http_kernel')->controller("ContentBundle:Partial:zone", array("id" => 25)));
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container-fluid\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12 container-vision-title container-vision-title2\">
\t\t\t\t";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Notre vision"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12 container-vision-description container-vision-description2\">
\t\t\t\t";
        // line 78
        echo $this->env->getExtension('http_kernel')->renderFragmentStrategy("esi", $this->env->getExtension('http_kernel')->controller("ContentBundle:Partial:zone", array("id" => 24)));
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container-fluid container-rose\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12 container-vision-title\">
\t\t\t\t";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nos beauty artists"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12 container-vision-description\">
\t\t\t\t";
        // line 93
        echo $this->env->getExtension('http_kernel')->renderFragmentStrategy("esi", $this->env->getExtension('http_kernel')->controller("ContentBundle:Partial:zone", array("id" => 26)));
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

";
        // line 99
        echo $this->env->getExtension('http_kernel')->renderFragmentStrategy("esi", $this->env->getExtension('http_kernel')->controller("CoreBundle:Partial:home"));
        echo "

";
        // line 101
        if ((isset($context["popup"]) ? $context["popup"] : null)) {
            // line 102
            echo "<div id=\"popupcookie\">
\t";
            // line 103
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies pour vous proposer des services adaptés à vos centres d’intérêts."), "html", null, true);
            echo " <a href=\"javascript:void(0);\" onclick=\"\$('#popupcookie').hide();\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fermer"), "html", null, true);
            echo "</a>
</div>
";
        }
        // line 106
        echo "
";
    }

    // line 110
    public function block_javascripts($context, array $blocks = array())
    {
        // line 111
        echo "<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=";
        echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getGmapApi(), "html", null, true);
        echo "&signed_in=false&libraries=places\"></script>
";
        // line 112
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script>

\$(document).ready(function(){
\t
\tinitMap();
\t
\tfunction extractFromAdress(components, type){
\t\tfor (var i=0; i<components.length; i++)
\t\t\tfor (var j=0; j<components[i].types.length; j++)
\t\t\t\tif (components[i].types[j]==type) return components[i].long_name;
\t\treturn \"\";
\t}
\t
\tfunction initMap() {
\t\tvar input = document.getElementById('search_ou');
\t    var options = {componentRestrictions: {country: \"fr\"}};
\t    var autocomplete = new google.maps.places.Autocomplete(input,options);
\t    google.maps.event.addListener(
\t\t\tautocomplete, 'place_changed', function() {
\t\t\t\tvar place = autocomplete.getPlace();
\t\t\t\tif (place.geometry){
\t\t\t\t\t\$('#search_ou_location').val(place.geometry.location);
\t\t\t\t\t\$('#search_ou_city').val(extractFromAdress(place.address_components, \"locality\"));
\t\t\t\t}
\t\t\t}
\t\t);
\t}
\t

  
});
</script>
";
    }

    public function getTemplateName()
    {
        return "CoreBundle:Home:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 112,  242 => 111,  239 => 110,  234 => 106,  226 => 103,  223 => 102,  221 => 101,  216 => 99,  207 => 93,  199 => 88,  186 => 78,  178 => 73,  165 => 63,  157 => 58,  141 => 47,  135 => 46,  127 => 43,  121 => 42,  113 => 39,  107 => 38,  99 => 35,  93 => 34,  85 => 31,  79 => 30,  65 => 19,  51 => 10,  47 => 9,  40 => 4,  37 => 3,  11 => 1,);
    }
}
