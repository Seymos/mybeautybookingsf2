<?php

/* AdminBundle:Home:infosheader.html.twig */
class __TwigTemplate_c3f780079e84df21d76e5298925e62c564af4878e2a225387c417470b3c6c7a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Utilisateurs : ";
        echo twig_escape_filter($this->env, (isset($context["nb_user"]) ? $context["nb_user"] : null), "html", null, true);
        echo "<br/>";
    }

    public function getTemplateName()
    {
        return "AdminBundle:Home:infosheader.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
