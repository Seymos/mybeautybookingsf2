<?php

/* MessageBundle:Message:nbmessages.html.twig */
class __TwigTemplate_bda1fd274a2812038e8f831353b3b779c0e427b74165d3b7359ecdc9e52c9117 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["result"]) ? $context["result"] : null) > 0)) {
            echo "<a style=\"text-decoration:none;\" href=\"";
            echo $this->env->getExtension('routing')->getPath("message_index");
            echo "\"><b>";
            echo twig_escape_filter($this->env, (isset($context["result"]) ? $context["result"] : null), "html", null, true);
            echo "</b> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("message(s) non lu(s)"), "html", null, true);
            echo "</a>";
        }
    }

    public function getTemplateName()
    {
        return "MessageBundle:Message:nbmessages.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
