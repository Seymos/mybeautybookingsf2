<?php

/* UserBundle:Form:edit.html.twig */
class __TwigTemplate_b0c63e860b06c4caae7817e1d0daae23ba2d3a4e24335d931f430935318bf744 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->getExtension('form')->renderer->setTheme((isset($context["form"]) ? $context["form"] : null), array(0 => "CoreBundle:Form:fields_errors.html.twig"));
        // line 2
        echo "
<form id=\"myform\" autocomplete=\"off\" method=\"POST\" ";
        // line 3
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'enctype');
        echo " class=\"form-horizontal col-lg-12\">
    ";
        // line 4
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
    
\t";
        // line 6
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "type", array()) == 1)) {
            // line 7
            echo "\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 10
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nom"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-9\">
\t        ";
            // line 13
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'errors');
            echo "
\t        ";
            // line 14
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prénom"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-9\">
\t        ";
            // line 24
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'errors');
            echo "
\t        ";
            // line 25
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
    <div class=\"row\">
\t    <div class=\"form-group\">
\t\t   <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-9\">
\t        ";
            // line 35
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
            echo "
\t        ";
            // line 36
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>\t
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t\t<h2 class=\"title title2 title_bottom title_top\">";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon adresse de réception"), "html", null, true);
            echo "</h2>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t  <div class=\"form-group\">
\t\t<div class=\"col-lg-3\">
\t\t\t<label>";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Votre adresse"), "html", null, true);
            echo "</label>
\t\t</div>
\t\t<div class=\"col-lg-9\">
\t\t\t";
            // line 51
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_1", array()), 'errors');
            echo "
\t\t\t";
            // line 52
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_1", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t</div>
\t  </div>
\t</div> 
\t<div class=\"row\">
\t      <div class=\"form-group\">
\t      \t<div class=\"col-lg-3\">
\t\t\t\t<label>";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Complément adresse"), "html", null, true);
            echo "</label>
\t\t    </div>
\t\t\t<div class=\"col-lg-9\">
\t\t\t\t";
            // line 62
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_2", array()), 'errors');
            echo "
\t\t\t\t";
            // line 63
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_2", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t\t</div>
\t\t  </div>
\t</div> 
\t<div class=\"row\">
\t      <div class=\"form-group\">
\t      \t<div class=\"col-lg-3\">
\t\t\t\t<label>";
            // line 70
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Code postal"), "html", null, true);
            echo "</label>
\t\t    </div>
\t\t\t<div class=\"col-lg-9\">
\t\t\t\t";
            // line 73
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'errors');
            echo "
\t\t\t\t";
            // line 74
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t\t</div>
\t\t  </div>
\t</div> 
\t<div class=\"row\">
\t      <div class=\"form-group\">
\t      \t<div class=\"col-lg-3\">
\t\t\t\t<label>";
            // line 81
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ville"), "html", null, true);
            echo "</label>
\t\t    </div>
\t\t\t<div class=\"col-lg-9\">
\t\t\t\t";
            // line 84
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'errors');
            echo "
\t\t\t\t";
            // line 85
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t\t</div>
\t\t  </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t\t<h2 class=\"title title2 title_bottom title_top\">";
            // line 91
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pour me joindre"), "html", null, true);
            echo "</h2>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t      ";
            // line 96
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Téléphone fixe")) ? array() : array("label" => $_label_)));
            echo "
\t      <div class=\"col-lg-9\">
\t        ";
            // line 98
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'errors');
            echo "
\t        ";
            // line 99
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t      ";
            // line 105
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Téléphone portable")) ? array() : array("label" => $_label_)));
            echo "
\t      <div class=\"col-lg-9\">
\t        ";
            // line 107
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'errors');
            echo "
\t        ";
            // line 108
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t
\t";
        } else {
            // line 114
            echo "\t
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t      <label class=\"col-lg-1\">";
            // line 117
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Photo"), "html", null, true);
            echo "</label>
\t      <div class=\"col-lg-2\" id=\"div_image_principale_old_img\" style=\"min-height:140px;\">
\t      \t";
            // line 119
            if ( !(null === $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()))) {
                // line 120
                echo "\t\t\t <img class=\"img-thumbnail\" id=\"image_old_img\" src=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()), "url", array()), "html", null, true);
                echo "\"/>
\t\t\t <input type=\"hidden\" id=\"image_old\" name=\"image_old\" value=\"";
                // line 121
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()), "filename", array()), "html", null, true);
                echo "\">
\t\t\t ";
                // line 122
                if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()), "filename", array()) != "no_image.png")) {
                    // line 123
                    echo "\t\t\t \t<div class=\"row\">
\t\t \t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t\t\t \t<a title=\"";
                    // line 125
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rotation à droite"), "html", null, true);
                    echo "\" href=\"javascript:void(0);\" onclick=\"rotationImg('div_image_principale_old_img','";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()), "id", array()), "html", null, true);
                    echo "','-90','1', '";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "strippedName", array()), "html", null, true);
                    echo "');\"><img style=\"width:20px;height:20px;\" src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/img-droite.png"), "html", null, true);
                    echo "\"></a>
\t\t\t\t\t\t\t&nbsp;&nbsp;
\t\t\t\t\t\t\t<a title=\"";
                    // line 127
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Supprimer"), "html", null, true);
                    echo "\" href=\"javascript:void(0);\" onclick=\"\$('#image_principale_old').val('');\$('#image_principale_old_img').hide();\$(this).hide();\"><img style=\"width:15px;height:15px;\" src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/image-supprimer.jpg"), "html", null, true);
                    echo "\"></a>
\t\t\t\t\t\t\t&nbsp;&nbsp;
\t\t\t\t\t\t\t<a title=\"";
                    // line 129
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rotation à gauche"), "html", null, true);
                    echo "\" href=\"javascript:void(0);\" onclick=\"rotationImg('div_image_principale_old_img','";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "image", array()), "id", array()), "html", null, true);
                    echo "','90','1', '";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "strippedName", array()), "html", null, true);
                    echo "');\"><img style=\"width:20px;height:20px;\" src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/img-gauche.png"), "html", null, true);
                    echo "\"></a>
\t\t\t\t\t</div>
\t\t\t\t </div>
\t\t\t ";
                }
                // line 133
                echo "\t\t\t";
            }
            // line 134
            echo "\t\t  </div>
\t      <div class=\"col-lg-5\">
\t        <div id=\"upload_image_div\" class=\"dropzone text-center\">
\t\t\t    <div class=\"dz-default dz-message\">
\t\t\t\t\t<p>";
            // line 138
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cliquez ici pour ajouter votre photo."), "html", null, true);
            echo "</p>
\t\t\t\t</div>
\t\t\t</div>
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 147
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Je suis"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-8\">
\t        ";
            // line 150
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "genre", array()), 'errors');
            echo "
\t        ";
            // line 151
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "genre", array()), 'widget');
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 158
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Métier"), "html", null, true);
            echo "</label>
\t\t  </div>
\t      <div class=\"col-lg-8\">
\t\t\t";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "metiersLabel", array()), "html", null, true);
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 168
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Statut"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-8\">
\t        ";
            // line 171
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "statut", array()), 'errors');
            echo "
\t        ";
            // line 172
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "statut", array()), 'widget');
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 179
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nom"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-5\">
\t        ";
            // line 182
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'errors');
            echo "
\t        ";
            // line 183
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "first_name", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t  <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 190
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Prénom"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-5\">
\t        ";
            // line 193
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'errors');
            echo "
\t        ";
            // line 194
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "last_name", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t   <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 201
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t      <div class=\"col-lg-5\">
\t        ";
            // line 204
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
            echo "
\t        ";
            // line 205
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t\t<h2 class=\"title title2 title_bottom title_top\">";
            // line 211
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Votre entreprise"), "html", null, true);
            echo "</h2>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t  <div class=\"col-lg-3\">
\t\t\t<label>";
            // line 217
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nom de l'entreprise"), "html", null, true);
            echo "</label> <b class=\"form-obligatoire\">*</b>
\t\t  </div>
\t\t  <div class=\"col-lg-5\">
\t\t\t";
            // line 220
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entreprise", array()), 'errors');
            echo "
\t\t\t";
            // line 221
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "entreprise", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t  </div>
\t\t  <div class=\"col-lg-4\">
\t\t\t";
            // line 224
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nom de l'entreprise ou raison sociale"), "html", null, true);
            echo "
\t\t  </div>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t      <div class=\"form-group\">
\t      \t<div class=\"col-lg-3\">
\t\t\t\t<label>";
            // line 231
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Votre adresse"), "html", null, true);
            echo "</label>
\t\t    </div>
\t\t\t<div class=\"col-lg-5\">
\t\t\t\t";
            // line 234
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_1", array()), 'errors');
            echo "
\t\t\t\t";
            // line 235
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_1", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t\t</div>
\t\t  </div>
\t</div> 
\t<div class=\"row\">
\t      <div class=\"form-group\">
\t      \t<div class=\"col-lg-3\">
\t\t\t\t<label>";
            // line 242
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Complément adresse"), "html", null, true);
            echo "</label>
\t\t    </div>
\t\t\t<div class=\"col-lg-5\">
\t\t\t\t";
            // line 245
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_2", array()), 'errors');
            echo "
\t\t\t\t";
            // line 246
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "address_2", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t\t</div>
\t\t  </div>
\t</div> 
\t<div class=\"row\">
\t      <div class=\"form-group\">
\t      \t<div class=\"col-lg-3\">
\t\t\t\t<label>";
            // line 253
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Code postal"), "html", null, true);
            echo "</label>
\t\t    </div>
\t\t\t<div class=\"col-lg-5\">
\t\t\t\t";
            // line 256
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'errors');
            echo "
\t\t\t\t";
            // line 257
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "postal_code", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t\t</div>
\t\t  </div>
\t</div> 
\t<div class=\"row\">
\t      <div class=\"form-group\">
\t      \t<div class=\"col-lg-3\">
\t\t\t\t<label>";
            // line 264
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ville"), "html", null, true);
            echo "</label>
\t\t    </div>
\t\t\t<div class=\"col-lg-5\">
\t\t\t\t";
            // line 267
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'errors');
            echo "
\t\t\t\t";
            // line 268
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "city", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t\t</div>
\t\t  </div>
\t</div> 
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t\t<h2 class=\"title title2 title_bottom title_top\">";
            // line 274
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Zone d'intervention"), "html", null, true);
            echo "</h2>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"col-lg-12\">
\t      <div class=\"form-group\">
      \t    ";
            // line 280
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "gmap_lat", array()), 'widget');
            echo "
      \t    ";
            // line 281
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "gmap_lng", array()), 'widget');
            echo "
\t        ";
            // line 282
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Précisez le centre de votre zone d'intervention. Saisissez une adresse ou déplacez le curseur sur la carte"), "html", null, true);
            echo "
\t\t\t<input id=\"pac-input\" name=\"pac-input\" onkeypress=\"refuserToucheEntree(event)\" class=\"form-control\" type=\"text\" placeholder=\"";
            // line 283
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Saisissez votre adresse"), "html", null, true);
            echo "\" value=\"\">
\t\t  </div>
\t    </div>
\t</div> 
\t<div class=\"row\">
\t    <div class=\"col-lg-12\">
\t      <div class=\"form-group\">
\t\t    <div id=\"map\" style=\"height:300px;\"></div>
\t\t  </div>
\t    </div>
\t</div> 
\t<div class=\"row\">
\t      <div class=\"form-group\">
\t      \t<div class=\"col-lg-3\">
\t\t\t\t<label>";
            // line 297
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rayon d'intervention"), "html", null, true);
            echo "</label>
\t\t    </div>
\t\t\t<div class=\"col-lg-9\">
\t\t\t\t";
            // line 300
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "distance", array()), 'errors');
            echo "
\t\t\t\t";
            // line 301
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "distance", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t\t</div>
\t\t  </div>
\t</div>
\t
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t\t\t<h2 class=\"title title2 title_bottom title_top\">";
            // line 308
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Présentation de votre entreprise"), "html", null, true);
            echo "</h2>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t     <div class=\"col-lg-3\">
\t\t\t    <label>";
            // line 314
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Qui êtes vous ?"), "html", null, true);
            echo "</label>
\t\t    </div>
\t    \t<div class=\"col-lg-9\">
\t      \t\t";
            // line 317
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'errors');
            echo "
\t        \t";
            // line 318
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "description", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => 5, "style" => "height:150px !important;", "placeholder" => $this->env->getExtension('translator')->trans("Quelques mots à propos de vous"))));
            echo "
\t        </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t      ";
            // line 324
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Téléphone fixe")) ? array() : array("label" => $_label_)));
            echo "
\t      <div class=\"col-lg-5\">
\t        ";
            // line 326
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'errors');
            echo "
\t        ";
            // line 327
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "phone", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t      ";
            // line 333
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("Téléphone portable")) ? array() : array("label" => $_label_)));
            echo "
\t      <div class=\"col-lg-5\">
\t        ";
            // line 335
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'errors');
            echo "
\t        ";
            // line 336
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "mobile", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t<div class=\"row\">
\t    <div class=\"form-group\">
\t      ";
            // line 342
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "siren", array()), 'label', array("label_attr" => array("class" => "col-lg-3")) + (twig_test_empty($_label_ = $this->env->getExtension('translator')->trans("SIRET")) ? array() : array("label" => $_label_)));
            echo "
\t      <div class=\"col-lg-5\">
\t        ";
            // line 344
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "siren", array()), 'errors');
            echo "
\t        ";
            // line 345
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "siren", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t      </div>
\t    </div>
\t</div>
\t
\t";
        }
        // line 351
        echo "\t<div class=\"row\">
\t  <div class=\"form-group\">
\t\t<div class=\"col-lg-3\">
\t\t\t<label>";
        // line 354
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Newsletter"), "html", null, true);
        echo "</label>
\t\t</div>
\t\t<div class=\"col-lg-9\">
\t\t\t";
        // line 357
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "newsletter", array()), 'widget');
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Je souhaite recevoir la newsletter"), "html", null, true);
        echo "<br/>
\t\t</div>
\t  </div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"form-group\">
\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t<button class=\"btn btn-primary btn-lg\"><span class=\"glyphicon glyphicon-ok-sign\"></span> ";
        // line 364
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enregistrer"), "html", null, true);
        echo "</button>
\t\t   </div>
\t\t</div>
\t</div>
";
        // line 368
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "_token", array()), 'widget');
        echo "
</form>";
    }

    public function getTemplateName()
    {
        return "UserBundle:Form:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  709 => 368,  702 => 364,  690 => 357,  684 => 354,  679 => 351,  670 => 345,  666 => 344,  661 => 342,  652 => 336,  648 => 335,  643 => 333,  634 => 327,  630 => 326,  625 => 324,  616 => 318,  612 => 317,  606 => 314,  597 => 308,  587 => 301,  583 => 300,  577 => 297,  560 => 283,  556 => 282,  552 => 281,  548 => 280,  539 => 274,  530 => 268,  526 => 267,  520 => 264,  510 => 257,  506 => 256,  500 => 253,  490 => 246,  486 => 245,  480 => 242,  470 => 235,  466 => 234,  460 => 231,  450 => 224,  444 => 221,  440 => 220,  434 => 217,  425 => 211,  416 => 205,  412 => 204,  406 => 201,  396 => 194,  392 => 193,  386 => 190,  376 => 183,  372 => 182,  366 => 179,  356 => 172,  352 => 171,  346 => 168,  336 => 161,  330 => 158,  320 => 151,  316 => 150,  310 => 147,  298 => 138,  292 => 134,  289 => 133,  276 => 129,  269 => 127,  258 => 125,  254 => 123,  252 => 122,  248 => 121,  243 => 120,  241 => 119,  236 => 117,  231 => 114,  222 => 108,  218 => 107,  213 => 105,  204 => 99,  200 => 98,  195 => 96,  187 => 91,  178 => 85,  174 => 84,  168 => 81,  158 => 74,  154 => 73,  148 => 70,  138 => 63,  134 => 62,  128 => 59,  118 => 52,  114 => 51,  108 => 48,  99 => 42,  90 => 36,  86 => 35,  80 => 32,  70 => 25,  66 => 24,  60 => 21,  50 => 14,  46 => 13,  40 => 10,  35 => 7,  33 => 6,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
