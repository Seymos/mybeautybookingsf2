<?php

/* AdminBundle:User:edit.html.twig */
class __TwigTemplate_b9dddb2335b1f5999505f6a997c8626795a638f81cdfa319aad6095ee14ead45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AdminBundle::admin.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
\t<script>
\t\$(document).ready(function () {
\t\t
\t\tDropzone.autoDiscover = false; // otherwise will be initialized twice
\t\tvar myDropzoneOptions = {
\t\t    url: \"";
        // line 11
        echo $this->env->getExtension('routing')->getUrl("upload_media", array("resize" => "1_250_250_2_800_600"));
        echo "\",
\t\t    acceptedFiles: \"image/jpeg,image/png,image/gif\",
\t        addRemoveLinks: true,
\t        dictRemoveFile : \"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Retirer cette photo"), "html", null, true);
        echo "\",
\t        dictCancelUploadConfirmation : \"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Êtes-vous sûr de vouloir annuler le transfert"), "html", null, true);
        echo "\",
\t        dictCancelUpload : \"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Annuler le transfert"), "html", null, true);
        echo "\",
\t        autoProcessQueue: true,
\t\t    uploadMultiple: false,
\t\t    parallelUploads: 1,
\t\t    maxFiles: 1,
\t\t    maxFilesize: 20, // MB
\t        success: function (file, response) {
\t            var imgName = response['name'];
\t            var input_hidden = Dropzone.createElement(\"<input type='hidden' name='image' value='\"+imgName+\"'>\");
\t            
\t            file.previewElement.classList.add(\"dz-success\");
      \t\t\tfile.previewElement.appendChild(input_hidden);
\t        },
\t        error: function (file, response) {
\t            file.previewElement.classList.add(\"dz-error\");
\t        },
\t        init: function() {
\t\t\t    this.on(\"addedfile\", function() {
\t\t\t      if (this.files[1]!=null){
\t\t\t        this.removeFile(this.files[0]);
\t\t\t      }
\t\t\t    });
\t\t\t}
\t\t};
\t\tvar myDropzone = new Dropzone('#upload_image_div', myDropzoneOptions);
\t\t
\t\tDropzone.autoDiscover = false; // otherwise will be initialized twice
\t\tvar myDropzoneOptions = {
\t\t    url: \"";
        // line 44
        echo $this->env->getExtension('routing')->getUrl("upload_media", array("resize" => "1_80_80_2_800_600"));
        echo "\",
\t\t    acceptedFiles: \"image/jpeg,image/png,image/gif\",
\t        addRemoveLinks: true,
\t        dictRemoveFile : \"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Retirer cette photo"), "html", null, true);
        echo "\",
\t        dictCancelUploadConfirmation : \"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Êtes-vous sûr de vouloir annuler le transfert"), "html", null, true);
        echo "\",
\t        dictCancelUpload : \"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Annuler le transfert"), "html", null, true);
        echo "\",
\t        autoProcessQueue: true,
\t\t    uploadMultiple: false,
\t\t    parallelUploads: 3,
\t\t    //maxFiles: 1,
\t\t    maxFilesize: 20, // MB
\t        success: function (file, response) {
\t            var imgName = response['name'];
\t            var input_hidden = Dropzone.createElement(\"<input type='hidden' name='images[]' value='\"+imgName+\"'>\");
\t            
\t            file.previewElement.classList.add(\"dz-success\");
      \t\t\tfile.previewElement.appendChild(input_hidden);
\t        },
\t        error: function (file, response) {
\t            file.previewElement.classList.add(\"dz-error\");
\t        },
\t\t};
\t\tvar myDropzone = new Dropzone('#upload_images_div', myDropzoneOptions);

\t});
\t</script>
\t
\t\t";
        // line 71
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "type", array()) == 2)) {
            // line 72
            echo "\t\t<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=";
            echo twig_escape_filter($this->env, $this->env->getExtension('FAPROD.TwigExtension')->getGmapApi(), "html", null, true);
            echo "&signed_in=false&libraries=places&callback=initMap\" async defer></script>
\t\t<script>
\t\t
\t\tfunction updatePosition(lat, lng){
\t\t\tvar input_lat = document.getElementById('faprod_userbundle_user_gmap_lat');
\t\t\tvar input_lng = document.getElementById('faprod_userbundle_user_gmap_lng');
\t\t\t
\t\t\tinput_lat.value = lat;
\t\t\tinput_lng.value = lng;
\t\t}

\t\tfunction initMap(lat = '', lng = '') {

\t\t\tif (lat == ''){
\t\t\t\tlat = ";
            // line 86
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "lat", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "lat", array()), "html", null, true);
            } else {
                echo "47.445908";
            }
            echo ";
\t\t\t}
\t\t\t   
\t\t\tif (lng == ''){
\t\t\t\tlng = ";
            // line 90
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "lng", array())) {
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "value", array()), "lng", array()), "html", null, true);
            } else {
                echo "2.615234";
            }
            echo ";
\t\t\t}


\t\t\tvar map = new google.maps.Map(document.getElementById('map'), {
\t\t\t\tcenter: {lat: Number(lat), lng: Number(lng) },
\t\t\t\tzoom: 13,
\t\t\t});
\t\t\tvar input = document.getElementById('pac-input');
\t\t
\t\t\tvar autocomplete = new google.maps.places.Autocomplete(input);
\t\t\tautocomplete.bindTo('bounds', map);
\t\t
\t\t\tvar center = map.center;
\t\t  
\t\t\tvar marker = new google.maps.Marker({
\t\t\t\tposition: center,
\t\t\t\tdraggable: true,
\t\t\t\tmap: map
\t\t\t});
\t\t
\t\t\tmarker.setIcon(/** @type {google.maps.Icon} */({
\t\t      url: '";
            // line 112
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/map.png"), "html", null, true);
            echo "',
\t\t      size: new google.maps.Size(62, 70),
\t\t      origin: new google.maps.Point(0, 0),
\t\t      anchor: new google.maps.Point(0, 35),
\t\t      scaledSize: new google.maps.Size(31, 35)
\t\t    }));
\t\t    marker.setVisible(true);
\t\t\tmarker.setDraggable(true);
\t\t\t
\t\t\tgoogle.maps.event.addListener(marker, 'dragend', function () {
\t\t\t\tmap.setCenter(this.getPosition());
\t\t\t\tupdatePosition(this.getPosition().lat(), this.getPosition().lng());
\t\t\t});
\t\t  
\t\t
\t\t\tautocomplete.addListener('place_changed', function() {

\t\t    marker.setVisible(false);
\t\t    var place = autocomplete.getPlace();
\t\t    if (!place.geometry) {
\t\t      window.alert(\"";
            // line 132
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Localisation introuvable"), "html", null, true);
            echo "\");
\t\t      return;
\t\t    } else {
\t\t\t\t\$('#faprod_userbundle_user_gmap_lat').val(place.geometry.location.lat());
\t\t\t\t\$('#faprod_userbundle_user_gmap_lng').val(place.geometry.location.lng());
\t\t\t\t\$('#faprod_userbundle_user_gmap_place_id').val(place.place_id);
\t\t\t\t\$('#faprod_userbundle_user_gmap_place_name').val(place.formatted_address);
\t\t    }
\t\t
\t\t    // If the place has a geometry, then present it on a map.
\t\t    if (place.geometry.viewport) {
\t\t      map.fitBounds(place.geometry.viewport);
\t\t    } else {
\t\t      map.setCenter(place.geometry.location);
\t\t      map.setZoom(13);  // Why 17? Because it looks good.
\t\t    }
\t\t    marker.setIcon(/** @type {google.maps.Icon} */({
\t\t      url: '";
            // line 149
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/template/map.png"), "html", null, true);
            echo "',
\t\t      size: new google.maps.Size(62, 70),
\t\t      origin: new google.maps.Point(0, 0),
\t\t      anchor: new google.maps.Point(0, 35),
\t\t      scaledSize: new google.maps.Size(31, 35)
\t\t    }));
\t\t    marker.setPosition(place.geometry.location);
\t\t    marker.setVisible(true);
\t\t\tmarker.setDraggable(true);
\t\t\t
\t\t\tgoogle.maps.event.addListener(marker, 'dragend', function () {
\t\t\t\tmap.setCenter(this.getPosition());
\t\t\t\tupdatePosition(this.getPosition().lat(), this.getPosition().lng());
\t\t\t});
\t\t
\t\t  });
\t\t}
\t\t
\t\t

\t\t\t
\t\t\t
\t\t\tfunction refuserToucheEntree(event){
\t\t\t\t// Compatibilité IE / Firefox
\t\t\t\tif(!event && window.event) {
\t\t\t\t\tevent = window.event;
\t\t\t\t}
\t\t\t\t// IE
\t\t\t\tif(event.keyCode == 13) {
\t\t\t\t\tevent.returnValue = false;
\t\t\t\t\tevent.cancelBubble = true;
\t\t\t\t}
\t\t\t\t// DOM
\t\t\t\tif(event.which == 13) {
\t\t\t\t\tevent.preventDefault();
\t\t\t\t\tevent.stopPropagation();
\t\t\t\t}
\t\t\t}

\t\t</script>
\t\t";
        }
        // line 190
        echo "\t
";
    }

    // line 193
    public function block_body($context, array $blocks = array())
    {
        // line 194
        echo "
  <h1>Modifier un Membre</h1>

  ";
        // line 197
        echo twig_include($this->env, $context, "AdminBundle:User:form.html.twig");
        echo "

";
    }

    public function getTemplateName()
    {
        return "AdminBundle:User:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  298 => 197,  293 => 194,  290 => 193,  285 => 190,  241 => 149,  221 => 132,  198 => 112,  169 => 90,  158 => 86,  140 => 72,  138 => 71,  113 => 49,  109 => 48,  105 => 47,  99 => 44,  68 => 16,  64 => 15,  60 => 14,  54 => 11,  45 => 5,  40 => 4,  37 => 3,  11 => 1,);
    }
}
