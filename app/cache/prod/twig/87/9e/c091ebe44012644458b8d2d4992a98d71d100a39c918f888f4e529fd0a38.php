<?php

/* UserBundle:Agenda:index.html.twig */
class __TwigTemplate_879ec091ebe44012644458b8d2d4992a98d71d100a39c918f888f4e529fd0a38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t<a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("home_index");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
        echo "</a> > <a href=\"";
        echo $this->env->getExtension('routing')->getPath("user_index");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
        echo "</a> > ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon planning"), "html", null, true);
        echo "
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"hidden-xs col-sm-3 col-md-3 col-lg-3\">
\t\t\t";
        // line 19
        echo twig_include($this->env, $context, "UserBundle:Partial:menu.html.twig");
        echo "
\t\t</div>
\t\t<div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9\">
\t\t\t
\t\t\t<div class=\"panel panel-default\">
\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t    <h3 class=\"panel-title\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon planning"), "html", null, true);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t";
        // line 31
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('http_kernel')->controller("UserBundle:Agenda:getagenda"));
        echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<form id=\"myform\" autocomplete=\"off\" method=\"POST\" class=\"form-horizontal col-lg-12\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<h2 class=\"title title2 title_top\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Vos horaires d'ouverture"), "html", null, true);
        echo "</h2>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t  <div class=\"form-group\">
\t\t\t\t\t\t<div class=\"col-lg-9\">
\t\t\t\t\t\t\t<ul class=\"horaires\">
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"left\">Lundi</div>
\t\t\t\t\t\t\t\t\t";
        // line 47
        $this->env->loadTemplate("UserBundle:Form:horaire.html.twig")->display(array_merge($context, array("jour" => 1, "horaires" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "horairesLundi", array()))));
        // line 48
        echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"left\">Mardi</div>
\t\t\t\t\t\t\t\t\t";
        // line 51
        $this->env->loadTemplate("UserBundle:Form:horaire.html.twig")->display(array_merge($context, array("jour" => 2, "horaires" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "horairesMardi", array()))));
        // line 52
        echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"left\">Mercredi</div>
\t\t\t\t\t\t\t\t\t";
        // line 55
        $this->env->loadTemplate("UserBundle:Form:horaire.html.twig")->display(array_merge($context, array("jour" => 3, "horaires" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "horairesMercredi", array()))));
        // line 56
        echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"left\">Jeudi</div>
\t\t\t\t\t\t\t\t\t";
        // line 59
        $this->env->loadTemplate("UserBundle:Form:horaire.html.twig")->display(array_merge($context, array("jour" => 4, "horaires" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "horairesJeudi", array()))));
        // line 60
        echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"left\">Vendredi</div>
\t\t\t\t\t\t\t\t\t";
        // line 63
        $this->env->loadTemplate("UserBundle:Form:horaire.html.twig")->display(array_merge($context, array("jour" => 5, "horaires" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "horairesVendredi", array()))));
        // line 64
        echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<div class=\"left\">Samedi</div>
\t\t\t\t\t\t\t\t\t";
        // line 67
        $this->env->loadTemplate("UserBundle:Form:horaire.html.twig")->display(array_merge($context, array("jour" => 6, "horaires" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "horairesSamedi", array()))));
        // line 68
        echo "\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class='last'>
\t\t\t\t\t\t\t\t\t<div class=\"left\">Dimanche</div>
\t\t\t\t\t\t\t\t\t";
        // line 71
        $this->env->loadTemplate("UserBundle:Form:horaire.html.twig")->display(array_merge($context, array("jour" => 7, "horaires" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "horairesDimanche", array()))));
        // line 72
        echo "\t\t\t\t\t\t\t\t</li>\t\t\t
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t<script>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\$(function(){\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\$(\"a.del_tranche\").click(function(){
\t\t\t\t\t\t\t\t\t\t\tvar jour = \$(this).parent().parent().parent().attr('data-placeholder');
\t\t\t\t\t\t\t\t\t\t\t\$('input#cnt_'+jour).val( parseInt(\$('input#cnt_'+jour).val()) - parseInt(1));
\t\t\t\t\t\t\t\t\t\t\tif(\$('input#cnt_'+jour).val()==0) \$('div.closedbox_'+jour).css('display','inline');
\t\t\t\t\t\t\t\t\t\t\t\$('div.tranche_'+jour+'_'+\$(this).attr('data-placeholder')).css('display','none');
\t\t\t\t\t\t\t\t\t\t\t\$('div.tranche_'+jour+'_'+\$(this).attr('data-placeholder')+' select').val('00');
\t\t\t\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\$(\"a.add_tranche\").click(function(){
\t\t\t\t\t\t\t\t\t\t\tvar jour = \$(this).attr('data-placeholder');\t
\t\t\t\t\t\t\t\t\t\t\tvar found = 0;\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\tif(\$('input#cnt_'+jour).val() < 5)
\t\t\t\t\t\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\t\t\t\t\tfor(i=0;i<5;i++) {
\t\t\t\t\t\t\t\t\t\t\t\t\t\tif(\$('div.tranche_'+jour+'_'+i).css('display') == 'none' && found == 0) {
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\$('div.tranche_'+jour+'_'+i).css('display','block');
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tvar found = 1;
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t\t}\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\$('input#cnt_'+jour).val( parseInt(\$('input#cnt_'+jour).val()) + parseInt(1));
\t\t\t\t\t\t\t\t\t\t\t\tif(\$('input#cnt_'+jour).val()==1) \$('div.closedbox_'+jour).css('display','none');
\t\t\t\t\t\t\t\t\t\t\t\t//alert(\$('input#cnt_'+jour).val());
\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t\t\t\t});\t\t\t\t
\t\t\t\t\t\t\t\t});

\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"col-lg-12 text-center\">
\t\t\t\t\t\t\t\t<button class=\"btn btn-primary btn-lg\"><span class=\"glyphicon glyphicon-ok-sign\"></span> ";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enregistrer"), "html", null, true);
        echo "</button>
\t\t\t\t\t\t   </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t</form>

\t\t    \t\t
\t\t    \t</div>
\t\t\t</div>
\t\t    
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "UserBundle:Agenda:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 113,  151 => 72,  149 => 71,  144 => 68,  142 => 67,  137 => 64,  135 => 63,  130 => 60,  128 => 59,  123 => 56,  121 => 55,  116 => 52,  114 => 51,  109 => 48,  107 => 47,  95 => 38,  85 => 31,  76 => 25,  67 => 19,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
