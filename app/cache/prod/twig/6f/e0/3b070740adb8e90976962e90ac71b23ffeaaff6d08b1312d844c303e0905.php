<?php

/* BookingBundle:Booking:index.html.twig */
class __TwigTemplate_6fe03b070740adb8e90976962e90ac71b23ffeaaff6d08b1312d844c303e0905 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("CoreBundle::layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body_content' => array($this, 'block_body_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "CoreBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"container-fluid railway\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-12\">
\t\t\t\t";
        // line 9
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 2)) {
            // line 10
            echo "\t\t\t\t<a href=\"";
            echo $this->env->getExtension('routing')->getPath("home_index");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
            echo "</a> > <a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_index");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon espace Pro"), "html", null, true);
            echo "</a> > ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
            echo "
\t\t\t\t";
        } else {
            // line 12
            echo "\t\t\t\t<a href=\"";
            echo $this->env->getExtension('routing')->getPath("home_index");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accueil"), "html", null, true);
            echo "</a> > <a href=\"";
            echo $this->env->getExtension('routing')->getPath("user_show");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mon profil"), "html", null, true);
            echo "</a> > ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
            echo "
\t\t\t\t";
        }
        // line 14
        echo "\t\t\t</div>
\t\t</div>
\t</div>
</div>

<div class=\"container\">
\t
\t<div class=\"row\">
\t\t<div class=\"hidden-xs col-sm-3 col-md-3 col-lg-3\">
\t\t\t";
        // line 23
        echo twig_include($this->env, $context, "UserBundle:Partial:menu.html.twig");
        echo "
\t\t</div>
\t\t<div class=\"col-xs-12 col-sm-9 col-md-9 col-lg-9\">
\t\t\t
\t\t\t<div class=\"panel panel-default\">
\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t    <h3 class=\"panel-title\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mes bookings"), "html", null, true);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-lg-12 box\">
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        // line 36
        if (twig_length_filter($this->env, (isset($context["liste"]) ? $context["liste"] : null))) {
            // line 37
            echo "\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t\t\t\t<table class=\"table\">
\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t<tr class=\"entete\">
\t\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t\t<th>Prestation</th>
\t\t\t\t\t\t\t\t\t\t\t";
            // line 44
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1)) {
                // line 45
                echo "\t\t\t\t\t\t\t\t\t\t\t<th>Pro</th>
\t\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 47
                echo "\t\t\t\t\t\t\t\t\t\t\t<th>Cliente</th>
\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 49
            echo "\t\t\t\t\t\t\t\t\t\t\t<th>Date</th>
\t\t\t\t\t\t\t\t\t\t\t<th>Heure</th>
\t\t\t\t\t\t\t\t\t\t\t<th>Prix</th>
\t\t\t\t\t\t\t\t\t\t\t<th>Statut</th>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t";
            // line 56
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["liste"]) ? $context["liste"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["booking"]) {
                // line 57
                echo "\t\t\t\t\t\t\t\t\t\t\t<tr class=\"";
                if (($this->getAttribute($context["booking"], "statut", array()) == 2)) {
                    echo "item-booking-avalider";
                }
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<td><a href=\"";
                // line 58
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("booking_show", array("booking_id" => $this->getAttribute($context["booking"], "id", array()))), "html", null, true);
                echo "\"><img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/loupe3.png"), "html", null, true);
                echo "\"></a></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                // line 59
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "offer", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 60
                if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "type", array()) == 1)) {
                    // line 61
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                    if ($this->getAttribute($context["booking"], "seller", array())) {
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["booking"], "seller", array()), "shortUsername", array()), "html", null, true);
                    }
                    echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 63
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                    if ($this->getAttribute($context["booking"], "user", array())) {
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["booking"], "user", array()), "shortUsername", array()), "html", null, true);
                    }
                    echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 65
                echo "\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "dateRdv", array(0 => "d/m/Y"), "method"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                // line 66
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "heureRdvLabel", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:100px;\">";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "priceTtcLabel", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute($context["booking"], "statutLabel", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['booking'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "\t\t\t\t\t\t\t\t\t</tbody>  
\t\t\t\t\t\t\t\t</table>\t

\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        } else {
            // line 76
            echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-12\">
\t\t\t\t\t\t\t\t\t\t";
            // line 79
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aucun booking pour le moment"), "html", null, true);
            echo ".<br/><br/>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        }
        // line 84
        echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        // line 85
        $this->env->loadTemplate("CoreBundle::pagination.html.twig")->display(array_merge($context, array("nbPages" => (isset($context["nbPages"]) ? $context["nbPages"] : null), "page" => (isset($context["page"]) ? $context["page"] : null), "sort" => (isset($context["sort"]) ? $context["sort"] : null), "sens" => (isset($context["sens"]) ? $context["sens"] : null), "action" => (isset($context["action"]) ? $context["action"] : null))));
        // line 86
        echo "\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t    \t</div>
\t\t\t</div>
\t\t    
\t\t</div>
\t</div>

</div>
";
    }

    public function getTemplateName()
    {
        return "BookingBundle:Booking:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 86,  220 => 85,  217 => 84,  209 => 79,  204 => 76,  197 => 71,  188 => 68,  184 => 67,  180 => 66,  175 => 65,  167 => 63,  159 => 61,  157 => 60,  153 => 59,  147 => 58,  140 => 57,  136 => 56,  127 => 49,  123 => 47,  119 => 45,  117 => 44,  108 => 37,  106 => 36,  96 => 29,  87 => 23,  76 => 14,  62 => 12,  48 => 10,  46 => 9,  39 => 4,  36 => 3,  11 => 1,);
    }
}
