<?php

/* UserBundle:Form:heuremin.html.twig */
class __TwigTemplate_1329dfe223ce87eb5cbe6906aec751c8cf87d9af183d09564e9ed16c61c5190d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["creneau"] = ((isset($context["creneau"]) ? $context["creneau"] : null) - 1);
        // line 2
        if (((twig_length_filter($this->env, (isset($context["horaires"]) ? $context["horaires"] : null)) > (isset($context["creneau"]) ? $context["creneau"] : null)) &&  !(null === $this->getAttribute((isset($context["horaires"]) ? $context["horaires"] : null), (isset($context["creneau"]) ? $context["creneau"] : null), array(), "array")))) {
            // line 3
            $context["heure_deb"] = $this->getAttribute($this->getAttribute((isset($context["horaires"]) ? $context["horaires"] : null), (isset($context["creneau"]) ? $context["creneau"] : null), array(), "array"), "heureDeb", array());
            // line 4
            $context["minute_deb"] = $this->getAttribute($this->getAttribute((isset($context["horaires"]) ? $context["horaires"] : null), (isset($context["creneau"]) ? $context["creneau"] : null), array(), "array"), "minuteDeb", array());
            // line 5
            $context["heure_fin"] = $this->getAttribute($this->getAttribute((isset($context["horaires"]) ? $context["horaires"] : null), (isset($context["creneau"]) ? $context["creneau"] : null), array(), "array"), "heureFin", array());
            // line 6
            $context["minute_fin"] = $this->getAttribute($this->getAttribute((isset($context["horaires"]) ? $context["horaires"] : null), (isset($context["creneau"]) ? $context["creneau"] : null), array(), "array"), "minuteFin", array());
        } else {
            // line 8
            $context["heure_deb"] = "";
            // line 9
            $context["minute_deb"] = "";
            // line 10
            $context["heure_fin"] = "";
            // line 11
            $context["minute_fin"] = "";
        }
        // line 13
        echo "<div>
<b>De</b> 
\t<select name=\"jour_";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["jour"]) ? $context["jour"] : null), "html", null, true);
        echo "_deb_h[]\" class=\"normal\">
\t\t<option value=\"\"></option>
\t\t";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('FAPROD.TwigExtension')->getHeures());
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 18
            echo "\t\t<option ";
            if (((isset($context["heure_deb"]) ? $context["heure_deb"] : null) == $context["value"])) {
                echo "selected=selected";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "h</option>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "\t</select> 
\t<select name=\"jour_";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["jour"]) ? $context["jour"] : null), "html", null, true);
        echo "_deb_m[]\" class=\"normal\">
\t\t<option value=\"\"></option>
\t\t";
        // line 23
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('FAPROD.TwigExtension')->getMinutes());
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 24
            echo "\t\t<option ";
            if (((isset($context["minute_deb"]) ? $context["minute_deb"] : null) == $context["value"])) {
                echo "selected=selected";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "</option>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "\t</select> 
<b>à</b> 
\t<select name=\"jour_";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["jour"]) ? $context["jour"] : null), "html", null, true);
        echo "_fin_h[]\" class=\"normal\">
\t\t<option value=\"\"></option>
\t\t";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('FAPROD.TwigExtension')->getHeures());
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 31
            echo "\t\t<option ";
            if (((isset($context["heure_fin"]) ? $context["heure_fin"] : null) == $context["value"])) {
                echo "selected=selected";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "h</option>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "\t</select> 
\t<select name=\"jour_";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["jour"]) ? $context["jour"] : null), "html", null, true);
        echo "_fin_m[]\" class=\"normal\">
\t\t<option value=\"\"></option>
\t\t";
        // line 36
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('FAPROD.TwigExtension')->getMinutes());
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 37
            echo "\t\t<option ";
            if (((isset($context["minute_fin"]) ? $context["minute_fin"] : null) == $context["value"])) {
                echo "selected=selected";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "</option>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "\t</select> 
</div>";
    }

    public function getTemplateName()
    {
        return "UserBundle:Form:heuremin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 39,  136 => 37,  132 => 36,  127 => 34,  124 => 33,  109 => 31,  105 => 30,  100 => 28,  96 => 26,  81 => 24,  77 => 23,  72 => 21,  69 => 20,  54 => 18,  50 => 17,  45 => 15,  41 => 13,  38 => 11,  36 => 10,  34 => 9,  32 => 8,  29 => 6,  27 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }
}
