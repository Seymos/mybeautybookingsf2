<?php

/* AdminBundle:User:show.html.twig */
class __TwigTemplate_f9dfc6e6e3e042e9e3f2b5178bc4722ab43cee494ba2acf3b9a78d7473ca8895 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("AdminBundle::admin.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AdminBundle::admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
<h1>Details Membre</h1>

<div class=\"form_admin form_admin2\">
<table>
  <tbody>
  \t<tr>
      <th>Connexion:</th>
      <td><a target=\"_blank\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("register_autologin", array("p1" => $this->env->getExtension('FAPROD.TwigExtension')->base64Encode($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array())), "p2" => $this->env->getExtension('FAPROD.TwigExtension')->base64Encode($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array())), "p3" => $this->env->getExtension('FAPROD.TwigExtension')->base64Encode($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "password", array())))), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/player_play.png"), "html", null, true);
        echo "\"></a></td>
      <th>ID:</th>
      <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()), "html", null, true);
        echo "</td>
    </tr>
\t<tr>
      <th>MANGOPAY ID:</th>
       <td>
\t\t";
        // line 19
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "mangoid", array())) {
            // line 20
            echo "\t\t\t";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "mangoid", array()), "html", null, true);
            echo "
\t\t";
        } else {
            // line 22
            echo "\t\t\t<a class=\"boldbuttons\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_mango", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
            echo "\"><span>Créer compte MangoPay</span></a>
\t\t";
        }
        // line 24
        echo "\t   </td>
\t   <th>Facebook ID:</th>
       <td>
\t\t\t";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "facebookId", array()), "html", null, true);
        echo "
\t   </td>
    </tr>
\t<tr>
      <th>Photo principale:</th>
      <td colspan=\"3\">
      \t";
        // line 33
        if ( !(null === $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "image", array()))) {
            // line 34
            echo "      \t\t<img src=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "image", array()), "url", array()), "html", null, true);
            echo "\"/>
      \t";
        }
        // line 36
        echo "  \t  </td>
    </tr>
\t<tr>
      <th>Nom:</th>
      <td>";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "firstName", array()), "html", null, true);
        echo "</td>
      <th>Prénom:</th>
      <td>";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "lastName", array()), "html", null, true);
        echo "</td>
    </tr>
\t<tr>
      <th>Adresse 1:</th>
      <td>";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "address1", array()), "html", null, true);
        echo "</td>
      <th>Adresse 2:</th>
      <td>";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "address2", array()), "html", null, true);
        echo "</td>
    </tr>
\t<tr>
      <th>Code postal:</th>
      <td>";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "postalCode", array()), "html", null, true);
        echo "</td>
      <th>Ville:</th>
      <td>";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "city", array()), "html", null, true);
        echo "</td>
    </tr>
\t<tr>
      <th>Téléphone fixe:</th>
      <td>";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "phone", array()), "html", null, true);
        echo "</td>
      <th>Téléphone portable:</th>
      <td>";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "mobile", array()), "html", null, true);
        echo "</td>
    </tr>
\t<tr>
      <th>Email:</th>
       <td colspan=\"3\">";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo "</td>
    </tr>
\t";
        // line 66
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "type", array()) == 1)) {
            // line 67
            echo "\t<tr>
      <th>Cheveux:</th>
       <td colspan=\"3\">";
            // line 69
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "cheveuxLabel", array()), "html", null, true);
            echo "</td>
    </tr>
\t<tr>
      <th>Peau:</th>
       <td colspan=\"3\">";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "peauLabel", array()), "html", null, true);
            echo "</td>
    </tr>
\t<tr>
      <th>Preferences:</th>
       <td colspan=\"3\">";
            // line 77
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "preferencesLabel", array()), "html", null, true);
            echo "</td>
    </tr>
\t";
        }
        // line 80
        echo "\t";
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "type", array()) == 2)) {
            // line 81
            echo "\t<tr>
      <th>Je suis:</th>
      <td>";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "genreLabel", array()), "html", null, true);
            echo "</td>
      <th>Metiers:</th>
      <td>";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "metiersLabel", array()), "html", null, true);
            echo "</td>
    </tr>
\t<tr>
      <th>Compétences Cheveux:</th>
       <td colspan=\"3\">";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "cheveuxLabel", array()), "html", null, true);
            echo "</td>
    </tr>
\t<tr>
      <th>Compétences Peau:</th>
       <td colspan=\"3\">";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "peauLabel", array()), "html", null, true);
            echo "</td>
    </tr>
\t<tr>
      <th>Statut:</th>
      <td>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "statutLabel", array()), "html", null, true);
            echo "</td>
      <th>Nom de l'entreprise:</th>
      <td>";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "entreprise", array()), "html", null, true);
            echo "</td>
    </tr>
\t<tr>
      <th>SIRET:</th>
       <td colspan=\"3\">";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "siren", array()), "html", null, true);
            echo "</td>
    </tr>
\t<tr>
       <th>Qui êtes vous ?:</th>
      <td colspan=\"3\">";
            // line 107
            echo nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "description", array()), "html", null, true));
            echo "</td>
    </tr>
\t<tr>
      <th>Charte de qualité:</th>
      <td>";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "chartelabel", array()), "html", null, true);
            echo "</td>
      <th>Newsletter:</th>
      <td>";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "newsletterlabel", array()), "html", null, true);
            echo "</td>
    </tr>
    <tr>
      <th>CGU:</th>
      <td>";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "cgulabel", array()), "html", null, true);
            echo "</td>
    \t<th>Compte actif</th>
    \t<td>";
            // line 119
            if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "validate", array())) {
                echo "<img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/success.png"), "html", null, true);
                echo "\">";
            } else {
                echo "<img src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/sup.gif"), "html", null, true);
                echo "\">";
            }
            echo "</td>
    </tr>
    <tr>
      <th>Date creation</th>
      <td>";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "createdat", array(0 => "d/m/Y"), "method"), "html", null, true);
            echo "</td>
      <th>Date modification:</th>
      <td>";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "updatedat", array(0 => "d/m/Y"), "method"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
      <th>Vues</th>
      <td>";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "vues", array()), "html", null, true);
            echo "</td>
      <th>Contacts</th>
      <td>";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "contacts", array()), "html", null, true);
            echo "</td>
    </tr>
    <tr>
    \t <td colspan=\"4\">
    \t\t<div id=\"linechart\" style=\"min-width: 400px; height: 400px; margin: 0 auto\"></div>
    \t</td>
    </tr>
\t";
        }
        // line 139
        echo "  </tbody>
   <thead>
      <tr>
        <td colspan=\"4\">
        <ul id=\"bout-action\">
          <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_edit", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
        echo "\"><span>Modifier</span></a>
\t\t\t</div>
          </li>
\t\t  <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_contact", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
        echo "\"><span>Envoyer un mail</span></a>
\t\t\t</div>
          </li>
          <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" onclick=\"if (confirm('En êtes-vous sûr?')) { f = document.createElement('form'); document.body.appendChild(f); f.method = 'POST'; f.action = this.href; f.submit(); };return false;\" href=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_delete", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
        echo "\"><span>Supprimer</span></a>
\t\t\t</div>
          </li>
          <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 161
        echo $this->env->getExtension('routing')->getPath("admin_user_index");
        echo "\"><span>Retour liste</span></a>
\t\t\t</div>
          </li>
        </ul>        
        </td>
      </tr>
    </thead>
\t<tfoot>
      <tr>
        <td colspan=\"4\">
        <ul id=\"bout-action\">
          <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 174
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_edit", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
        echo "\"><span>Modifier</span></a>
\t\t\t</div>
          </li>
\t\t  <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_contact", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
        echo "\"><span>Envoyer un mail</span></a>
\t\t\t</div>
          </li>
          <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" onclick=\"if (confirm('En êtes-vous sûr?')) { f = document.createElement('form'); document.body.appendChild(f); f.method = 'POST'; f.action = this.href; f.submit(); };return false;\" href=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_user_delete", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "id", array()))), "html", null, true);
        echo "\"><span>Supprimer</span></a>
\t\t\t</div>
          </li>
          <li>
          \t<div class=\"buttonwrapper\">
\t\t\t\t<a class=\"boldbuttons\" href=\"";
        // line 189
        echo $this->env->getExtension('routing')->getPath("admin_user_index");
        echo "\"><span>Retour liste</span></a>
\t\t\t</div>
          </li>
        </ul>        
        </td>
      </tr>
    </tfoot>
</table>
\t
</div>
";
        // line 199
        if (($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "type", array()) == 2)) {
            // line 200
            echo "  <h1 style=\"margin-top:30px;\">Liste offres</h1>
  
  <div class=\"table_admin\">
\t<table>
\t  <thead> 
\t    <tr class=\"entete\">
\t      <th>Titre</th>
\t      <th>Durée</th>
\t      <th>Prix</th>
\t    </tr>
\t  </thead>
\t  <tbody>
\t    ";
            // line 212
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "offers", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["offer"]) {
                // line 213
                echo "\t    <tr class=\"item";
                if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                    echo "1";
                }
                echo "\">
\t      <td>";
                // line 214
                echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "title", array()), "html", null, true);
                echo "</td>
\t      <td>";
                // line 215
                echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "dureeLabel", array()), "html", null, true);
                echo "</td>
\t\t  <td>";
                // line 216
                echo twig_escape_filter($this->env, $this->getAttribute($context["offer"], "PriceLabel", array()), "html", null, true);
                echo "</td>
\t    </tr>
\t    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 219
            echo "\t  </tbody>
\t</table>
\t
  </div>
  
  <h1 style=\"margin-top:30px;\">Liste documents</h1>
  
  <div class=\"table_admin\">
\t<table>
\t  <thead> 
\t    <tr class=\"entete\">
\t      <th>Titre</th>
\t      <th>Télécharger</th>
\t    </tr>
\t  </thead>
\t  <tbody>
\t    ";
            // line 235
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "documents", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
                // line 236
                echo "\t    <tr class=\"item";
                if ((0 == $this->getAttribute($context["loop"], "index", array()) % 2)) {
                    echo "1";
                }
                echo "\">
\t      <td>";
                // line 237
                echo twig_escape_filter($this->env, $context["document"], "html", null, true);
                echo "</td>
\t      <td>";
                // line 238
                if ($this->getAttribute($context["document"], "document", array())) {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["document"], "url", array()), "html", null, true);
                    echo "\" target=\"_blank\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Télécharger"), "html", null, true);
                    echo "</a>";
                }
                echo "</td>
\t    </tr>
\t    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 241
            echo "\t  </tbody>
\t</table>
\t
  </div>
";
        }
        // line 246
        echo "

";
    }

    // line 251
    public function block_javascripts($context, array $blocks = array())
    {
        // line 252
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//code.highcharts.com/4.0.1/highcharts.js\"></script>
<script src=\"//code.highcharts.com/4.0.1/modules/exporting.js\"></script>
<script type=\"text/javascript\">
    ";
        // line 256
        echo $this->env->getExtension('highcharts_extension')->chart((isset($context["chart"]) ? $context["chart"] : null));
        echo "
</script>
";
    }

    public function getTemplateName()
    {
        return "AdminBundle:User:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  548 => 256,  541 => 252,  538 => 251,  532 => 246,  525 => 241,  502 => 238,  498 => 237,  491 => 236,  474 => 235,  456 => 219,  439 => 216,  435 => 215,  431 => 214,  424 => 213,  407 => 212,  393 => 200,  391 => 199,  378 => 189,  370 => 184,  362 => 179,  354 => 174,  338 => 161,  330 => 156,  322 => 151,  314 => 146,  305 => 139,  294 => 131,  289 => 129,  282 => 125,  277 => 123,  262 => 119,  257 => 117,  250 => 113,  245 => 111,  238 => 107,  231 => 103,  224 => 99,  219 => 97,  212 => 93,  205 => 89,  198 => 85,  193 => 83,  189 => 81,  186 => 80,  180 => 77,  173 => 73,  166 => 69,  162 => 67,  160 => 66,  155 => 64,  148 => 60,  143 => 58,  136 => 54,  131 => 52,  124 => 48,  119 => 46,  112 => 42,  107 => 40,  101 => 36,  95 => 34,  93 => 33,  84 => 27,  79 => 24,  73 => 22,  67 => 20,  65 => 19,  57 => 14,  50 => 12,  40 => 4,  37 => 3,  11 => 1,);
    }
}
