<?php

/* AdminBundle::menu.html.twig */
class __TwigTemplate_f9ecde77bcb22367872c80333a5d0145cc7577447139776c757975ca692ee5f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"myslidemenu_div\">
\t";
        // line 2
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 3
            echo "\t<div id=\"myslidemenu\" class=\"jqueryslidemenu\">
\t\t<ul>
\t\t\t<li style=\"margin-left:30px;\"><a href=\"";
            // line 5
            echo $this->env->getExtension('routing')->getPath("admin_user_index");
            echo "\">Membres</a></li>
            <li>
\t\t\t\t<a href=\"";
            // line 7
            echo $this->env->getExtension('routing')->getPath("admin_booking_index");
            echo "\">Réservation</a>
\t\t\t    <ul id=\"\">
                    <li><a href=\"";
            // line 9
            echo $this->env->getExtension('routing')->getPath("admin_booking_1");
            echo "\">Paiement</a></li>
                    <li><a href=\"";
            // line 10
            echo $this->env->getExtension('routing')->getPath("admin_booking_2");
            echo "\">Confirmation</a></li>
                    <li><a href=\"";
            // line 11
            echo $this->env->getExtension('routing')->getPath("admin_booking_3");
            echo "\">A réaliser</a></li>
                    <li><a href=\"";
            // line 12
            echo $this->env->getExtension('routing')->getPath("admin_booking_4");
            echo "\">Témoignage</a></li>
                    <li><a href=\"";
            // line 13
            echo $this->env->getExtension('routing')->getPath("admin_booking_5");
            echo "\">Clôturée</a></li>
                </ul>
\t\t\t</li>
\t\t\t<li><a href=\"";
            // line 16
            echo $this->env->getExtension('routing')->getPath("admin_reduc_index");
            echo "\">Codes réduction</a></li>
        \t<li><a href=\"";
            // line 17
            echo $this->env->getExtension('routing')->getPath("admin_discussion_index");
            echo "\">Messages</a></li>
            <li><a href=\"";
            // line 18
            echo $this->env->getExtension('routing')->getPath("admin_content_index");
            echo "\">Pages</a></li>
            <li><a href=\"";
            // line 19
            echo $this->env->getExtension('routing')->getPath("admin_zone_index");
            echo "\">Zones</a></li>
            <li><a href=\"";
            // line 20
            echo $this->env->getExtension('routing')->getPath("admin_media");
            echo "\">Médiathèque</a></li>
            <li><a href=\"";
            // line 21
            echo $this->env->getExtension('routing')->getPath("admin_publicite_index");
            echo "\">Publicité</a></li>
\t\t\t<li><a href=\"";
            // line 22
            echo $this->env->getExtension('routing')->getPath("admin_category_index");
            echo "\">Critéres</a></li>
\t\t\t<li><a href=\"";
            // line 23
            echo $this->env->getExtension('routing')->getPath("admin_prestation_index");
            echo "\">Prestations</a></li>
            <li>
            \t<a href=\"";
            // line 25
            echo $this->env->getExtension('routing')->getPath("admin_configs_index");
            echo "\">Autres</a>
            \t<ul id=\"\">
            \t\t<li><a href=\"";
            // line 27
            echo $this->env->getExtension('routing')->getPath("admin_configs_index");
            echo "\">Paramètres</a></li>
\t                <li><a href=\"";
            // line 28
            echo $this->env->getExtension('routing')->getPath("admin_admin_index");
            echo "\">Administrateurs</a></li>
\t                <li><a href=\"";
            // line 29
            echo $this->env->getExtension('routing')->getPath("admin_newsletter_index");
            echo "\">Newsletters</a></li>
\t            </ul> 
        \t</li>
        \t<li><a href=\"";
            // line 32
            echo $this->env->getExtension('routing')->getPath("admin_cache");
            echo "\">Vider cache</a></li>
         </ul>
\t</div>
\t";
        }
        // line 36
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "AdminBundle::menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 36,  112 => 32,  106 => 29,  102 => 28,  98 => 27,  93 => 25,  88 => 23,  84 => 22,  80 => 21,  76 => 20,  72 => 19,  68 => 18,  64 => 17,  60 => 16,  54 => 13,  50 => 12,  46 => 11,  42 => 10,  38 => 9,  33 => 7,  28 => 5,  24 => 3,  22 => 2,  19 => 1,);
    }
}
