<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/admin852741')) {
            if (0 === strpos($pathinfo, '/admin852741/admin')) {
                // admin_admin_index
                if (0 === strpos($pathinfo, '/admin852741/admin/index') && preg_match('#^/admin852741/admin/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_admin_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\AdminController::indexAction',));
                }

                // admin_admin_show
                if (0 === strpos($pathinfo, '/admin852741/admin/show') && preg_match('#^/admin852741/admin/show/(?P<admin_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_admin_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\AdminController::showAction',));
                }

                // admin_admin_add
                if ($pathinfo === '/admin852741/admin/add') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\AdminController::addAction',  '_route' => 'admin_admin_add',);
                }

                // admin_admin_edit
                if (0 === strpos($pathinfo, '/admin852741/admin/edit') && preg_match('#^/admin852741/admin/edit/(?P<admin_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_admin_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\AdminController::editAction',));
                }

                // admin_admin_delete
                if (0 === strpos($pathinfo, '/admin852741/admin/delete') && preg_match('#^/admin852741/admin/delete/(?P<admin_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_admin_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\AdminController::deleteAction',));
                }

                // admin_admin_export
                if ($pathinfo === '/admin852741/admin/export') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\AdminController::exportAction',  '_route' => 'admin_admin_export',);
                }

                // admin_admin_gosearch
                if ($pathinfo === '/admin852741/admin/gosearch') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\AdminController::gosearchAction',  '_route' => 'admin_admin_gosearch',);
                }

                // admin_admin_search
                if (0 === strpos($pathinfo, '/admin852741/admin/search') && preg_match('#^/admin852741/admin/search(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_admin_search')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\AdminController::searchAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin852741/booking')) {
                // admin_booking_index
                if (0 === strpos($pathinfo, '/admin852741/booking/index') && preg_match('#^/admin852741/booking/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_index')), array (  'page' => '1',  'sort' => 'created_at',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::indexAction',));
                }

                // admin_booking_realise
                if (0 === strpos($pathinfo, '/admin852741/booking/realise') && preg_match('#^/admin852741/booking/realise/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_realise')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::bookingrerealiseAction',));
                }

                if (0 === strpos($pathinfo, '/admin852741/booking/booking')) {
                    // admin_booking_1
                    if (0 === strpos($pathinfo, '/admin852741/booking/booking1') && preg_match('#^/admin852741/booking/booking1(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_1')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::booking1Action',));
                    }

                    // admin_booking_2
                    if (0 === strpos($pathinfo, '/admin852741/booking/booking2') && preg_match('#^/admin852741/booking/booking2(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_2')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::booking2Action',));
                    }

                    // admin_booking_3
                    if (0 === strpos($pathinfo, '/admin852741/booking/booking3') && preg_match('#^/admin852741/booking/booking3(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_3')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::booking3Action',));
                    }

                    // admin_booking_4
                    if (0 === strpos($pathinfo, '/admin852741/booking/booking4') && preg_match('#^/admin852741/booking/booking4(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_4')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::booking4Action',));
                    }

                    // admin_booking_5
                    if (0 === strpos($pathinfo, '/admin852741/booking/booking5') && preg_match('#^/admin852741/booking/booking5(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_5')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::booking5Action',));
                    }

                }

                // admin_booking_canceled
                if (0 === strpos($pathinfo, '/admin852741/booking/annulee') && preg_match('#^/admin852741/booking/annulee(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_canceled')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::canceledAction',));
                }

                // admin_booking_show
                if (0 === strpos($pathinfo, '/admin852741/booking/show') && preg_match('#^/admin852741/booking/show/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_booking_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::showAction',));
                }

                // booking_paid
                if (0 === strpos($pathinfo, '/admin852741/booking/pay') && preg_match('#^/admin852741/booking/pay/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_paid')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::bookingpaidAction',));
                }

                // booking_cancel
                if (0 === strpos($pathinfo, '/admin852741/booking/cancel') && preg_match('#^/admin852741/booking/cancel/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_cancel')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\BookingController::bookingcancelAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin852741/c')) {
                if (0 === strpos($pathinfo, '/admin852741/category')) {
                    // admin_category_index
                    if (0 === strpos($pathinfo, '/admin852741/category/index') && preg_match('#^/admin852741/category/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_category_index')), array (  'page' => '1',  'sort' => 'no_order',  'sens' => '1',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\CategoryController::indexAction',));
                    }

                    // admin_category_show
                    if (0 === strpos($pathinfo, '/admin852741/category/show') && preg_match('#^/admin852741/category/show/(?P<category_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_category_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\CategoryController::showAction',));
                    }

                    // admin_category_add
                    if ($pathinfo === '/admin852741/category/add') {
                        return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\CategoryController::addAction',  '_route' => 'admin_category_add',);
                    }

                    // admin_category_edit
                    if (0 === strpos($pathinfo, '/admin852741/category/edit') && preg_match('#^/admin852741/category/edit/(?P<category_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_category_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\CategoryController::editAction',));
                    }

                    // admin_category_delete
                    if (0 === strpos($pathinfo, '/admin852741/category/delete') && preg_match('#^/admin852741/category/delete/(?P<category_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_category_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\CategoryController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/admin852741/con')) {
                    if (0 === strpos($pathinfo, '/admin852741/configs')) {
                        // admin_configs_index
                        if (0 === strpos($pathinfo, '/admin852741/configs/index') && preg_match('#^/admin852741/configs/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_configs_index')), array (  'page' => '1',  'sort' => 'label',  'sens' => '1',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ConfigsController::indexAction',));
                        }

                        // admin_configs_show
                        if (0 === strpos($pathinfo, '/admin852741/configs/show') && preg_match('#^/admin852741/configs/show/(?P<configs_id>\\d+)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_configs_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ConfigsController::showAction',));
                        }

                        // admin_configs_edit
                        if (0 === strpos($pathinfo, '/admin852741/configs/edit') && preg_match('#^/admin852741/configs/edit/(?P<configs_id>\\d+)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_configs_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ConfigsController::editAction',));
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin852741/content')) {
                        if (0 === strpos($pathinfo, '/admin852741/contentcomment')) {
                            // admin_contentcomment_index
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/index') && preg_match('#^/admin852741/contentcomment/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::indexAction',));
                            }

                            // admin_contentcomment_avalider
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/avalider') && preg_match('#^/admin852741/contentcomment/avalider(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_avalider')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::avaliderAction',));
                            }

                            // admin_contentcomment_enligne
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/enligne') && preg_match('#^/admin852741/contentcomment/enligne(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_enligne')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::enligneAction',));
                            }

                            // admin_contentcomment_refuse
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/refuse') && preg_match('#^/admin852741/contentcomment/refuse(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_refuse')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::refuseAction',));
                            }

                            // admin_contentcomment_show
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/show') && preg_match('#^/admin852741/contentcomment/show/(?P<comment_id>\\d+)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::showAction',));
                            }

                            // admin_contentcomment_accepter
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/accepter') && preg_match('#^/admin852741/contentcomment/accepter/(?P<comment_id>\\d+)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_accepter')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::accepterAction',));
                            }

                            // admin_contentcomment_refuser
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/refuser') && preg_match('#^/admin852741/contentcomment/refuser/(?P<comment_id>\\d+)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_refuser')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::refuserAction',));
                            }

                            // admin_contentcomment_edit
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/edit') && preg_match('#^/admin852741/contentcomment/edit/(?P<comment_id>\\d+)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::editAction',));
                            }

                            // admin_contentcomment_delete
                            if (0 === strpos($pathinfo, '/admin852741/contentcomment/delete') && preg_match('#^/admin852741/contentcomment/delete/(?P<comment_id>\\d+)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_contentcomment_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentCommentController::deleteAction',));
                            }

                        }

                        // admin_content_index
                        if (0 === strpos($pathinfo, '/admin852741/content/index') && preg_match('#^/admin852741/content/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_content_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentController::indexAction',));
                        }

                        // admin_content_show
                        if (0 === strpos($pathinfo, '/admin852741/content/show') && preg_match('#^/admin852741/content/show/(?P<content_id>\\d+)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_content_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentController::showAction',));
                        }

                        // admin_content_add
                        if ($pathinfo === '/admin852741/content/add') {
                            return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentController::addAction',  '_route' => 'admin_content_add',);
                        }

                        // admin_content_edit
                        if (0 === strpos($pathinfo, '/admin852741/content/edit') && preg_match('#^/admin852741/content/edit/(?P<content_id>\\d+)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_content_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentController::editAction',));
                        }

                        // admin_content_delete
                        if (0 === strpos($pathinfo, '/admin852741/content/delete') && preg_match('#^/admin852741/content/delete/(?P<content_id>\\d+)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_content_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ContentController::deleteAction',));
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin852741/discussion')) {
                // admin_discussion_index
                if (0 === strpos($pathinfo, '/admin852741/discussion/index') && preg_match('#^/admin852741/discussion/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_discussion_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\DiscussionController::indexAction',));
                }

                // admin_discussion_show
                if (0 === strpos($pathinfo, '/admin852741/discussion/show') && preg_match('#^/admin852741/discussion/show/(?P<discussion_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_discussion_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\DiscussionController::showAction',));
                }

                // admin_discussion_delete
                if (0 === strpos($pathinfo, '/admin852741/discussion/delete') && preg_match('#^/admin852741/discussion/delete/(?P<discussion_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_discussion_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\DiscussionController::deleteAction',));
                }

            }

            // admin_index
            if (rtrim($pathinfo, '/') === '/admin852741') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'admin_index');
                }

                return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\HomeController::indexAction',  '_route' => 'admin_index',);
            }

            // admin_cache
            if ($pathinfo === '/admin852741/cache') {
                return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\HomeController::cacheAction',  '_route' => 'admin_cache',);
            }

            // admin_media
            if ($pathinfo === '/admin852741/media') {
                return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\HomeController::mediaAction',  '_route' => 'admin_media',);
            }

            // admin_infosheader
            if ($pathinfo === '/admin852741/infosheader') {
                return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\HomeController::infosheaderAction',  '_route' => 'admin_infosheader',);
            }

            if (0 === strpos($pathinfo, '/admin852741/login')) {
                // admin_login_index
                if ($pathinfo === '/admin852741/login') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\LoginController::indexAction',  '_route' => 'admin_login_index',);
                }

                if (0 === strpos($pathinfo, '/admin852741/login/log')) {
                    // admin_login_logout
                    if ($pathinfo === '/admin852741/login/logout') {
                        return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\LoginController::logoutAction',  '_route' => 'admin_login_logout',);
                    }

                    // admin_login_login
                    if ($pathinfo === '/admin852741/login/login') {
                        return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\LoginController::loginAction',  '_route' => 'admin_login_login',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin852741/newsletter')) {
                // admin_newsletter_index
                if (0 === strpos($pathinfo, '/admin852741/newsletter/index') && preg_match('#^/admin852741/newsletter/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_newsletter_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\NewsletterController::indexAction',));
                }

                // admin_newsletter_show
                if (0 === strpos($pathinfo, '/admin852741/newsletter/show') && preg_match('#^/admin852741/newsletter/show/(?P<newsletter_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_newsletter_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\NewsletterController::showAction',));
                }

                // admin_newsletter_add
                if ($pathinfo === '/admin852741/newsletter/add') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\NewsletterController::addAction',  '_route' => 'admin_newsletter_add',);
                }

                if (0 === strpos($pathinfo, '/admin852741/newsletter/e')) {
                    // admin_newsletter_edit
                    if (0 === strpos($pathinfo, '/admin852741/newsletter/edit') && preg_match('#^/admin852741/newsletter/edit/(?P<newsletter_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_newsletter_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\NewsletterController::editAction',));
                    }

                    // admin_newsletter_envoyer
                    if (0 === strpos($pathinfo, '/admin852741/newsletter/envoyer') && preg_match('#^/admin852741/newsletter/envoyer/(?P<newsletter_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_newsletter_envoyer')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\NewsletterController::envoyerAction',));
                    }

                }

                // admin_newsletter_test
                if (0 === strpos($pathinfo, '/admin852741/newsletter/test') && preg_match('#^/admin852741/newsletter/test/(?P<newsletter_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_newsletter_test')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\NewsletterController::testAction',));
                }

                // admin_newsletter_delete
                if (0 === strpos($pathinfo, '/admin852741/newsletter/delete') && preg_match('#^/admin852741/newsletter/delete/(?P<newsletter_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_newsletter_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\NewsletterController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin852741/p')) {
                if (0 === strpos($pathinfo, '/admin852741/prestation')) {
                    // admin_prestation_index
                    if (0 === strpos($pathinfo, '/admin852741/prestation/index') && preg_match('#^/admin852741/prestation/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_prestation_index')), array (  'page' => '1',  'sort' => 'no_order',  'sens' => '1',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PrestationController::indexAction',));
                    }

                    // admin_prestation_show
                    if (0 === strpos($pathinfo, '/admin852741/prestation/show') && preg_match('#^/admin852741/prestation/show/(?P<prestation_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_prestation_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PrestationController::showAction',));
                    }

                    // admin_prestation_add
                    if ($pathinfo === '/admin852741/prestation/add') {
                        return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PrestationController::addAction',  '_route' => 'admin_prestation_add',);
                    }

                    // admin_prestation_edit
                    if (0 === strpos($pathinfo, '/admin852741/prestation/edit') && preg_match('#^/admin852741/prestation/edit/(?P<prestation_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_prestation_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PrestationController::editAction',));
                    }

                    // admin_prestation_delete
                    if (0 === strpos($pathinfo, '/admin852741/prestation/delete') && preg_match('#^/admin852741/prestation/delete/(?P<prestation_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_prestation_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PrestationController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/admin852741/publicite')) {
                    // admin_publicite_index
                    if (0 === strpos($pathinfo, '/admin852741/publicite/index') && preg_match('#^/admin852741/publicite/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_publicite_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PubliciteController::indexAction',));
                    }

                    // admin_publicite_show
                    if (0 === strpos($pathinfo, '/admin852741/publicite/show') && preg_match('#^/admin852741/publicite/show/(?P<publicite_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_publicite_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PubliciteController::showAction',));
                    }

                    // admin_publicite_add
                    if ($pathinfo === '/admin852741/publicite/add') {
                        return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PubliciteController::addAction',  '_route' => 'admin_publicite_add',);
                    }

                    // admin_publicite_edit
                    if (0 === strpos($pathinfo, '/admin852741/publicite/edit') && preg_match('#^/admin852741/publicite/edit/(?P<publicite_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_publicite_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PubliciteController::editAction',));
                    }

                    // admin_publicite_delete
                    if (0 === strpos($pathinfo, '/admin852741/publicite/delete') && preg_match('#^/admin852741/publicite/delete/(?P<publicite_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_publicite_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\PubliciteController::deleteAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin852741/reduc')) {
                // admin_reduc_index
                if (0 === strpos($pathinfo, '/admin852741/reduc/index') && preg_match('#^/admin852741/reduc/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_reduc_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ReducController::indexAction',));
                }

                // admin_reduc_show
                if (0 === strpos($pathinfo, '/admin852741/reduc/show') && preg_match('#^/admin852741/reduc/show/(?P<reduc_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_reduc_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ReducController::showAction',));
                }

                // admin_reduc_add
                if ($pathinfo === '/admin852741/reduc/add') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ReducController::addAction',  '_route' => 'admin_reduc_add',);
                }

                // admin_reduc_edit
                if (0 === strpos($pathinfo, '/admin852741/reduc/edit') && preg_match('#^/admin852741/reduc/edit/(?P<reduc_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_reduc_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ReducController::editAction',));
                }

                // admin_reduc_delete
                if (0 === strpos($pathinfo, '/admin852741/reduc/delete') && preg_match('#^/admin852741/reduc/delete/(?P<reduc_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_reduc_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ReducController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin852741/typecontent')) {
                // admin_typecontent_index
                if (0 === strpos($pathinfo, '/admin852741/typecontent/index') && preg_match('#^/admin852741/typecontent/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_typecontent_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\TypecontentController::indexAction',));
                }

                // admin_typecontent_show
                if (0 === strpos($pathinfo, '/admin852741/typecontent/show') && preg_match('#^/admin852741/typecontent/show/(?P<typecontent_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_typecontent_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\TypecontentController::showAction',));
                }

                // admin_typecontent_add
                if ($pathinfo === '/admin852741/typecontent/add') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\TypecontentController::addAction',  '_route' => 'admin_typecontent_add',);
                }

                // admin_typecontent_edit
                if (0 === strpos($pathinfo, '/admin852741/typecontent/edit') && preg_match('#^/admin852741/typecontent/edit/(?P<typecontent_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_typecontent_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\TypecontentController::editAction',));
                }

                // admin_typecontent_delete
                if (0 === strpos($pathinfo, '/admin852741/typecontent/delete') && preg_match('#^/admin852741/typecontent/delete/(?P<typecontent_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_typecontent_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\TypecontentController::deleteAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin852741/user')) {
                // admin_user_index
                if (0 === strpos($pathinfo, '/admin852741/user/index') && preg_match('#^/admin852741/user/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_index')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::indexAction',));
                }

                // admin_user_contact
                if (0 === strpos($pathinfo, '/admin852741/user/contact') && preg_match('#^/admin852741/user/contact/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_contact')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::contactAction',));
                }

                // admin_user_mango
                if (0 === strpos($pathinfo, '/admin852741/user/mango') && preg_match('#^/admin852741/user/mango/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_mango')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::mangoAction',));
                }

                // admin_user_show
                if (0 === strpos($pathinfo, '/admin852741/user/show') && preg_match('#^/admin852741/user/show/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::showAction',));
                }

                // admin_user_add
                if ($pathinfo === '/admin852741/user/add') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::addAction',  '_route' => 'admin_user_add',);
                }

                // admin_user_edit
                if (0 === strpos($pathinfo, '/admin852741/user/edit') && preg_match('#^/admin852741/user/edit/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::editAction',));
                }

                // admin_user_delete
                if (0 === strpos($pathinfo, '/admin852741/user/delete') && preg_match('#^/admin852741/user/delete/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::deleteAction',));
                }

                // admin_user_export
                if ($pathinfo === '/admin852741/user/export') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::exportAction',  '_route' => 'admin_user_export',);
                }

                // admin_user_gosearch
                if ($pathinfo === '/admin852741/user/gosearch') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::gosearchAction',  '_route' => 'admin_user_gosearch',);
                }

                // admin_user_search
                if (0 === strpos($pathinfo, '/admin852741/user/search') && preg_match('#^/admin852741/user/search(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_search')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\UserController::searchAction',));
                }

            }

            if (0 === strpos($pathinfo, '/admin852741/zone')) {
                // admin_zone_index
                if (0 === strpos($pathinfo, '/admin852741/zone/index') && preg_match('#^/admin852741/zone/index(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_zone_index')), array (  'page' => '1',  'sort' => 'title',  'sens' => '1',  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ZoneController::indexAction',));
                }

                // admin_zone_show
                if (0 === strpos($pathinfo, '/admin852741/zone/show') && preg_match('#^/admin852741/zone/show/(?P<zone_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_zone_show')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ZoneController::showAction',));
                }

                // admin_zone_add
                if ($pathinfo === '/admin852741/zone/add') {
                    return array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ZoneController::addAction',  '_route' => 'admin_zone_add',);
                }

                // admin_zone_edit
                if (0 === strpos($pathinfo, '/admin852741/zone/edit') && preg_match('#^/admin852741/zone/edit/(?P<zone_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_zone_edit')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ZoneController::editAction',));
                }

                // admin_zone_delete
                if (0 === strpos($pathinfo, '/admin852741/zone/delete') && preg_match('#^/admin852741/zone/delete/(?P<zone_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_zone_delete')), array (  '_controller' => 'FAPROD\\AdminBundle\\Controller\\ZoneController::deleteAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/con')) {
            if (0 === strpos($pathinfo, '/connexion')) {
                // register_loginfacebook
                if ($pathinfo === '/connexion/loginfacebook') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\FacebookController::loginfacebookAction',  '_route' => 'register_loginfacebook',);
                }

                if (0 === strpos($pathinfo, '/connexion/facebook')) {
                    // register_facebook
                    if ($pathinfo === '/connexion/facebook') {
                        return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\FacebookController::facebookAction',  '_route' => 'register_facebook',);
                    }

                    // register_facebook2
                    if ($pathinfo === '/connexion/facebook2') {
                        return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\FacebookController::facebook2Action',  '_route' => 'register_facebook2',);
                    }

                }

                // register_autocomplete
                if ($pathinfo === '/connexion/autocomplete') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::autocompleteAction',  '_route' => 'register_autocomplete',);
                }

                // register_index
                if (rtrim($pathinfo, '/') === '/connexion') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'register_index');
                    }

                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::indexAction',  '_route' => 'register_index',);
                }

                if (0 === strpos($pathinfo, '/connexion/cliente')) {
                    // register_cliente
                    if ($pathinfo === '/connexion/cliente') {
                        return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::clienteAction',  '_route' => 'register_cliente',);
                    }

                    // register_cheveux
                    if ($pathinfo === '/connexion/cliente/cheveux') {
                        return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::cheveuxAction',  '_route' => 'register_cheveux',);
                    }

                    if (0 === strpos($pathinfo, '/connexion/cliente/p')) {
                        // register_peau
                        if ($pathinfo === '/connexion/cliente/peau') {
                            return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::peauAction',  '_route' => 'register_peau',);
                        }

                        // register_preferences
                        if ($pathinfo === '/connexion/cliente/preferences') {
                            return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::preferencesAction',  '_route' => 'register_preferences',);
                        }

                    }

                }

                // register_professionnel
                if ($pathinfo === '/connexion/professionnel') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::professionnelAction',  '_route' => 'register_professionnel',);
                }

                // register_endregister
                if ($pathinfo === '/connexion/endregister') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::endregisterAction',  '_route' => 'register_endregister',);
                }

                // register_connexion
                if ($pathinfo === '/connexion/connexion') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::connexionAction',  '_route' => 'register_connexion',);
                }

                // register_validate
                if ($pathinfo === '/connexion/validate') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::validateAction',  '_route' => 'register_validate',);
                }

                if (0 === strpos($pathinfo, '/connexion/log')) {
                    // register_login_order
                    if (0 === strpos($pathinfo, '/connexion/loginorder') && preg_match('#^/connexion/loginorder/(?P<p1>[^/]++)/(?P<p2>[^/]++)/(?P<p3>[^/]++)/(?P<p4>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'register_login_order')), array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::loginorderAction',));
                    }

                    // register_logout
                    if ($pathinfo === '/connexion/logout') {
                        return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::logoutAction',  '_route' => 'register_logout',);
                    }

                    if (0 === strpos($pathinfo, '/connexion/login')) {
                        // register_login
                        if ($pathinfo === '/connexion/login') {
                            return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::loginAction',  '_route' => 'register_login',);
                        }

                        // register_login_message
                        if (0 === strpos($pathinfo, '/connexion/loginmessage') && preg_match('#^/connexion/loginmessage/(?P<p1>[^/]++)/(?P<p2>[^/]++)/(?P<p3>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'register_login_message')), array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::loginmessageAction',));
                        }

                    }

                }

                // register_autologin
                if ($pathinfo === '/connexion/autologin') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::autologinAction',  '_route' => 'register_autologin',);
                }

                // register_password
                if ($pathinfo === '/connexion/password') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::passwordAction',  '_route' => 'register_password',);
                }

                // register_sendpassword
                if ($pathinfo === '/connexion/sendpassword') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::sendpasswordAction',  '_route' => 'register_sendpassword',);
                }

                // register_endpassword
                if ($pathinfo === '/connexion/endpassword') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::endpasswordAction',  '_route' => 'register_endpassword',);
                }

                // register_changepassword
                if ($pathinfo === '/connexion/changepassword') {
                    return array (  '_controller' => 'FAPROD\\RegisterBundle\\Controller\\RegisterController::changepasswordAction',  '_route' => 'register_changepassword',);
                }

            }

            // contact_index
            if (rtrim($pathinfo, '/') === '/contact') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'contact_index');
                }

                return array (  '_controller' => 'FAPROD\\ContactBundle\\Controller\\ContactController::indexAction',  '_route' => 'contact_index',);
            }

        }

        if (0 === strpos($pathinfo, '/m')) {
            if (0 === strpos($pathinfo, '/mag')) {
                // content_partage
                if (0 === strpos($pathinfo, '/mag/partage') && preg_match('#^/mag/partage/(?P<content_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'content_partage')), array (  '_controller' => 'FAPROD\\ContentBundle\\Controller\\ContentController::partageAction',));
                }

                // content_comment
                if (0 === strpos($pathinfo, '/mag/comment') && preg_match('#^/mag/comment/(?P<content_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'content_comment')), array (  '_controller' => 'FAPROD\\ContentBundle\\Controller\\ContentController::commentAction',));
                }

                // content_gosearch
                if ($pathinfo === '/mag/gosearch') {
                    return array (  '_controller' => 'FAPROD\\ContentBundle\\Controller\\ContentController::gosearchAction',  '_route' => 'content_gosearch',);
                }

                // content_search
                if ($pathinfo === '/mag/recherche') {
                    return array (  '_controller' => 'FAPROD\\ContentBundle\\Controller\\ContentController::searchAction',  '_route' => 'content_search',);
                }

                // content_index
                if ($pathinfo === '/mag') {
                    return array (  '_controller' => 'FAPROD\\ContentBundle\\Controller\\ContentController::indexAction',  '_route' => 'content_index',);
                }

                if (0 === strpos($pathinfo, '/mag/a')) {
                    // content_ajaxcontents
                    if ($pathinfo === '/mag/ajaxcontents') {
                        return array (  '_controller' => 'FAPROD\\ContentBundle\\Controller\\ContentController::ajaxcontentsAction',  '_route' => 'content_ajaxcontents',);
                    }

                    // content_show
                    if (0 === strpos($pathinfo, '/mag/article') && preg_match('#^/mag/article/(?P<t>[^/]++)/(?P<content_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'content_show')), array (  '_controller' => 'FAPROD\\ContentBundle\\Controller\\ContentController::showAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/member')) {
                // agenda_index
                if ($pathinfo === '/member/agenda') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\AgendaController::indexAction',  '_route' => 'agenda_index',);
                }

                // agenda_disponibilite
                if ($pathinfo === '/member/disponibilite') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\AgendaController::disponibiliteAction',  '_route' => 'agenda_disponibilite',);
                }

                // agenda_getagenda
                if ($pathinfo === '/member/getagenda') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\AgendaController::getagendaAction',  '_route' => 'agenda_getagenda',);
                }

                if (0 === strpos($pathinfo, '/member/document')) {
                    if (0 === strpos($pathinfo, '/member/documents')) {
                        // document_index
                        if ($pathinfo === '/member/documents') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\DocumentController::indexAction',  '_route' => 'document_index',);
                        }

                        // user_showdocuments
                        if ($pathinfo === '/member/documents/popup') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\DocumentController::showdocumentsAction',  '_route' => 'user_showdocuments',);
                        }

                        // document_show
                        if (0 === strpos($pathinfo, '/member/documents/show') && preg_match('#^/member/documents/show/(?P<document_id>\\d+)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'document_show')), array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\DocumentController::showAction',));
                        }

                    }

                    // document_edit
                    if (0 === strpos($pathinfo, '/member/document/edit') && preg_match('#^/member/document/edit/(?P<document_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'document_edit')), array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\DocumentController::editAction',));
                    }

                    // document_delete
                    if (0 === strpos($pathinfo, '/member/documents/delete') && preg_match('#^/member/documents/delete/(?P<document_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'document_delete')), array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\DocumentController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/member/offer')) {
                    if (0 === strpos($pathinfo, '/member/offers')) {
                        // offer_index
                        if ($pathinfo === '/member/offers') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\OfferController::indexAction',  '_route' => 'offer_index',);
                        }

                        // user_showprestations
                        if ($pathinfo === '/member/offers/prestations') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\OfferController::showprestationsAction',  '_route' => 'user_showprestations',);
                        }

                        // offer_show
                        if (0 === strpos($pathinfo, '/member/offers/show') && preg_match('#^/member/offers/show/(?P<offer_id>\\d+)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'offer_show')), array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\OfferController::showAction',));
                        }

                    }

                    // offer_edit
                    if (0 === strpos($pathinfo, '/member/offer/edit') && preg_match('#^/member/offer/edit/(?P<offer_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'offer_edit')), array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\OfferController::editAction',));
                    }

                    if (0 === strpos($pathinfo, '/member/offers')) {
                        // offer_prestation
                        if ($pathinfo === '/member/offers/prestation') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\OfferController::prestationAction',  '_route' => 'offer_prestation',);
                        }

                        // offer_delete
                        if (0 === strpos($pathinfo, '/member/offers/delete') && preg_match('#^/member/offers/delete/(?P<offer_id>\\d+)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'offer_delete')), array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\OfferController::deleteAction',));
                        }

                    }

                }

                // user_avis
                if ($pathinfo === '/member/avis') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::avisAction',  '_route' => 'user_avis',);
                }

                // user_showavis
                if ($pathinfo === '/member/profil/avis') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::showavisAction',  '_route' => 'user_showavis',);
                }

                // user_addfavoris
                if (0 === strpos($pathinfo, '/member/addfavoris') && preg_match('#^/member/addfavoris/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_addfavoris')), array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::addfavorisAction',));
                }

                // user_deletefavoris
                if (0 === strpos($pathinfo, '/member/deletefavoris') && preg_match('#^/member/deletefavoris/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_deletefavoris')), array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::deletefavorisAction',));
                }

                // user_favoris
                if ($pathinfo === '/member/favoris') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::favorisAction',  '_route' => 'user_favoris',);
                }

                // user_showfavoris
                if ($pathinfo === '/member/profil/favoris') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::showfavorisAction',  '_route' => 'user_showfavoris',);
                }

                // user_rotateimg
                if ($pathinfo === '/member/rotateimg') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::rotateimgAction',  '_route' => 'user_rotateimg',);
                }

                if (0 === strpos($pathinfo, '/member/delcompte')) {
                    // user_delcompte
                    if ($pathinfo === '/member/delcompte') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::delcompteAction',  '_route' => 'user_delcompte',);
                    }

                    // user_delcomptego
                    if ($pathinfo === '/member/delcomptego') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::delcomptegoAction',  '_route' => 'user_delcomptego',);
                    }

                }

                // user_index
                if (rtrim($pathinfo, '/') === '/member') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'user_index');
                    }

                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::indexAction',  '_route' => 'user_index',);
                }

                // user_statistique
                if ($pathinfo === '/member/statistique') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::statistiqueAction',  '_route' => 'user_statistique',);
                }

                // user_boutique
                if ($pathinfo === '/member/boutique') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::boutiqueAction',  '_route' => 'user_boutique',);
                }

                // user_editpassword
                if ($pathinfo === '/member/editpassword') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::editpasswordAction',  '_route' => 'user_editpassword',);
                }

                if (0 === strpos($pathinfo, '/member/c')) {
                    // user_competences
                    if ($pathinfo === '/member/competences') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::competencesAction',  '_route' => 'user_competences',);
                    }

                    // user_cheveux
                    if ($pathinfo === '/member/cheveux') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::cheveuxAction',  '_route' => 'user_cheveux',);
                    }

                }

                if (0 === strpos($pathinfo, '/member/p')) {
                    // user_peau
                    if ($pathinfo === '/member/peau') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::peauAction',  '_route' => 'user_peau',);
                    }

                    // user_preferences
                    if ($pathinfo === '/member/preferences') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::preferencesAction',  '_route' => 'user_preferences',);
                    }

                }

                // user_edit
                if ($pathinfo === '/member/edit') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::editAction',  '_route' => 'user_edit',);
                }

                if (0 === strpos($pathinfo, '/member/profil')) {
                    // user_show
                    if ($pathinfo === '/member/profil') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::showAction',  '_route' => 'user_show',);
                    }

                    // user_showinfos
                    if ($pathinfo === '/member/profil/infos') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::showinfosAction',  '_route' => 'user_showinfos',);
                    }

                    if (0 === strpos($pathinfo, '/member/profil/c')) {
                        // user_showcompetences
                        if ($pathinfo === '/member/profil/competences') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::showcompetencesAction',  '_route' => 'user_showcompetences',);
                        }

                        // user_showcheveux
                        if ($pathinfo === '/member/profil/cheveux') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::showcheveuxAction',  '_route' => 'user_showcheveux',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/member/profil/p')) {
                        // user_showpeau
                        if ($pathinfo === '/member/profil/peau') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::showpeauAction',  '_route' => 'user_showpeau',);
                        }

                        // user_showpreferences
                        if ($pathinfo === '/member/profil/preferences') {
                            return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::showpreferencesAction',  '_route' => 'user_showpreferences',);
                        }

                    }

                }

                // user_bank
                if ($pathinfo === '/member/show/bank') {
                    return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::bankAction',  '_route' => 'user_bank',);
                }

                if (0 === strpos($pathinfo, '/member/photos')) {
                    // user_photos
                    if ($pathinfo === '/member/photos') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::photosAction',  '_route' => 'user_photos',);
                    }

                    // user_photospro
                    if ($pathinfo === '/member/photospro') {
                        return array (  '_controller' => 'FAPROD\\UserBundle\\Controller\\UserController::photosproAction',  '_route' => 'user_photospro',);
                    }

                }

                if (0 === strpos($pathinfo, '/member/messages')) {
                    // message_index
                    if (preg_match('#^/member/messages(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>[^/]++))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'message_index')), array (  'page' => '1',  'sort' => 'updated_at',  'sens' => 0,  '_controller' => 'FAPROD\\MessageBundle\\Controller\\MessageController::indexAction',));
                    }

                    // message_popup
                    if ($pathinfo === '/member/messages/popup') {
                        return array (  '_controller' => 'FAPROD\\MessageBundle\\Controller\\MessageController::popupAction',  '_route' => 'message_popup',);
                    }

                    // message_newdiscussion
                    if (0 === strpos($pathinfo, '/member/messages/newdiscussion') && preg_match('#^/member/messages/newdiscussion/(?P<destinataire_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'message_newdiscussion')), array (  '_controller' => 'FAPROD\\MessageBundle\\Controller\\MessageController::newdiscussionAction',));
                    }

                    // message_show
                    if (0 === strpos($pathinfo, '/member/messages/discussion') && preg_match('#^/member/messages/discussion/(?P<discussion_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'message_show')), array (  '_controller' => 'FAPROD\\MessageBundle\\Controller\\MessageController::showAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/member/publicite')) {
                    // publicite_index
                    if ($pathinfo === '/member/publicite') {
                        return array (  '_controller' => 'FAPROD\\PubBundle\\Controller\\PubController::indexAction',  '_route' => 'publicite_index',);
                    }

                    // publicite_show
                    if (0 === strpos($pathinfo, '/member/publicite/show') && preg_match('#^/member/publicite/show/(?P<publicite_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'publicite_show')), array (  '_controller' => 'FAPROD\\PubBundle\\Controller\\PubController::showAction',));
                    }

                    // publicite_edit
                    if (0 === strpos($pathinfo, '/member/publicite/edit') && preg_match('#^/member/publicite/edit/(?P<publicite_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'publicite_edit')), array (  '_controller' => 'FAPROD\\PubBundle\\Controller\\PubController::editAction',));
                    }

                    // publicite_delete
                    if (0 === strpos($pathinfo, '/member/publicite/delete') && preg_match('#^/member/publicite/delete/(?P<publicite_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'publicite_delete')), array (  '_controller' => 'FAPROD\\PubBundle\\Controller\\PubController::deleteAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/member/booking')) {
                    // booking_newbooking
                    if ($pathinfo === '/member/booking/newbooking') {
                        return array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::newbookingAction',  '_route' => 'booking_newbooking',);
                    }

                    // booking_index
                    if (0 === strpos($pathinfo, '/member/booking/liste') && preg_match('#^/member/booking/liste(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>[^/]++))?)?)?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_index')), array (  'page' => '1',  'sort' => 'updated_at',  'sens' => 0,  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::indexAction',));
                    }

                    if (0 === strpos($pathinfo, '/member/booking/p')) {
                        // booking_popup
                        if ($pathinfo === '/member/booking/popup') {
                            return array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::showbookingAction',  '_route' => 'booking_popup',);
                        }

                        if (0 === strpos($pathinfo, '/member/booking/paiements')) {
                            // booking_paiements
                            if (preg_match('#^/member/booking/paiements(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>[^/]++))?)?)?$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_paiements')), array (  'page' => '1',  'sort' => 'updated_at',  'sens' => 0,  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::paiementsAction',));
                            }

                            // booking_paiementspopup
                            if ($pathinfo === '/member/booking/paiements/popup') {
                                return array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::showpaiementsAction',  '_route' => 'booking_paiementspopup',);
                            }

                        }

                    }

                    // booking_show
                    if (0 === strpos($pathinfo, '/member/booking/voir') && preg_match('#^/member/booking/voir/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_show')), array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::showAction',));
                    }

                    // booking_note
                    if (0 === strpos($pathinfo, '/member/booking/note') && preg_match('#^/member/booking/note/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_note')), array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::noteAction',));
                    }

                    // booking_payer
                    if (0 === strpos($pathinfo, '/member/booking/payer') && preg_match('#^/member/booking/payer/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_payer')), array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::payerAction',));
                    }

                    // booking_suitepayer
                    if ($pathinfo === '/member/booking/suitepayer') {
                        return array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::suitepayerAction',  '_route' => 'booking_suitepayer',);
                    }

                    // check_3ds
                    if ($pathinfo === '/member/booking/check_3ds') {
                        return array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::check_3ds',  '_route' => 'check_3ds',);
                    }

                    // booking_accept
                    if (0 === strpos($pathinfo, '/member/booking/accept') && preg_match('#^/member/booking/accept/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_accept')), array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::bookingacceptAction',));
                    }

                    // booking_refuse
                    if (0 === strpos($pathinfo, '/member/booking/refuse') && preg_match('#^/member/booking/refuse/(?P<booking_id>\\d+)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'booking_refuse')), array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::bookingrefuseAction',));
                    }

                    // booking_NOK
                    if ($pathinfo === '/member/booking/NOK') {
                        return array (  '_controller' => 'FAPROD\\BookingBundle\\Controller\\BookingController::booking_NOK',  '_route' => 'booking_NOK',);
                    }

                }

            }

        }

        // check_index
        if ($pathinfo === '/check') {
            return array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\CheckController::indexAction',  '_route' => 'check_index',);
        }

        // home_invitation
        if ($pathinfo === '/invitation') {
            return array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::invitationAction',  '_route' => 'home_invitation',);
        }

        // home_partage
        if (0 === strpos($pathinfo, '/pros/partage') && preg_match('#^/pros/partage/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'home_partage')), array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::partageAction',));
        }

        // upload_media
        if (0 === strpos($pathinfo, '/upload/process') && preg_match('#^/upload/process(?:/(?P<resize>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'upload_media')), array (  'resize' => '',  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::uploadAction',));
        }

        // home_clic
        if (0 === strpos($pathinfo, '/clic') && preg_match('#^/clic/(?P<publicite_id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'home_clic')), array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::clicAction',));
        }

        // home_index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home_index');
            }

            return array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::indexAction',  '_route' => 'home_index',);
        }

        // home_welcome
        if ($pathinfo === '/welcome') {
            return array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::welcomeAction',  '_route' => 'home_welcome',);
        }

        // home_endcb
        if ($pathinfo === '/endcb') {
            return array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::endcbAction',  '_route' => 'home_endcb',);
        }

        // home_gosearch
        if ($pathinfo === '/gosearch') {
            return array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::gosearchAction',  '_route' => 'home_gosearch',);
        }

        // home_search
        if (0 === strpos($pathinfo, '/recherche') && preg_match('#^/recherche(?:/(?P<page>\\d+)(?:/(?P<sort>[^/]++)(?:/(?P<sens>\\d+))?)?)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'home_search')), array (  'page' => '1',  'sort' => 'id',  'sens' => '',  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::searchAction',));
        }

        if (0 === strpos($pathinfo, '/pro')) {
            // home_comment
            if (0 === strpos($pathinfo, '/pro/newcomment') && preg_match('#^/pro/newcomment/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'home_comment')), array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::commentAction',));
            }

            // home_show
            if (preg_match('#^/pro/(?P<t>[^/]++)/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'home_show')), array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\HomeController::showAction',));
            }

        }

        if (0 === strpos($pathinfo, '/a')) {
            // home_agenda
            if (0 === strpos($pathinfo, '/agenda') && preg_match('#^/agenda/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'home_agenda')), array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\PartialController::agendaAction',));
            }

            // home_ajaxphonepro
            if (0 === strpos($pathinfo, '/ajaxphonepro') && preg_match('#^/ajaxphonepro/(?P<user_id>\\d+)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'home_ajaxphonepro')), array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\PartialController::ajaxphoneproAction',));
            }

        }

        // home_prestation
        if ($pathinfo === '/prestation') {
            return array (  '_controller' => 'FAPROD\\CoreBundle\\Controller\\PartialController::prestationAction',  '_route' => 'home_prestation',);
        }

        // cron_daily
        if ($pathinfo === '/daily') {
            return array (  '_controller' => 'FAPROD\\CronBundle\\Controller\\DailyController::indexAction',  '_route' => 'cron_daily',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
