/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
function getBaseURL() {
    var url = location.href;
    var baseURL = url.substring(0, url.indexOf('/', 14)); 
 	
    if (baseURL.indexOf('http://127.0.0.1:8080') != -1) { 
        return 'http://127.0.0.1:8080/projects/lovethnik/web/';
    }
    else {
        return baseURL;
    }
 
} 

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	//CKEDITOR.plugins.addExternal('NonverBlaster', getBaseURL() + '/js/ckeditor/plugins/NonverBlaster/');
	//CKEDITOR.config.toolbar_Basic = ['blabla','blabla','jwplayer','blabla'];
	//config.extraPlugins = 'NonverBlaster';
	//config.skin = 'kama';
	config.allowedContent = true;
	
	config.filebrowserBrowseUrl = getBaseURL()+'/js/fileman/index.html';
    config.filebrowserImageBrowseUrl = getBaseURL()+'/js/fileman/index.html?type=image';
    config.filebrowserFlashBrowseUrl = getBaseURL()+'/js/fileman/index.html';
    config.filebrowserUploadUrl = getBaseURL()+'/js/fileman/index.html';
    config.filebrowserImageUploadUrl = getBaseURL()+'/js/fileman/index.html?type=image';
    config.filebrowserFlashUploadUrl = getBaseURL()+'/js/fileman/index.html';
    
    
    config.height = '400px';
    
    config.toolbar = 'MyToolbar';
 
	config.toolbar_MyToolbar =
	[
		['Source','-','Save'],

        ['Cut','Copy','Paste','PasteText','PasteFromWord'],
        
        ['Link','Unlink','Anchor'],
        
        ['Image','NonverBlaster','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
        
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],

        ['Undo','Redo','Maximize'],

        '/',

        ['Bold','Italic','Underline','Strike'],
        
        ['TextColor','BGColor'],

        ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],

        ['Styles','Format','Font','FontSize'],        
        
	];
};
